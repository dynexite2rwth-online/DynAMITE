#pragma once

#include <QDoubleSpinBox>
#include <QGridLayout>

class ViewSettingsWindow : public QWidget {
    Q_OBJECT

public:
    explicit ViewSettingsWindow(QWidget *parent = nullptr);

Q_SIGNALS:
    void gradingTabBucketSizeChanged(double newBucketSize);
    void taskTabBucketSizeChanged(double newBucketSize);

private Q_SLOTS:
    void pointsChanged();
    void gradingBucketSpinBoxChanged(double d);
    void taskBucketSpinBoxChanged(double d);

private:
    QDoubleSpinBox mGradingTabBucketSize{};
    QDoubleSpinBox mTaskTabBucketSize{};
    QGridLayout mLayout;

    double mCurrentGradingBucketSize;
    double mCurrentTaskBucketSize;

    friend class GuiDagTest;
};
