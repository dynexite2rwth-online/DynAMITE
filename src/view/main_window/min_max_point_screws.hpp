#pragma once

#include "util/enums.hpp"
#include "util/storage_helper.hpp"

#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>

#include <array>
#include <cstddef>

enum class GradeBoundaryStepSize;
struct GradeBoundary;
class QAbstractSlider;
struct FontSize;
class CustomSlider;
class HelpWidget;
class QDoubleSpinBox;

/**
 * \brief One column of sliders and spin boxes in the upper left quadrant of the
 * grading tab
 */
class MinMaxPointScrewColumn : public QWidget {
    Q_OBJECT

public:
    explicit MinMaxPointScrewColumn(GradingType grading,
                                    QWidget *parent = nullptr) noexcept;

private:
    /**
     * \brief This could be a template parameter, but it is not possible to mix
     * template classes and Q_OBJECT
     */
    GradingType mGrading;

    QVBoxLayout mLayout{this};

    /**
     * \brief Every "line" in the mLayout consists of 2-3 horizontally arranged
     * widgets. The layouts for each line are in this array.
     */
    std::array<QHBoxLayout *, 4> mHLayouts{};

    std::array<QLabel *, 2> mDescriptions{};
    std::array<QDoubleSpinBox *, 4> mSpinBoxes{};
    std::array<CustomSlider *, 2> mSliders{};

    QLabel mInitialMaximum;
    HelpWidget *mMaxQuestionIcon{};
    HelpWidget *mMinQuestionIcon{};

    /**
     * \brief Enable/disable all sliders and spin boxes
     */
    void setWidgetsEnabled(bool enable) const noexcept;

    /**
     * \brief Set the range of spin box to [min, max] and the current value to
     * start. It will also update the number of shown decimals and the single
     * step size.
     *
     * It does not send any signals about any of the changes.
     */
    void setSpinBoxRange(QDoubleSpinBox &spinBox, GradeBoundary min,
                         GradeBoundary max, GradeBoundary start,
                         GradeBoundaryStepSize minPointStep) const noexcept;
    /**
     * \brief For a pair of slider+spin box, sets the slider range to
     * [0, rangeMax] and the current slider value to start. The spin box range
     * and value is set to the same, but as percentages of maxPoints.
     *
     * Also updates the single step size of the spin box.
     *
     * It does not send any signals about any of the changes.
     */
    void setSliderRange(QAbstractSlider &slider, QDoubleSpinBox &spinBox,
                        GradeBoundary rangeMax, GradeBoundary start,
                        GradeBoundary maxPoints) const noexcept;

    void createLabel(std::size_t labelIdx, std::size_t row,
                     bool isMin) noexcept;
    void createSlider(std::size_t sliderIdx, std::size_t row) noexcept;

    void createQuestionIcon(QLayout &layout, bool isMin) noexcept;

private Q_SLOTS:
    /**
     * \brief Update visibility and displayed max. value of mInitialMaximum
     */
    void updateInitialMax() noexcept;

    /**
     * \brief Update ranges of sliders and spin boxes
     */
    void updateRanges() noexcept;

    /**
     * \brief Update the text in mMinQuestionIcon
     */
    void updateHelpText() noexcept;

    void pointsChanged() noexcept;
    void sliderChange(std::size_t sliderIdx, int value) noexcept;
    void spinBoxChange(std::size_t spinBoxIdx, double value) noexcept;
};

/**
 * \brief The upper left quadrant of the grading tab
 */
class MinMaxPointScrews : public QWidget {
    Q_OBJECT

public:
    explicit MinMaxPointScrews(QWidget *parent = nullptr) noexcept;

    void changeFontSize(FontSize newFontSize) noexcept;

private Q_SLOTS:
    void gradesChanged() noexcept;

private:
    QVBoxLayout mVLayout{this};
    QHBoxLayout mHLayout{};
    QGridLayout mAvgLayout{};

    EnumStorage<MinMaxPointScrewColumn *, GradingType, 2> mColumns;

    QLabel mAverageGrade{}, mAverageGradePassed{}, mNrPassed{}, mNrFailed{};
    QLabel mAvgLabel, mAvgPassedLabel, mNrPassedLabel, mNrFailedLabel;

    void createScrewColumns() noexcept;
    void createAvgLabels() noexcept;
};
