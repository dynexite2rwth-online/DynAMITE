#include "view_settings.hpp"

#include "data/data_manager.hpp"
#include "util/defaults.hpp"
#include "util/grade_boundary_step.hpp"

#include <QLabel>
#include <QSignalBlocker>

ViewSettingsWindow::ViewSettingsWindow(QWidget *parent)
    : QWidget(parent), mLayout(this), mCurrentGradingBucketSize(1),
      mCurrentTaskBucketSize(1) {

    setWindowTitle("Settings");

    mGradingTabBucketSize.setRange(defaults::minBucketSize,
                                   defaults::maxBucketSize);
    mGradingTabBucketSize.setSingleStep(defaults::bucketStep);
    mGradingTabBucketSize.setDecimals(2);

    mTaskTabBucketSize.setRange(defaults::minBucketSize,
                                defaults::maxBucketSize);
    mTaskTabBucketSize.setSingleStep(defaults::bucketStep);
    mTaskTabBucketSize.setDecimals(2);

    mLayout.addWidget(
        new QLabel("Grading tab point histogram bucket size:", this), 0, 0);
    mLayout.addWidget(&mGradingTabBucketSize, 0, 1);
    mLayout.addWidget(new QLabel("Task tab point histogram bucket size:", this),
                      1, 0);
    mLayout.addWidget(&mTaskTabBucketSize, 1, 1);

    // receive notification from combo boxes
    connect(&DataManager::getInstance(), &DataManager::examChanged, this,
            &ViewSettingsWindow::pointsChanged);
    connect(&mGradingTabBucketSize, &QDoubleSpinBox::valueChanged, this,
            &ViewSettingsWindow::gradingBucketSpinBoxChanged);
    connect(&mTaskTabBucketSize, &QDoubleSpinBox::valueChanged, this,
            &ViewSettingsWindow::taskBucketSpinBoxChanged);
}

void ViewSettingsWindow::pointsChanged() {
    const auto cur = DataManager::getCurrent();

    if (!cur)
        return;

    QSignalBlocker blocker(mGradingTabBucketSize);
    QSignalBlocker blocker2(mTaskTabBucketSize);
    mCurrentGradingBucketSize =
        as<double>(cur->mUserPointConfig.mGradeBoundaryStepSize);
    mCurrentTaskBucketSize = mCurrentGradingBucketSize;

    mGradingTabBucketSize.setValue(mCurrentGradingBucketSize);
    mTaskTabBucketSize.setValue(mCurrentTaskBucketSize);

    // Don't emit the "histogram bucket size changed" signal here, because
    // pointsChanged already implies it
}

void ViewSettingsWindow::gradingBucketSpinBoxChanged(const double d) {
    mCurrentGradingBucketSize = d;

    Q_EMIT gradingTabBucketSizeChanged(d);
}
void ViewSettingsWindow::taskBucketSpinBoxChanged(const double d) {
    mCurrentTaskBucketSize = d;

    Q_EMIT taskTabBucketSizeChanged(d);
}
