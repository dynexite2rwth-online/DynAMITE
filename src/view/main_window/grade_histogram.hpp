#pragma once

#include "util/storage_helper.hpp"

#include <QChartView>
#include <QList>

class GradeModel;
class QBarSet;
class QValueAxis;
enum class GradingType;
struct FontSize;

class GradeHistogram : public QChartView {
    Q_OBJECT
public:
    explicit GradeHistogram(const QList<GradingType> &gradingTypes,
                            QWidget *parent = nullptr);

    void handleResize(QRectF plotArea, FontSize largeFontSize,
                      FontSize smallFontSize);

    FontSize getFontSize(const QRectF &plotArea);

public Q_SLOTS:
    void gradesChanged(const EnumStorage<bool> &changedTypes);

    void showHoverText(bool status, int index, QBarSet *barset);

private:
    void updateYAxis();

    void updateBarLabelSize(FontSize largeFontSize);

    /* todo: if it's already a QList, might as well make them member objects
     * instead of pointers */
    QList<GradeModel *> mModels{};

    // store pointers for convenience
    QList<QBarSet *> mBarSets{};
    QValueAxis *mAxisY;

    QRectF mPlotArea;

    int mMaxNrYLabels = 6;
};
