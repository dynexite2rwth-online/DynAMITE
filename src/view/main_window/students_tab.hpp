#pragma once

#include "util/storage_helper.hpp"

#include <QObject>
#include <QTableWidget>
#include <QTimer>
#include <QWidget>

class StudentsTab : public QWidget {
    Q_OBJECT

public:
    explicit StudentsTab(QWidget *parent = nullptr) noexcept;

    /**
     * \brief Adapt the row/column size to the new font size
     */
    void handleResize() noexcept;

    /**
     * \brief Returns the internally used QTableWidget (used to export it as
     * CSV)
     */
    [[nodiscard]] const QTableWidget &getTable() const noexcept;

public Q_SLOTS:
    /**
     * \brief Update number of column and rows, the student ids and the points.
     */
    void examChanged() noexcept;
    void pointsChanged(const EnumStorage<bool> &changedTypes) noexcept;

    /**
     * \brief Update grades.
     */
    void gradesChanged(const EnumStorage<bool> &changedTypes);

private Q_SLOTS:
    void resizeTable() noexcept;

private:
    QTableWidget mTable;

    QTimer mResizeTimer;
};
