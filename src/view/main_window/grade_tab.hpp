#pragma once

#include "util/testing.hpp"

#include <QWidget>

#include <array>

class GradeHistogram;
class MinMaxPointScrews;
class PointHistogram;
class QGridLayout;
class QTabWidget;
class ViewSettingsWindow;
struct FontSize;

class GradeTab : public QWidget {
    Q_OBJECT

public:
    explicit GradeTab(const ViewSettingsWindow &viewSettings,
                      QWidget *parent = nullptr);

    void handleResize(FontSize largeFontSize, FontSize smallFontSize) const;
    [[nodiscard]] FontSize getFontPxSize(QSize windowSize) const;

public Q_SLOTS:
    void pointsChanged() noexcept;

private:
    void createGradeHistograms();
    void createPointHistograms(const ViewSettingsWindow &viewSettings);

private:
    MinMaxPointScrews *mScrews;

    QTabWidget *mGradeHistoTabs;
    std::array<GradeHistogram *, 4> mGradeHistos;

    QTabWidget *mPointHistoTabs;
    std::array<PointHistogram *, 3> mPointHistos;

    QGridLayout *mLayout;

    FRIEND_TEST(GuiDagTest, CheckKnownTestFiles);
    friend class GuiDagTest;
};
