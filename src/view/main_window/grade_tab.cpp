#include "grade_tab.hpp"

#include "grade_histogram.hpp"
#include "min_max_point_screws.hpp"
#include "point_histogram.hpp"

#include "data/data_manager.hpp"
#include "util/defaults.hpp"
#include "util/enums.hpp"
#include "util/font.hpp"
#include "util/points.hpp"

#include <QGridLayout>
#include <QTabWidget>

#include <cstddef>

inline constexpr std::size_t openGradesIdx = 0;
inline constexpr std::size_t closedGradesIdx = 1;
inline constexpr std::size_t combinedGradesIdx = 2;
inline constexpr std::size_t allGradesIdx = 3;

inline constexpr std::size_t openPointsIdx = 0;
inline constexpr std::size_t closedPointsIdx = 1;
inline constexpr std::size_t combinedPointsIdx = 2;

GradeTab::GradeTab(const ViewSettingsWindow &viewSettings, QWidget *parent)
    : QWidget(parent), mScrews(new MinMaxPointScrews(this)),
      mGradeHistoTabs(new QTabWidget(this)), mGradeHistos(),
      mPointHistoTabs(new QTabWidget(this)), mPointHistos(),
      mLayout(new QGridLayout(this)) {

    mLayout->addWidget(mScrews, 0, 0);
    mLayout->addWidget(mGradeHistoTabs, 0, 1);
    mLayout->addWidget(mPointHistoTabs, 1, 0, 1, 2);

    createGradeHistograms();
    createPointHistograms(viewSettings);

    // 30% - 70% split (specified in defaults::gradeTabHistogramWidthPercentage)
    static_assert(defaults::gradeTabHistogramWidthPercentage >= 0);
    static_assert(defaults::gradeTabHistogramWidthPercentage <= 100);

    mLayout->setColumnStretch(0,
                              100 - defaults::gradeTabHistogramWidthPercentage);
    mLayout->setColumnStretch(1, defaults::gradeTabHistogramWidthPercentage);
    mLayout->setRowStretch(0, 1);
    mLayout->setRowStretch(1, 1);

    connect(&DataManager::getInstance(), &DataManager::taskEliminationChanged,
            this, &GradeTab::pointsChanged);
}

void GradeTab::handleResize(const FontSize largeFontSize,
                            const FontSize smallFontSize) const {
    const auto plotAreaGradeHistos =
        mGradeHistos[static_cast<std::size_t>(mGradeHistoTabs->currentIndex())]
            ->chart()
            ->plotArea();

    for (const auto &gradeHisto : mGradeHistos) {
        gradeHisto->handleResize(plotAreaGradeHistos, largeFontSize,
                                 smallFontSize);
    }

    const auto plotAreaPointHistos =
        mPointHistos[static_cast<std::size_t>(mPointHistoTabs->currentIndex())]
            ->chart()
            ->plotArea();
    for (const auto &pointHisto : mPointHistos) {
        pointHisto->handleResize(plotAreaPointHistos, largeFontSize,
                                 smallFontSize);
    }

    mScrews->changeFontSize(largeFontSize);
}

FontSize GradeTab::getFontPxSize(const QSize windowSize) const {

    // Guess plot area size
    // We can't just read out the plot area size, because it is not updated if a
    // different tab is loaded and it is updated wrong if the user restores the
    // window size after a maximize.
    // But half the window size is a good estimate.

    return mGradeHistos[0]->getFontSize(
        QRectF(QPointF(0., 0.), QSizeF(windowSize * 0.5)));
}

void GradeTab::pointsChanged() noexcept {
    const auto cur = DataManager::getCurrent();

    if (!cur)
        return;

    // Only show multiple tabs if there were open AND closed tasks
    {
        const auto hasClosedAndOpen = cur->hasType(GradingType::Combined);
        for (int tabIdx = 0; tabIdx < static_cast<int>(mGradeHistos.size());
             ++tabIdx) {

            mGradeHistoTabs->setTabVisible(
                tabIdx, hasClosedAndOpen || tabIdx == combinedGradesIdx);
        }

        for (int tabIdx = 0; tabIdx < static_cast<int>(mPointHistos.size());
             ++tabIdx) {

            mPointHistoTabs->setTabVisible(
                tabIdx, hasClosedAndOpen || tabIdx == combinedPointsIdx);
        }
    }
}

void GradeTab::createGradeHistograms() {
    mGradeHistos[openGradesIdx] = new GradeHistogram({GradingType::Open}, this);
    mGradeHistos[closedGradesIdx] =
        new GradeHistogram({GradingType::Closed}, this);
    mGradeHistos[combinedGradesIdx] =
        new GradeHistogram({GradingType::Combined}, this);
    mGradeHistos[allGradesIdx] = new GradeHistogram(
        {GradingType::Open, GradingType::Closed, GradingType::Combined}, this);
    mGradeHistoTabs->addTab(mGradeHistos[openGradesIdx],
                            "Histogram of open grades");
    mGradeHistoTabs->addTab(mGradeHistos[closedGradesIdx],
                            "Histogram of closed grades");
    mGradeHistoTabs->addTab(mGradeHistos[combinedGradesIdx],
                            "Histogram of resulting grades");
    mGradeHistoTabs->addTab(mGradeHistos[allGradesIdx], "Show all histograms");
    mGradeHistoTabs->setCurrentIndex(combinedGradesIdx);
}

void GradeTab::createPointHistograms(const ViewSettingsWindow &viewSettings) {
    mPointHistos[combinedPointsIdx] =
        new PointHistogram(GradingType::Combined, viewSettings, this);
    mPointHistos[openPointsIdx] =
        new PointHistogram(GradingType::Open, viewSettings, this);
    mPointHistos[closedPointsIdx] =
        new PointHistogram(GradingType::Closed, viewSettings, this);
    mPointHistoTabs->addTab(mPointHistos[openPointsIdx],
                            "Histogram of open points");
    mPointHistoTabs->addTab(mPointHistos[closedPointsIdx],
                            "Histogram of closed points");
    mPointHistoTabs->addTab(mPointHistos[combinedPointsIdx],
                            "Histogram of total points");
    mPointHistoTabs->setCurrentIndex(combinedPointsIdx);
}
