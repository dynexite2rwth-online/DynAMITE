/**
 * \file point_histogram.cpp
 */

#include "point_histogram.hpp"

#include "view_settings.hpp"

#include "data/data_manager.hpp"
#include "util/defaults.hpp"
#include "util/font.hpp"
#include "util/grade_boundary_step.hpp"
#include "util/points.hpp"

#include <QBarSet>
#include <QChart>
#include <QColor>
#include <QFontMetrics>
#include <QStackedBarSeries>
#include <QString>
#include <QValueAxis>

#include <algorithm>
#include <cmath>
#include <limits>

static const EnumStorage<QString> failStr{
    QString("Students receiving a grade of 5.0 in the closed part"),
    QString("Students receiving a grade of 5.0 in the open part"),
    QString("Students who fail the exam")};
static const EnumStorage<QString> passStr{
    QString("Students receiving a passing grade in the closed part"),
    QString("Students receiving a passing grade in the open part"),
    QString("Students who pass the exam")};

namespace {
// The maximum achieved points may be larger than the total number of achievable
// points because of bonus points
[[nodiscard]] Points
getMaxPointsInclBonus(const GradingType gradingType) noexcept {

    const auto cur = DataManager::getCurrent();

    return std::max(
        cur->getMaxPoints(gradingType),
        *std::ranges::max_element(cur->getPointsWithBonus(gradingType)));
}
} // namespace

namespace impl {
qsizetype getBarIdx(const double bucketSize, const Points p,
                    const qsizetype size) noexcept {
    // The largest bucket is centered at max, so it's bucket goes up to max +
    // half a bucket size
    return std::min(size - 1, static_cast<qsizetype>(std::ceil(
                                  (p.p - bucketSize / 2.) / bucketSize)));
}
} // namespace impl

PointHistogram::PointHistogram(const GradingType gradingType,
                               const ViewSettingsWindow &viewSettings,
                               QWidget *parent) noexcept
    : QChartView(new QChart(), parent), mGradingType(gradingType),
      mSeries(new QStackedBarSeries()),
      mBarSetPassed(new QBarSet(passStr[gradingType])),
      mBarSetFailed(new QBarSet(failStr[gradingType])),
      mAxisX(new QValueAxis()), mAxisY(new QValueAxis()),
      mAxisLabelCharWidth(
          QFontMetrics(mAxisX->labelsFont()).horizontalAdvance('0')),
      mAxisLabelCharHeight(QFontMetrics(mAxisY->labelsFont()).lineSpacing()) {

    // First, set options for all chart objects
    // Bar sets
    mBarSetPassed->setColor(defaults::passedColor);
    mBarSetPassed->setBorderColor(defaults::passedColor.darker());
    mBarSetFailed->setColor(defaults::notPassedColor);
    mBarSetFailed->setBorderColor(defaults::notPassedColor.darker());

    // x-axis
    mAxisX->setLabelFormat("%d");
    mAxisX->setTickType(QValueAxis::TicksDynamic);
    mAxisX->setTickAnchor(0);

    // y-axis
    mAxisY->setLabelFormat("%d");
    mAxisY->setTickType(QValueAxis::TicksDynamic);
    mAxisY->setTickAnchor(0);

    setMinimumHeight(380);

    // Stacked bar series
    // Bar width of one = no space between adjacent bars
    mSeries->setBarWidth(1.);
    mSeries->setLabelsVisible(false);

    // Legend
    chart()->legend()->setVisible(true);
    chart()->legend()->setAlignment(Qt::AlignBottom);

    // Chart
    chart()->setMinimumSize(400, 200);
    chart()->setAnimationOptions(QChart::NoAnimation);

    // Chart view
    setInteractive(false);

    //-----------------------------------------------------------------------

    // The order of connection steps is important, so that all bars are
    // distributed evenly across the whole range.
    // 1) append bar sets to series and add axis to chart
    // 2) add series to chart
    // 3) attach y axis (we intentionally skip the x axis, see rescaleX for
    // details)

    mSeries->append(mBarSetFailed);
    mSeries->append(mBarSetPassed);

    chart()->addAxis(mAxisX, Qt::AlignBottom);
    chart()->addAxis(mAxisY, Qt::AlignLeft);

    chart()->addSeries(mSeries);

    mSeries->attachAxis(mAxisY);

    //-----------------------------------------------------------------------

    // receive update when points change (rescale axis, change number of bars)
    connect(&DataManager::getInstance(), &DataManager::taskEliminationChanged,
            this, &PointHistogram::pointsChanged);
    // receive update when grades change (change colors)
    connect(&DataManager::getInstance(), &DataManager::gradesChanged, this,
            &PointHistogram::gradesChanged);
    // receive update when user changes bucket size
    connect(&viewSettings, &ViewSettingsWindow::gradingTabBucketSizeChanged,
            this, &PointHistogram::pointBucketSizeChanged);
}

void PointHistogram::handleResize(const QRectF &plotArea,
                                  const FontSize largeFontSize,
                                  const FontSize smallFontSize) noexcept {
    mPlotArea = plotArea;

    // axis font size
    {
        auto font = mAxisX->labelsFont();
        setFontSize(font, largeFontSize);
        mAxisX->setLabelsFont(font);
    }
    {
        auto font = mAxisY->labelsFont();
        setFontSize(font, largeFontSize);
        mAxisY->setLabelsFont(font);
    }
    {
        auto font = chart()->legend()->font();
        setFontSize(font, smallFontSize);
        chart()->legend()->setFont(font);
    }

    // Round down
    mMaxNrYLabels = static_cast<int>(mPlotArea.height() /
                                     (2 * largeFontSize.getPixelSize()));

    rescaleXLabels();
    rescaleYLabels();
}

void PointHistogram::pointsChanged() noexcept {

    const auto cur = DataManager::getCurrent();
    if (!cur) {
        return;
    }

    // Overwrite the bucket size when the exam points change
    mPointBucketSize =
        std::clamp(as<double>(cur->mUserPointConfig.mGradeBoundaryStepSize),
                   defaults::minBucketSize, defaults::maxBucketSize);

    rescaleX();
    resizePointBucketLists();
    // the rest is done in the following gradesChanged-call
}
void PointHistogram::gradesChanged(
    const EnumStorage<bool> &changedTypes) noexcept {

    if (changedTypes[mGradingType]) {

        const auto cur = DataManager::getCurrent();
        if (!cur) {
            return;
        }

        // x axis doesn't change
        // bucket list size doesn't change
        recalculatePointBucketLists();
        updateBarSets();
        rescaleY();
    }
}

void PointHistogram::pointBucketSizeChanged(const double d) noexcept {

    mPointBucketSize = d;

    rescaleX();
    resizePointBucketLists();
    recalculatePointBucketLists();
    updateBarSets();
    rescaleY();
}

void PointHistogram::resizePointBucketLists() noexcept {
    const auto cur = DataManager::getCurrent();

    if (!cur) {
        mPassedStudentsPerPointBucket.resize(0);
        mFailedStudentsPerPointBucket.resize(0);

        return;
    }
    const auto &max = getMaxPointsInclBonus(mGradingType);

    const auto nrBars = impl::getBarIdx(mPointBucketSize, max,
                                        std::numeric_limits<qsizetype>::max()) +
                        1;
    mPassedStudentsPerPointBucket.resize(nrBars);
    mFailedStudentsPerPointBucket.resize(nrBars);
}

void PointHistogram::recalculatePointBucketLists() noexcept {
    const auto cur = DataManager::getCurrent();

    if (!cur)
        return;

    // Reset all counts to 0
    mPassedStudentsPerPointBucket.fill(0.);
    mFailedStudentsPerPointBucket.fill(0.);

    const auto &pointList = cur->getPointsWithBonus(mGradingType);
    const auto &gradeList = cur->getGradeList(mGradingType);

    // For every point value, increment the correct bucket in the list of passed
    // / not-passed buckets depending on the grade
    const auto nrBars = mFailedStudentsPerPointBucket.size();
    for (int i = 0; i < pointList.size(); ++i) {
        if (gradeList[i] == Grade::grade5_0) {
            ++mFailedStudentsPerPointBucket[impl::getBarIdx(
                mPointBucketSize, pointList[i], nrBars)];
        } else {
            ++mPassedStudentsPerPointBucket[impl::getBarIdx(
                mPointBucketSize, pointList[i], nrBars)];
        }
    }
}

void PointHistogram::updateBarSets() noexcept {

    // Pausing view port update reduces flickering
    const auto currentMode = viewportUpdateMode();
    setViewportUpdateMode(NoViewportUpdate);

    {
        // Replace the bar set values with the newly calculated ones
        mBarSetPassed->remove(0, mBarSetPassed->count());
        mBarSetFailed->remove(0, mBarSetFailed->count());

        mBarSetPassed->append(mPassedStudentsPerPointBucket);
        mBarSetFailed->append(mFailedStudentsPerPointBucket);
    }

    setViewportUpdateMode(currentMode);
}

void PointHistogram::rescaleX() const noexcept {

    const auto cur = DataManager::getCurrent();

    if (!cur) {
        return;
    }

    const auto stepSize =
        as<double>(cur->mUserPointConfig.mGradeBoundaryStepSize);

    const auto max = std::max(1., getMaxPointsInclBonus(mGradingType).p);
    // First, evenly spread the buckets over the full width
    mSeries->attachAxis(mAxisX);
    mAxisX->setRange(-stepSize / 2, max / mPointBucketSize + stepSize / 2);

    // Then, set the axis labels to the correct values (instead of 0-"number
    // of buckets")
    mSeries->detachAxis(mAxisX);
    mAxisX->setRange(-stepSize / 2, max + stepSize / 2);

    rescaleXLabels();
}

void PointHistogram::rescaleY() const noexcept {
    // y-axis-max is the maximum of the elementwise sum of the number of
    // students that failed/passed within each point bucket
    if (!mPassedStudentsPerPointBucket.empty()) {

        QList<qreal> totalStudentsPerPoints;
        std::ranges::transform(
            mPassedStudentsPerPointBucket, mFailedStudentsPerPointBucket,
            std::back_inserter(totalStudentsPerPoints), std::plus());

        const auto highestBar =
            *std::ranges::max_element(totalStudentsPerPoints);

        const auto newMax = highestBar * defaults::yAxisRangeScaling;
        if (mAxisY->max() != newMax) {
            mAxisY->setMax(newMax);
        }
    } else {
        mAxisY->setMax(1.);
    }

    rescaleYLabels();
}

void PointHistogram::rescaleXLabels() const noexcept {

    // Get maximum label value (roughly)
    const auto maxLabelValue = static_cast<int>(mAxisX->max());

    const auto maxLabelValueDigits =
        static_cast<int>(QString::number(maxLabelValue).length());

    // Leave one space character width around label
    const auto maxLabelPixelWidth =
        mAxisLabelCharWidth * (maxLabelValueDigits + 2);

    const auto maxNrLabels =
        std::max(2, static_cast<int>(mPlotArea.width()) / maxLabelPixelWidth);

    // We need an extra label for 0
    const auto minLabelStep = static_cast<int>(
        std::ceil(mAxisX->max() / static_cast<qreal>(maxNrLabels - 1)));

    // Round label step to next multiple of 5
    const auto labelStep =
        std::ceil(static_cast<double>(minLabelStep) / 5.) * 5.;

    mAxisX->setTickInterval(labelStep * 2);
}

void PointHistogram::rescaleYLabels() const noexcept {

    // We need an extra label for 0
    const auto minLabelStep = static_cast<int>(
        std::ceil(mAxisY->max() / static_cast<qreal>(mMaxNrYLabels - 1)));

    mAxisY->setTickInterval(std::max(minLabelStep, 1));
}
