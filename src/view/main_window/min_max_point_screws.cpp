#include "min_max_point_screws.hpp"

#include "data/data_manager.hpp"
#include "util/font.hpp"
#include "util/grade.hpp"
#include "util/grade_boundary.hpp"
#include "util/grade_boundary_step.hpp"
#include "util/strings.hpp"
#include "view/gui/custom_slider.hpp"
#include "view/gui/help_widget.hpp"

#include <QAbstractSlider>
#include <QDoubleSpinBox>
#include <QPushButton>
#include <QSignalBlocker>
#include <QSizePolicy>
#include <QSpacerItem>
#include <QString>

#include <cassert>

MinMaxPointScrewColumn::MinMaxPointScrewColumn(const GradingType grading,
                                               QWidget *parent) noexcept
    : QWidget(parent), mGrading(grading), mInitialMaximum("", this) {

    assert(grading == GradingType::Closed || grading == GradingType::Open);

    mInitialMaximum.setAlignment(Qt::AlignRight);
    mLayout.addWidget(&mInitialMaximum);

    {
        std::size_t row = 0;
        std::size_t label = 0;
        std::size_t slider = 0;
        createLabel(label++, row++, false);
        createSlider(slider++, row++);

        mLayout.addSpacing(20);

        createLabel(label++, row++, true);
        createSlider(slider++, row++);
    }

    // enables/disables slider and spin boxes
    connect(&DataManager::getInstance(), &DataManager::taskEliminationChanged,
            this, &MinMaxPointScrewColumn::pointsChanged);
    // update ranges when grades change
    connect(&DataManager::getInstance(), &DataManager::gradesChanged, this,
            &MinMaxPointScrewColumn::updateRanges);
    // update "initial max" when grades (max points) change
    connect(&DataManager::getInstance(), &DataManager::gradesChanged, this,
            &MinMaxPointScrewColumn::updateInitialMax);
    // update closed help text when grades (max points) change
    if (mGrading == GradingType::Closed) {
        connect(&DataManager::getInstance(), &DataManager::gradesChanged, this,
                &MinMaxPointScrewColumn::updateHelpText);
    }
}

void MinMaxPointScrewColumn::setWidgetsEnabled(
    const bool enable) const noexcept {
    for (const auto spinBox : mSpinBoxes) {
        spinBox->setEnabled(enable);
    }
    for (const auto slider : mSliders) {
        slider->setEnabled(enable);
    }
}

void MinMaxPointScrewColumn::createLabel(const std::size_t labelIdx,
                                         const std::size_t row,
                                         const bool isMin) noexcept {

    const auto &text = QString(isMin ? str::ui::gradingTab::minSlider
                                     : str::ui::gradingTab::maxSlider)
                           .arg(defaults::newGeneralExaminationRegulations
                                    ? ""
                                    : QString(" (%1)").arg(toString(mGrading)));

    // Create widgets
    mDescriptions[labelIdx] = new QLabel(text, this);
    mSpinBoxes[row] = new QDoubleSpinBox(this);

    connect(mSpinBoxes[row], &QDoubleSpinBox::valueChanged, this,
            [this, row](const double value) { spinBoxChange(row, value); });

    // Add row to vertical layout
    mHLayouts[row] = new QHBoxLayout();
    mLayout.addLayout(mHLayouts[row]);

    // Add widgets to horizontal layout
    mHLayouts[row]->addWidget(mDescriptions[labelIdx]);
    createQuestionIcon(*mHLayouts[row], isMin);
    mHLayouts[row]->addWidget(mSpinBoxes[row]);
}

void MinMaxPointScrewColumn::createSlider(const std::size_t sliderIdx,
                                          const std::size_t row) noexcept {

    // Create widgets
    mSliders[sliderIdx] = new CustomSlider(this);
    mSpinBoxes[row] = new QDoubleSpinBox(this);

    mSpinBoxes[row]->setSuffix("%");
    mSpinBoxes[row]->setDecimals(2);
    mSpinBoxes[row]->setKeyboardTracking(false);

    connect(
        mSliders[sliderIdx], &CustomSlider::valueChanged, this,
        [this, sliderIdx](const int value) { sliderChange(sliderIdx, value); });
    connect(mSpinBoxes[row], &QDoubleSpinBox::valueChanged, this,
            [this, row](const double value) { spinBoxChange(row, value); });

    // Add row to vertical layout
    mHLayouts[row] = new QHBoxLayout();
    mLayout.addLayout(mHLayouts[row]);

    // Add widgets to horizontal layout
    mHLayouts[row]->addWidget(mSliders[sliderIdx]);
    mHLayouts[row]->addWidget(mSpinBoxes[row]);
}

void MinMaxPointScrewColumn::createQuestionIcon(QLayout &layout,
                                                const bool isMin) noexcept {
    // Only add the question icon to the closed column and the min row
    if (!isMin) {
        mMaxQuestionIcon = new HelpWidget(
            this, str::ui::gradingTab::maxExplanationTitle,
            QString(str::ui::gradingTab::maxExplanation)
                .arg(str::control::richTextStart, str::control::richTextEnd));
        layout.addWidget(mMaxQuestionIcon);
    } else if (mGrading == GradingType::Closed) {
        mMinQuestionIcon =
            new HelpWidget(this, str::title::closedPassingMarkHelp, "");
        layout.addWidget(mMinQuestionIcon);
    } else {
        // Leave the same amount of space
        const auto uselessButton = new QPushButton(this);
        uselessButton->setFlat(true);
        uselessButton->setEnabled(false);
        layout.addWidget(uselessButton);
    }
}

void MinMaxPointScrewColumn::updateInitialMax() noexcept {

    const auto cur = DataManager::getCurrent();
    if (!cur)
        return;

    const auto initialNrMaxPoints = cur->getOriginalMaxPointsBoundary(mGrading);
    const auto currentNrMaxPoints = cur->getChangedMaxPointsBoundary(mGrading);

    // The "initial maximum" string should only be visible if the current
    // maximum is not the total maximum.
    // It should nevertheless always occupy the same amount of space.
    // Therefore we don't change the visibility of it, but set the text to an
    // empty string.

    if (initialNrMaxPoints == currentNrMaxPoints) {
        mInitialMaximum.setText(" ");
    } else {
        const auto stepSize = cur->mUserPointConfig.mGradeBoundaryStepSize;

        mInitialMaximum.setText(
            QString(str::info::initialMax)
                .arg(initialNrMaxPoints.toString(stepSize)));
    }
}

void MinMaxPointScrewColumn::setSpinBoxRange(
    QDoubleSpinBox &spinBox, const GradeBoundary min, const GradeBoundary max,
    const GradeBoundary start,
    const GradeBoundaryStepSize minPointStep) const noexcept {

    // don't trigger the valueChange-callback
    QSignalBlocker blocker(spinBox);

    spinBox.setRange(toPoints(min, minPointStep).p,
                     toPoints(max, minPointStep).p);
    spinBox.setValue(toPoints(start, minPointStep).p);

    spinBox.setDecimals(nrDecimalPlaces(minPointStep));
    spinBox.setSingleStep(as<double>(minPointStep));
}

void MinMaxPointScrewColumn::setSliderRange(
    QAbstractSlider &slider, QDoubleSpinBox &spinBox,
    const GradeBoundary rangeMax, const GradeBoundary start,
    const GradeBoundary maxPoints) const noexcept {

    // don't trigger the valueChange-callback
    QSignalBlocker sliderBlocker(slider);
    QSignalBlocker spinBoxBlocker(spinBox);

    slider.setRange(0, rangeMax.mMultiple);
    slider.setValue(start.mMultiple);

    // Make sure we don't divide by zero
    if (maxPoints.mMultiple > 0) {
        // If the range max is smaller than 10, Qt makes the spin box smaller
        spinBox.setRange(0., std::max(10., rangeMax / maxPoints * 100.));
        spinBox.setValue(start / maxPoints * 100.);
    } else {
        spinBox.setRange(0., 50.);
        spinBox.setValue(0.);
    }

    if (rangeMax.mMultiple > 0) {
        spinBox.setSingleStep((spinBox.maximum() - spinBox.minimum()) /
                              rangeMax.mMultiple);
    } else {
        spinBox.setSingleStep(50.);
    }
}

void MinMaxPointScrewColumn::updateRanges() noexcept {

    const auto cur = DataManager::getCurrent();
    if (!cur)
        return;

    // Since our sliders and spin boxes work in multiples of the step size, we
    // want the max point grade boundary here instead of just the max points
    const auto maxTotalPoints = cur->getOriginalMaxPointsBoundary(mGrading);
    const auto maxBoundary4_0 = cur->getMaxPassingLimit(mGrading);

    const auto currentTotalPoints = cur->getChangedMaxPointsBoundary(mGrading);
    const auto currentBoundary4_0 =
        cur->getUserGradeBoundaries(mGrading)[Grade::grade4_0];

    const auto minPointStep = cur->mUserPointConfig.mGradeBoundaryStepSize;

    // Point spin box
    setSpinBoxRange(*mSpinBoxes[0], GradeBoundary{0}, maxTotalPoints,
                    currentTotalPoints, minPointStep);
    setSpinBoxRange(*mSpinBoxes[2], GradeBoundary{0}, maxBoundary4_0,
                    currentBoundary4_0, minPointStep);

    // Percentage slider & spin box
    setSliderRange(*mSliders[0], *mSpinBoxes[1], maxTotalPoints,
                   currentTotalPoints, maxTotalPoints);
    setSliderRange(*mSliders[1], *mSpinBoxes[3], maxBoundary4_0,
                   currentBoundary4_0, currentTotalPoints);
}

void MinMaxPointScrewColumn::updateHelpText() noexcept {

    const auto cur = DataManager::getCurrent();
    if (!cur)
        return;

    /*
     * relPassMarkDecisive is true if the passing limit for closed points is
     * <60%, because the relative passing limit is used instead of the absolute
     * passing limit
     */
    const GradeBoundaryFactory f{cur->mUserPointConfig.mGradeBoundaryStepSize,
                                 cur->mUserPointConfig.mGradeBoundaryRounding};

    const auto relPassMarkDecisive =
        cur->getRelPassingLimit().has_value() &&
        f(*cur->getRelPassingLimit()) < f(cur->getAbsPassingLimit());

    // Use rich text here for smart line breaks
    QString questionIconHoverText;
    if (!relPassMarkDecisive) {
        questionIconHoverText =
            QString(str::info::sixtyPercentHover1)
                .arg(str::control::richTextStart, str::control::richTextEnd);
    } else {
        /* Always use two decimals because this is a percentage */
        questionIconHoverText =
            QString(str::info::sixtyPercentHover1)
                .arg(str::control::richTextStart, "<br><br>") +
            QString(str::info::sixtyPercentHover2)
                .arg(mSpinBoxes[3]->maximum(), 0, 'f', 2)
                .arg(str::control::richTextEnd);
    }
    mMinQuestionIcon->setHelpText(questionIconHoverText);
}

void MinMaxPointScrewColumn::pointsChanged() noexcept {
    const auto cur = DataManager::getCurrent();

    if (!cur)
        return;

    setWidgetsEnabled(cur->hasType(mGrading));
}

void MinMaxPointScrewColumn::sliderChange(const std::size_t sliderIdx,
                                          const int value) noexcept {

    // This works because one slider unit equals one stepSize, and the slider
    // always starts at 0.
    const GradeBoundary realValue{value};

    if (sliderIdx == 0) {
        DataManager::maxChange(mGrading, realValue);
    } else {
        DataManager::boundaryChange(mGrading, Grade::grade4_0, realValue);
    }
}

void MinMaxPointScrewColumn::spinBoxChange(const std::size_t spinBoxIdx,
                                           const double value) noexcept {
    const auto cur = DataManager::getCurrent();
    if (!cur)
        return;

    // Whether this spin box has percentages as unit
    const bool isPercent = spinBoxIdx % 2;
    //  Whether the spin box relates to minimum points or maximum points
    const auto isMin = spinBoxIdx > 1;

    const auto stepSize = cur->mUserPointConfig.mGradeBoundaryStepSize;

    // What "100%" in the percent spin box equates to
    // The max spin boxes go to the original max point boundary, the passing
    // limit (min) spin boxes go to the changed max point boundary, which is
    // also the current value of the max spin box
    const auto max =
        toPoints(isMin ? cur->getChangedMaxPointsBoundary(mGrading)
                       : cur->getOriginalMaxPointsBoundary(mGrading),
                 stepSize);

    // value converted to a GradeBoundary
    GradeBoundary boundary;

    if (isPercent) {
        boundary = GradeBoundary{max * (value / 100.), stepSize,
                                 GradeBoundaryRounding::Nearest};

    } else {
        boundary = GradeBoundary{Points{value}, stepSize,
                                 cur->mUserPointConfig.mGradeBoundaryRounding};
    }

    if (isMin) {
        DataManager::boundaryChange(mGrading, Grade::grade4_0, boundary);
    } else {
        DataManager::maxChange(mGrading, boundary);
    }
}

MinMaxPointScrews::MinMaxPointScrews(QWidget *parent) noexcept
    : QWidget(parent), mAvgLabel(str::ui::gradingTab::avgAttended),
      mAvgPassedLabel(str::ui::gradingTab::avgPassed),
      mNrPassedLabel(str::ui::gradingTab::attendedPassed),
      mNrFailedLabel(str::ui::gradingTab::attendedFailed) {

    // Even space above and below the screw columns, avg at the very bottom
    mVLayout.addItem(
        new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding));

    createScrewColumns();

    mVLayout.addItem(
        new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding));

    createAvgLabels();

    // receive update when grades change
    connect(&DataManager::getInstance(), &DataManager::gradesChanged, this,
            &MinMaxPointScrews::gradesChanged);
}

void MinMaxPointScrews::gradesChanged() noexcept {

    using namespace str::ui::gradingTab;

    const auto cur = DataManager::getCurrent();
    if (!cur) {
        mAverageGrade.setText(gradeResultNone);
        mAverageGradePassed.setText(gradeResultNone);
        mNrPassed.setText(attendedResultNone);
        mNrFailed.setText(attendedResultNone);
        return;
    }

    const auto gradeAverages = cur->getGradeAverages();

    // Number of decimal places for average grade is always 2, independent of
    // the grade boundary step size, because this is an averaged grade, not a
    // point count
    if (gradeAverages.mAverageGrade.has_value()) {
        mAverageGrade.setText(
            QString::number(*gradeAverages.mAverageGrade, 'f', 2));
    } else {
        mAverageGrade.setText("-");
    }
    if (gradeAverages.mAverageGradePassed.has_value()) {
        mAverageGradePassed.setText(
            QString::number(*gradeAverages.mAverageGradePassed, 'f', 2));
    } else {
        mAverageGradePassed.setText("-");
    }

    {
        const auto &nrPassed = gradeAverages.mNrPassed;
        const auto &nrAttended = cur->getStats().mNrAttended;
        const auto &nrFailed = nrAttended - nrPassed;
        if (nrAttended == 0) {
            mNrPassed.setText(attendedResultNone);
            mNrFailed.setText(attendedResultNone);
        } else {
            mNrPassed.setText(
                QString(attendedResultAny)
                    .arg(nrPassed)
                    .arg(nrAttended)
                    .arg(nrPassed / static_cast<double>(nrAttended) * 100., 0,
                         'f', 2));

            mNrFailed.setText(
                QString(attendedResultAny)
                    .arg(nrFailed)
                    .arg(nrAttended)
                    .arg(nrFailed / static_cast<double>(nrAttended) * 100., 0,
                         'f', 2));
        }
    }
}

void MinMaxPointScrews::createScrewColumns() noexcept {
    using enum GradingType;

    mColumns[Open] = new MinMaxPointScrewColumn(Open, this);
    if constexpr (!defaults::newGeneralExaminationRegulations) {
        mColumns[Closed] = new MinMaxPointScrewColumn(Closed, this);
    }

    mVLayout.addLayout(&mHLayout);

    mHLayout.addSpacing(10);
    mHLayout.addStretch(1);
    mHLayout.addWidget(mColumns[Open], 5);
    if constexpr (!defaults::newGeneralExaminationRegulations) {
        mHLayout.addSpacing(20);
        mHLayout.addStretch(2);
        mHLayout.addWidget(mColumns[Closed], 5);
    }
    mHLayout.addSpacing(10);
    mHLayout.addStretch(1);
}

void MinMaxPointScrews::changeFontSize(const FontSize newFontSize) noexcept {

    auto font = mAverageGrade.font();
    setFontSize(font, newFontSize);

    // Since these labels use the large font size as opposed to the default font
    // size, we have to update them when it changes
    for (const auto &label :
         {&mAverageGrade, &mAverageGradePassed, &mNrPassed, &mAvgLabel,
          &mAvgPassedLabel, &mNrPassedLabel, &mNrFailed, &mNrFailedLabel}) {
        label->setFont(font);
    }
}

void MinMaxPointScrews::createAvgLabels() noexcept {
    mVLayout.addLayout(&mAvgLayout);

    static constexpr int textColumn = 0;
    static constexpr int avgNumberColumn = 1;
    int row = 0;

    mAvgLayout.addWidget(&mAvgLabel, row, textColumn);
    mAvgLayout.addWidget(&mAverageGrade, row, avgNumberColumn);
    ++row;

    mAvgLayout.addWidget(&mAvgPassedLabel, row, textColumn);
    mAvgLayout.addWidget(&mAverageGradePassed, row, avgNumberColumn);
    ++row;

    mAvgLayout.addWidget(&mNrPassedLabel, row, textColumn);
    mAvgLayout.addWidget(&mNrPassed, row, avgNumberColumn);
    ++row;

    mAvgLayout.addWidget(&mNrFailedLabel, row, textColumn);
    mAvgLayout.addWidget(&mNrFailed, row, avgNumberColumn);
}
