#pragma once

#include <QTextStream>

class QWidget;
class QTableWidget;

/**
 * \brief Exports the students which were in the input RWTHonline file to a new
 * file, with grades and remarks filled in.
 * \param parent Parent of the file dialog (only used if exportLocationString is
 * not empty)
 * \param exportStream If not null, the stream to write to. If null, a new file
 * will be created.
 */
bool exportKnownStudents(QWidget *parent, QTextStream *exportStream = nullptr);
bool exportUnknownStudents(QWidget *parent,
                           QTextStream *exportStream = nullptr);
void exportManualAllStudents(QWidget *parent, const QTableWidget &studentTable);
