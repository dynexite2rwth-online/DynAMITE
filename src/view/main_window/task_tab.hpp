#pragma once

#include "model/task_model.hpp"

#include <QCheckBox>
#include <QComboBox>
#include <QSortFilterProxyModel>
#include <QTableView>

struct FontSize;
class ViewSettingsWindow;

class TaskTab : public QWidget {
    Q_OBJECT

public:
    explicit TaskTab(const ViewSettingsWindow &viewSettings,
                     QWidget *parent = nullptr);

    void handleResize(FontSize largeFontSize) const;

protected:
    // Adjust size of rows & columns once they are shown (not possible before)
    void showEvent(QShowEvent *event) override;

private Q_SLOTS:
    void toggleShowPlot();
    void overwriteSortByComboBox(int index, Qt::SortOrder order);
    void overwriteTableHeaderSort(int index, Qt::SortOrder order);
    void modelAboutToBeReset();
    void modelReset();
    void joinTaskPoolsStateChanged(int state);
    void gradingTypeSelectionChanged(int index);

private:
    [[nodiscard]] bool plotShown() const noexcept;

    [[nodiscard]] int mapComboBoxToColumn(int index) const noexcept;
    [[nodiscard]] int mapColumnToComboBox(int index) const noexcept;

    void toggleShowPlotNoResize();
    void filterRows();
    void showAllRows();

    void resizeCells() noexcept;

private:
    QComboBox mGradingTypeSelection;
    QComboBox mSortBy;
    QComboBox mSortOrder;
    QCheckBox mShowHistogram;
    QCheckBox mShowBoxPlot;
    QCheckBox mJoinTaskPools;

    QTableView mTable;

    TaskModel mTaskModel;
    QSortFilterProxyModel mProxyModel;
};
