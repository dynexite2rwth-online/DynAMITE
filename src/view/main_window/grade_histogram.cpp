#include "grade_histogram.hpp"

#include "data/data_manager.hpp"
#include "model/grade_model.hpp"
#include "util/enums.hpp"
#include "util/font.hpp"
#include "util/grade.hpp"
#include "util/heuristics.hpp"

#include <QBarCategoryAxis>
#include <QBarSeries>
#include <QBarSet>
#include <QChart>
#include <QColor>
#include <QDebug>
#include <QEasingCurve>
#include <QString>
#include <QToolTip>
#include <QValueAxis>

#include <algorithm>
#include <cassert>
#include <cmath>

GradeHistogram::GradeHistogram(const QList<GradingType> &gradingTypes,
                               QWidget *parent)
    : QChartView(new QChart(), parent), mAxisY(new QValueAxis()) {

    /* todo: do these heap variables have to be class members? check for other
     * classes */

    assert(!gradingTypes.empty());

    const auto series = new QBarSeries();
    chart()->addSeries(series);

    for (const auto &g : gradingTypes) {
        mModels.emplaceBack(new GradeModel(g, parent));
        mBarSets.emplaceBack(new QBarSet(toString(g)));

        mBarSets.back()->setColor(defaults::gradingTypeColors[g]);
        mBarSets.back()->setBorderColor(
            defaults::gradingTypeColors[g].darker());
        mBarSets.back()->setLabelColor(QColor(255, 255, 255));

        auto font = mBarSets.back()->labelFont();
        font.setBold(true);
        mBarSets.back()->setLabelFont(font);

        series->append(mBarSets.back());
    }

    if (gradingTypes.size() > 1)
        series->setBarWidth(0.75);

    // Initialize all grade counts to zero (todo: move to model?)
    {
        // count for each grade and one for "not attended"
        const QList<qreal> zeroes(static_cast<qsizetype>(Grade::NR_VALS) + 1);
        for (const auto &barSet : mBarSets) {
            barSet->append(zeroes);
        }
    }

    series->setLabelsVisible(
        true); // todo: write above the bar if bar too small

    // x axis
    QStringList categories;
    categories << "1.0"
               << "1.3"
               << "1.7"
               << "2.0"
               << "2.3"
               << "2.7"
               << "3.0"
               << "3.3"
               << "3.7"
               << "4.0"
               << "5.0"
               << "Not attended";

    const auto axisX = new QBarCategoryAxis();
    axisX->setCategories(categories);
    chart()->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);

    // y axis
    mAxisY->setTickType(QValueAxis::TicksDynamic);

    // temp values until max is known
    mAxisY->setRange(0, 10);
    mAxisY->setTickInterval(5);

    mAxisY->setTickAnchor(0);
    mAxisY->setLabelFormat("%d");
    chart()->addAxis(mAxisY, Qt::AlignLeft);
    series->attachAxis(mAxisY);

    // only show legend entry if we have multiple bars
    if (gradingTypes.size() > 1) {
        chart()->legend()->setVisible(true);
        chart()->legend()->setAlignment(Qt::AlignBottom);
    } else {
        chart()->legend()->setVisible(false);
    }

    setRenderHint(QPainter::Antialiasing);
    setInteractive(false);
    chart()->setMinimumSize(400, 200);

    // Animation settings
    chart()->setAnimationOptions(QChart::SeriesAnimations);
    chart()->setAnimationEasingCurve(QEasingCurve::Linear);
    chart()->setAnimationDuration(50);

    // receive update when grades change
    connect(&DataManager::getInstance(), &DataManager::gradesChanged, this,
            &GradeHistogram::gradesChanged);

    // show bar value when bar set is hovered (wip)
    connect(series, &QBarSeries::hovered, this, &GradeHistogram::showHoverText);
}

void GradeHistogram::handleResize(const QRectF plotArea,
                                  const FontSize largeFontSize,
                                  const FontSize smallFontSize) {

    mPlotArea = plotArea;

    // adjust bar label font size
    updateBarLabelSize(largeFontSize);

    // adjust x/y axis font size
    {
        const auto xaxes = chart()->axes(Qt::Orientation::Horizontal);
        for (const auto &xax : xaxes) {

            auto font = xax->labelsFont();
            setFontSize(font, largeFontSize);
            xax->setLabelsFont(font);
        }
    }
    {
        auto font = mAxisY->labelsFont();
        setFontSize(font, largeFontSize);
        mAxisY->setLabelsFont(font);
    }
    {
        auto font = chart()->legend()->font();
        setFontSize(font, smallFontSize);
        chart()->legend()->setFont(font);
    }

    // Round down
    mMaxNrYLabels = static_cast<int>(mPlotArea.height() /
                                     (2 * largeFontSize.getPixelSize()));

    updateYAxis();
}

// todo: QBarSet, QBarSeries probably don't have to be recreated
// todo: use the fact GradeModel is a model
void GradeHistogram::gradesChanged(const EnumStorage<bool> &changedTypes) {

    if (!DataManager::getCurrent()) {
        return;
    }

    for (int modelIdx = 0; const auto &model : mModels) {
        if (changedTypes[model->mGradingType]) {
            model->updateData();

            // grades
            for (int i = 0; i < static_cast<int>(Grade::NR_VALS); ++i) {
                mBarSets[modelIdx]->replace(i, model->mGradeCounts[i]);
            }

            // not attended
            mBarSets[modelIdx]->replace(
                static_cast<int>(Grade::NR_VALS),
                DataManager::getCurrent()->getStats().mNrRegisteredNotAttended);

            // color non-passed-grades (5.0 and not attended) red by selecting
            // them and setting the selection-color to red
            mBarSets[modelIdx]->setSelectedColor(defaults::notPassedColor);
            mBarSets[modelIdx]->selectBars({10, 11});
        }
        ++modelIdx;
    }

    updateYAxis();
}

void GradeHistogram::updateYAxis() {

    // find largest bar (either the not-attended bar or the bar of a grade of a
    // grading type)
    const auto cur = DataManager::getCurrent();
    int largestBar = cur ? cur->getStats().mNrRegisteredNotAttended : 0;
    for (const auto &model : mModels) {
        largestBar = std::max(largestBar,
                              *std::ranges::max_element(model->mGradeCounts));
    }

    {
        const auto [yAxisMax, yAxisStepSize] =
            niceYAxis(largestBar, mMaxNrYLabels);

        if (mAxisY->tickInterval() != yAxisStepSize)
            mAxisY->setTickInterval(yAxisStepSize);

        const auto newMax = yAxisMax * defaults::yAxisRangeScaling;
        if (mAxisY->max() != newMax)
            mAxisY->setMax(newMax);
    }

    const auto oldMax = static_cast<int>(mAxisY->max());
    const auto newMax = static_cast<int>(mAxisY->max());
    const auto digitDiff = static_cast<int>(QString::number(newMax).length() -
                                            QString::number(oldMax).length());

    if (digitDiff != 0) {
        auto oldMargins = chart()->margins();
        oldMargins.setLeft(oldMargins.left() - digitDiff * 4);
        chart()->setMargins(oldMargins);
    }
}

/**
 * \brief Estimates the number of digits that are needed to display the largest
 * histogram bar label
 *
 * We assume at max. 3 digits for a label if there are 200-1999 students, 4
 * digits if there are 2000-19999 students etc. (since all students are
 * distributed across the different bars)
 */
int getMaxNrBarDigits(const qsizetype nrStudents) {
    if (nrStudents <= 3)
        return 1;
    else
        return static_cast<int>(std::ceil(std::log10(nrStudents / 2)));
}

FontSize GradeHistogram::getFontSize(const QRectF &plotArea) {

    mPlotArea = plotArea;

    const auto cur = DataManager::getCurrent();

    // default value
    if (!cur) {
        return {16., FontSizeUnit::Pixel};
    }

    // Calculate the maximum number of digits the largest label on a bar will
    // probably have
    const auto nrStudents = cur->getGradeList(GradingType::Combined).size();
    const auto numberDigits = getMaxNrBarDigits(nrStudents);

    // get width of single digit
    auto font = mBarSets.back()->labelFont();
    font.setPixelSize(16);
    const QFontMetrics metrics(font);
    const auto digitPixelWidth = metrics.horizontalAdvance("0");
    const auto heightWidthRatio =
        font.pixelSize() / static_cast<double>(digitPixelWidth);

    // Set the font size so that a number with numberDigits of digits fits
    // horizontally into a combined bar.
    // Note that the combined bar is narrower, if both closed and open points
    // exist.
    const bool hasCombined = cur->hasType(GradingType::Combined);

    // Width of the bar as percentage of the space reserved for one bar
    const auto barWidthPortion = hasCombined ? 0.75 : 0.5;
    const auto realPlotAreaWidth = mPlotArea.width();

    const auto barSpaceWidth =
        realPlotAreaWidth / static_cast<double>(Grade::NR_VALS);

    // If we have a combined bar chart, there are multiple bar sets, resulting
    // in thinner bars
    const auto barWidth =
        barSpaceWidth * barWidthPortion / (hasCombined ? 3 : 1);

    const auto pxPerDigit = barWidth / numberDigits;

    return {std::floor(pxPerDigit * heightWidthRatio), FontSizeUnit::Pixel};
}

/**
 * \brief Doesn't work (never triggered), probably because the axis is
 * (intentionally) not properly attached.
 */
void GradeHistogram::showHoverText(const bool status, const int index,
                                   QBarSet * /*barset*/) {
    qDebug() << "showHoverText called with status =" << status
             << "and index =" << index;

    if (status) {
        QToolTip::showText(QCursor::pos(), "text");
    } else {
        QToolTip::hideText();
    }
}

void GradeHistogram::updateBarLabelSize(const FontSize largeFontSize) {

    auto font = mBarSets.back()->labelFont();
    setFontSize(font, largeFontSize);
    for (const auto &bs : mBarSets) {
        bs->setLabelFont(font);
    }
}
