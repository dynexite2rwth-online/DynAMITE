/**
 * \file main_window.cpp
 * \brief Definition of the \ref MainWindow member functions.
 */

#include "main_window.hpp"

#include "export_rwthonline.hpp"
#include "grade_tab.hpp"
#include "students_tab.hpp"
#include "task_tab.hpp"

#include "data/data_manager.hpp"
#include "file_io/d2r_parser.hpp"
#include "file_io/file.hpp"
#include "util/defaults.hpp"
#include "util/enums.hpp"
#include "util/font.hpp"
#include "util/grade.hpp"
#include "util/grade_boundary.hpp"
#include "util/grade_boundary_step.hpp"

#include <QAction>
#include <QApplication>
#include <QClipboard>
#include <QCloseEvent>
#include <QDesktopServices>
#include <QFileDialog>
#include <QLabel>
#include <QMenuBar>
#include <QMessageBox>
#include <QString>
#include <QUrl>

#include <cmath>

namespace {
// Surround the given string evenly with spaces, until it has a size of `size`
QString padToSize(QString str, const int size) {
    const auto spacesLeft = static_cast<int>(
        qMax(0., std::floor(static_cast<double>(size - str.size()) / 2.)));
    const auto spacesRight = static_cast<int>(
        qMax(0., std::ceil(static_cast<double>(size - str.size()) / 2.)));

    str.prepend(QString(spacesLeft, ' '));
    str.append(QString(spacesRight, ' '));

    return str;
}

void printBoundaries(QString &str, const GradingType type, const bool hasType,
                     const GradeBoundaryStepSize stepSize) {

    const auto cur = DataManager::getCurrent();
    if (!cur)
        return;

    if (hasType) {
        if constexpr (!defaults::newGeneralExaminationRegulations) {
            str += QString(toString(type)) + " part:\n";
        }
        str += "Grade --- [credits >=] --- [credits <]\n";

        // Initial value doesn't matter, because it gets overwritten with "max"
        auto lastGradeBoundary = cur->getChangedMaxPointsBoundary(type);

        for (const auto g : grades) {
            const auto minBoundary = cur->getUserGradeBoundaries(type)[g];

            // We don't display the max points of 1.0.
            // For other max points, we go one grade boundary step below the
            // next higher grade.
            const auto maxBoundary = g == Grade::grade1_0
                                         ? QString("max")
                                         : lastGradeBoundary.toString(stepSize);

            str += QString("%1:%2-%3\n")
                       .arg(padToSize(toString(g), 7),
                            padToSize(minBoundary.toString(stepSize), 16),
                            padToSize(maxBoundary, 13));
            lastGradeBoundary = cur->getUserGradeBoundaries(type)[g];
        }

        str += "\n";
    }
}

void copyGradingSchemeToClipboard() {
    const auto cur = DataManager::getCurrent();
    if (!cur)
        return;

    auto *clipboard = QGuiApplication::clipboard();

    const bool hasClosed = cur->hasType(GradingType::Closed);
    const bool hasOpen = cur->hasType(GradingType::Open);

    QString gradingSchemeText = "";

    const auto stepSize = cur->mUserPointConfig.mGradeBoundaryStepSize;

    printBoundaries(gradingSchemeText, GradingType::Open, hasOpen, stepSize);
    printBoundaries(gradingSchemeText, GradingType::Closed, hasClosed,
                    stepSize);

    if (!hasOpen && !hasClosed) {
        gradingSchemeText +=
            "Selected exam has neither open nor closed points.";
    }

    clipboard->setText(gradingSchemeText);
}

/**
 * \brief Returns true if the settings were successfully saved (the user can
 * abort the export location selection).
 */
bool exportD2R(QWidget *parent) {
    const auto cur = DataManager::getCurrent();
    if (!cur)
        return false;

    const auto exportLocationString = QFileDialog::getSaveFileName(
        parent, "Save data all-in-one (d2r)", "",
        "All-in-one (D2R) file (*.d2r);;All files (*.*)");

    if (!exportLocationString.isEmpty()) {
        auto [file, stream] = openFileToWrite(exportLocationString);
        writeCurrentSettingsToD2r(*stream);
        DataManager::setUnsavedChanges(false);
        return true;
    } else {
        return false;
    }
}
} // namespace

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), mInputWindow(*this, parent), mBoundaryWindow(parent),
      mViewSettings(parent), mStatisticsWindow(parent),
      mCalculationWindow(parent), mEliminateTasksWindow(parent) /*,
mGradeTab(mViewSettings), mStudentsTab()*/
{

    setWindowTitle("DynAMITE");

    setCentralWidget(&mTabs);

    mGradeTab = new GradeTab{mViewSettings};
    mTaskTab = new TaskTab{mViewSettings};
    mStudentsTab = new StudentsTab{};
    mTabs.addTab(mGradeTab, "Grading");
    mTabs.addTab(mTaskTab, "Tasks");
    mTabs.addTab(mStudentsTab, "Students");

    // Create handle for all windows for operations that need to iterate over
    // all of them
    mSubWindowPtrs[0] = &mInputWindow;
    mSubWindowPtrs[1] = &mBoundaryWindow;
    mSubWindowPtrs[2] = &mViewSettings;
    mSubWindowPtrs[3] = &mStatisticsWindow;
    mSubWindowPtrs[4] = &mCalculationWindow;
    mSubWindowPtrs[5] = &mEliminateTasksWindow;

    // Set appropriate flags for all small openable windows: not maximizable,
    // stay on top, closable
    for (const auto &window : mSubWindowPtrs) {
        window->setWindowFlags(
            Qt::WindowStaysOnTopHint | Qt::WindowCloseButtonHint |
            Qt::WindowMinimizeButtonHint | Qt::MSWindowsFixedSizeDialogHint);
        window->setWindowFlags(mBoundaryWindow.windowFlags() &
                               ~Qt::WindowMaximizeButtonHint);
    }

    // start input file selection
    mInputWindow.show();

    createMenuBar();

    connect(&mTabs, &QTabWidget::currentChanged, this, &MainWindow::tabChanged);
}

void MainWindow::resetToInputFiles(const DynexiteExam &exam,
                                   const StudentData &students,
                                   const BonusPoints &bonus,
                                   const GradeBoundaryStepSize stepSize,
                                   const std::optional<D2rSettings> &settings) {

    UserPointConfig pc;
    pc.mGradeBoundaryStepSize = stepSize;
    if (DataManager::loadNewExam(exam, students, bonus, UserGradeConfig{},
                                 pc)) {

        // emit grades-changed-signal only when there are no further settings to
        // apply
        DataManager::makeNewCurrent(!settings);

        if (settings) {
            DataManager::applySettings(*settings);
        }

        // close input window and show main window
        mInputWindow.hide();
        showMaximized();

        updateMenuItems();

        // Change font size to adapt to new bar sizes
        handleResize();
    }
}

void MainWindow::menuNewFile() {

    // It would be nicer if we only ask to save once "import" is clicked, but it
    // is easier to implement to ask for save when "open new file" is clicked.

    const auto ret = askForSave();

    switch (ret) {
    case QMessageBox::Save:
        // Save was clicked; open new file dialogue if export wasn't aborted
        if (exportD2R(this)) {
            mInputWindow.show();
        }
        break;
    case QMessageBox::Discard:
        // Don't Save was clicked
        mInputWindow.show();
        break;
    case QMessageBox::Cancel:
        // Cancel was clicked
        break;
    default:
        // should never be reached
        assert(false);
        break;
    }
}

void MainWindow::menuOpenBoundaries() { mBoundaryWindow.show(); }

void MainWindow::menuOpenViewSettings() { mViewSettings.show(); }

void MainWindow::menuOpenStatistics() { mStatisticsWindow.show(); }

void MainWindow::menuOpenCalculations() { mCalculationWindow.show(); }

void MainWindow::menuOpenEliminateTasks() { mEliminateTasksWindow.show(); }

void MainWindow::tabChanged(int) {
    // handle resize here again, because sizes of non-displayed tabs are not
    // calculated correctly
    handleResize();
}

void MainWindow::closeEvent(QCloseEvent *event) {

    const auto ret = askForSave();

    switch (ret) {
    case QMessageBox::Save:
        // Save was clicked; close if export wasn't aborted
        if (exportD2R(this)) {
            event->accept();
        } else {
            event->ignore();
        }
        break;
    case QMessageBox::Discard:
        // Don't Save was clicked
        event->accept();
        break;
    case QMessageBox::Cancel:
        // Cancel was clicked
        event->ignore();
        break;
    default:
        // should never be reached
        assert(false);
        event->ignore();
        break;
    }
}

void MainWindow::resizeEvent(QResizeEvent *event) {
    QMainWindow::resizeEvent(event);

    handleResize();
}

void MainWindow::handleResize() {

    static FontSize defaultFontSize;

    static bool firstExecution = true;
    if (firstExecution) {

        const auto defaultFont = QLabel{}.font();

        // Default font size might be given in points or in pixels
        if (defaultFont.pixelSize() > 0) {
            defaultFontSize = {static_cast<qreal>(defaultFont.pixelSize()),
                               FontSizeUnit::Pixel};
        } else {
            defaultFontSize = {defaultFont.pointSizeF(), FontSizeUnit::Point};
        }

        firstExecution = false;
    }

    // Find out new font size (not smaller than default font)
    const auto possibleLargeFontSize = mGradeTab->getFontPxSize(this->size());

    // Large font size may not be smaller than the default font size
    auto largeFontSize = qMax(possibleLargeFontSize, defaultFontSize);

    // Don't make large font size too large
    largeFontSize = qMin(largeFontSize, defaultFontSize * 4);

    // Small font size is a fixed ratio smaller than the large font size, but
    // also not smaller than defaultFontSize
    const auto smallFontSize =
        qMax(largeFontSize * defaults::smallFontSizeRatio, defaultFontSize);

    // Set font size of all labels, text boxes etc
    auto defaultFont = QApplication::font();
    setFontSize(defaultFont, smallFontSize);
    QApplication::setFont(defaultFont);

    // only the currently opened tab can be resized correctly, other tabs will
    // be resized when opened
    const auto currentTabIdx = mTabs.currentIndex();
    switch (currentTabIdx) {
    default:
        assert(false);
        [[fallthrough]];
    case 0:
        // Set font size for axes/legend and adapt axis spacing
        mGradeTab->handleResize(largeFontSize, smallFontSize);
        break;
    case 1:
        // Set font size for axes
        mTaskTab->handleResize(largeFontSize);
        break;
    case 2:
        mStudentsTab->handleResize();
        break;
    }

    mEliminateTasksWindow.handleResize();
}

int MainWindow::askForSave() const {

    // Only ask if there are any changes
    if (DataManager::hasUnsavedChanges()) {
        // Hide all children windows so that they don't interfere with the
        // save-window
        for (const auto &window : mSubWindowPtrs) {
            window->hide();
        }

        // Ask user to save
        // Would be better if we only asked whether to save if there were any
        // changes
        QMessageBox msgBox{};
        msgBox.setText(str::ui::saveDialog);
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard |
                                  QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);
        return msgBox.exec();
    }

    return QMessageBox::Discard;
}

void MainWindow::updateMenuItems() const {
    const auto cur = DataManager::getCurrent();

    const auto nrUnknownStudents =
        cur ? cur->getStats().mNrAttendedNotRegistered : 0;

    mExportUnknownStudentsAct->setText("Manual: Unknown Students (" +
                                       QString::number(nrUnknownStudents) +
                                       ")");
    mExportUnknownStudentsAct->setEnabled(nrUnknownStudents > 0);
}

void MainWindow::createMenuBar() {

    /***************** Exam **************/
    mFileMenu = menuBar()->addMenu("&Exam");

    // "Open": loads a new exam
    {
        const auto newAct = new QAction("&Open", this);
        newAct->setShortcuts(QKeySequence::Open);
        connect(newAct, &QAction::triggered, this, &MainWindow::menuNewFile);
        mFileMenu->addAction(newAct);
    }

    // "Options": change the required points for e.g. 2.7, change requirePassAll
    {
        const auto boundaryAct = new QAction("Options", this);
        boundaryAct->setShortcuts(QKeySequence::Preferences);
        connect(boundaryAct, &QAction::triggered, this,
                &MainWindow::menuOpenBoundaries);
        mFileMenu->addAction(boundaryAct);
    }

    // "Eliminate tasks": set for each task individually whether it is counted
    // as normal, all students get full points or no students get any points and
    // the max is reduced
    {
        const auto eliminateTasksAct = new QAction("Eliminate tasks", this);
        connect(eliminateTasksAct, &QAction::triggered, this,
                &MainWindow::menuOpenEliminateTasks);
        mFileMenu->addAction(eliminateTasksAct);
    }

    // "Statistics": show more detailed statistics like number of attended
    // students
    {
        const auto statisticsAct = new QAction("Statistics", this);
        connect(statisticsAct, &QAction::triggered, this,
                &MainWindow::menuOpenStatistics);
        mFileMenu->addAction(statisticsAct);
    }

    // "Calculating the Closed Pass Mark": show more detailed pass mark
    // calculations
    if constexpr (!defaults::newGeneralExaminationRegulations) {
        const auto calculationsAct =
            new QAction("Calculating the Closed Pass Mark", this);
        connect(calculationsAct, &QAction::triggered, this,
                &MainWindow::menuOpenCalculations);
        mFileMenu->addAction(calculationsAct);
    }

    /***************** View **************/
    mViewMenu = menuBar()->addMenu("&View");

    // "Settings": change visual options
    {
        const auto viewSettingsAct = new QAction("Settings", this);
        connect(viewSettingsAct, &QAction::triggered, this,
                &MainWindow::menuOpenViewSettings);
        mViewMenu->addAction(viewSettingsAct);
    }

    /**************** Export *************/
    mExportMenu = menuBar()->addMenu("E&xport");

    // Export grading scheme to clipboard
    {
        mExportGradingMenu = mExportMenu->addMenu("Grading scheme");
        const auto exportGradingSchemeClipboardAct =
            new QAction("To Clipboard", this);
        connect(exportGradingSchemeClipboardAct, &QAction::triggered,
                copyGradingSchemeToClipboard);
        mExportGradingMenu->addAction(exportGradingSchemeClipboardAct);
    }

    // Export students
    {
        mExportStudentsMenu = mExportMenu->addMenu("Results");
        // Export known students as CSV
        {
            const auto exportKnownStudentsAct =
                new QAction("RWTHonline: Known Students", this);
            connect(exportKnownStudentsAct, &QAction::triggered,
                    exportKnownStudentsAct,
                    [this] { exportKnownStudents(this); });
            mExportStudentsMenu->addAction(exportKnownStudentsAct);
        }

        // Export unknown students as CSV
        {
            mExportUnknownStudentsAct =
                new QAction("Manual: Unknown Students", this);
            connect(mExportUnknownStudentsAct, &QAction::triggered,
                    mExportUnknownStudentsAct,
                    [this] { exportUnknownStudents(this); });
            mExportStudentsMenu->addAction(mExportUnknownStudentsAct);
        }

        // Export all students as CSV
        {
            const auto exportAllStudentsAct =
                new QAction("Export students tab", this);
            connect(exportAllStudentsAct, &QAction::triggered,
                    exportAllStudentsAct, [this] {
                        exportManualAllStudents(this, mStudentsTab->getTable());
                    });
            mExportStudentsMenu->addAction(exportAllStudentsAct);
        }
    }

    // Export to d2r
    {
        const auto exportD2RAct = new QAction("D2R", this);
        connect(exportD2RAct, &QAction::triggered, exportD2RAct,
                [this] { exportD2R(this); });
        mExportMenu->addAction(exportD2RAct);
    }

    /**************** Help *************/
    mHelpMenu = menuBar()->addMenu("&Help");

    // Open Handreichung / Guidelines
    {
        mOpenGuidelinesMenu = mHelpMenu->addMenu("Open grading guidelines");

        // German
        {
            const auto mOpenGuidelinesGerman = new QAction("German", this);
            connect(mOpenGuidelinesGerman, &QAction::triggered, [] {
                QDesktopServices::openUrl(
                    QUrl(str::ui::gradingGuidelinesUrlGerman));
            });
            mOpenGuidelinesMenu->addAction(mOpenGuidelinesGerman);
        }

        // English
        {
            const auto mOpenGuidelinesEnglish = new QAction("English", this);
            connect(mOpenGuidelinesEnglish, &QAction::triggered, [] {
                QDesktopServices::openUrl(
                    QUrl(str::ui::gradingGuidelinesUrlEnglish));
            });
            mOpenGuidelinesMenu->addAction(mOpenGuidelinesEnglish);
        }
    }

    // "About": Displays version, license info and dependencies
    {
        const auto aboutAct = new QAction("About", this);
        connect(aboutAct, &QAction::triggered, aboutAct, [this] {
            QMessageBox::about(this, "About", str::info::aboutString);
        });
        mHelpMenu->addAction(aboutAct);
    }
}
