/**
 * \file main_window.hpp
 * \brief Declaration of the \ref MainWindow class.
 */

#pragma once

#include "view_settings.hpp"

#include "file_io/d2r_parser.hpp"
#include "util/testing.hpp"
#include "view/boundary_window/boundary_window.hpp"
#include "view/eliminate_tasks_window/eliminate_tasks_window.hpp"
#include "view/input_window/input_window.hpp"
#include "view/statistic_window/calculation_window.hpp"
#include "view/statistic_window/statistic_window.hpp"

#include <QMainWindow>
#include <QTabWidget>

#include <optional>

class GradeTab;
class QAction;
class QMenu;
class StudentsTab;
class TaskTab;
struct DynexiteExam;
struct StudentData;

/**
 * \brief Main display window.
 *
 * The MainWindow displays the exam results.
 */
class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    /**
     * \brief Constructs a MainWindow object.
     *
     * Does not immediately draw itself, but instead creates and displays
     * the \ref InputWindow to get input files.
     *
     * \param[in] parent Parent widget.
     */
    explicit MainWindow(QWidget *parent = nullptr);

public Q_SLOTS:
    /**
     * \brief Loads the given exam and, if no errors occurred, displays it.
     */
    void resetToInputFiles(const DynexiteExam &exam,
                           const StudentData &students,
                           const BonusPoints &bonus,
                           GradeBoundaryStepSize stepSize,
                           const std::optional<D2rSettings> &settings);

private Q_SLOTS:
    void menuNewFile();
    void menuOpenBoundaries();
    void menuOpenViewSettings();
    void menuOpenStatistics();
    void menuOpenCalculations();
    void menuOpenEliminateTasks();
    void tabChanged(int index);

private:
    /**
     * \brief Close all windows when main window is closed.
     */
    void closeEvent(QCloseEvent *event) override;

    void resizeEvent(QResizeEvent *event) override;

    void updateMenuItems() const;

    void createMenuBar();

    void handleResize();

    /**
     * \brief If there are any unsaved changes, asks the user whether they want
     * to save.
     *
     * If there are no unsaved changes, does nothing.
     *
     * \return The return code of their selection, or QMessageBox::Discard, if
     * there was no selection.
     */
    [[nodiscard]] int askForSave() const;

private:
    /**
     * \brief The window where the user selects input files.
     */
    InputWindow mInputWindow;

    /**
     * \brief The window where the user selects options.
     */
    BoundaryWindow mBoundaryWindow;

    /**
     * \brief Window with visual settings.
     */
    ViewSettingsWindow mViewSettings;

    /**
     * \brief Window with additional statistics.
     */
    StatisticWindow mStatisticsWindow;

    /**
     * \brief Window displaying pass mark calculations.
     */
    CalculationWindow mCalculationWindow;

    /**
     * \brief Window to eliminate individual tasks.
     */
    EliminateTasksWindow mEliminateTasksWindow;

    /**
     * \brief List of all windows managed by this window, e.g. settings window.
     */
    std::array<QWidget *, 6> mSubWindowPtrs{};

    /**
     * \brief Main tab that displays general exam results and lets the user
     * change passing limit and maximal points.
     */
    GradeTab *mGradeTab;
    TaskTab *mTaskTab;
    StudentsTab *mStudentsTab;
    QTabWidget mTabs;

    /**
     * \brief Menu objects
     */
    QMenu *mFileMenu{};
    QMenu *mViewMenu{};
    QMenu *mExportMenu{};
    QMenu *mExportGradingMenu{};
    QMenu *mExportStudentsMenu{};
    QMenu *mHelpMenu{};
    QMenu *mOpenGuidelinesMenu{};
    QAction *mExportUnknownStudentsAct{};

    FRIEND_TEST(GuiDagTest, CheckKnownTestFiles);
    friend class GuiDagTest;
};
