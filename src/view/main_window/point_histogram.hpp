/**
 * \file point_histogram.hpp
 */

#pragma once

#include "util/enums.hpp"
#include "util/storage_helper.hpp"
#include "util/testing.hpp"

#include <QChartView>
#include <QList>

class QBarSet;
class QStackedBarSeries;
class QValueAxis;
class ViewSettingsWindow;
struct FontSize;
struct Points;

namespace impl {
/**
 * \brief Get the bar set index for the given point value.
 */
[[nodiscard]] qsizetype getBarIdx(double bucketSize, Points p,
                                  qsizetype size) noexcept;
} // namespace impl

/**
 * \brief Histogram specialized for the display of exam points.
 */
class PointHistogram : public QChartView {
    Q_OBJECT
public:
    /**
     * \param gradingType For which grading type the points are displayed.
     * \param viewSettings Reference to the view settings window, so that we can
     * connect to its signals.
     * \param parent Parent widget.
     */
    explicit PointHistogram(GradingType gradingType,
                            const ViewSettingsWindow &viewSettings,
                            QWidget *parent = nullptr) noexcept;

    void handleResize(const QRectF &plotArea, FontSize largeFontSize,
                      FontSize smallFontSize) noexcept;

public Q_SLOTS:
    /**
     * \brief Call when the points have changed.
     */
    void pointsChanged() noexcept;
    /**
     * \brief Call when the grades have changed.
     *
     * This only has to update whether students pass or not, so basically the
     * color of the bars.
     */
    void gradesChanged(const EnumStorage<bool> &changedTypes) noexcept;
    /**
     * \brief Call when the user changed the bucket size in the view settings.
     *
     * \warning Don't call when the bucket size was automatically updated (by
     * loading a new exam).
     */
    void pointBucketSizeChanged(double d) noexcept;

private:
    /**
     * \brief For which grading type the points are displayed.
     *
     * If the grading type is "Combined", the displayed points are the sum of
     * the open and closed points.
     */
    GradingType mGradingType;

    /**
     * \brief Stores the number of passed students for each point bucket.
     *
     * \todo Move to model.
     */
    QList<qreal> mPassedStudentsPerPointBucket;
    /**
     * \brief Stores the number of students that didn't pass for each point
     * bucket.
     *
     * \todo Move to model.
     */
    QList<qreal> mFailedStudentsPerPointBucket;

    // Store pointers to chart objects for convenience
    QStackedBarSeries *mSeries;
    QBarSet *mBarSetPassed, *mBarSetFailed;
    QValueAxis *mAxisX, *mAxisY;

    /**
     * \brief Axis label font character width/height, used for calculating
     * spacing between axis labels.
     */
    int mAxisLabelCharWidth, mAxisLabelCharHeight;
    int mMaxNrYLabels = 6;

    /**
     * \brief Correct plot area (the one returned by chart()->plotArea might be
     * wrong if the point histogram is not currently shown)
     */
    QRectF mPlotArea;

    /**
     * \brief Range of points each point bucket covers.
     */
    double mPointBucketSize = 1.;

    /**
     * \brief Resizes the point bucket lists according to the max points and the
     * point bucket size.
     */
    void resizePointBucketLists() noexcept;

    /**
     * \brief Recalculates the point bucket list values based on the currently
     * loaded exam and the point bucket size.
     */
    void recalculatePointBucketLists() noexcept;

    /**
     * \brief Updates the bar sets to the values currently in
     * \ref mPassedStudentsPerPointBucket / \ref mFailedStudentsPerPointBucket
     */
    void updateBarSets() noexcept;

    /**
     * \brief Set x axis range to the correct values.
     *
     * Called when bucket size or points change.
     */
    void rescaleX() const noexcept;

    /**
     * \brief Set y axis maximum to the correct value.
     *
     * Called when bucket size, points or grades change.
     */
    void rescaleY() const noexcept;

    /**
     * \brief Change distance between x axis labels
     */
    void rescaleXLabels() const noexcept;

    /**
     * \brief Change distance between y axis labels
     */
    void rescaleYLabels() const noexcept;

    FRIEND_TEST(GuiDagTest, CheckKnownTestFiles);
    friend class GuiDagTest;
};
