#include "export_rwthonline.hpp"

#include "data/csv_dump.hpp"
#include "data/data_manager.hpp"
#include "file_io/csv_parser.hpp"
#include "file_io/file.hpp"
#include "util/enums.hpp"
#include "util/grade.hpp"
#include "util/points.hpp"
#include "util/strings.hpp"

#include <QFileDialog>
#include <QHash>
#include <QString>
#include <QTableWidget>

#include <algorithm>
#include <cstdio>
#include <ranges>

namespace {
qsizetype getColumn(const CsvLine &headerLine, const QString &headerText) {

    for (qsizetype i = 0; i < headerLine.size(); ++i) {

        if (QString::compare(headerLine[i], headerText, Qt::CaseInsensitive) ==
            0) {
            return i;
        }
    }
    return headerLine.size();
}

[[nodiscard]] QString getRemark(const GradingType type, const qsizetype idx) {
    const auto cur = DataManager::getCurrent();
    if (!cur)
        return "";

    const auto pointsWithoutBonus =
        cur->getPointsWithoutBonus(type)[idx].toString();
    const auto grade = cur->getGradeList(type)[idx];
    const auto nrBonusPoints = cur->mBonus.mTotalBonusPointsPerStudent
                                   .value(cur->mExam.mIdentifier[idx])
                                   .toString();
    const auto maxGradeBoundary = cur->getChangedMaxPointsBoundary(type);
    const auto max =
        maxGradeBoundary.toString(cur->mUserPointConfig.mGradeBoundaryStepSize);

    QString remark;
    if constexpr (defaults::newGeneralExaminationRegulations) {
        if (cur->hasBonus()) {
            // Change position of bonus points in the remark depending on
            // whether or not the student passed. This is to clarify that
            // passing due to bonus points might not possible.
            const auto remarkTemplate =
                grade == Grade::grade5_0
                    ? str::fileExport::rwthOnlineRemarkNewGerNotPassedBonus
                    : str::fileExport::rwthOnlineRemarkNewGerPassedBonus;
            remark = QString(remarkTemplate)
                         .arg(pointsWithoutBonus, toString(grade),
                              nrBonusPoints, max);

        } else {
            remark = QString(str::fileExport::rwthOnlineRemarkNewGerNoBonus)
                         .arg(pointsWithoutBonus, toString(grade), max);
        }
    } else {
        if (cur->hasBonus()) {
            // Change position of bonus points in the remark depending on
            // whether or not the student passed. This is to clarify that
            // passing due to bonus points might not possible.
            const auto remarkTemplate =
                grade == Grade::grade5_0
                    ? str::fileExport::rwthOnlineRemarkNotPassedBonus
                    : str::fileExport::rwthOnlineRemarkPassedBonus;
            remark = QString(remarkTemplate)
                         .arg(toString(type), pointsWithoutBonus,
                              toString(grade), nrBonusPoints, max);

        } else {
            remark = QString(str::fileExport::rwthOnlineRemarkNoBonus)
                         .arg(toString(type), pointsWithoutBonus,
                              toString(grade), max);
        }
    }

    if (type == GradingType::Closed) {
        // Add info on average points of candidates taking the exam for the
        // first time
        const auto avgFirstTryPoints = cur->getAvgFirstTryPoints();
        if (!avgFirstTryPoints.has_value()) {
            remark += str::fileExport::rwthOnlineRemarkAverageFirstNone;
        } else {
            remark += QString(str::fileExport::rwthOnlineRemarkAverageFirstAny)
                          .arg(avgFirstTryPoints->toString());
        }
    }

    return remark;
}
} // namespace

bool exportKnownStudents(QWidget *parent, QTextStream *exportStream) {

    const auto cur = DataManager::getCurrent();
    if (!cur) {
        return false;
    }

    // Ask user for file location if this function doesn't export to a given
    // stream
    QString exportLocationString = "";
    if (exportStream == nullptr) {
        exportLocationString = QFileDialog::getSaveFileName(
            parent, str::fileExport::knownStudentSaveLocationCaption, "",
            str::ui::input::csvFilter);

        // There still might be no file location if the user cancels the dialog.
        // In that case cancel the export
        if (exportLocationString.isEmpty()) {
            return false;
        }
    }

    // Copy the csv data because we write to the grade and remark columns
    auto exportCsvDump = cur->mStudents.mRwthOnline;
    auto &exportCsv = exportCsvDump.data;

    if (exportCsv.empty()) {
        return false;
    }

    // Insert student data into csv
    {
        const auto gradeColumn = getColumn(exportCsv[0], "grade");
        const auto remarkColumn = getColumn(exportCsv[0], "remark");
        if (gradeColumn >= exportCsv[0].size() ||
            remarkColumn >= exportCsv[0].size()) {
            return false;
        }

        // Clear previous cell content
        for (auto &l : exportCsv | std::views::drop(1)) {

            l[gradeColumn] = "";
            l[remarkColumn] = "";
        }

        // Calculate lookup table for indices
        // When we want to match up a student from the RWTHonline file with the
        // same student in the dynexite file, we have to find the index of that
        // student in the unsorted dynexite identifier list. To improve
        // performance, we store the indices in a map with the identifier as
        // key.
        QHash<QString, int> identifierIndices;
        identifierIndices.reserve(cur->mExam.mIdentifier.size());
        for (int i = 0; i < cur->mExam.mIdentifier.size(); ++i) {
            identifierIndices[cur->mExam.mIdentifier[i]] = i;
        }

        // Insert new content where we have data
        for (qsizetype i = 0; auto &l : exportCsv | std::views::drop(1)) {

            using enum GradingType;

            // Search for same student in dynexite data
            const auto it =
                identifierIndices.find(cur->mStudents.mRegistrationNr[i]);
            qsizetype idx;

            // Convert to index, or skip if not found
            if (it == identifierIndices.end()) {
                ++i;
                continue;
            } else {
                idx = it.value();
            }

            // Save result in new csv
            l[gradeColumn] = toString(cur->getGradeList(Combined)[idx], ',');
            // Only print closed/open points if there were any to earn
            if (cur->hasType(Closed)) {
                l[remarkColumn] = getRemark(Closed, idx);
            }
            if (cur->hasType(Combined)) {
                l[remarkColumn] += " - ";
            }
            if (cur->hasType(Open)) {
                l[remarkColumn] += getRemark(Open, idx);
            }

            ++i;
        }

        // Mark students without data as missing
        for (auto &l : exportCsv | std::views::drop(1)) {

            if (l[gradeColumn].isEmpty()) {
                l[gradeColumn] = "X";
            }
        }
    }

    if (exportStream == nullptr) {
        auto [file, stream] = openFileToWrite(exportLocationString);
        writeCsv(*stream, exportCsvDump);
    } else {
        writeCsv(*exportStream, exportCsvDump);
    }

    return true;
}

bool exportUnknownStudents(QWidget *parent, QTextStream *exportStream) {
    const auto cur = DataManager::getCurrent();
    if (!cur)
        return false;

    // Ask user for file location if this function doesn't export to a given
    // stream
    QString exportLocationString = "";
    if (exportStream == nullptr) {
        exportLocationString = QFileDialog::getSaveFileName(
            parent, str::fileExport::unknownStudentSaveLocationCaption, "",
            str::ui::input::csvFilter);

        // There still might be no file location if the user cancels the dialog.
        // In that case cancel the export
        if (exportLocationString.isEmpty()) {
            return false;
        }
    }

    CsvDump exportDump;
    auto &exportCsv = exportDump.data;

    // Header line
    exportCsv = {{"REGISTRATION_NUMBER", "GRADE", "REMARK"}};

    // Calculate lookup table for indices
    QSet<QString> registeredStudents;
    registeredStudents.reserve(cur->mStudents.mRegistrationNr.size());
    for (const auto &regNr : cur->mStudents.mRegistrationNr) {
        registeredStudents.insert(regNr);
    }

    // Insert students not in the input RWTHonline file
    for (qsizetype i = 0; auto &id : cur->mExam.mIdentifier) {

        using enum GradingType;

        // Search for same student in RWTHonline
        if (registeredStudents.contains(id)) {
            ++i;
            continue;
        }

        // Save result in new line
        exportCsv.emplace_back(3);
        exportCsv.back()[0] = id;
        exportCsv.back()[1] = toString(cur->getGradeList(Combined)[i], ',');
        // Only print closed/open points if there were any to earn

        if (cur->hasType(Closed)) {
            exportCsv.back()[2] = getRemark(Closed, i);
        }
        if (cur->hasType(Combined)) {
            exportCsv.back()[2] += " - ";
        }
        if (cur->hasType(Open)) {
            exportCsv.back()[2] += getRemark(Open, i);
        }

        ++i;
    }

    if (exportStream == nullptr) {
        auto [file, stream] = openFileToWrite(exportLocationString);
        writeCsv(*stream, exportDump);
    } else {
        writeCsv(*exportStream, exportDump);
    }

    return true;
}

void exportManualAllStudents(QWidget *parent,
                             const QTableWidget &studentTable) {
    const auto cur = DataManager::getCurrent();
    if (!cur)
        return;

    const auto exportLocationString = QFileDialog::getSaveFileName(
        parent, str::fileExport::allStudentsSaveLocationCaption, "",
        str::ui::input::csvFilter);

    if (!exportLocationString.isEmpty()) {
        // Store data from studentTable in a CsvDump
        CsvDump exportDump;
        auto &exportCsv = exportDump.data;

        const auto columns = studentTable.columnCount();
        const auto rows = studentTable.rowCount();

        // headers
        exportCsv.emplace_back();
        for (int column = 0; column < columns; ++column) {
            exportCsv.back().emplace_back(
                studentTable.horizontalHeaderItem(column)->text());
        }

        // data
        for (int row = 0; row < rows; ++row) {
            exportCsv.emplace_back();
            for (int column = 0; column < columns; ++column) {
                exportCsv.back().emplace_back(
                    studentTable.item(row, column)->text());
            }
        }

        auto [file, stream] = openFileToWrite(exportLocationString);
        writeCsv(*stream, exportDump);
    }
}
