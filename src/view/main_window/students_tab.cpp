#include "students_tab.hpp"

#include "data/data_manager.hpp"
#include "data/graph.hpp"
#include "data/nodes/dynexite_exam.hpp"
#include "util/enums.hpp"
#include "util/grade.hpp"
#include "util/points.hpp"
#include "util/strings.hpp"
#include "view/gui/delegates.hpp"

#include <QBrush>
#include <QLabel>
#include <QList>
#include <QMessageBox>
#include <QScopeGuard>
#include <QString>
#include <QTableWidgetItem>
#include <QVBoxLayout>
#include <QVariant>

#include <array>
#include <cassert>
#include <initializer_list>
#include <ranges>

/*
 * Order of headers:
 *
 * - Student ID
 * - Closed headers (if there are closed points):
 *      - Points without Bonus (if there are bonus points)
 *      - Points with Bonus
 *      - Grade
 * - Open headers (if there are open points):
 *      - Points without Bonus (if there are bonus points)
 *      - Points with Bonus
 *      - Grade
 * - Combined grade (if there are closed and open points)
 */

namespace {
constexpr auto studentTableHeader = "Student";
constexpr auto combinedTableHeader = "Combined Grade";

constexpr EnumStorage<std::array<const char *, 3>> bonusTableHeadersPerType = {
    std::array{"Closed points (excl. Bonus)", "Closed points (incl. Bonus)",
               "Closed Grade"},
    std::array{"Open points (excl. Bonus)", "Open points (incl. Bonus)",
               "Open Grade"}};
constexpr EnumStorage<std::array<const char *, 2>> noBonusTableHeadersPerType =
    {std::array{"Closed points", "Closed Grade"},
     std::array{"Open points", "Open Grade"}};

constexpr EnumStorage<std::array<const char *, 3>>
    newGerBonusTableHeadersPerType = {
        std::array{"Points (excl. Bonus)", "Points (incl. Bonus)", "Grade"},
        std::array{"Points (excl. Bonus)", "Points (incl. Bonus)", "Grade"}};
constexpr EnumStorage<std::array<const char *, 2>>
    newGerNoBonusTableHeadersPerType = {std::array{"Points", "Grade"},
                                        std::array{"Points", "Grade"}};

/**
 * \brief Wait this amount of time after a resize is signaled to change table
 * column width and table row height.
 *
 * If we resize during the actual resizeEvent, the old window size is sometimes
 * used (especially when going from full screen to window-sized). The value of
 * this variable is chosen arbitrarily. In the future we might find a signal
 * that fires after a resize is completed, and can use that to resize the table,
 * but currently I don't know of any such signal.
 */
constexpr auto resizeWaitTime = std::chrono::milliseconds{100};

/**
 * \brief Calculates how many columns both \ref GradingType (closed or open)
 * occupy on their own.
 */
[[nodiscard]] int calcColumnsPerType(const DAG &cur) noexcept {
    return static_cast<int>(
        cur.hasBonus()
            ? std::tuple_size_v<decltype(bonusTableHeadersPerType)::value_type>
            : std::tuple_size_v<
                  decltype(noBonusTableHeadersPerType)::value_type>);
}

/**
 * \brief Calculates how many columns are currently needed in the table.
 */
[[nodiscard]] int calcTotalNrColumns(const DAG &cur) noexcept {
    using enum GradingType;
    const auto columnsPerType = calcColumnsPerType(cur);
    // 1 column for Student ID
    // calcColumnsPerType(...) columns for closed (if exist)
    // calcColumnsPerType(...) columns for open (if exist)
    // 1 column for combined (if open and closed exist)
    return 1 + columnsPerType * static_cast<int>(cur.hasType(Closed)) +
           columnsPerType * static_cast<int>(cur.hasType(Open)) +
           static_cast<int>(cur.hasType(Combined));
}

/**
 * \brief Calculates the indices of the grade columns for all \ref GradingType
 * \param cur Current exam
 *
 * If an exam does not use all grading types, the returned value at the index of
 * an unused grading type is invalid.
 */
[[nodiscard]] auto getGradeHeaderIndices(const DAG &cur) noexcept {
    const auto columnsPerType = calcColumnsPerType(cur);

    // The grade column is the last of the gradingType-specific columns.
    // Therefore the first grade column is the (1+columnsPerType)th column,
    // meaning the column at index columnsPerType.
    return getStorage(columnsPerType,
                      cur.hasType(GradingType::Closed) ? 2 * columnsPerType
                                                       : columnsPerType,
                      2 * columnsPerType + 1);
}

/**
 * \brief The background color of a grade cell changes depending on the grade.
 */
[[nodiscard]] QBrush getGradeBackgroundColor(const Grade g) noexcept {
    switch (g) {
    case Grade::grade1_0:
        return defaults::passedColor1_0;
    case Grade::grade5_0:
        return defaults::notPassedColor;
    default:
        return defaults::passedColor;
    }
}

/**
 * \brief Inserts a new QTableWidgetItem into the table at the specified row and
 * column, using our preferred flags.
 */
void createTableItem(QTableWidget &table, const QVariant &value, const int row,
                     const int column) noexcept {
    const auto idItem = new QTableWidgetItem;
    idItem->setData(Qt::DisplayRole, value);
    idItem->setFlags(idItem->flags() ^ Qt::ItemIsEditable);
    table.setItem(row, column, idItem);
}

/**
 * \brief Create horizontal headers / columns.
 */
void createHeaders(QTableWidget &table, const DAG &cur) noexcept {
    using enum GradingType;
    table.setColumnCount(calcTotalNrColumns(cur));

    int columnIdx = 0;

    // Create the "student id"-header
    table.setHorizontalHeaderItem(columnIdx++,
                                  new QTableWidgetItem(studentTableHeader));

    // Create the point- and grade-headers for both closed and open
    for (const auto g : {Closed, Open}) {
        if (cur.hasType(g)) {

            // Create a header for each string in the range
            const auto createHeadersFromRange =
                [&table, g, &columnIdx](const std::ranges::range auto &range) {
                    for (size_t i = 0; i < std::ranges::size(range[g]); ++i) {
                        table.setHorizontalHeaderItem(
                            columnIdx, new QTableWidgetItem(range[g][i]));

                        // Print points (but not grades) using toString
                        if (i != std::ranges::size(range[g]) - 1) {

                            table.setItemDelegateForColumn(
                                columnIdx, new PointDelegate(&table));
                        }

                        ++columnIdx;
                    }
                };

            if (cur.hasBonus()) {
                if constexpr (defaults::newGeneralExaminationRegulations) {
                    createHeadersFromRange(newGerBonusTableHeadersPerType);
                } else {
                    createHeadersFromRange(bonusTableHeadersPerType);
                }
            } else {
                if constexpr (defaults::newGeneralExaminationRegulations) {
                    createHeadersFromRange(newGerNoBonusTableHeadersPerType);
                } else {
                    createHeadersFromRange(noBonusTableHeadersPerType);
                }
            }
        }
    }

    // Create the "combined grade"-header if necessary
    if (cur.hasType(Combined)) {
        table.setHorizontalHeaderItem(
            columnIdx++, new QTableWidgetItem(combinedTableHeader));
    }
}

/**
 * \brief Create cells in each row, fill in Student ID and points (but not
 * grade).
 */
void createRows(QTableWidget &table, const DAG &cur) {
    using enum GradingType;

    // Sorting has to be disabled while inserting items
    table.setSortingEnabled(false);
    const auto enableSortingAgain =
        qScopeGuard([&table] { table.setSortingEnabled(true); });

    const auto &ids = cur.mExam.mIdentifier;

    table.setRowCount(static_cast<int>(ids.size()));

    // Write the student IDs, points and grades into each row
    for (qsizetype row = 0; row < ids.size(); ++row) {
        int columnIdx = 0;

        // Inserts the value into the current row and increments the column idx
        const auto createTableItemThisRow = [&table, row, &columnIdx](
                                                const QVariant &value) {
            createTableItem(table, value, static_cast<int>(row), columnIdx++);
        };

        // Student ID
        createTableItemThisRow(ids[row]);

        // Reserve space for points and grades.
        // Are filled in later.
        for (const auto g : {Closed, Open}) {
            if (cur.hasType(g)) {
                // Points
                if (cur.hasBonus()) {
                    createTableItemThisRow(0.);
                }
                createTableItemThisRow(0.);

                // Grade
                createTableItemThisRow("");
            }
        }

        // Combined grade
        createTableItemThisRow("");
    }
}
} // namespace

StudentsTab::StudentsTab(QWidget *parent) noexcept
    : QWidget(parent), mResizeTimer(this) {

    const auto layout = new QVBoxLayout(this);

    layout->addWidget(new QLabel(QString(str::info::studentsTableTabLegend)
                                     .arg(defaults::rwthColors::black.name(),
                                          defaults::passedColor1_0.name(),
                                          defaults::passedColor.name(),
                                          defaults::notPassedColor.name())));
    layout->addWidget(&mTable);

    connect(&DataManager::getInstance(), &DataManager::examChanged, this,
            &StudentsTab::examChanged);
    connect(&DataManager::getInstance(), &DataManager::taskEliminationChanged,
            this, &StudentsTab::pointsChanged);
    connect(&DataManager::getInstance(), &DataManager::gradesChanged, this,
            &StudentsTab::gradesChanged);

    mResizeTimer.callOnTimeout(this, &StudentsTab::resizeTable);
    // fire only once when timeouted, not repeatedly
    mResizeTimer.setSingleShot(true);
}

void StudentsTab::handleResize() noexcept {
    // Wait until resize is done and then fix column width and row height
    mResizeTimer.start(resizeWaitTime);
}

const QTableWidget &StudentsTab::getTable() const noexcept { return mTable; }

void StudentsTab::examChanged() noexcept {
    const auto cur = DataManager::getCurrent();

    // Clear content and headers
    mTable.clear();

    if (!cur) {
        return;
    }

    createHeaders(mTable, *cur);
    createRows(mTable, *cur);
}

template <typename F1, typename F2>
void insertData(QTableWidget &table, const DAG &cur, QWidget *parent,
                const EnumStorage<bool> &gradingTypes,
                const EnumStorage<int> &columns, F1 &&getValue, F2 &&getColor) {

    using enum GradingType;

    // Sorting has to be disabled while changing items
    table.setSortingEnabled(false);
    const auto enableSortingAgain =
        qScopeGuard([&table] { table.setSortingEnabled(true); });

    const auto nrRows = table.rowCount();

    // Store error strings
    QList<QString> notFoundIds;

    // Map table row to dynamite student idx
    QHash<QString, qsizetype> studentIdToDynamiteIdx;
    studentIdToDynamiteIdx.reserve(cur.mExam.mIdentifier.size());
    for (qsizetype i = 0; i < cur.mExam.mIdentifier.size(); ++i) {
        studentIdToDynamiteIdx[cur.mExam.mIdentifier[i]] = i;
    }

    for (int i = 0; i < nrRows; ++i) {
        const auto id = table.item(i, 0)->text();

        // Find the student id in the dynamite data
        const auto it = studentIdToDynamiteIdx.find(id);

        // Not found?
        if (it == studentIdToDynamiteIdx.end()) {
            notFoundIds.push_back(id);
            continue;
        }

        const auto idx = it.value();

        // Set new point strings and new background color
        // Use strings instead of doubles for a fixed number of decimal places
        for (const auto type : {Closed, Open, Combined}) {
            if (cur.hasType(type) && gradingTypes[type]) {
                const auto item = table.item(i, columns[type]);
                const auto value = getValue(type, idx);
                item->setData(Qt::DisplayRole, value);
                item->setBackground(getColor(type, idx));
            }
        }
    }

    // Display warnings if some occurred
    if (!notFoundIds.empty()) {
        assert(false);

        QString informativeText = "";
        for (const auto &id : notFoundIds) {
            informativeText += id + "\n";
        }

        QMessageBox msgBox(parent);
        msgBox.setTextFormat(Qt::TextFormat::MarkdownText);
        msgBox.setWindowTitle("Students-Tab warning.");
        msgBox.setText("**Failed to look up data for the following\nstudent "
                       "ids in the students tab:**\n");
        msgBox.setInformativeText(informativeText);
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.exec();
    }
}

void StudentsTab::pointsChanged(
    const EnumStorage<bool> &changedTypes) noexcept {

    const auto cur = DataManager::getCurrent();

    if (!cur) {
        return;
    }

    // There are no combined points
    auto gradingTypes = changedTypes;
    gradingTypes[GradingType::Combined] = false;

    // Index of the points-without-bonus-columns for all grading types
    // Index of combined doesn't matter, because we set gradingTypes[Combined] =
    // false, so it won't be inserted, because we don't want to display combined
    // points.
    const auto openColumnIdx =
        1 + (cur->hasType(GradingType::Closed) ? calcColumnsPerType(*cur) : 0);
    const auto columnsNoBonus = EnumStorage<int>{1, openColumnIdx, -1};

    insertData(
        mTable, *cur, this, gradingTypes, columnsNoBonus,
        [cur](GradingType g, qsizetype idx) {
            // Transferred double values are treated as points by PointDelegate
            return cur->getPointsWithoutBonus(g)[idx].p;
        },
        [](GradingType, qsizetype) { return Qt::white; });

    if (cur->hasBonus()) {
        const auto bonusOpenColumnIdx = openColumnIdx + 1;
        const auto columnsBonus = EnumStorage<int>{2, bonusOpenColumnIdx, -1};

        insertData(
            mTable, *cur, this, gradingTypes, columnsBonus,
            [cur](GradingType g, qsizetype idx) {
                // Transferred double values are treated as points by
                // PointDelegate
                return cur->getPointsWithBonus(g)[idx].p;
            },
            [](GradingType, qsizetype) { return Qt::white; });
    }
}

void StudentsTab::gradesChanged(const EnumStorage<bool> &changedTypes) {

    using enum GradingType;

    const auto cur = DataManager::getCurrent();

    if (!cur) {
        return;
    }

    insertData(
        mTable, *cur, this, changedTypes, getGradeHeaderIndices(*cur),
        [cur](GradingType g, qsizetype idx) {
            // Use strings instead of doubles for a fixed number of decimal
            // places
            return toString(cur->getGradeList(g)[idx]);
        },
        [cur](GradingType g, qsizetype idx) {
            return getGradeBackgroundColor(cur->getGradeList(g)[idx]);
        });
}

void StudentsTab::resizeTable() noexcept {
    mTable.resizeColumnsToContents();
    mTable.resizeRowsToContents();
}
