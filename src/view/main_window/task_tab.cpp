#include "task_tab.hpp"

#include "util/defaults.hpp"
#include "util/font.hpp"
#include "util/strings.hpp"
#include "view/gui/delegates.hpp"
#include "view/gui/help_widget.hpp"
#include "view/gui/multi_plot_widget.hpp"
#include "view/main_window/view_settings.hpp"

#include <QGridLayout>
#include <QHeaderView>
#include <QLabel>
#include <QSpacerItem>

TaskTab::TaskTab(const ViewSettingsWindow &viewSettings, QWidget *parent)
    : QWidget(parent), mGradingTypeSelection(this), mSortBy(this),
      mSortOrder(this), mShowHistogram("Show histogram", this),
      mShowBoxPlot("Show box plot", this),
      mJoinTaskPools("Join pooled items", this), mTable(this), mTaskModel(this),
      mProxyModel(this) {

    mGradingTypeSelection.addItems(
        {"Closed points", "Open points", "Combined points"});
    mGradingTypeSelection.setCurrentIndex(2);

    mSortBy.addItems({"Index", "Task name", "Average", "Spread"});
    mSortOrder.addItems({"Ascending", "Descending"});

    mShowHistogram.setCheckState(Qt::Checked);
    mShowBoxPlot.setCheckState(Qt::Checked);
    mJoinTaskPools.setCheckState(Qt::Unchecked);

    // Make model sortable
    mProxyModel.setSourceModel(&mTaskModel);
    mProxyModel.setSortCaseSensitivity(Qt::CaseInsensitive);
    mTable.setModel(&mProxyModel);
    mTable.horizontalHeader()->setSortIndicator(0, Qt::AscendingOrder);
    mTable.setSortingEnabled(true);

    mTable.setItemDelegateForColumn(TaskColumnIndex::Plot,
                                    new PlotDelegate(viewSettings, this));
    mTable.setItemDelegateForColumn(TaskColumnIndex::Avg,
                                    new PercentageDelegate(this));
    mTable.setItemDelegateForColumn(TaskColumnIndex::Spread,
                                    new FixedNrDecimalsDelegate<3>(this));

    // Use all remaining horizontal space for the plot column, if it is shown
    mTable.horizontalHeader()->setSectionResizeMode(TaskColumnIndex::Plot,
                                                    QHeaderView::Stretch);

    mTable.setWordWrap(true);

    // Columns of the grid above the table
    static constexpr int nrColumns = 7;
    static constexpr int leftSpacerColumn = 0;
    static constexpr int leftColumn = 1;
    static constexpr int middleColumn = 3;
    static constexpr int rightColumn = 4;
    static constexpr int rightSpacerColumn = 6;

    const auto layout = new QGridLayout(this);
    layout->addItem(
        new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding),
        0, leftSpacerColumn, 4, 1);
    {
        const auto line = new QFrame(this);
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        layout->addWidget(line, 0, middleColumn, 4, 1);
    }
    layout->addItem(
        new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding),
        0, rightSpacerColumn, 4, 1);

    if constexpr (defaults::newGeneralExaminationRegulations) {
        mGradingTypeSelection.hide();
    } else {
        layout->addWidget(new QLabel("Show only tasks with", this), 1,
                          leftColumn);
    }
    layout->addWidget(&mGradingTypeSelection, 1, leftColumn + 1);
    layout->addWidget(new QLabel("Sort by", this), 2, leftColumn);
    layout->addWidget(&mSortBy, 2, leftColumn + 1);
    layout->addWidget(new QLabel("Sort order:", this), 3, leftColumn);
    layout->addWidget(&mSortOrder, 3, leftColumn + 1);
    layout->addWidget(&mShowHistogram, 1, rightColumn, 1, 2);
    layout->addWidget(&mShowBoxPlot, 2, rightColumn);
    layout->addWidget(new HelpWidget{this, str::title::boxPlotHelpText,
                                     str::info::boxPlotHelpText},
                      2, rightColumn + 1);
    layout->addWidget(&mJoinTaskPools, 3, rightColumn, 1, 2);
    layout->addWidget(&mTable, 4, 0, 1, nrColumns);

    connect(&mShowHistogram, &QCheckBox::stateChanged, this,
            &TaskTab::toggleShowPlot);
    connect(&mShowBoxPlot, &QCheckBox::stateChanged, this,
            &TaskTab::toggleShowPlot);
    connect(&mJoinTaskPools, &QCheckBox::stateChanged, this,
            &TaskTab::joinTaskPoolsStateChanged);
    connect(&mGradingTypeSelection, &QComboBox::currentIndexChanged, this,
            &TaskTab::gradingTypeSelectionChanged);

    connect(&mShowBoxPlot, &QCheckBox::stateChanged, this,
            &TaskTab::toggleShowPlot);

    connect(&mProxyModel, &QSortFilterProxyModel::modelAboutToBeReset, this,
            &TaskTab::modelAboutToBeReset);
    connect(&mProxyModel, &QSortFilterProxyModel::modelReset, this,
            &TaskTab::modelReset);

    // Change selected value in mSortBy and mSortOrder if the user sorts by
    // clicking the table headers
    connect(mTable.horizontalHeader(), &QHeaderView::sortIndicatorChanged, this,
            &TaskTab::overwriteSortByComboBox);
    connect(&mSortBy, &QComboBox::currentIndexChanged, this,
            [this](const int index) {
                overwriteTableHeaderSort(index, static_cast<Qt::SortOrder>(
                                                    mSortOrder.currentIndex()));
            });
    connect(&mSortOrder, &QComboBox::currentIndexChanged, this,
            [this](int index) {
                overwriteTableHeaderSort(mSortBy.currentIndex(),
                                         static_cast<Qt::SortOrder>(index));
            });

    mTable.hideColumn(TaskColumnIndex::TaskType);
    mTable.hideColumn(TaskColumnIndex::GradingType);

    filterRows();

    // Show/hide plots according to initial values of the check boxes
    toggleShowPlotNoResize();

    resizeCells();
}

void TaskTab::handleResize(const FontSize largeFontSize) const {

    const auto d = dynamic_cast<PlotDelegate *>(
        mTable.itemDelegateForColumn(TaskColumnIndex::Plot));
    assert(d->getEditors().size() == mProxyModel.rowCount());
    for (int i = 0; i < mProxyModel.rowCount(); ++i) {
        // getEditors includes MultiPlotWidgets and QLabels
        const auto mpwOrNull =
            dynamic_cast<MultiPlotWidget *>(d->getEditors()[i]);
        if (mpwOrNull) {
            mpwOrNull->setAxisFontSize(largeFontSize);
        }
    }
}

void TaskTab::showEvent(QShowEvent *) {
    // mTable.resizeColumnsToContents();
    // mTable.resizeRowsToContents();
}

void TaskTab::overwriteSortByComboBox(const int index,
                                      const Qt::SortOrder order) {

    if (index == TaskColumnIndex::Plot) {
        // You can't sort by plot, so just don't change
        return;
    } else {
        mSortOrder.setCurrentIndex(order);
        mSortBy.setCurrentIndex(mapColumnToComboBox(index));
    }
}

void TaskTab::overwriteTableHeaderSort(const int index,
                                       const Qt::SortOrder order) {

    mTable.sortByColumn(mapComboBoxToColumn(index), order);
}

void TaskTab::modelAboutToBeReset() {
    // Close persistent editors
    for (int i = 0; i < mProxyModel.rowCount(); ++i) {
        mTable.closePersistentEditor(
            mProxyModel.index(i, TaskColumnIndex::Plot));
    }

    showAllRows();
}

void TaskTab::modelReset() {
    // Open persistent editors
    [[maybe_unused]] const auto nrRows = mProxyModel.rowCount();
    [[maybe_unused]] const auto nrColumns = mProxyModel.columnCount();

    for (int i = 0; i < mProxyModel.rowCount(); ++i) {
        const auto myIndex = mProxyModel.index(i, TaskColumnIndex::Plot);

        mTable.openPersistentEditor(myIndex);
    }

    filterRows();

    toggleShowPlotNoResize();

    resizeCells();
}

void TaskTab::joinTaskPoolsStateChanged([[maybe_unused]] const int state) {

    filterRows();

    resizeCells();
}

void TaskTab::gradingTypeSelectionChanged([[maybe_unused]] const int index) {
    filterRows();

    resizeCells();
}

bool TaskTab::plotShown() const noexcept {
    return mShowHistogram.checkState() == Qt::Checked ||
           mShowBoxPlot.checkState() == Qt::Checked;
}

int TaskTab::mapComboBoxToColumn(const int index) const noexcept {
    if (index < 0 || index >= 4) {
        assert(false);
        return 0;
    }

    using namespace TaskColumnIndex;

    if (plotShown()) {
        static constexpr std::array columnIndicesWithPlots = {
            TaskIndex, TaskNameAvgSpread, Avg, Spread};
        return columnIndicesWithPlots[static_cast<std::size_t>(index)];
    } else {
        static constexpr std::array columnIndicesWithoutPlots = {
            TaskIndex, TaskName, Avg, Spread};
        return columnIndicesWithoutPlots[static_cast<std::size_t>(index)];
    }
}

int TaskTab::mapColumnToComboBox(const int index) const noexcept {
    if (index < 0 || index >= 6) {
        assert(false);
        return 0;
    }

    if (index == TaskColumnIndex::Plot) {
        return mSortBy.currentIndex();
    }

    static constexpr std::array comboBoxIndices = {0, 1, 1, -1, 2, 3};
    return comboBoxIndices[static_cast<std::size_t>(index)];
}

void TaskTab::toggleShowPlotNoResize() {
    const auto showHistograms = mShowHistogram.checkState() == Qt::Checked;
    const auto showBoxPlots = mShowBoxPlot.checkState() == Qt::Checked;

    if (showHistograms || showBoxPlots) {
        mTable.showColumn(TaskColumnIndex::Plot);

        mTable.hideColumn(TaskColumnIndex::TaskName);
        mTable.showColumn(TaskColumnIndex::TaskNameAvgSpread);

        mTable.hideColumn(TaskColumnIndex::Avg);
        mTable.hideColumn(TaskColumnIndex::Spread);
    } else {
        mTable.hideColumn(TaskColumnIndex::Plot);

        mTable.showColumn(TaskColumnIndex::TaskName);
        mTable.hideColumn(TaskColumnIndex::TaskNameAvgSpread);

        mTable.showColumn(TaskColumnIndex::Avg);
        mTable.showColumn(TaskColumnIndex::Spread);
    }

    const auto d = dynamic_cast<PlotDelegate *>(
        mTable.itemDelegateForColumn(TaskColumnIndex::Plot));
    assert(d->getEditors().size() == mProxyModel.rowCount());
    for (int i = 0; i < mProxyModel.rowCount(); ++i) {
        // getEditors includes MultiPlotWidgets and QLabels
        const auto mpwOrNull =
            dynamic_cast<MultiPlotWidget *>(d->getEditors()[i]);
        if (mpwOrNull) {
            mpwOrNull->showPlot(showHistograms, showBoxPlots);
        }
    }
}

void TaskTab::filterRows() {
    TaskType::TaskType showThisTaskType = TaskType::TaskItem;
    const auto taskTypeCheckBoxState = mJoinTaskPools.checkState();

    switch (taskTypeCheckBoxState) {
    case Qt::Unchecked:
        showThisTaskType = TaskType::TaskItem;
        break;
    case Qt::Checked:
        showThisTaskType = TaskType::TaskPool;
        break;
    case Qt::PartiallyChecked:
        [[fallthrough]];
    default:
        assert(false);
        break;
    }

    auto showThisGradingType = GradingType::Combined;
    const auto gradingTypeIndex = mGradingTypeSelection.currentIndex();
    if (gradingTypeIndex >= 0 && gradingTypeIndex <= 2) {
        showThisGradingType = static_cast<GradingType>(gradingTypeIndex);
    } else {
        assert(false);
    }

    for (int row = 0; row < mProxyModel.rowCount(); ++row) {

        const auto taskTypeVariant =
            mProxyModel.index(row, TaskColumnIndex::TaskType).data();
        const auto gradingTypeVariant =
            mProxyModel.index(row, TaskColumnIndex::GradingType).data();

        bool conversionSuccess;
        const auto taskType = static_cast<TaskType::TaskType>(
            taskTypeVariant.toInt(&conversionSuccess));

        if (!conversionSuccess) {
            assert(false);
            continue;
        }

        const auto gradingType = static_cast<GradingType>(
            gradingTypeVariant.toInt(&conversionSuccess));

        if (!conversionSuccess) {
            assert(false);
            continue;
        }

        const auto gradingTypeShowRow = showThisGradingType == gradingType;
        const bool taskTypeShowRow = taskType == TaskType::SingletonTaskItem ||
                                     taskType == showThisTaskType;

        if (gradingTypeShowRow && taskTypeShowRow) {
            mTable.showRow(row);
        } else {
            mTable.hideRow(row);
        }
    }
}

void TaskTab::showAllRows() {
    for (int row = 0; row < mProxyModel.rowCount(); ++row) {
        mTable.showRow(row);
    }
}

void TaskTab::resizeCells() noexcept {
    // Row size is dependent on which columns are shown
    // Column size does not change, however only visible columns are resized in
    // resizeColumnsToContents in showEvent, so the remaining columns need to be
    // resized once they are shown. For simplicity, we resize them every time
    // new ones are shown.
    mTable.resizeColumnsToContents();
    mTable.resizeRowsToContents();

    // We have to remind Qt here that the plot column gets all remaining
    // horizontal space, else clumsy Qt forgets.
    mTable.horizontalHeader()->setSectionResizeMode(TaskColumnIndex::Plot,
                                                    QHeaderView::Stretch);
}

void TaskTab::toggleShowPlot() {

    toggleShowPlotNoResize();

    resizeCells();
}
