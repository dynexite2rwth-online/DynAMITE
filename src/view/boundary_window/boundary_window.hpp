#pragma once

#include "boundary_tab.hpp"

#include "util/storage_helper.hpp"

#include <QCheckBox>
#include <QComboBox>
#include <QLineEdit>
#include <QTabWidget>
#include <QVBoxLayout>

class BoundaryWindow : public QWidget {
    Q_OBJECT

public:
    explicit BoundaryWindow(QWidget *parent = nullptr);

public Q_SLOTS:
    void requirePassAllChanged(int state);
    void passDueToBonusChanged(int state);
    void stepSizeChanged(int index);
    void roundingChanged(int index);

    void pointsChanged() noexcept;
    void gradesChanged();
    void autoCheckBoxTouched();

private:
    QVBoxLayout mLayout;
    QTabWidget mTabs;

    QCheckBox mRequirePassAllCheckbox;
    QCheckBox mPassDueToBonusCheckbox;
    QComboBox mStepSize{};
    QComboBox mRounding{};
    // todo: clear up ownership
    EnumStorage<BoundaryTab, GradingType, 2> mBoundaries;

    /// Flag that indicates whether the user toggled the auto check box of one
    /// of the boundary tabs at any time since loading the last exam.
    bool mAutoCheckBoxTouched = false;
};
