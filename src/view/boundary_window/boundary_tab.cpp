#include "boundary_tab.hpp"

#include "data/data_manager.hpp"
#include "util/strings.hpp"

#include <QDoubleSpinBox>
#include <QFrame>
#include <QSignalBlocker>
#include <QSpacerItem>
#include <QString>
#include <QStyle>

#include <cassert>

inline constexpr int rowHeaderColumn = 0;
inline constexpr int separatorColumn = 1;
inline constexpr int currentPointsColumn = 2;
inline constexpr int maxPointsColumn = 3;

BoundaryTab::BoundaryTab(const GradingType grading, QWidget *parent)
    : QWidget(parent), mGrading(grading), mLayout(this),
      mAutoCheckBox(str::ui::boundary::autoCheckbox) {

    if (grading == GradingType::Closed) {
        mQuestionIcon = new HelpWidget(parent, "Grade boundary info",
                                       QString(str::control::richTextStart) +
                                           str::ui::boundary::closedInfoLabel +
                                           str::control::richTextEnd);
    }

    createGradeBoundaryTable(0);

    // receive update when points/grades change
    connect(&DataManager::getInstance(), &DataManager::gradesChanged, this,
            &BoundaryTab::updateBoundaryTableValues);
}

void BoundaryTab::createGradeBoundaryTable(int startingRow) {
    // We could use a QTableWidget, but that looks (even) uglier.

    createAutoCheckbox(startingRow);
    createHeader(startingRow);
    createSeparator(startingRow);
    createTableContent(startingRow);
}

void BoundaryTab::createAutoCheckbox(int &startingRow) {

    // "auto" checkbox (automatically calculates the grade boundaries)
    mAutoCheckBox.setChecked(true);
    if (mQuestionIcon) {
        mLayout.addWidget(mQuestionIcon, startingRow, currentPointsColumn, 1, 1,
                          Qt::AlignCenter);
    }
    mLayout.addWidget(&mAutoCheckBox, startingRow, 0, 1, 2, Qt::AlignCenter);

    // Disable spin box when auto is set
    connect(&mAutoCheckBox, &QCheckBox::stateChanged, this,
            &BoundaryTab::checkboxChanged);

    // add just a little bit of breathing room here
    mLayout.addItem(new QSpacerItem(0, 10), startingRow + 1, 0, 1, 1);

    startingRow += 2;
}

void BoundaryTab::createHeader(int &startingRow) {
    const auto minLabel = new QLabel(str::ui::boundary::min);
    const auto maxLabel = new QLabel(str::ui::boundary::max);

    minLabel->setTextFormat(Qt::RichText);
    maxLabel->setTextFormat(Qt::RichText);

    minLabel->setAlignment(Qt::AlignCenter);
    maxLabel->setAlignment(Qt::AlignCenter);

    mLayout.addWidget(minLabel, startingRow, currentPointsColumn,
                      Qt::AlignCenter);
    mLayout.addWidget(maxLabel, startingRow, maxPointsColumn, Qt::AlignCenter);

    ++startingRow;
}
void BoundaryTab::createSeparator(const int &startingRow) {
    // separator line
    auto *line = new QFrame();
    line->setGeometry(QRect(0, 0, 0, 0));
    line->setFrameShape(QFrame::VLine);
    mLayout.addWidget(line, startingRow, separatorColumn,
                      mNrVisibleGradeBoundaries, 1, Qt::AlignHCenter);
}

void BoundaryTab::createTableContent(int &startingRow) {
    // spin boxes and labels of the actual grade boundaries
    using enum Grade;
    for (auto gradeIdx = 0;
         gradeIdx < static_cast<int>(mNrVisibleGradeBoundaries); ++gradeIdx) {

        const auto grade = static_cast<Grade>(gradeIdx);

        // First column: row label (grade string)
        {
            const auto gradeLabel = new QLabel(
                QString(str::ui::boundary::grade).arg(toString(grade)), this);
            gradeLabel->setTextFormat(Qt::RichText);
            mLayout.addWidget(gradeLabel, startingRow + gradeIdx,
                              rowHeaderColumn);
        }

        // Second column: minimal possible grade boundary
#if 0
        {
            mMinBoundaries[grade] = new QLabel("");
            mLayout.addWidget(mMinBoundaries[grade], startingRow + gradeIdx,
                              minPointsColumn);
        }
#endif

        // Third column: selected grade boundary
        createBoundarySelection(startingRow + gradeIdx, grade);

        // Forth column: maximal possible grade boundary
        {
            mMaxBoundaries[grade] = new QLabel("");
            mMaxBoundaries[grade]->setAlignment(Qt::AlignCenter);
            mLayout.addWidget(mMaxBoundaries[grade], startingRow + gradeIdx,
                              maxPointsColumn, Qt::AlignCenter);
        }
    }

    // adjust minimal size, so that labels and checkboxes have the same size
    assert(gradeBoundaryIsUserChangeable(mGrading, grade1_0));
    const auto spinBoxSize = mBoundaries[grade1_0]->sizeHint();
    for (auto gradeIdx = 0;
         gradeIdx < static_cast<int>(mNrVisibleGradeBoundaries); ++gradeIdx) {

        mBoundaries[static_cast<Grade>(gradeIdx)]->setMinimumSize(spinBoxSize);
    }

    startingRow += static_cast<int>(mNrVisibleGradeBoundaries);
}
void BoundaryTab::createBoundarySelection(const int row, Grade grade) {
    // Add a spin box for flexible grade boundaries and a label for
    // fixed ones
    auto &currentWidget = mBoundaries[grade];

    if (gradeBoundaryIsUserChangeable(mGrading, grade)) {
        currentWidget = new QDoubleSpinBox(this);

        auto *currentSpinBox = static_cast<QDoubleSpinBox *>(currentWidget);

        // Disable the spin box if auto is checked
        // todo: check data base instead of checkbox? (is probably not
        // initialized yet)
        currentWidget->setEnabled(!mAutoCheckBox.isChecked());

        // When the user changes the spin box, send updates to the data
        // manager
        connect(currentSpinBox, &QDoubleSpinBox::valueChanged, this,
                [grade, grading = mGrading](const double v) {
                    DataManager::boundaryChange(grading, grade, Points{v});
                });

        currentSpinBox->setAlignment(Qt::AlignCenter);

        // these values are updates once the database updates
        currentSpinBox->setSingleStep(0.1);
        currentSpinBox->setDecimals(1);

    } else {
        currentWidget = new QLabel("", this);
        static_cast<QLabel *>(currentWidget)->setAlignment(Qt::AlignCenter);
    }

    // Same size policy for labels and spin-boxes (don't expand
    // vertically)
    currentWidget->setSizePolicy({QSizePolicy::Preferred, QSizePolicy::Fixed});

    // Either way, add the new widget
    mLayout.addWidget(currentWidget, row, currentPointsColumn, Qt::AlignCenter);
}

void BoundaryTab::updateSpinBoxSettings() const {

    const auto cur = DataManager::getCurrent();

    if (!cur)
        return;

    const auto minPointDif = cur->mUserPointConfig.mGradeBoundaryStepSize;

    // Update step size and nr of decimal places shown of the spin boxes
    for (auto gradeIdx = 0;
         gradeIdx < static_cast<int>(mNrVisibleGradeBoundaries); ++gradeIdx) {

        const auto grade = static_cast<Grade>(gradeIdx);

        if (gradeBoundaryIsUserChangeable(mGrading, grade)) {

            const auto &b = static_cast<QDoubleSpinBox *>(mBoundaries[grade]);

            b->setSingleStep(as<double>(minPointDif));
            b->setDecimals(nrDecimalPlaces(minPointDif));
        }
    }
}

void BoundaryTab::updateBoundaryTableValues(
    const EnumStorage<bool> &changedBoundaries) {

    const auto cur = DataManager::getCurrent();

    if (!cur)
        return;

    if (!changedBoundaries[mGrading])
        return;

    // update auto checkbox
    {
        QSignalBlocker blocker(mAutoCheckBox);

        const auto autoChangeBoundaries =
            cur->getUserGradeBoundarySetAuto(mGrading);

        mAutoCheckBox.setCheckState(autoChangeBoundaries
                                        ? Qt::CheckState::Checked
                                        : Qt::CheckState::Unchecked);

        for (std::size_t gradeIdx = 0; gradeIdx < mNrVisibleGradeBoundaries;
             ++gradeIdx) {

            const auto grade = static_cast<Grade>(gradeIdx);

            if (gradeBoundaryIsUserChangeable(mGrading, grade)) {
                // Enable the spin boxes iff the auto-checkbox is unchecked
                mBoundaries[grade]->setEnabled(!autoChangeBoundaries);
            }
        }
    }

    updateSpinBoxSettings();

    const auto minPointDif = cur->mUserPointConfig.mGradeBoundaryStepSize;

    for (std::size_t gradeIdx = 0; gradeIdx < mNrVisibleGradeBoundaries;
         ++gradeIdx) {

        const auto grade = static_cast<Grade>(gradeIdx);

        // The current value of the spin-box,
        // which is the grade boundary for the current grade
        const auto &val = cur->getUserGradeBoundaries(mGrading)[grade];

        // String for the max number of points for the current grade,
        // equal to the next grade boundary minus one step, or the max amount of
        // points if we look at grade 1.0.
        const auto &maxPoints = [cur, grading = mGrading, gradeIdx,
                                 minPointDif] {
            GradeBoundary res;
            // Special case for grade 1.0: use max overall points as max points
            // of current grade boundary
            if (gradeIdx == 0) {
                res = cur->getChangedMaxPointsBoundary(grading);
            } else {
                // max is one step below the next boundary
                // If that is below the current boundary, write "-" instead
                res = cur->getUserGradeBoundaries(
                    grading)[static_cast<Grade>(gradeIdx - 1)];
                if (res == cur->getUserGradeBoundaries(
                               grading)[static_cast<Grade>(gradeIdx)]) {
                    return QString(str::ui::boundary::maxSmaller);
                } else {
                    res.mMultiple -= 1;
                }
            }
            return res.toString(minPointDif);
        }();
        // Max value the spin-box can have.
        // Other than maxPoints, this goes exactly to the next boundary, or the
        // max amount of points if we look at grade 1.0.
        const auto &rangeMax = [cur, grading = mGrading, gradeIdx] {
            // Special case for grade 1.0
            if (gradeIdx == 0) {
                return cur->getChangedMaxPointsBoundary(grading);
            } else {
                return cur->getUserGradeBoundaries(
                    grading)[static_cast<Grade>(gradeIdx - 1)];
            }
        }();

        const auto &min = [cur, grading = mGrading, gradeIdx] {
            if (gradeIdx == static_cast<std::size_t>(Grade::grade5_0)) {
                return GradeBoundary{0};
            } else {
                return cur->getUserGradeBoundaries(
                    grading)[static_cast<Grade>(gradeIdx + 1)];
            }
        }();

        if (gradeBoundaryIsUserChangeable(mGrading, grade)) {

            // todo: move logic to data/model

            // update min
            // mMinBoundaries[grade]->setText(min.toString(minPointDif)));

            // update current
            // Is changeable -> is spin box
            const auto &b = static_cast<QDoubleSpinBox *>(mBoundaries[grade]);
            // This is only a visual update, so don't call the data change
            // callback again
            {

                QSignalBlocker blocker(b);
                b->setRange(toPoints(min, minPointDif).p,
                            toPoints(rangeMax, minPointDif).p);
                b->setValue(toPoints(val, minPointDif).p);
            }

        } else {

            // Is not changeable -> is label
            dynamic_cast<QLabel *>(mBoundaries[grade])
                ->setText(val.toString(minPointDif));
        }

        // update max
        mMaxBoundaries[grade]->setText(maxPoints);
    }
}

void BoundaryTab::checkboxChanged(const int state) const {
    using enum Grade;
    if (state == Qt::Unchecked || state == Qt::Checked) {

        // update values in the spin boxes / labels
        DataManager::autoBoundaryChange(mGrading, state == Qt::Checked);

        for (std::size_t gradeIdx = 0; gradeIdx < mNrVisibleGradeBoundaries;
             ++gradeIdx) {

            const auto grade = static_cast<Grade>(gradeIdx);

            if (gradeBoundaryIsUserChangeable(mGrading, grade)) {
                // Enable the spin boxes iff the auto-checkbox is unchecked
                mBoundaries[grade]->setEnabled(state == Qt::Unchecked);
            }
        }
    } else {
        assert(false);
    }
}
