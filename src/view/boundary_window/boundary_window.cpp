#include "boundary_window.hpp"

#include "data/data_manager.hpp"
#include "util/defaults.hpp"
#include "util/enums.hpp"
#include "util/grade_boundary_step.hpp"
#include "util/strings.hpp"

#include <QMessageBox>
#include <QSignalBlocker>

#include <cassert>

BoundaryWindow::BoundaryWindow(QWidget *parent)
    : QWidget(parent), mLayout(this),
      mRequirePassAllCheckbox(str::ui::boundary::requirePassAllCheckbox),
      mPassDueToBonusCheckbox(str::ui::boundary::passDueToBonusCheckbox),
      mBoundaries{BoundaryTab{GradingType::Closed},
                  BoundaryTab{GradingType::Open}} {

    mStepSize.addItems({str::ui::boundary::round1, str::ui::boundary::round0_5,
                        str::ui::boundary::round0_25});
    mRounding.addItems({str::ui::boundary::roundDown,
                        str::ui::boundary::roundUp,
                        str::ui::boundary::roundNearest});

    if constexpr (defaults::showRequireBothPartsSetting) {
        mLayout.addWidget(&mRequirePassAllCheckbox);
    }
    mLayout.addWidget(&mPassDueToBonusCheckbox);
    mLayout.addWidget(&mStepSize);
    if constexpr (defaults::showRoundingSetting) {
        mLayout.addWidget(&mRounding);
    }
    mLayout.addWidget(&mTabs);

    setWindowTitle(str::title::boundaryWindow);

    mTabs.addTab(&mBoundaries[GradingType::Open],
                 defaults::newGeneralExaminationRegulations
                     ? str::ui::boundary::allPointsTab
                     : str::ui::boundary::openTab);
    mTabs.addTab(&mBoundaries[GradingType::Closed],
                 str::ui::boundary::closedTab);

    // set checkbox to correct initial value when points change
    connect(&DataManager::getInstance(), &DataManager::taskEliminationChanged,
            this, &BoundaryWindow::pointsChanged);
    connect(&DataManager::getInstance(), &DataManager::gradesChanged, this,
            &BoundaryWindow::gradesChanged);

    // receive notification from checkboxes
    connect(&mRequirePassAllCheckbox, &QCheckBox::stateChanged, this,
            &BoundaryWindow::requirePassAllChanged);
    connect(&mPassDueToBonusCheckbox, &QCheckBox::stateChanged, this,
            &BoundaryWindow::passDueToBonusChanged);

    // receive notification from combo boxes
    connect(&mStepSize, &QComboBox::activated, this,
            &BoundaryWindow::stepSizeChanged);
    connect(&mRounding, &QComboBox::activated, this,
            &BoundaryWindow::roundingChanged);

    // Display auto checkbox info popup the first time one is changed
    connect(&mBoundaries[GradingType::Closed].mAutoCheckBox,
            &QCheckBox::stateChanged, this,
            &BoundaryWindow::autoCheckBoxTouched);
    connect(&mBoundaries[GradingType::Open].mAutoCheckBox,
            &QCheckBox::stateChanged, this,
            &BoundaryWindow::autoCheckBoxTouched);
}

void BoundaryWindow::pointsChanged() noexcept {

    const auto cur = DataManager::getCurrent();

    if (!cur)
        return;

    {
        mTabs.setTabVisible(0, cur->hasType(GradingType::Open));
        mTabs.setTabVisible(1, cur->hasType(GradingType::Closed));

        // The mRequirePassAllCheckbox is only relevant if there are both open
        // and closed points
        mRequirePassAllCheckbox.setEnabled(cur->hasType(GradingType::Combined));

        // The mPassDueToBonusCheckbox is only relevant if there are bonus
        // points
        mPassDueToBonusCheckbox.setEnabled(cur->hasBonus());
    }
}

void BoundaryWindow::gradesChanged() {

    const auto cur = DataManager::getCurrent();

    if (!cur)
        return;

    {
        QSignalBlocker blocker(mRequirePassAllCheckbox);

        const auto requirePassAll = cur->mUserGradeConfig.mRequirePassAllParts;

        mRequirePassAllCheckbox.setCheckState(requirePassAll
                                                  ? Qt::CheckState::Checked
                                                  : Qt::CheckState::Unchecked);
    }

    {
        QSignalBlocker blocker(mPassDueToBonusCheckbox);

        const auto passDueToBonus = cur->mUserGradeConfig.mPassDueToBonus;

        mPassDueToBonusCheckbox.setCheckState(passDueToBonus
                                                  ? Qt::CheckState::Checked
                                                  : Qt::CheckState::Unchecked);
    }

    {
        const auto stepSize = cur->mUserPointConfig.mGradeBoundaryStepSize;
        const auto rounding = cur->mUserPointConfig.mGradeBoundaryRounding;

        QSignalBlocker blocker1(mStepSize);
        QSignalBlocker blocker2(mRounding);

        switch (stepSize) {
        default:
            assert(false);
            [[fallthrough]];
        case GradeBoundaryStepSize::Min0_25:
            mStepSize.setCurrentIndex(2);
            break;
        case GradeBoundaryStepSize::Min0_50:
            mStepSize.setCurrentIndex(1);
            break;
        case GradeBoundaryStepSize::Min1_00:
            mStepSize.setCurrentIndex(0);
            break;
        }

        switch (rounding) {
        default:
            assert(false);
            [[fallthrough]];
        case GradeBoundaryRounding::Down:
            mRounding.setCurrentIndex(0);
            break;
        case GradeBoundaryRounding::Up:
            mRounding.setCurrentIndex(1);
            break;
        case GradeBoundaryRounding::Nearest:
            mRounding.setCurrentIndex(2);
            break;
        }
    }
}

void BoundaryWindow::autoCheckBoxTouched() {
    if (!mAutoCheckBoxTouched) {

        // Show pop-up information with warning regarding changing the
        // passing boundaries after un-setting the auto-check-box
        QMessageBox::information(this, str::title::disabledAutoGradeBoundaries,
                                 str::info::disabledAutoGradeBoundaries);

        mAutoCheckBoxTouched = true;
    }
}

void BoundaryWindow::stepSizeChanged(const int index) {
    const auto cur = DataManager::getCurrent();

    if (!cur)
        return;

    auto currentConfig = cur->mUserPointConfig;
    switch (index) {
    default:
        assert(false);
        [[fallthrough]];
    case 0:
        currentConfig.mGradeBoundaryStepSize = GradeBoundaryStepSize::Min1_00;
        break;
    case 1:
        currentConfig.mGradeBoundaryStepSize = GradeBoundaryStepSize::Min0_50;
        break;
    case 2:
        currentConfig.mGradeBoundaryStepSize = GradeBoundaryStepSize::Min0_25;
        break;
    }

    DataManager::pointConfigChange(currentConfig);
}

void BoundaryWindow::roundingChanged(const int index) {
    const auto cur = DataManager::getCurrent();

    if (!cur)
        return;

    auto currentConfig = cur->mUserPointConfig;
    switch (index) {
    default:
        assert(false);
        [[fallthrough]];
    case 0:
        currentConfig.mGradeBoundaryRounding = GradeBoundaryRounding::Down;
        break;
    case 1:
        currentConfig.mGradeBoundaryRounding = GradeBoundaryRounding::Up;
        break;
    case 2:
        currentConfig.mGradeBoundaryRounding = GradeBoundaryRounding::Nearest;
        break;
    }

    DataManager::pointConfigChange(currentConfig);
}

void BoundaryWindow::requirePassAllChanged(const int state) {
    assert(state == Qt::Unchecked || state == Qt::Checked);

    const auto cur = DataManager::getCurrent();

    if (!cur)
        return;

    auto currentGradeConfig = cur->mUserGradeConfig;
    currentGradeConfig.mRequirePassAllParts = state == Qt::Checked;
    DataManager::gradeConfigChange(currentGradeConfig);
}

void BoundaryWindow::passDueToBonusChanged(const int state) {
    assert(state == Qt::Unchecked || state == Qt::Checked);

    const auto cur = DataManager::getCurrent();

    if (!cur)
        return;

    auto currentGradeConfig = cur->mUserGradeConfig;
    currentGradeConfig.mPassDueToBonus = state == Qt::Checked;
    DataManager::gradeConfigChange(currentGradeConfig);
}
