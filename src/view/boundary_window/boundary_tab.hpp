#pragma once

#include "util/enums.hpp"
#include "util/grade.hpp"
#include "util/storage_helper.hpp"
#include "view/gui/help_widget.hpp"

#include <QCheckBox>
#include <QGridLayout>
#include <QLabel>

class BoundaryTab : public QWidget {
    Q_OBJECT

public:
    explicit BoundaryTab(GradingType grading, QWidget *parent = nullptr);

public Q_SLOTS:
    void updateBoundaryTableValues(const EnumStorage<bool> &changedBoundaries);
    void checkboxChanged(int state) const;

private:
    void createGradeBoundaryTable(int startingRow);
    void createAutoCheckbox(int &startingRow);
    void createHeader(int &startingRow);
    void createSeparator(const int &startingRow);
    void createTableContent(int &startingRow);

    void createBoundarySelection(int row, Grade grade);

    void updateSpinBoxSettings() const;

    const GradingType mGrading;

    QGridLayout mLayout;

    // Checkbox whether to automatically calculate the grade boundaries
    QCheckBox mAutoCheckBox;

    HelpWidget *mQuestionIcon = nullptr;

    static constexpr auto mNrVisibleGradeBoundaries =
        static_cast<std::size_t>(Grade::NR_VALS);

    // Widgets (Labels or Spin-boxes) for the grades 1.0-5.0
    EnumStorage<QWidget *, Grade, mNrVisibleGradeBoundaries> mBoundaries{};

    // Labels that indicate max possible points to still get the corresponding
    // grade
    EnumStorage<QLabel *, Grade, mNrVisibleGradeBoundaries> mMaxBoundaries{};

    friend class BoundaryWindow;
};
