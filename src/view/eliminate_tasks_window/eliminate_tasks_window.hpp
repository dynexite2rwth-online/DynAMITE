#pragma once

#include <QComboBox>
#include <QGridLayout>
#include <QLabel>
#include <QScrollArea>
#include <QTimer>
#include <QWidget>

class HelpWidget;

class EliminateTasksWindow : public QScrollArea {
    Q_OBJECT

public:
    explicit EliminateTasksWindow(QWidget *parent = nullptr) noexcept;

    QSize sizeHint() const override;

    void handleResize();

public Q_SLOTS:
    void pointsChanged() noexcept;

    void eliminationChanged(int taskPoolIdx, int comboBoxIndex);

private Q_SLOTS:
    void resizeLabels() noexcept;

private:
    // Disable elimination of all tasks which are not yet eliminated and would
    // cause the percentage of bonus points to go above 20% of the total points,
    // if eliminated.
    void disableEliminationsBecauseBonus();

    QGridLayout *mLayout = nullptr;
    QLabel *mEliminationInfoLabel = nullptr;
    QList<QComboBox *> mTaskSelections;
    QList<HelpWidget *> mHelpWidgets;

    QTimer mResizeTimer;
};
