#include "eliminate_tasks_window.hpp"

#include "data/data_manager.hpp"
#include "util/numeric.hpp"
#include "util/strings.hpp"
#include "view/gui/help_widget.hpp"

#include <QGuiApplication>
#include <QLabel>
#include <QMessageBox>
#include <QScreen>
#include <QScrollBar>
#include <QSizePolicy>

EliminateTasksWindow::EliminateTasksWindow(QWidget *parent) noexcept
    : QScrollArea(parent), mResizeTimer(this) {

    setWidgetResizable(false);

    const auto contents = new QWidget(this);
    mLayout = new QGridLayout(contents);
    setWidget(contents);

    connect(&DataManager::getInstance(), &DataManager::examChanged, this,
            &EliminateTasksWindow::pointsChanged);

    mResizeTimer.callOnTimeout(this, &EliminateTasksWindow::resizeLabels);
    // fire only once when timeouted, not repeatedly
    mResizeTimer.setSingleShot(true);
}

QSize EliminateTasksWindow::sizeHint() const {
    QSize sz = QScrollArea::viewportSizeHint();
    const int f = frameWidth() * 2;
    sz += QSize(f, f);
    if (true /*verticalScrollBarPolicy() == Qt::ScrollBarAlwaysOn*/)
        sz.setWidth(sz.width() + verticalScrollBar()->sizeHint().width());
    if (horizontalScrollBarPolicy() == Qt::ScrollBarAlwaysOn)
        sz.setHeight(sz.height() + horizontalScrollBar()->sizeHint().height());
    return sz;
}

constexpr auto resizeWaitTime = std::chrono::milliseconds{200};
void EliminateTasksWindow::handleResize() {

    // Wait until resize is done and then fix column width and row height
    mResizeTimer.start(resizeWaitTime);
}

void EliminateTasksWindow::resizeLabels() noexcept {
    const auto screenSize = QGuiApplication::primaryScreen()->size();
    const auto layoutSize = mLayout->sizeHint();

    // Make height/width slightly larger than layoutSize, or else scroll bars
    // show up anyway and block even more space
    resize(QSize{std::min(screenSize.width() / 2,
                          static_cast<int>(layoutSize.width() * 1.05)),
                 std::min(screenSize.height() / 2,
                          static_cast<int>(layoutSize.width() * 1.05))});
}

void EliminateTasksWindow::pointsChanged() noexcept {

    const auto cur = DataManager::getCurrent();

    if (!cur)
        return;

    // there doesn't seem to be an easier way to clear a layout...
    {
        delete mLayout;
        // The following deletes all child widgets, i.e. the charts and
        // chartViews
        delete takeWidget();
        mTaskSelections.clear();
        mHelpWidgets.clear();
    }
    const auto contents = new QWidget(this);
    mLayout = new QGridLayout(contents);
    mLayout->setSizeConstraint(QLayout::SetMinimumSize);

    int currentRow = 0;

    // Create task elimination info label
    mEliminationInfoLabel = new QLabel();
    mEliminationInfoLabel->setText(QString(str::info::eliminationInfoLabel)
                                       .arg(defaults::notPassedColor.name()));

    mLayout->addWidget(mEliminationInfoLabel, currentRow, currentRow, 1, 3);
    ++currentRow;

    // Make sure the QComboBox-column is not wider than it needs to be
    mLayout->setColumnStretch(0, 0);
    mLayout->setColumnStretch(1, 1);

    // Create combo box and label for each task pool
    for (int taskPoolIdx = 0; const auto &pool : cur->mExam.mTaskPools) {

        // HelpWidget that informs the user if task elimination is disabled for
        // this task because there are too few bonus points (bonus points may
        // only be a certain percentage of the total points)
        mHelpWidgets.push_back(new HelpWidget(
            this, str::title::eliminateTaskUnavailable,
            QString(str::info::eliminateTaskUnavailable)
                .arg(defaults::maxRatioBonusPoints * 100, 0, 'f', 0)));
        mHelpWidgets.back()->setVisible(false);
        {
            auto sp = mHelpWidgets.back()->sizePolicy();
            sp.setRetainSizeWhenHidden(cur->hasBonus());
            mHelpWidgets.back()->setSizePolicy(sp);
        }
        mLayout->addWidget(mHelpWidgets.back(), currentRow, 0);

        // Create label from all task names from the pool
        const auto taskNames = pool.constructTaskPoolName();

        const auto selection = new QComboBox{this};
        selection->addItems(str::ui::fileElim::comboOptions);
        selection->setCurrentIndex(static_cast<int>(
            cur->mExam.mTaskPools[taskPoolIdx].mEliminationStatus));

        mLayout->addWidget(selection, currentRow, 1);
        auto taskNameLabel = new QLabel(taskNames);
        mLayout->addWidget(taskNameLabel, currentRow, 2);
        mTaskSelections.push_back(selection);

        // receive notification from combo box
        connect(selection, &QComboBox::activated, this,
                [this, taskPoolIdx](const int index) {
                    eliminationChanged(taskPoolIdx, index);
                });

        ++taskPoolIdx;
        ++currentRow;
    }

    setWidget(contents);
}

/**
 * \brief Find a selected index in the list of combo boxes that is not zero.
 * Skip combo box at list index skipMe
 */
[[nodiscard]] int
findSelectedIndexOfOthers(const QList<QComboBox *> &comboBoxes,
                          const int skipMe) noexcept {

    for (int i = 0; i < comboBoxes.size(); ++i) {
        if (i == skipMe) {
            continue;
        }

        const auto selectedIndex = comboBoxes[i]->currentIndex();
        if (selectedIndex != 0) {
            return selectedIndex;
        }
    }
    return 0;
}

void EliminateTasksWindow::eliminationChanged(const int taskPoolIdx,
                                              int comboBoxIndex) {

    /*
     * If there are several erroneous tasks in one exam, the remedy must be
     * consistent throughout, i.e., either all the affected tasks shall be
     * withdrawn, or the full score shall be credited for all the affected tasks
     */

    const auto commonComboIndex =
        findSelectedIndexOfOthers(mTaskSelections, taskPoolIdx);
    if (commonComboIndex != 0 && comboBoxIndex != 0 &&
        comboBoxIndex != commonComboIndex) {

        const auto cur = DataManager::getCurrent();
        assert(cur);

        const auto taskName =
            cur->mExam.mTaskPools[taskPoolIdx].constructTaskPoolName("<br>");

        const auto &selectedOption =
            str::ui::fileElim::comboOptions[comboBoxIndex];
        const auto &commonOption =
            str::ui::fileElim::comboOptions[commonComboIndex];

        // Display warning and change the comboBox to the common selected option
        QMessageBox::warning(this, str::title::invalidElim,
                             QString(str::info::invalidElimText)
                                 .arg(taskName, selectedOption, commonOption));

        // We don't need a QSignalBlocker here, because an `activated` signal is
        // only sent when the user chooses an item in the combobox
        mTaskSelections[taskPoolIdx]->setCurrentIndex(commonComboIndex);
        comboBoxIndex = commonComboIndex;
    }

    DataManager::taskEliminationChange(
        taskPoolIdx, static_cast<TaskEliminationType>(comboBoxIndex));

    disableEliminationsBecauseBonus();
}

void EliminateTasksWindow::disableEliminationsBecauseBonus() {

    const auto cur = DataManager::getCurrent();

    if (!cur) {
        return;
    }

    if (!cur->hasBonus()) {
        return;
    }

    const auto examMaxPoints = cur->getMaxPoints(GradingType::Combined);

    const auto maxBonusPoints = cur->mBonus.mMaxBonusPoints;

    for (int i = 0; const auto &comboBox : mTaskSelections) {

        auto setDisabled = false;

        if (comboBox->currentIndex() == 0) {

            // Combined points of the task this combo box refers to
            const auto pointsOfTask =
                cur->mExam.mTaskPools[i].mMaxPoints[GradingType::Combined];

            if (definitelyGreaterThan(maxBonusPoints,
                                      (examMaxPoints - pointsOfTask) *
                                          defaults::maxRatioBonusPoints)) {

                setDisabled = true;
            }
        }

        comboBox->setDisabled(setDisabled);
        mHelpWidgets[i]->setVisible(setDisabled);

        ++i;
    }
}
