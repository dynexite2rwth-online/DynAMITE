#pragma once

#include <QGridLayout>
#include <QLabel>
#include <QWidget>

class CalculationWindow : public QWidget {
    Q_OBJECT

public:
    explicit CalculationWindow(QWidget *parent = nullptr);

public Q_SLOTS:
    void gradesChanged();

private:
    QGridLayout mLayout;

    QLabel mMaxClosed, mAvgFirstTry, mAbsPassMark, mRelPassMark0_5,
        mRelPassMark0_78, mSummary;
};
