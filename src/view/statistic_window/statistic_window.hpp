#pragma once

#include <QGridLayout>
#include <QLabel>
#include <QWidget>

class StatisticWindow : public QWidget {
    Q_OBJECT

public:
    explicit StatisticWindow(QWidget *parent = nullptr);

public Q_SLOTS:
    void nrStudentsChanged();

private:
    QGridLayout mLayout;

    QLabel mNrAttended, mNrAttendedRegistered, mNrAttendedNotRegistered,
        mNrRegistered, mNrRegisteredAttended, mNrRegisteredNotAttended;
};
