#include "statistic_window.hpp"

#include "data/data_manager.hpp"

#include <algorithm>
#include <iterator>

StatisticWindow::StatisticWindow(QWidget *parent)
    : QWidget(parent), mLayout(this) {

    setWindowTitle("Statistics");

    mLayout.addWidget(new QLabel("Attended students:", this), 0, 0);
    mLayout.addWidget(new QLabel("\tRegistered:", this), 1, 0);
    mLayout.addWidget(new QLabel("\tNot registered:", this), 2, 0);
    mLayout.addWidget(new QLabel("Registered students:", this), 3, 0);
    mLayout.addWidget(new QLabel("\tAttended:", this), 4, 0);
    mLayout.addWidget(new QLabel("\tNot attended:", this), 5, 0);

    mLayout.addWidget(&mNrAttended, 0, 1);
    mLayout.addWidget(&mNrAttendedRegistered, 1, 1);
    mLayout.addWidget(&mNrAttendedNotRegistered, 2, 1);
    mLayout.addWidget(&mNrRegistered, 3, 1);
    mLayout.addWidget(&mNrRegisteredAttended, 4, 1);
    mLayout.addWidget(&mNrRegisteredNotAttended, 5, 1);

    connect(&DataManager::getInstance(), &DataManager::examChanged, this,
            &StatisticWindow::nrStudentsChanged);
}

void StatisticWindow::nrStudentsChanged() {

    const auto cur = DataManager::getCurrent();
    if (!cur) {
        mNrAttended.setText("-");
        mNrAttendedRegistered.setText("-");
        mNrAttendedNotRegistered.setText("-");
        mNrRegistered.setText("-");
        mNrRegisteredAttended.setText("-");
        mNrRegisteredNotAttended.setText("-");
        return;
    }

    const auto &stats = cur->getStats();

    mNrAttended.setText(QString::number(stats.mNrAttended));
    mNrAttendedRegistered.setText(QString::number(stats.mNrAttendedRegistered));
    mNrAttendedNotRegistered.setText(
        QString::number(stats.mNrAttendedNotRegistered));
    mNrRegistered.setText(QString::number(stats.mNrRegistered));
    mNrRegisteredAttended.setText(QString::number(stats.mNrAttendedRegistered));
    mNrRegisteredNotAttended.setText(
        QString::number(stats.mNrRegisteredNotAttended));
}
