#include "calculation_window.hpp"

#include "data/data_manager.hpp"

#include <cassert>

CalculationWindow::CalculationWindow(QWidget *parent)
    : QWidget(parent), mLayout(this) {

    setWindowTitle("Calculating the Closed Pass Mark");

    int currentRow = 0;

    int maxClosedRow;
    int avgFirstTryRow;
    int absPassMarkRow;
    int relPassMark0_5Row;
    int relPassMark0_78Row;

    {
        // Suppositions
        mLayout.addWidget(new QLabel("<b>Suppositions:</b>", this),
                          currentRow++, 0);
        maxClosedRow = currentRow;
        mLayout.addWidget(
            new QLabel("Maximum number of points that can be achieved:", this),
            currentRow++, 0);
        avgFirstTryRow = currentRow;
        mLayout.addWidget(
            new QLabel("Average number of points achieved by all "
                       "students taking the\nexam for the first time:",
                       this),
            currentRow++, 0);
    }

    {
        // Separator line
        const auto line = new QFrame(this);
        line->setFrameShape(QFrame::HLine);
        mLayout.addWidget(line, currentRow++, 0, 1, 3);
    }

    {
        // Absolute Pass Mark
        mLayout.addWidget(new QLabel("<b>Absolute Pass Mark:</b>", this),
                          currentRow++, 0);
        absPassMarkRow = currentRow;
        mLayout.addWidget(
            new QLabel(
                "60% of the maximum number of points that can be achieved:",
                this),
            currentRow++, 0);
    }

    {
        // Separator line
        const auto line = new QFrame(this);
        line->setFrameShape(QFrame::HLine);
        mLayout.addWidget(line, currentRow++, 0, 1, 3);
    }

    {
        // Relative Pass Mark
        mLayout.addWidget(new QLabel("<b>Relative Pass Mark:</b>", this),
                          currentRow++, 0);
        relPassMark0_5Row = currentRow;
        mLayout.addWidget(
            new QLabel(
                "50% of the maximum number of points that can be achieved:",
                this),
            currentRow++, 0);
        relPassMark0_78Row = currentRow;
        mLayout.addWidget(
            new QLabel("78% of the average number of points achieved by all "
                       "students\ntaking the exam for the first time:",
                       this),
            currentRow++, 0);
    }

    {
        // Separator line
        const auto line = new QFrame(this);
        line->setFrameShape(QFrame::HLine);
        mLayout.addWidget(line, currentRow++, 0, 1, 3);
    }

    {
        // Summary
        mLayout.addWidget(&mSummary, currentRow++, 0, 1, 3);
    }

    {
        // Vertical separator line
        // Don't separate summary and supposition
        const auto line = new QFrame(this);
        line->setFrameShape(QFrame::VLine);
        mLayout.addWidget(line, 1, 1, currentRow - 2, 1);
    }

    mLayout.addWidget(&mMaxClosed, maxClosedRow, 2);
    mLayout.addWidget(&mAvgFirstTry, avgFirstTryRow, 2);
    mLayout.addWidget(&mAbsPassMark, absPassMarkRow, 2);
    mLayout.addWidget(&mRelPassMark0_5, relPassMark0_5Row, 2);
    mLayout.addWidget(&mRelPassMark0_78, relPassMark0_78Row, 2);

    connect(&DataManager::getInstance(), &DataManager::gradesChanged, this,
            &CalculationWindow::gradesChanged);
}

void CalculationWindow::gradesChanged() {
    const auto cur = DataManager::getCurrent();
    if (!cur) {
        mMaxClosed.setText("-");
        mAvgFirstTry.setText("-");
        mAbsPassMark.setText("-");
        mRelPassMark0_5.setText("-");
        mRelPassMark0_78.setText("-");
        mSummary.setText("-");
        return;
    }

    const auto firstTryPoints = cur->getAvgFirstTryPoints();

    const auto hasFirstTry = firstTryPoints.has_value();
    const auto stepSize = cur->mUserPointConfig.mGradeBoundaryStepSize;

    const auto changedMaxPoints =
        cur->getChangedMaxPointsBoundary(GradingType::Closed);
    mMaxClosed.setText(changedMaxPoints.toString(stepSize));
    if (hasFirstTry) {
        mAvgFirstTry.setText(firstTryPoints->toStringHigherPrecision());
    } else {
        mAvgFirstTry.setText("None");
    }

    const auto absPassMark = cur->getAbsPassingLimit();
    mAbsPassMark.setText(QString("0.6 * %1 = <b>%2</b>")
                             .arg(changedMaxPoints.toString(stepSize),
                                  absPassMark.toStringHigherPrecision()));

    // Relative pass mark
    const auto relPassMark0_5 =
        toPoints(changedMaxPoints,
                 cur->mUserPointConfig.mGradeBoundaryStepSize) *
        0.5;
    const auto relPassMark0_78 = firstTryPoints.value_or(Points{0.}) * 0.78;

    // Change which one is bold depending on which one is bigger
    QString relPassMark0_5Str, relPassMark0_78Str;
    if (relPassMark0_5 >= relPassMark0_78) {
        relPassMark0_5Str =
            QString("<b>%1</b>").arg(relPassMark0_5.toStringHigherPrecision());
        relPassMark0_78Str =
            QString("%1").arg(relPassMark0_78.toStringHigherPrecision());
    } else {
        relPassMark0_5Str =
            QString("%1").arg(relPassMark0_5.toStringHigherPrecision());
        relPassMark0_78Str =
            QString("<b>%1</b>").arg(relPassMark0_78.toStringHigherPrecision());
    }

    mRelPassMark0_5.setText(
        QString("0.5 * %1 = %2")
            .arg(changedMaxPoints.toString(stepSize), relPassMark0_5Str));
    if (!hasFirstTry) {
        mRelPassMark0_78.setText("None");
    } else {
        mRelPassMark0_78.setText(
            QString("0.78 * %1 = %2")
                .arg(firstTryPoints->toStringHigherPrecision(),
                     relPassMark0_78Str));
    }
    const auto finalRelPassMark =
        hasFirstTry ? qMax(relPassMark0_5, relPassMark0_78) : relPassMark0_5;

    const auto absPassMarkUsed =
        hasFirstTry ? absPassMark < finalRelPassMark : true;
    const auto finalPassMark = absPassMarkUsed ? absPassMark : finalRelPassMark;
    const auto finalPassMarkGradeBoundary = GradeBoundary(
        finalPassMark, cur->mUserPointConfig.mGradeBoundaryStepSize,
        cur->mUserPointConfig.mGradeBoundaryRounding);
    const auto passMarkUsedStr = absPassMarkUsed ? "absolute" : "relative";

    mSummary.setText(
        QString("The (rounded) %1 pass mark of <b>%2 points</b> is decisive.")
            .arg(passMarkUsedStr,
                 finalPassMarkGradeBoundary.toString(stepSize)));
}
