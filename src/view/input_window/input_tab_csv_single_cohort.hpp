/**
 * \file input_tab_csv_single_cohort.hpp
 */

#pragma once

#include "input_tab_base.hpp"
#include "load_file_view.hpp"

#include "file_io/d2r_parser.hpp"
#include "util/grade_boundary_step.hpp"
#include "util/storage_helper.hpp"

#include <QRadioButton>

#include <optional>

/**
 * \brief Widget that lets the user select csv input files.
 *
 * With the ChooseInputCsvWidget the user can interactively select dynexite- and
 * RWTHonline csv-files to analyze.
 * The "Browse..."-buttons open file dialogs to select .csv files, and the
 * import button confirms the selection.
 */
class InputTabCsvSingleCohort : public InputTabBase {
    Q_OBJECT

public:
    explicit InputTabCsvSingleCohort(QWidget *parent = nullptr);

private:
    /**
     * \brief Displays the selected dynexite file and its status.
     */
    LoadFileView mExamFileView;

    /**
     * \brief Displays the selected RWTHonline file and its status.
     */
    LoadFileView mStudentFileView;

    /**
     * \brief Displays the selected bonus point file and its status.
     */
    LoadFileView mBonusFileView;

    /**
     * \brief Radio buttons to select the grade boundary step size.
     */
    EnumStorage<QRadioButton, GradeBoundaryStepSize> mGradeBoundaryStepSize;

private Q_SLOTS:
    /**
     * \brief A file was selected.
     */
    void fileSelectChange();

    /**
     * \brief Import button clicked.
     *
     * Collect input data structures and send them on to the main window.
     */
    void handleImportButton();

Q_SIGNALS:
    /**
     * \brief An exam with the given data structures is requested to be loaded.
     */
    void inputTabFilesImported(const DynexiteExam &exam,
                               const StudentData &students,
                               const BonusPoints &bonus,
                               GradeBoundaryStepSize stepSize,
                               const std::optional<D2rSettings> &settings);
};
