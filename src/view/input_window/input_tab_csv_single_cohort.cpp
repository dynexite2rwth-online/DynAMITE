/**
 * \file input_tab_csv_single_cohort.cpp
 */

#include "input_tab_csv_single_cohort.hpp"

#include "file_io/d2r_parser.hpp"
#include "util/enums.hpp"
#include "util/grade_boundary_step.hpp"
#include "util/heuristics.hpp"

#include <QLabel>

// inline constexpr int labelRow = 0;
inline constexpr int dynexiteRow = 0;
inline constexpr int rwthOnlineRow = 1;
inline constexpr int bonusRow = 2;
inline constexpr EnumStorage<int, GradeBoundaryStepSize> boundaryStepRow = {
    3, 4, 5};
inline constexpr int importRow = 6;

InputTabCsvSingleCohort::InputTabCsvSingleCohort(QWidget *parent)
    : InputTabBase(parent), mExamFileView(FileType::dynexite),
      mStudentFileView(FileType::rwth), mBonusFileView(FileType::bonus),
      mGradeBoundaryStepSize{QRadioButton{str::ui::input::radioRound1},
                             QRadioButton{str::ui::input::radioRound0_5},
                             QRadioButton{str::ui::input::radioRound0_25}} {

    // File selects
    mMainLayout.addWidget(&mExamFileView, dynexiteRow, 0, 1, 2);
    mMainLayout.addWidget(&mStudentFileView, rwthOnlineRow, 0, 1, 2);
    mMainLayout.addWidget(&mBonusFileView, bonusRow, 0, 1, 2);

    mMainLayout.addWidget(new QLabel(str::ui::input::radioRoundQuestion),
                          *std::ranges::min_element(boundaryStepRow.mData), 0,
                          3, 1);
    {
        using enum GradeBoundaryStepSize;

        for (const auto s : {Min1_00, Min0_50, Min0_25}) {

            mMainLayout.addWidget(&mGradeBoundaryStepSize[s],
                                  boundaryStepRow[s], 1, 1, 1);
        }
    }
    // Don't enable the import button as long as no files are selected.
    mImportButton.setEnabled(false);
    mMainLayout.addWidget(&mImportButton, importRow, 0, 1, 2);

    // Listen for signals
    connect(&mExamFileView, &LoadFileView::modelChange, this,
            &InputTabCsvSingleCohort::fileSelectChange);
    connect(&mStudentFileView, &LoadFileView::modelChange, this,
            &InputTabCsvSingleCohort::fileSelectChange);
    connect(&mBonusFileView, &LoadFileView::modelChange, this,
            &InputTabCsvSingleCohort::fileSelectChange);
    connect(&mImportButton, &QPushButton::clicked, this,
            &InputTabCsvSingleCohort::handleImportButton);

    // Temporary shortcut to speed up testing the app
    mExamFileView.tempSetDeveloperPaths();
    mStudentFileView.tempSetDeveloperPaths();
}

void InputTabCsvSingleCohort::handleImportButton() {

    // Display warning and abort import if the maximum bonus points to be
    // achieved are more than 20% of the total score.
    if (mBonusFileView.mFileState == FileSelectState::ok) {
        if (warnIfTooManyBonusPoints(this, mExamFileView.model(),
                                     mBonusFileView.model())) {
            // Abort import
            return;
        }
    }

    // Get selected grade boundary step size
    auto stepSize = GradeBoundaryStepSize::Min0_25;
    {
        using enum GradeBoundaryStepSize;
        for (const auto s : {Min1_00, Min0_50, Min0_25}) {
            if (mGradeBoundaryStepSize[s].isChecked()) {
                stepSize = s;
                break;
            }
        }
    }

    // Careful: this calls the fileSelectChange-callback, which resets the radio
    // buttons. So make sure we read them out first.
    const auto dynexiteExam =
        std::get<QSharedPointer<DynexiteExam>>(mExamFileView.get());
    const auto studentFile =
        std::get<QSharedPointer<StudentData>>(mStudentFileView.get());

    // Extracting bonus points from variant might fail if it wasn't set
    BonusPoints bonusPoints;
    const auto bonusVariant = mBonusFileView.get();
    if (std::holds_alternative<QSharedPointer<BonusPoints>>(bonusVariant)) {
        const auto bonusPointPtr =
            std::get<QSharedPointer<BonusPoints>>(bonusVariant);

        if (bonusPointPtr) {
            bonusPoints = *bonusPointPtr;
        }
    }

    if (dynexiteExam && studentFile)
        Q_EMIT inputTabFilesImported(*dynexiteExam, *studentFile, bonusPoints,
                                     stepSize, std::nullopt);
    else {
        assert(false);
    }
}

void InputTabCsvSingleCohort::fileSelectChange() {

    const auto essentialFilesReady =
        mExamFileView.mFileState == FileSelectState::ok &&
        mStudentFileView.mFileState == FileSelectState::ok;

    // Display warning if the selected files don't reference the same students
    if (essentialFilesReady) {
        warnIfLowStudentIntersection(
            this, mExamFileView.model(), mStudentFileView.model(),
            mBonusFileView.model(), mBonusFileView.mFileState);
    }

    // Update clickability of import button and grade boundary steps
    if (essentialFilesReady) {
        // Enable the import button
        mImportButton.setEnabled(true);

        // Enable grade boundary buttons, select the radio button the heuristic
        // suggests
        for (auto &r : mGradeBoundaryStepSize) {
            r.setChecked(false);
        }
        const auto gradeBoundaryStepSize =
            guessGradeBoundaryStepSize(*mExamFileView.read<DynexiteExam>());
        mGradeBoundaryStepSize[gradeBoundaryStepSize].setChecked(true);
    } else {
        // Disable the import button
        mImportButton.setEnabled(false);
    }
}
