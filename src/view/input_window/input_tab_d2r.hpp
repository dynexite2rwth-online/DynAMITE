/**
 * \file input_tab_d2r.hpp
 */

#pragma once

#include "input_tab_base.hpp"
#include "load_file_view.hpp"

#include "file_io/d2r_parser.hpp"

#include <optional>

enum class GradeBoundaryStepSize;

/**
 * \brief Tab that lets the user select d2r input files.
 *
 * D2R files are our own file format consisting of the original csv input files
 * and all changes to grade boundaries etc.
 */
class InputTabD2R : public InputTabBase {
    Q_OBJECT

public:
    explicit InputTabD2R(QWidget *parent = nullptr);

private Q_SLOTS:
    /**
     * \brief A file was selected.
     */
    void fileSelectChange();

    /**
     * \brief Import button clicked.
     *
     * Collect input data structures and send them on to the main window.
     */
    void handleImportButton();

Q_SIGNALS:
    /**
     * \brief An exam with the given data structures is requested to be loaded.
     */
    void inputTabFilesImported(const DynexiteExam &exam,
                               const StudentData &students,
                               const BonusPoints &bonus,
                               GradeBoundaryStepSize stepSize,
                               const std::optional<D2rSettings> &settings);

private:
    /** \brief View-object that displays the selected file and its status. */
    LoadFileView mD2RFileView;
};
