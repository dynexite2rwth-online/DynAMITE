/**
 * \file input_window.hpp
 */

#pragma once

#include "input_tab_csv_single_cohort.hpp"
#include "input_tab_d2r.hpp"

#include <QLabel>
#include <QTabWidget>
#include <QVBoxLayout>

class MainWindow;

/**
 * \brief Window that lets the user select input files from various different
 * formats
 */
class InputWindow : public QWidget {
    Q_OBJECT
public:
    explicit InputWindow(const MainWindow &mainWindow,
                         QWidget *parent = nullptr);

private:
    /**
     * \brief Link to document describing how to grade multiple choice exams
     */
    QLabel mGuidelineLink;

    /**
     * \brief Main layout of the window.
     */
    QVBoxLayout mLayout;

    /**
     * \brief The different input methods are separated into different tabs.
     */
    QTabWidget mTabs;

    /**
     * \brief The input dialog tab where the user selects csv input files.
     */
    InputTabCsvSingleCohort mInputCsvSingle;

    /**
     * \brief The input dialog tab where the user selects d2r input files.
     */
    InputTabD2R mInputD2R;
};
