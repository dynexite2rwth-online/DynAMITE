#include "input_window.hpp"

#include "view/main_window/main_window.hpp"

inline constexpr int csvTabIdx = 0;

InputWindow::InputWindow(const MainWindow &mainWindow, QWidget *parent)
    : QWidget(parent), mGuidelineLink(str::ui::input::guidelineHint),
      mLayout(this) {

    setWindowTitle(str::title::inputWindow);

    // Link to guidelines
    mGuidelineLink.setTextFormat(Qt::RichText);
    mGuidelineLink.setOpenExternalLinks(true);
    mGuidelineLink.setTextInteractionFlags(Qt::TextBrowserInteraction);
    mGuidelineLink.setWordWrap(true);

    // Tab for every type of file import
    mTabs.addTab(&mInputCsvSingle, str::ui::input::csvTab);
    mTabs.addTab(&mInputD2R, str::ui::input::d2rTab);
    mTabs.setCurrentIndex(csvTabIdx);

    mLayout.addWidget(&mGuidelineLink);
    mLayout.addWidget(&mTabs);

    // Notify main window when a tab has imported input files
    connect(&mInputCsvSingle, &InputTabCsvSingleCohort::inputTabFilesImported,
            &mainWindow, &MainWindow::resetToInputFiles);
    connect(&mInputD2R, &InputTabD2R::inputTabFilesImported, &mainWindow,
            &MainWindow::resetToInputFiles);
    // add other tabs here once implemented
}
