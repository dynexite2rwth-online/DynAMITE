#include "load_file_view.hpp"

#include "data/nodes/bonus_points.hpp"
#include "data/nodes/dynexite_exam.hpp"
#include "data/nodes/student_data.hpp"
#include "file_io/d2r_parser.hpp"
#include "util/storage_helper.hpp"
#include "util/strings.hpp"

#include <QFileDialog>
#include <QFrame>
#include <QHeaderView>
#include <QMessageBox>
#include <QSpacerItem>
#include <QString>
#include <QStyle>

#include <array>
#include <cassert>

namespace {
QString getFileNameDesc(const FileType t) {
    switch (t) {
    case FileType::dynexite:
        return "Dynexite";
    case FileType::rwth:
        return "RWTHonline";
    case FileType::d2r:
        return "D2R";
    case FileType::bonus:
        return "Bonus points (optional)";
    default:
        assert(false);
        return "Unknown";
    }
}
[[nodiscard]] int getFileHelp(const FileType t, QGridLayout &layout,
                              const QStyle *style, QWidget *parent) {

    // Question mark icon
    const auto iconLabel = new QLabel("", parent);
    iconLabel->setPixmap(
        style->standardIcon(QStyle::SP_MessageBoxInformation).pixmap(16));
    layout.addWidget(iconLabel, 0, 0);

    if (static_cast<std::size_t>(t) >= str::ui::input::helpTexts.size()) {
        assert(false);
        return 1;
    }

    // If this is the bonus help text, it includes a hyperlink
    QLabel *helpText;
    if (t == FileType::bonus) {
        helpText = new QLabel(
            QString(str::ui::input::helpTexts[static_cast<std::size_t>(t)])
                .arg(str::ui::bonusFormatUrl),
            parent);

        helpText->setOpenExternalLinks(true);
        helpText->setTextInteractionFlags(Qt::TextBrowserInteraction);
    } else {
        helpText = new QLabel(
            str::ui::input::helpTexts[static_cast<std::size_t>(t)], parent);
    }

    layout.addWidget(helpText, 0, 1);

    // Separator line
    const auto line = new QFrame(parent);
    line->setFrameShape(QFrame::HLine);
    layout.addItem(
        new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum), 0,
        2);

    layout.addWidget(line, 1, 0, 1, 3);
    return 2;
}

/**
 * \brief Create a new QTableWidgetItem using our preferred flags, with the
 * given value and boldness.
 */
[[nodiscard]] auto createTableItem(const QVariant &value,
                                   const bool bold = false) noexcept {
    const auto idItem = new QTableWidgetItem;
    idItem->setData(Qt::DisplayRole, value);
    idItem->setFlags(idItem->flags() ^ Qt::ItemIsEditable);

    if (bold) {
        QFont font;
        font.setBold(true);
        idItem->setData(Qt::FontRole, font);
    }

    return idItem;
}
/**
 * \brief Writes the first pair element of each pair in tableEntries into a
 * vertical header, and the second table item into the column next to it.
 */
void printGenericInfo(QTableWidget &table,
                      const QList<QPair<QVariant, QVariant>> &tableEntries) {

    if (tableEntries.empty()) {
        assert(false);
        return;
    }

    const auto nrRows = static_cast<int>(tableEntries.size());

    table.setVisible(true);

    table.setRowCount(nrRows);
    table.setColumnCount(1);

    // Print label bold in the vertical header
    for (auto i = 0; i < nrRows; ++i) {
        table.setVerticalHeaderItem(
            i, createTableItem(tableEntries[i].first, true));
    }

    // Print column non-bold in the actual table
    for (auto i = 0; i < nrRows; ++i) {
        table.setItem(i, 0, createTableItem(tableEntries[i].second, false));
    }

    table.verticalHeader()->setVisible(true);

    table.resizeColumnsToContents();
    table.resizeRowsToContents();
}

[[nodiscard]] auto printDynexiteInfo(const DynexiteExam &exam) {
    QList<QPair<QVariant, QVariant>> tableEntries;
    tableEntries.emplace_back("# attended students", exam.mIdentifier.size());
    tableEntries.emplace_back("# task items", exam.mTaskItems.size());

    if constexpr (defaults::newGeneralExaminationRegulations) {
        tableEntries.emplace_back(
            "# points", exam.mPointsMaxRaw[GradingType::Open].toString());
    } else {
        tableEntries.emplace_back(
            "# closed points",
            exam.mPointsMaxRaw[GradingType::Closed].toString());
        tableEntries.emplace_back(
            "# open points", exam.mPointsMaxRaw[GradingType::Open].toString());
        tableEntries.emplace_back(
            "# combined points",
            exam.mPointsMaxRaw[GradingType::Combined].toString());
    }

    return tableEntries;
}

[[nodiscard]] auto printRwthOnlineInfo(const StudentData &students) {

    const auto titlePluralS = students.mCourseTitles.size() > 1 ? "s" : "";
    const auto semesterPluralS = students.mSemesters.size() > 1 ? "s" : "";

    QList<QPair<QVariant, QVariant>> tableEntries;
    tableEntries.emplace_back(QString("Course title%1").arg(titlePluralS),
                              students.mCourseTitles.join("; "));
    tableEntries.emplace_back(QString("Semester%1").arg(semesterPluralS),
                              students.mSemesters.join("; "));
    tableEntries.emplace_back("# registered students",
                              students.mRegistrationNr.size());

    return tableEntries;
}

[[nodiscard]] auto printBonusInfo(const BonusPoints &bonus) {
    QList<QPair<QVariant, QVariant>> tableEntries;
    tableEntries.emplace_back("# students with bonus points",
                              bonus.mTotalBonusPointsPerStudent.count());
    tableEntries.emplace_back("Max bonus points",
                              bonus.mMaxBonusPoints.toString());

    return tableEntries;
}

[[nodiscard]] auto printD2rInfo(const ExamSave &d2r) {
    QList<QPair<QVariant, QVariant>> tableEntries;

    tableEntries.append(printDynexiteInfo(d2r.mDynexite));
    tableEntries.append(printRwthOnlineInfo(d2r.mStudents));

    if (!d2r.mBonus.mTotalBonusPointsPerStudent.empty()) {
        tableEntries.append(printBonusInfo(d2r.mBonus));
    }

    tableEntries.emplace_back("Grade boundary step", as<float>(d2r.mStepSize));
    tableEntries.emplace_back("DynAMITE version", d2r.mSettings.mVersion);

    return tableEntries;
}
} // namespace

LoadFileView::LoadFileView(const FileType type, QWidget *parent)
    : QGroupBox(getFileNameDesc(type), parent), mInputModel(type) {

    connect(&mBrowseButton, &QPushButton::clicked, this,
            &LoadFileView::handleBrowseButton);

    // First, an explanatory text
    auto nextRow = getFileHelp(type, mOuterLayout, style(), this);

    // The file selection is surrounded by two spacers, so that it is in the
    // middle of the remaining space
    mOuterLayout.addItem(
        new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding),
        nextRow++, 0, 1, 3);

    // the elements of each file selection are arranged horizontally within one
    // row of the outer layout
    mOuterLayout.addLayout(&mHLayout, nextRow++, 0, 1, 3);
    mHLayout.addWidget(&mFileNameLabel);
    mHLayout.addWidget(&mFileParseStatusIcon);
    mHLayout.addWidget(&mBrowseButton);

    // mInfoTable shows an overview over the selected file, e.g. number of
    // students.
    mInfoTable.setVisible(false);
    mInfoTable.horizontalHeader()->hide();
    mInfoTable.setSortingEnabled(false);
    mOuterLayout.addWidget(&mInfoTable, nextRow++, 0, 1, 3);

    mOuterLayout.addItem(
        new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding),
        nextRow++, 0, 1, 3);
}

void LoadFileView::printInfo(const FileType fileType) {

    QList<QPair<QVariant, QVariant>> tableEntries;

    switch (fileType) {
    case FileType::dynexite:
        tableEntries = printDynexiteInfo(*mInputModel.read<DynexiteExam>());
        break;
    case FileType::rwth:
        tableEntries = printRwthOnlineInfo(*mInputModel.read<StudentData>());
        break;
    case FileType::bonus:
        tableEntries = printBonusInfo(*mInputModel.read<BonusPoints>());
        break;
    case FileType::d2r:
        tableEntries = printD2rInfo(*mInputModel.read<ExamSave>());
        break;
    case FileType::NR_VALS:
    default:
        assert(false);
        break;
    }

    printGenericInfo(mInfoTable, tableEntries);
}

void LoadFileView::clearInfo() {
    mInfoTable.clear();
    mInfoTable.setVisible(false);
}

void LoadFileView::handleBrowseButton() {
    // window captions for the file selection dialog for each file type
    static constexpr EnumStorage<const char *, FileType> captionTexts = {
        "Open exam results file from dynexite",
        "Open student info file from RWTHonline",
        "Open eTest bonus points file", "Open all-in-one (d2r) file"};

    // let the user select a file
    const auto fileName = QFileDialog::getOpenFileName(
        this, captionTexts[mInputModel.mType], "",
        mInputModel.mType == FileType::d2r ? str::ui::input::d2rFilter
                                           : str::ui::input::csvFilter);

    // if the user didn't select any files, don't change anything
    if (!fileName.isEmpty()) {
        // write the path to the selected file to the label of the corresponding
        // file type
        mFileNameLabel.setText(fileName);

        const auto [errorCode, errorDetails, errorWarnings] =
            mInputModel.loadCsv(mFileNameLabel.text(), this);
        if (errorCode == FileOpenErrorCode::Success) {

            // Display warnings if there are any
            if (!errorWarnings.empty()) {
                QString errorText = str::error::readingFileWarning;
                for (const auto &s : errorWarnings) {
                    errorText += '\n' + s;
                }
                QMessageBox::warning(this, str::title::readingFileWarning,
                                     errorText);
            }

            printInfo(mInputModel.mType);

            setFileStatus(FileSelectState::ok);
        } else {

            QMessageBox::warning(this, str::title::readingFileError,
                                 errorDetails);

            setFileStatus(FileSelectState::error);

            clearInfo();
        }
    }
}

void LoadFileView::reset() {
    // remove input from the labels displaying the selected files, reset icons,
    // unload csv-file
    mFileNameLabel.setText("");

    // We deliberately keep the current data loaded in the model, so that we can
    // reset the view and extract the data from the model (which clears it
    // there)

    setFileStatus(FileSelectState::empty);

    clearInfo();
}

void LoadFileView::tempSetDeveloperPaths() {
#if defined(DYNEXITE_EXAMPLE_FILE_PATH) && defined(RWTHONLINE_EXAMPLE_FILE_PATH)
    if (mInputModel.mType == FileType::dynexite) {
        mFileNameLabel.setText(DYNEXITE_EXAMPLE_FILE_PATH);
    } else {
        mFileNameLabel.setText(RWTHONLINE_EXAMPLE_FILE_PATH);
    }

    if (mInputModel.loadCsv(mFileNameLabel.text(), this).mEc ==
        FileOpenErrorCode::Success) {
        setFileStatus(FileSelectState::ok);
    } else {
        setFileStatus(FileSelectState::error);
    }
#endif
}

void LoadFileView::setFileStatus(const FileSelectState status) {
    // icons corresponding to each file select state
    static constexpr EnumStorage<QStyle::StandardPixmap, FileSelectState>
        icons = {
            QStyle::SP_MessageBoxQuestion, // empty, no icon, not used
            QStyle::SP_MessageBoxCritical,
            QStyle::SP_DialogOkButton // todo: find better icon
        };

    // update status
    mFileState = status;

    // update icon
    if (status != FileSelectState::empty) {
        mFileParseStatusIcon.setPixmap(
            style()->standardIcon(icons[status]).pixmap(16));
    } else {
        mFileParseStatusIcon.setText(" ");
    }
    Q_EMIT modelChange();
}
