/**
 * \file input_tab_d2r.cpp
 */

#include "input_tab_d2r.hpp"

#include "file_io/d2r_parser.hpp"
#include "util/enums.hpp"

InputTabD2R::InputTabD2R(QWidget *parent)
    : InputTabBase(parent), mD2RFileView(FileType::d2r) {

    mMainLayout.addWidget(&mD2RFileView);

    // Don't enable the import button as long as no files are selected.
    mImportButton.setEnabled(false);
    mMainLayout.addWidget(&mImportButton);

    connect(&mD2RFileView, &LoadFileView::modelChange, this,
            &InputTabD2R::fileSelectChange);
    connect(&mImportButton, &QPushButton::clicked, this,
            &InputTabD2R::handleImportButton);
}

void InputTabD2R::fileSelectChange() {

    // update clickability of import button
    mImportButton.setEnabled(mD2RFileView.mFileState == FileSelectState::ok);
}

void InputTabD2R::handleImportButton() {

    const auto exam = std::get<QSharedPointer<ExamSave>>(mD2RFileView.get());

    Q_EMIT inputTabFilesImported(exam->mDynexite, exam->mStudents, exam->mBonus,
                                 exam->mStepSize, exam->mSettings);
}
