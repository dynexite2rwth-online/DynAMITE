/**
 * \file input_tab_base.hpp
 */

#pragma once

#include "util/strings.hpp"

#include <QGridLayout>
#include <QPushButton>

/**
 *  \brief Common elements of a tab of the input window.
 */
class InputTabBase : public QWidget {
    Q_OBJECT

public:
    explicit InputTabBase(QWidget *parent = nullptr);

protected:
    /**
     *  \brief "Import"-button to finalize the selection.
     *
     *  Only enabled when valid files are selected.
     */
    QPushButton mImportButton{str::ui::input::importButton};

    /**
     * \brief Layout for the different widgets in each tab.
     */
    QGridLayout mMainLayout{this};
};
