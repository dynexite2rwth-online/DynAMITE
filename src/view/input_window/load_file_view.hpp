#pragma once

#include "model/input_model.hpp"
#include "util/enums.hpp"

#include <QGridLayout>
#include <QGroupBox>
#include <QLabel>
#include <QPushButton>
#include <QTableWidget>

class LoadFileView : public QGroupBox {

    Q_OBJECT
public:
    explicit LoadFileView(FileType type, QWidget *parent = nullptr);

    /**
     * \brief Resets all user input.
     */
    void reset();

    void tempSetDeveloperPaths();

    [[nodiscard]] auto get() {
        reset();
        return mInputModel.get();
    }
    template <typename T>
    [[nodiscard]] auto read() const {
        return mInputModel.read<T>();
    }

    [[nodiscard]] const InputModel &model() noexcept { return mInputModel; }

    /** \brief State of each file selection. */
    FileSelectState mFileState = FileSelectState::empty;

Q_SIGNALS:
    /**
     * \brief Signals that the underlying data was changed.
     */
    void modelChange();

private:
    /** \brief Label that displays the path of a file selection. */
    QLabel mFileNameLabel{"Select File...", this};

    /** \brief Icon that displays the status of a file selection. */
    QLabel mFileParseStatusIcon{"", this};

    /** \brief Outer layout. */
    QGridLayout mOuterLayout{this};

    /** \brief Inner horizontal layout. */
    QHBoxLayout mHLayout{nullptr};

    /** \brief Button UI elements which open a dialog to select a file. */
    QPushButton mBrowseButton{"Browse...", this};

    /** \brief Table that shows information about the selected file. */
    QTableWidget mInfoTable{this};

    /**
     * \brief Updates the icon and state of the given FileType to the new
     * status.
     */
    void setFileStatus(FileSelectState status);

    InputModel mInputModel;

private Q_SLOTS:
    void handleBrowseButton();

private:
    void printInfo(FileType fileType);
    void clearInfo();
};
