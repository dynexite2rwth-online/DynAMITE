#include "help_widget.hpp"

#include <QMessageBox>
#include <QStyle>

HelpWidget::HelpWidget(QWidget *parent, const QString &title,
                       const QString &text)
    : QPushButton(parent), mMessageBox(new QMessageBox(parentWidget())) {

    // Apart from `setWindowModality` , we copy the behaviour of
    // `QMessageBox::information`.
    // We don't use `QMessageBox::information` here so that we can set the
    // window modality to non-modal, otherwise the app can become (nearly)
    // softlocked if the info window opens behind another window.
    mMessageBox->setText(text);
    mMessageBox->setWindowTitle(title);
    mMessageBox->setIcon(QMessageBox::Information);
    mMessageBox->setWindowModality(Qt::WindowModality::NonModal);

    setFlat(true);
    setIcon(style()->standardIcon(QStyle::SP_MessageBoxQuestion));

    setToolTip(text);
    // Don't automatically hide the tooltip after a few seconds
    setToolTipDuration(0);

    connect(this, &HelpWidget::clicked, this, &HelpWidget::questionMarkClicked);
}

void HelpWidget::setHelpText(const QString &text) {
    mMessageBox->setText(text);
    setToolTip(text);
}

QString HelpWidget::helpText() const { return mMessageBox->text(); }

void HelpWidget::setTitle(const QString &title) const {
    mMessageBox->setWindowTitle(title);
}

QString HelpWidget::title() const { return mMessageBox->windowTitle(); }

void HelpWidget::questionMarkClicked() const { mMessageBox->show(); }
