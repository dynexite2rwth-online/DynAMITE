#pragma once

#include "util/grade_boundary_step.hpp"

#include <QColor>
#include <QList>
#include <QWheelEvent>

#ifndef _MSC_VER
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wfloat-conversion"
#endif
#include <qcustomplot/qcustomplot.h>
#ifndef _MSC_VER
#pragma GCC diagnostic pop
#endif

struct FontSize;
class ViewSettingsWindow;

class MultiPlotWidget : public QCustomPlot {
    Q_OBJECT
public:
    MultiPlotWidget(const QList<double> &sortedData, double max,
                    GradeBoundaryStepSize stepSizeParam, QColor backgroundColor,
                    const ViewSettingsWindow &viewSettings, QWidget *parent);

    void showPlot(bool showHistogram, bool showBoxPlot);

    void setAxisFontSize(FontSize font_size);

private Q_SLOTS:
    void bucketSizeChanged(double d) noexcept;

protected:
    // Skip QCustomPlot mouse wheel interactions, or else scrolling doesn't
    // work.
    void wheelEvent(QWheelEvent *event) override;

private:
    void buildHistogram();

    QList<double> mSortedData;
    double mStepSize = 0.;
    double mMaxPoints = 0.;
};
