#pragma once

#include <QProxyStyle>
#include <QSlider>

class DirectClickStyle;

/**
 * \brief QSlider that instantly jumps to a value that was directly clicked on,
 * instead of doing a pageStep into that direction.
 */
class CustomSlider : public QSlider {
    Q_OBJECT
public:
    explicit CustomSlider(QWidget *parent = nullptr);

private:
    DirectClickStyle *mStyle;
};

/**
 * \author
 * https://www.qtcentre.org/threads/9208-QSlider-step-customize?p=49035#post49035
 */
class DirectClickStyle : public QProxyStyle {
    Q_OBJECT
public:
    int styleHint(const QStyle::StyleHint hint,
                  const QStyleOption *option = nullptr,
                  const QWidget *widget = nullptr,
                  QStyleHintReturn *returnData = nullptr) const override;

    DirectClickStyle() : QProxyStyle(nullptr) {}
};
