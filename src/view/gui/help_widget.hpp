#pragma once

#include <QPushButton>
#include <QString>

class QMessageBox;

/**
 * \brief An icon, that displays an info text either when hovered over in a
 * ToolTip or when clicked on in a new window.
 */
class HelpWidget : public QPushButton {
    Q_OBJECT
public:
    HelpWidget(QWidget *parent, const QString &title, const QString &text);

    void setHelpText(const QString &text);
    [[nodiscard]] QString helpText() const;
    void setTitle(const QString &title) const;
    [[nodiscard]] QString title() const;

private:
    void questionMarkClicked() const;

    // title and text are stored in `mMessageBox->text()` /
    // `mMessageBox->windowTitle()`
    QMessageBox *mMessageBox{};
};
