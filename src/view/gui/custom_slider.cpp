#include "custom_slider.hpp"

namespace {
/**
 * \brief Because of the destructor problems described below, we limit ourselves
 * to one re-used DirectClickStyle. The drawback is that all DirectClickStyle
 * will be based on the default native style, instead of a given style.
 *
 * We could solve that problem with a map of DirectClickStyles, but that would
 * be overkill here. We only use the default native style anyways.
 */
DirectClickStyle *getDefaultStyle() {
    static auto style = new DirectClickStyle();
    return style;
}
} // namespace

CustomSlider::CustomSlider(QWidget *parent)
    : QSlider(Qt::Orientation::Horizontal, parent), mStyle(getDefaultStyle()) {

    // According to the documentation, setStyle does not take ownership of
    // mStyle. However something inside of Qt tries to access the style at
    // program shutdown and causes a read access violation, if we deleted the
    // style in the destructor of this class. Therefore, we have to leak it. No,
    // even a QSharedPointer doesn't help.
    setStyle(mStyle);
}

int DirectClickStyle::styleHint(const QStyle::StyleHint hint,
                                const QStyleOption *option,
                                const QWidget *widget,
                                QStyleHintReturn *returnData) const {
    if (hint == QStyle::SH_Slider_AbsoluteSetButtons)
        return (Qt::LeftButton | Qt::MiddleButton | Qt::RightButton);
    return QProxyStyle::styleHint(hint, option, widget, returnData);
}
