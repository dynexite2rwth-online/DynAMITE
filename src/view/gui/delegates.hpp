#pragma once

#include "view/main_window/view_settings.hpp"

#include <QList>
#include <QLocale>
#include <QString>
#include <QStyledItemDelegate>
#include <QVariant>

#include <cassert>

// Displays a double with NrDecimalPlaces decimal places
template <int NrDecimalPlaces>
class FixedNrDecimalsDelegate : public QStyledItemDelegate {
    // Can not use Q_OBJECT here, because this is a template class
public:
    using QStyledItemDelegate::QStyledItemDelegate;

    QString displayText(const QVariant &value,
                        const QLocale &locale) const override {

        if (value.canConvert<double>()) {
            return QString::number(value.toDouble(), 'f', NrDecimalPlaces);
        } else {
            assert(false);
            return QStyledItemDelegate::displayText(value, locale);
        }
    }
};

// Displays a double with two decimal places and a percentage sign thereafter
class PercentageDelegate : public FixedNrDecimalsDelegate<2> {
    Q_OBJECT
public:
    using FixedNrDecimalsDelegate::FixedNrDecimalsDelegate;

    QString displayText(const QVariant &value,
                        const QLocale &locale) const override;
};

// Displays a double formatted as a Points value using Points::toString
class PointDelegate : public QStyledItemDelegate {
    Q_OBJECT
public:
    using QStyledItemDelegate::QStyledItemDelegate;

    QString displayText(const QVariant &value,
                        const QLocale &locale) const override;
};

// Displays a histogram and a box plot
class PlotDelegate : public QStyledItemDelegate {
    Q_OBJECT
public:
    PlotDelegate(const ViewSettingsWindow &viewSettings,
                 QObject *parent = nullptr);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const override;

    void destroyEditor(QWidget *editor,
                       const QModelIndex &index) const override;

    const QList<QWidget *> &getEditors() const;

private:
    /**
     * \brief createEditor and destroyEditor override functions in
     * QStyledItemDelegate and must therefore be const. However, to keep track
     * of our created widgets and destroy them when we are done, we store them
     * in this list.
     */
    mutable QList<QWidget *> mEditors;

    // Reference for signal-connection
    const ViewSettingsWindow &mViewSettings;
};
