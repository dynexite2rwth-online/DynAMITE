#include "multi_plot_widget.hpp"

#include "util/defaults.hpp"
#include "util/font.hpp"
#include "util/points.hpp"
#include "view/main_window/point_histogram.hpp"
#include "view/main_window/view_settings.hpp"

#include <QPair>
#include <QtGlobal>

#include <algorithm>
#include <cassert>
#include <cmath>
#include <functional>

namespace {
/**
 * \brief Return the median of a sorted non-empty list.
 *
 * If the list size is even, return the arithmetic mean of the two middle
 * values.
 */
[[nodiscard]] auto getMedian(const QList<qreal> &sortedPoints) noexcept {

    qreal median;
    const auto nrStudents = sortedPoints.size();
    if (nrStudents % 2 == 1) {
        // For an odd number of students, the median is the middle
        // value
        const auto medianIdx = (nrStudents - 1) / 2;
        median = sortedPoints[medianIdx];
    } else {
        // For an even number of students, the median is the arithmetic mean of
        // the two middle values
        const auto lowerMedianIdx = (nrStudents - 2) / 2;
        const auto upperMedianIdx = lowerMedianIdx + 1;
        median =
            (sortedPoints[lowerMedianIdx] + sortedPoints[upperMedianIdx]) / 2;
    }
    return median;
}

/**
 * \brief Returns the first and third quartiles. Calculates them by calculating
 * the median of the lower and upper half of the data, excluding the median.
 */
[[nodiscard]] auto getQuartiles(const QList<qreal> &sortedPoints) noexcept {

    const auto nrStudents = sortedPoints.size();

    // Special case for very low number of students
    if (nrStudents == 1) {
        return qMakePair(sortedPoints[0], sortedPoints[0]);
    }

    // If there is an odd number of students, do not include the median in
    // either half. Else, split the data set exactly in half.
    const auto halfListSize =
        nrStudents % 2 == 1 ? (nrStudents - 1) / 2 : nrStudents / 2;

    return qMakePair(getMedian(sortedPoints.first(halfListSize)),
                     getMedian(sortedPoints.last(halfListSize)));
}

/**
 * \brief Returns a pair of the minimum and maximum value in the list.
 */
[[nodiscard]] auto
getMinMaxWhiskers(const QList<qreal> &sortedPoints) noexcept {
    return qMakePair(sortedPoints.first(), sortedPoints.last());
}

/**
 * \brief Returns a pair of values from the list at the percentiles given in
 * \ref defaults::boxPlotWhiskerPercentile.
 */
[[nodiscard]] auto
getPercentileWhiskers(const QList<qreal> &sortedPoints) noexcept {

    const auto nrStudents = sortedPoints.size();

    // Special case for very low number of students
    if (nrStudents == 1) {
        return qMakePair(sortedPoints[0], sortedPoints[0]);
    }

    const auto exactPos = static_cast<double>(nrStudents - 1) *
                          defaults::boxPlotWhiskerPercentile;
    const auto decimalPart = exactPos - std::floor(exactPos);

    const auto idxRoundDown = static_cast<int>(std::floor(exactPos));
    const auto idxRoundUp = idxRoundDown + 1;

    const auto lowerValue = std::lerp(sortedPoints[idxRoundDown],
                                      sortedPoints[idxRoundUp], decimalPart);
    const auto upperValue =
        std::lerp(sortedPoints[nrStudents - 1 - idxRoundDown],
                  sortedPoints[nrStudents - 1 - idxRoundUp], decimalPart);

    return qMakePair(lowerValue, upperValue);
}

/**
 * \brief Calculates the boundary of the whiskers based on the 1.5 IQR value.
 */
[[nodiscard]] auto
get1_5IqrWhiskers(const QList<qreal> &sortedPoints,
                  const QPair<qreal, qreal> quartiles) noexcept {

    const auto iqr = quartiles.second - quartiles.first;

    const auto lowerBoundary = quartiles.first - iqr * 1.5;
    const auto upperBoundary = quartiles.second + iqr * 1.5;

    const auto firstLaEqThanLower =
        std::ranges::lower_bound(sortedPoints, lowerBoundary);
    const auto firstLargerThanUpper =
        std::ranges::upper_bound(sortedPoints, upperBoundary);
    const auto firstLoEqThanUpper = firstLargerThanUpper == sortedPoints.begin()
                                        ? firstLargerThanUpper
                                        : firstLargerThanUpper - 1;

    assert(firstLaEqThanLower != sortedPoints.end() &&
           firstLoEqThanUpper != sortedPoints.end());
    return qMakePair(*firstLaEqThanLower, *firstLoEqThanUpper);
}

/**
 * \brief Calculates the boundary of the whiskers based on the method specified
 * in \ref defaults::boxPlotWhiskersMethod .
 */
[[nodiscard]] auto getWhiskers(const QList<qreal> &sortedPoints,
                               const QPair<qreal, qreal> quartiles) noexcept {

    // Sadly there exists no "switch constexpr", and using just switch generates
    // warnings

    if constexpr (defaults::boxPlotWhiskersMethod ==
                  defaults::BoxPlotWhiskers::Percentile) {
        return getPercentileWhiskers(sortedPoints);

    } else if constexpr (defaults::boxPlotWhiskersMethod ==
                         defaults::BoxPlotWhiskers::Iqr) {
        return get1_5IqrWhiskers(sortedPoints, quartiles);
    } else {
        if constexpr (defaults::boxPlotWhiskersMethod !=
                      defaults::BoxPlotWhiskers::MinMax) {
            assert(false);
        }
        return getMinMaxWhiskers(sortedPoints);
    }
}

struct BoxPlotData {
    explicit BoxPlotData(const QList<qreal> &sortedPoints) noexcept
        : mMedian(getMedian(sortedPoints)),
          mQuartiles(getQuartiles(sortedPoints)),
          mExtremes(getWhiskers(sortedPoints, mQuartiles)) {}

    qreal mMedian;
    QPair<qreal, qreal> mQuartiles;
    QPair<qreal, qreal> mExtremes;

    void addDataToStatistical(QCPStatisticalBox &statistical,
                              const qreal minDistance,
                              const QList<double> &sortedData) const noexcept {
        const qreal lowerQuartile =
            std::min(mQuartiles.first, mMedian - minDistance);
        const qreal lowerExtreme =
            std::min(mExtremes.first, lowerQuartile - minDistance);
        const qreal upperQuartile =
            std::max(mQuartiles.second, mMedian + minDistance);
        const qreal upperExtreme =
            std::max(mExtremes.second, upperQuartile + minDistance);

        QList<double> outliers;
        const auto firstLargerThanLowerExtreme =
            std::ranges::lower_bound(sortedData, lowerExtreme);
        if (firstLargerThanLowerExtreme != sortedData.begin()) {
            outliers.append(
                QList(sortedData.begin(), firstLargerThanLowerExtreme));
        }
        const auto firstLargerThanUpperExtreme = std::ranges::lower_bound(
            sortedData, upperExtreme, std::less_equal{});
        outliers.append(QList(firstLargerThanUpperExtreme, sortedData.end()));

        // TODO: outliers
        statistical.addData(0, lowerExtreme, lowerQuartile, mMedian,
                            upperQuartile, upperExtreme, outliers);
    }
};

} // namespace

class AxisTickerFixedInteger : public QCPAxisTickerFixed {
public:
    using QCPAxisTickerFixed::QCPAxisTickerFixed;

    /**
     * \brief Number of sub ticks between vertical ticks
     */
    int getSubTickCount(const double tickStep) override {
        // tickStep is an integer

        if (tickStep > 10) {
            return 9;
        }

        return static_cast<int>(tickStep) - 1;
    }
};

void setAxisVisible(QCPAxis &axis, const bool isVisible) {
    if (isVisible) {
        axis.setBasePen(Qt::SolidLine);
    } else {
        axis.setBasePen(Qt::NoPen);
    }

    axis.setTickLabels(isVisible);
    axis.setTicks(isVisible);
}

MultiPlotWidget::MultiPlotWidget(const QList<double> &sortedData,
                                 const double max,
                                 const GradeBoundaryStepSize stepSizeParam,
                                 const QColor backgroundColor,
                                 const ViewSettingsWindow &viewSettings,
                                 QWidget *parent)
    : QCustomPlot(parent), mSortedData(sortedData) {

    assert(std::ranges::is_sorted(mSortedData));

    const auto nrStudents = mSortedData.size();

    if (nrStudents == 0) {
        assert(false);
    } else {
        mStepSize = as<double>(stepSizeParam);
        // Make sure that max != 0
        mMaxPoints = std::max({mSortedData.back(), max, 1.});

        const auto xTicker = QSharedPointer<AxisTickerFixedInteger>::create();
        const auto yTicker = QSharedPointer<AxisTickerFixedInteger>::create();
        xTicker->setTickStep(1.);
        xTicker->setScaleStrategy(
            AxisTickerFixedInteger::ScaleStrategy::ssMultiples);
        yTicker->setTickStep(1.);
        yTicker->setScaleStrategy(
            AxisTickerFixedInteger::ScaleStrategy::ssMultiples);
        xAxis->setTicker(xTicker);
        yAxis->setTicker(yTicker);

        // Histogram (accessible via plottable(0))
        new QCPBars(xAxis, yAxis);
        buildHistogram();

        // Box Plot
        {
            // create bottom axis rect for volume bar chart
            auto *volumeAxisRect = new QCPAxisRect(this);
            plotLayout()->addElement(1, 0, volumeAxisRect);
            volumeAxisRect->setMaximumSize(QSize(QWIDGETSIZE_MAX, 50));

            // interconnect x axis ranges of main and bottom axis rects
            // This is no infinite loop, since a signal is only sent out, if
            // the value actually has changed
            connect(xAxis, qOverload<const QCPRange &>(&QCPAxis::rangeChanged),
                    volumeAxisRect->axis(QCPAxis::atBottom),
                    qOverload<const QCPRange &>(&QCPAxis::setRange));
            connect(volumeAxisRect->axis(QCPAxis::atBottom),
                    qOverload<const QCPRange &>(&QCPAxis::rangeChanged), xAxis,
                    qOverload<const QCPRange &>(&QCPAxis::setRange));

            // bring bottom and main axis rect closer together:
            plotLayout()->setRowSpacing(0);
            volumeAxisRect->setAutoMargins(QCP::msLeft | QCP::msRight |
                                           QCP::msBottom);
            volumeAxisRect->setMargins(QMargins(0, 0, 0, 0));

            const auto statistical =
                new QCPStatisticalBox(volumeAxisRect->axis(QCPAxis::atLeft),
                                      volumeAxisRect->axis(QCPAxis::atBottom));

            setAxisVisible(*volumeAxisRect->axis(QCPAxis::atLeft), false);
            volumeAxisRect->axis(QCPAxis::atLeft)->setRange(-1, 1);
            volumeAxisRect->axis(QCPAxis::atLeft)->grid()->setVisible(false);

            QBrush boxBrush(QColor(60, 60, 255, 100));
            boxBrush.setStyle(Qt::Dense6Pattern); // make it look oldschool
            statistical->setBrush(boxBrush);

            // TODO: store as ratio of volumeAxisRect-left-range in defaults
            statistical->setWidth(1.3);

            const BoxPlotData boxPlotData{mSortedData};

            // TODO: store in defaults
            const qreal minDistance = mStepSize * 0.075;
            boxPlotData.addDataToStatistical(*statistical, minDistance,
                                             mSortedData);

            const auto fixedTicker =
                QSharedPointer<AxisTickerFixedInteger>::create();
            fixedTicker->setTickStep(1.);
            fixedTicker->setScaleStrategy(
                AxisTickerFixedInteger::ScaleStrategy::ssMultiples);
            volumeAxisRect->axis(QCPAxis::atBottom)->setTicker(fixedTicker);
            xAxis->setTicker(fixedTicker);

            yAxis->rescale();

            // Scale x axis with a constant factor from the center, so that all
            // x axes in the tab have 0 and max at the same position (or almost
            // the same, y axis width throws this a tiny bit off).
            xAxis->setRange(0, mMaxPoints);
            xAxis->scaleRange(1 + mStepSize / 0.25 * 0.05,
                              xAxis->range().center());

            /* TODO: set x axis labels to fixed width, using largest width of
             * any x label otherwise 0 and max of multiple charts are not quite
             * aligned
             * (almost doesn't matter, so probably ignore this)
             */

            yAxis->scaleRange(1.1, 0);

            // make axis rects' left side line up:
            const auto group = new QCPMarginGroup(this);
            axisRect()->setMarginGroup(QCP::msLeft | QCP::msRight, group);
            volumeAxisRect->setMarginGroup(QCP::msLeft | QCP::msRight, group);
        }
    }

    // receive update when user changes bucket size
    connect(&viewSettings, &ViewSettingsWindow::taskTabBucketSizeChanged, this,
            &MultiPlotWidget::bucketSizeChanged);

    setBackground(backgroundColor);

    // QCustomPlot by default disables mouse propagation due to an old problem
    // on KDE (doesn't seem to be a problem anymore). However, this, and wheel
    // events not being forwarded by QCustomPlot, makes it impossible to scroll
    // if the mouse is over the plot. Therefore we enable mouse propagation
    // again and also forward scroll events directly to the underlying QWidget,
    // skipping QCustomPlot::wheelEvent (see MultiPlotWidget::wheelEvent).
    setAttribute(Qt::WA_NoMousePropagation, false);

    showPlot(true, true);
}

void MultiPlotWidget::showPlot(bool showHistogram, bool showBoxPlot) {

    const auto showHistogramInt = static_cast<int>(showHistogram);
    const auto showBoxPlotInt = static_cast<int>(showBoxPlot);

    plotLayout()->element(0, 0)->setMinimumSize(300, showHistogramInt * 300);
    plotLayout()->element(0, 0)->setMaximumSize(QWIDGETSIZE_MAX,
                                                showHistogramInt * 300);
    plotLayout()->element(1, 0)->setMinimumSize(300, showBoxPlotInt * 50);
    plotLayout()->element(1, 0)->setMaximumSize(QWIDGETSIZE_MAX,
                                                showBoxPlotInt * 50);

    plotLayout()->element(0, 0)->setVisible(showHistogram);
    plotLayout()->element(1, 0)->setVisible(showBoxPlot);

    setAxisVisible(*dynamic_cast<QCPAxisRect *>(plotLayout()->element(1, 0))
                        ->axis(QCPAxis::atBottom),
                   !showHistogram);

    if (showHistogram || showBoxPlot) {
        replot();
    }
}

void MultiPlotWidget::setAxisFontSize(const FontSize font_size) {
    const auto firstGraph =
        dynamic_cast<QCPAxisRect *>(plotLayout()->element(0, 0));
    const auto secondGraph =
        dynamic_cast<QCPAxisRect *>(plotLayout()->element(1, 0));
    assert(firstGraph && secondGraph);

    for (const auto &graph : {firstGraph, secondGraph}) {
        for (const auto &axis : graph->axes()) {
            auto font = axis->tickLabelFont();
            setFontSize(font, font_size);
            axis->setTickLabelFont(font);
        }
    }

    replot();
}

void MultiPlotWidget::bucketSizeChanged(const double d) noexcept {
    if (d != mStepSize) {
        mStepSize = d;

        buildHistogram();
        xAxis->setRange(0, mMaxPoints);
        xAxis->scaleRange(1.2, xAxis->range().center());
        xAxis->scaleRange(1 + (mStepSize / 0.25 - 4) * 0.0125,
                          xAxis->range().center());
        replot();
    }
}

void MultiPlotWidget::wheelEvent(QWheelEvent *event) {
    // Skip QCustomPlot::wheelEvent, otherwise scrolling won't work.
    return QWidget::wheelEvent(event);
}

void MultiPlotWidget::buildHistogram() {

    const auto histogramGraph = dynamic_cast<QCPBars *>(this->plottable(0));
    if (!histogramGraph) {
        assert(false);
        return;
    }

    const auto nrBuckets =
        static_cast<int>(std::ceil(mMaxPoints / mStepSize) + 1);

    // For each possible point value, count how many students
    // got that many points
    QList<qreal> nrStudentsPerBucket;
    nrStudentsPerBucket.resize(nrBuckets);

    for (const auto p : mSortedData) {
        ++nrStudentsPerBucket[impl::getBarIdx(mStepSize, Points{p}, nrBuckets)];
    }

    // x-values of the point buckets
    QList<qreal> bucketPointValues;
    bucketPointValues.reserve(nrBuckets);

    for (int i = 0; i < nrBuckets; ++i) {
        bucketPointValues.emplace_back(i * mStepSize);
    }

    histogramGraph->setData(bucketPointValues, nrStudentsPerBucket, true);

    histogramGraph->setWidth(0.75 * mStepSize);
}
