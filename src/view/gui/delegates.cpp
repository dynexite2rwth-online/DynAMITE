#include "delegates.hpp"

#include "data/task_plot_data.hpp"
#include "multi_plot_widget.hpp"
#include "util/points.hpp"

#include <QLabel>

#include <cassert>

QString PercentageDelegate::displayText(const QVariant &value,
                                        const QLocale &locale) const {

    if (value.canConvert<double>()) {
        return FixedNrDecimalsDelegate::displayText(value, locale) +
               QStringLiteral("%");
    } else {
        assert(false);
        return FixedNrDecimalsDelegate::displayText(value, locale);
    }
}

QString PointDelegate::displayText(const QVariant &value,
                                   const QLocale &locale) const {

    if (value.canConvert<double>()) {
        return Points{value.toDouble()}.toString();
    } else {
        assert(false);
        return QStyledItemDelegate::displayText(value, locale);
    }
}

PlotDelegate::PlotDelegate(const ViewSettingsWindow &viewSettings,
                           QObject *parent)
    : QStyledItemDelegate(parent), mViewSettings(viewSettings) {}

QWidget *PlotDelegate::createEditor(QWidget *parent,
                                    const QStyleOptionViewItem &option,
                                    const QModelIndex &index) const {

    if (index.data().canConvert<TaskPlotData>()) {

        const auto data = index.data().value<TaskPlotData>();

        if (data.mPoints.empty()) {
            const auto editor =
                new QLabel("No student participated in this task", parent);
            mEditors.push_back(editor);
            return editor;
        } else {
            const auto mpw = new MultiPlotWidget{
                data.mPoints,          data.mMaxPoints, data.mStepSize,
                data.mBackgroundColor, mViewSettings,   parent};
            mEditors.push_back(mpw);
            return mpw;
        }
    } else {
        return QStyledItemDelegate::createEditor(parent, option, index);
    }
}

void PlotDelegate::destroyEditor(QWidget *editor,
                                 const QModelIndex &index) const {
    // TODO: test how often this and createEditor and setEditorData are
    // called
    [[maybe_unused]] const auto nrElementsRemoved = erase(mEditors, editor);

    assert(nrElementsRemoved == 1);

    return QStyledItemDelegate::destroyEditor(editor, index);
}

const QList<QWidget *> &PlotDelegate::getEditors() const { return mEditors; }
