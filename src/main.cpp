/**
 * \file main.cpp
 * \brief Program startup
 */

#include "view/main_window/main_window.hpp"

#include <QApplication>
#include <QLocale>

#include <locale.h>

int main(int argc, char *argv[]) {
    // Qt uses system locale to display numbers in widgets, for a consistent
    // experience with the english interface we always use an english locale
    QLocale::setDefault(QLocale(QLocale::English, QLocale::UnitedKingdom));

    QApplication a(argc, argv);

    // Qt changes C locale on Unix/Linux, see
    // https://stackoverflow.com/q/25661295
    setlocale(LC_NUMERIC, "C");

    MainWindow window;

    return QApplication::exec();
}
