#include "task_model.hpp"

#include "data/data_manager.hpp"
#include "util/defaults.hpp"
#include "util/strings.hpp"

#include <QVariant>

#include <algorithm>
#include <cassert>
#include <cmath>
#include <limits>
#include <numeric>
#include <ranges>

namespace {
/**
 * \brief Given a sum of all achieved points in a task and the number of
 * students that participated in that task, returns the average percentage of
 * achieved points (0-100).
 *
 * If no student participated, returns NaN.
 * If the max is 0, returns 100%.
 */
[[nodiscard]] auto getAveragePointsPercentage(const Points pointSum,
                                              const int nrParticipatedStudents,
                                              const Points maxPoints) noexcept {

    if (nrParticipatedStudents == 0) {
        return std::numeric_limits<double>::quiet_NaN();

    } else {
        const auto avgPoints = pointSum / nrParticipatedStudents;

        // If maxPoints is zero, average is 100%
        return (maxPoints > Points{0} ? (avgPoints / maxPoints) : 1.) * 100.;
    }
}

/**
 * \brief Given a task pool, calculates for all item in that pool and for the
 * whole pool the average points in the given grading type.
 *
 * The returned vector contains first for each item in the pool the average
 * percentage of that item alone (0-100) and then the average percentage of the
 * pool as a whole.
 *
 * If the pool only contains a single item, return just a single average,
 * instead of two equal averages.
 */
[[nodiscard]] auto
getPoolAveragePointsPercentage(const TaskPool &pool,
                               const GradingType gradingType) noexcept {

    QList<double> results;

    auto poolPointSum = Points{0};
    auto nrParticipatedStudentsSum = 0;

    // Compute average for each item in the pool
    for (const auto &task : pool.mTasks) {
        // Sum up points in that item
        // Accuracy of reduce is fine here, because the result is displayed as
        // information and not used to calculate grades
        const auto pointSum = std::transform_reduce(
            task->mEarnedPoints[gradingType].begin(),
            task->mEarnedPoints[gradingType].end(), Points{0}, std::plus{},
            [](const std::optional<Points> &p) {
                return p.value_or(Points{0});
            });

        // Count students in that item
        const auto nrParticipatedStudents =
            static_cast<int>(task->mParticipatedStudents.count(true));

        // Only compute item averages if we have more than one, else only
        // compute a pool average.
        if (pool.mTasks.size() > 1) {
            results.push_back(
                getAveragePointsPercentage(pointSum, nrParticipatedStudents,
                                           task->mMaxPoints[gradingType]));
        }

        poolPointSum += pointSum;
        nrParticipatedStudentsSum += nrParticipatedStudents;
    }

    // Compute average over the whole pool
    results.push_back(getAveragePointsPercentage(
        poolPointSum, nrParticipatedStudentsSum, pool.mMaxPoints[gradingType]));

    return results;
}

/**
 * \brief Equivalent to \ref getPoolAveragePointsPercentage, but using standard
 * deviation instead of average.
 */
[[nodiscard]] auto
getStandardDeviation(const TaskPool &pool,
                     std::ranges::input_range auto &&avgPercentages,
                     const GradingType gradingType) noexcept {

    assert((pool.mTasks.size() == 1 && avgPercentages.size() == 1) ||
           static_cast<int>(avgPercentages.size()) == pool.mTasks.size() + 1);

    QList<double> results;

    auto squaredDiffSum = Points{0};
    auto nrParticipatedStudentsSum = 0;
    auto percentageIterator = avgPercentages.begin();

    // Compute standard deviation for each individual task
    for (const auto &task : pool.mTasks) {

        const auto &taskPoints = task->mEarnedPoints[gradingType];

        const auto mean =
            task->mMaxPoints[gradingType] * (*percentageIterator++) / 100.;

        const auto nrParticipatedStudents =
            static_cast<int>(task->mParticipatedStudents.count(true));

        // Accuracy is fine here, because the result is displayed as information
        // and not used to calculate grades
        const auto squaredDiff = std::transform_reduce(
            taskPoints.begin(), taskPoints.end(), Points{0}, std::plus{},
            [&mean](const std::optional<Points> &p) {
                if (p.has_value()) {
                    return Points{(*p - mean).p * (*p - mean).p};
                } else {
                    return Points{0};
                }
            });

        if (pool.mTasks.size() > 1) {
            if (nrParticipatedStudents == 0) {
                results.push_back(std::numeric_limits<double>::quiet_NaN());

            } else {
                const auto variance = squaredDiff / nrParticipatedStudents;

                results.push_back(std::sqrt(variance.p));
            }
        }

        squaredDiffSum += squaredDiff;
        nrParticipatedStudentsSum += nrParticipatedStudents;
    }

    // Compute standard deviation over the whole pool
    results.push_back(std::sqrt(squaredDiffSum.p / nrParticipatedStudentsSum));

    return results;
}
} // namespace

TaskModel::TaskModel(QObject *parent) : QAbstractTableModel(parent) {
    connect(&DataManager::getInstance(), &DataManager::examChanged, this,
            &TaskModel::examChanged);
}

int TaskModel::rowCount(const QModelIndex &parent) const {

    // When implementing a table based model, rowCount() should return 0 when
    // the parent is valid.
    if (parent.isValid()) {
        return 0;
    }

    return static_cast<int>(mTaskNames.size());
}

int TaskModel::columnCount(const QModelIndex &parent) const {

    // When implementing a table based model, columnCount() should return 0 when
    // the parent is valid.
    if (parent.isValid()) {
        return 0;
    }

    return TaskColumnIndex::NR_VALS;
}

QVariant TaskModel::data(const QModelIndex &index, const int role) const {

    if (!index.isValid()) {
        return {};
    }

    const auto row = index.row();
    const auto column = index.column();

    if (role == Qt::BackgroundRole) {

        if (mTaskColorIndices[row] == -1) {
            return QColorConstants::White;
        } else {
            return defaults::rwthColors::semiLightColors
                [static_cast<std::size_t>(mTaskColorIndices[row])];
        }
    } else if (role == Qt::TextAlignmentRole &&
               column == TaskColumnIndex::TaskIndex) {
        return Qt::AlignCenter;
    } else if (role != Qt::DisplayRole) {
        return {};
    }

    switch (column) {
    case TaskColumnIndex::TaskIndex:
        return row + 1;
    case TaskColumnIndex::TaskName:
        return mTaskNames[row];
    case TaskColumnIndex::TaskNameAvgSpread:
        return QString(str::ui::tasks::namePlusStats)
            .arg(mTaskNames[row])
            .arg(mAveragePercentage[row], 0, 'f', 2)
            .arg(mSpread[row], 0, 'f', 2);
    case TaskColumnIndex::Plot:
        // todo: this copies, test performance
        return QVariant::fromValue<TaskPlotData>(mPlotData[row]);
    case TaskColumnIndex::Avg:
        return mAveragePercentage[row];
    case TaskColumnIndex::Spread:
        return mSpread[row];
    case TaskColumnIndex::TaskType:
        return mTaskTypes[row];
    case TaskColumnIndex::GradingType:
        return static_cast<int>(mGradingTypes[row]);
    default:
        return {};
    }
}

QVariant TaskModel::headerData(const int section,
                               const Qt::Orientation orientation,
                               const int role) const {

    if (role == Qt::InitialSortOrderRole) {
        return Qt::AscendingOrder;
    } else if (role == Qt::DisplayRole) {
        switch (orientation) {
        case Qt::Horizontal:
            switch (section) {
            case TaskColumnIndex::TaskIndex:
                return str::ui::tasks::headerIndex;
            case TaskColumnIndex::TaskName:
                [[fallthrough]];
            case TaskColumnIndex::TaskNameAvgSpread:
                return str::ui::tasks::headerName;
            case TaskColumnIndex::Plot:
                return str::ui::tasks::headerPlot;
            case TaskColumnIndex::Avg:
                return str::ui::tasks::headerAvg;
            case TaskColumnIndex::Spread:
                return str::ui::tasks::headerSpread;
            case TaskColumnIndex::TaskType:
                return str::ui::tasks::headerTaskType;
            case TaskColumnIndex::GradingType:
                return str::ui::tasks::headerGradingType;
            default:
                assert(false);
                return {};
            }
        case Qt::Vertical:
            return {};
        default:
            assert(false);
            return "?";
        }
    } else {

        return {};
    }
}

void TaskModel::examChanged() {

    // this has to be called before internal data structures change
    beginResetModel();

    mTaskNames.clear();
    mAveragePercentage.clear();
    mSpread.clear();
    mTaskColorIndices.clear();
    mPlotData.clear();
    mTaskTypes.clear();
    mGradingTypes.clear();

    const auto cur = DataManager::getCurrent();

    if (cur) {
        auto colorIdx = 0;

        for (const auto &pool : cur->mExam.mTaskPools) {

            // Determine for which of closed and open to create a row (always
            // create a row for combined).
            // Special case:
            // If a task has 0 combined points in a mixed exam, create a
            // closed+open+combined row, because we don't know what kind of task
            // it is.
            using enum GradingType;
            QList<GradingType> potentialRows;
            if (cur->hasType(Combined)) {
                for (const auto g : {Closed, Open}) {
                    if (pool.mMaxPoints[g] != Points{0} ||
                        pool.mMaxPoints[Combined] == Points{0}) {
                        potentialRows.push_back(g);
                    }
                }
            } else if (cur->hasType(Closed)) {
                potentialRows = {Closed};
            } else {
                potentialRows = {Open};
            }
            potentialRows.push_back(Combined);

            // If there is only a single item in the task pool we add only a
            // single task for that item/pool to the table. Else we add a task
            // for each item and then one for the pool.
            const auto nrItemsInPool = pool.mTasks.size();
            const auto nrTasksToAdd =
                nrItemsInPool == 1 ? 1 : (nrItemsInPool + 1);

            // Task names
            {
                QList<QString> poolTaskNames;
                if (nrItemsInPool > 1) {
                    std::ranges::transform(
                        pool.mTasks, std::back_inserter(poolTaskNames),
                        [](Task *t) { return t->mTaskName; });
                }
                poolTaskNames.push_back(pool.constructTaskPoolName());
                for (int i = 0; i < potentialRows.size(); ++i) {
                    mTaskNames.append(poolTaskNames);
                }
            }

            // Color
            if (nrItemsInPool == 1) {
                mTaskColorIndices.append(QList<int>(potentialRows.size(), -1));
            } else {
                mTaskColorIndices.append(QList<int>(
                    nrTasksToAdd * potentialRows.size(), colorIdx++));
                colorIdx %= static_cast<int>(
                    defaults::rwthColors::semiLightColors.size());
                // TODO: use different fill pattern if colorIdx wrapped
            }

            // Average, spread, plot
            for (const auto gradingType : potentialRows) {

                // Average
                mAveragePercentage.append(
                    getPoolAveragePointsPercentage(pool, gradingType));

                // Spread
                mSpread.append(getStandardDeviation(
                    pool,
                    std::ranges::subrange{mAveragePercentage.end() -
                                              nrTasksToAdd,
                                          mAveragePercentage.end()},
                    gradingType));

                // Plot
                TaskPlotData plotData;
                plotData.mStepSize =
                    cur->mUserPointConfig.mGradeBoundaryStepSize;
                plotData.mMaxPoints = pool.mMaxPoints[gradingType].p;
                if (mTaskColorIndices.back() == -1) {
                    plotData.mBackgroundColor = QColorConstants::White;
                } else {
                    plotData.mBackgroundColor =
                        defaults::rwthColors::lightColors[static_cast<
                            std::size_t>(mTaskColorIndices.back())];
                }

                // Add points for each item
                if (nrItemsInPool > 1) {
                    for (const auto &task : pool.mTasks) {
                        mPlotData.push_back(plotData);

                        const auto pointList =
                            task->assignedEarnedPoints(gradingType);
                        std::ranges::transform(
                            pointList,
                            std::back_inserter(mPlotData.back().mPoints),
                            [](const Points &p) { return p.p; });
                        std::ranges::sort(mPlotData.back().mPoints);
                    }
                }

                // Add points for whole pool
                mPlotData.push_back(plotData);
                for (const auto &task : pool.mTasks) {
                    const auto pointList =
                        task->assignedEarnedPoints(gradingType);
                    std::ranges::transform(
                        pointList, std::back_inserter(mPlotData.back().mPoints),
                        [](const Points &p) { return p.p; });
                }

                std::ranges::sort(mPlotData.back().mPoints);

                // Task types, grading types
                if (nrItemsInPool == 1) {
                    mTaskTypes.push_back(TaskType::SingletonTaskItem);
                } else {
                    mTaskTypes.append(QList<TaskType::TaskType>(
                        nrItemsInPool, TaskType::TaskItem));
                    mTaskTypes.push_back(TaskType::TaskPool);
                }
                mGradingTypes.append(
                    QList<GradingType>(nrTasksToAdd, gradingType));
            }
        }

        assert(mTaskNames.size() == mTaskColorIndices.size());
        assert(mTaskNames.size() == mAveragePercentage.size());
        assert(mTaskNames.size() == mSpread.size());
        assert(mTaskNames.size() == mPlotData.size());
        assert(mTaskNames.size() == mTaskTypes.size());
        assert(mTaskNames.size() == mGradingTypes.size());
    }

    endResetModel();
}
