#pragma once

#include <QList>
#include <QSharedPointer>
#include <QString>

#include <variant>

class QWidget;
enum class FileSelectState;
enum class FileType;
struct BonusPoints;
struct DynexiteExam;
struct ExamSave;
struct StudentData;

enum class FileOpenErrorCode {
    Success,
    OsError,
    CsvParseError,
    D2rParseError,
    UnknownError
};

struct FileOpenError {
    FileOpenErrorCode mEc = FileOpenErrorCode::Success;
    QString mDetails{};

    QList<QString> mWarnings{};
};

class InputModel {
public:
    /**
     * \brief Construct a model that controls file loading for the given file
     * type.
     */
    explicit InputModel(FileType t);

    /**
     * \brief Validates that the user selected a correct file and stores the
     * content in the appropriate data structure in `mLoadedData`.
     *
     * \param path Path to the file
     * \param parent Widget for which potential warnings are shown
     *
     * \return Success
     */
    FileOpenError loadCsv(const QString &path, QWidget *parent);

    /**
     * \brief Which file type this model stores.
     */
    const FileType mType;

    /**
     * \brief Moves ownership of the stored data to the caller, is empty
     * afterwards.
     * \return The stored data.
     */
    [[nodiscard]] auto get() { return std::move(mLoadedData); }

    /**
     * \brief Returns a plain pointer to the data stored in this model.
     *
     * \warning Data of type T must be currently stored in this model, or this
     * function will throw
     */
    template <typename T>
    [[nodiscard]] T *read() const {

        // First, convert the variant mLoadedData to the QSharedPointer
        // contained inside it.
        // Then, extract the raw pointer from the QSharedPointer with the member
        // function .get().
        return std::get<QSharedPointer<T>>(mLoadedData).get();
    }

private:
    std::variant<QSharedPointer<DynexiteExam>, QSharedPointer<StudentData>,
                 QSharedPointer<BonusPoints>, QSharedPointer<ExamSave>>
        mLoadedData{};
};

// Display a warning pop-up if too low an amount of students in the dynexite
// file are also in the RWTHonline file
void warnIfLowStudentIntersection(QWidget *parent,
                                  const InputModel &dynexiteModel,
                                  const InputModel &rwthModel,
                                  const InputModel &bonusModel,
                                  const FileSelectState &bonusState) noexcept;

/**
 * \brief Display a warning if the number of bonus points is higher than is
 * allowed given the maximum exam points
 *
 * \return True if there are too many bonus points and the warning was shown.
 */
bool warnIfTooManyBonusPoints(QWidget *parent, const InputModel &dynexiteModel,
                              const InputModel &bonusModel) noexcept;
