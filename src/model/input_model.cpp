#include "input_model.hpp"

#include "data/nodes/bonus_points.hpp"
#include "data/nodes/dynexite_exam.hpp"
#include "data/nodes/student_data.hpp"
#include "file_io/csv_parser.hpp"
#include "file_io/d2r_parser.hpp"
#include "file_io/file.hpp"
#include "util/defaults.hpp"
#include "util/enums.hpp"
#include "util/numeric.hpp"
#include "util/strings.hpp"

#include <QMessageBox>

#include <algorithm>
#include <cassert>

InputModel::InputModel(const FileType t) : mType(t) {}

FileOpenError InputModel::loadCsv(const QString &path, QWidget *parent) {

    // We could move this to a separate thread to keep the interface
    // responsive, but it only takes 1ms on my machine, so let's not
    // bother.
    try {
        FileOpenError error;
        const auto fileContent = readFileIntoString(path);

        if (mType == FileType::d2r) {

            mLoadedData.emplace<QSharedPointer<ExamSave>>(
                parseD2r(fileContent, parent));

        } else {
            const auto dump = parseCsv(path, fileContent);

            switch (mType) {
            default:
                assert(false);
                [[fallthrough]];
            case FileType::dynexite:
                mLoadedData.emplace<QSharedPointer<DynexiteExam>>(
                    new DynexiteExam(*dump));
                break;
            case FileType::rwth:
                mLoadedData.emplace<QSharedPointer<StudentData>>(
                    new StudentData(*dump));
                {
                    const auto &courseTitles =
                        std::get<QSharedPointer<StudentData>>(mLoadedData)
                            ->mCourseTitles;
                    if (courseTitles.size() > 1) {
                        error.mWarnings.append(
                            QString(str::error::multipleCourseTitles)
                                .arg(courseTitles.join("; ")));
                    }
                    const auto &semesters =
                        std::get<QSharedPointer<StudentData>>(mLoadedData)
                            ->mSemesters;
                    if (semesters.size() > 1) {
                        error.mWarnings.append(
                            QString(str::error::multipleSemesters)
                                .arg(semesters.join("; ")));
                    }
                }
                break;
            case FileType::bonus:
                mLoadedData.emplace<QSharedPointer<BonusPoints>>(
                    new BonusPoints(*dump));
                {
                    const auto &invalidRegNrs =
                        std::get<QSharedPointer<BonusPoints>>(mLoadedData)
                            ->mInvalidRegNrs;

                    for (auto i = invalidRegNrs.cbegin(),
                              end = invalidRegNrs.cend();
                         i != end; ++i) {
                        error.mWarnings.append(
                            QString(str::error::addBonusPointsFailed)
                                .arg(i.value(), i.key()));
                    }
                    break;
                }
            }
        }

        return error;
    } catch (const std::exception &e) {
        switch (mType) {
        default:
            assert(false);
            [[fallthrough]];
        case FileType::dynexite:
            return FileOpenError{FileOpenErrorCode::UnknownError,
                                 QString(str::error::defaultDynexite) +
                                     e.what()};
        case FileType::rwth:
            return FileOpenError{FileOpenErrorCode::UnknownError,
                                 QString(str::error::defaultRwthOnline) +
                                     e.what()};
        case FileType::bonus:
            return FileOpenError{FileOpenErrorCode::UnknownError,
                                 QString(str::error::defaultBonus) + e.what()};
        case FileType::d2r:
            return FileOpenError{FileOpenErrorCode::UnknownError,
                                 QString(str::error::defaultD2R) + e.what()};
        }
    }
}

void warnIfLowStudentIntersection(QWidget *parent,
                                  const InputModel &dynexiteModel,
                                  const InputModel &rwthModel,
                                  const InputModel &bonusModel,
                                  const FileSelectState &bonusState) noexcept {
    auto dynexiteStudents = dynexiteModel.read<DynexiteExam>()->mIdentifier;
    auto rwthOnlineStudents = rwthModel.read<StudentData>()->mRegistrationNr;
    // convert map to list of keys
    auto bonusStudents =
        bonusState == FileSelectState::ok
            ? bonusModel.read<BonusPoints>()->mTotalBonusPointsPerStudent.keys()
            : QList<QString>{};

    std::ranges::sort(dynexiteStudents);
    std::ranges::sort(rwthOnlineStudents);
    std::ranges::sort(bonusStudents);

    QList<QString> dbIntersection, drIntersection;
    dbIntersection.reserve(
        std::min(dynexiteStudents.size(), bonusStudents.size()));
    drIntersection.reserve(
        std::min(dynexiteStudents.size(), rwthOnlineStudents.size()));

    // Find out number of students that are both in the dynexite file and the
    // RWTHonline/bonus file
    std::ranges::set_intersection(dynexiteStudents, bonusStudents,
                                  std::back_inserter(dbIntersection));
    std::ranges::set_intersection(dynexiteStudents, rwthOnlineStudents,
                                  std::back_inserter(drIntersection));

    QList<QString> singleWarnings;

    // Display warning if few students in dynexite are also in RWTHonline
    if (static_cast<double>(drIntersection.size()) <
        defaults::studentIntersectWarningThreshold *
            static_cast<double>(dynexiteStudents.size())) {
        const auto percentage = static_cast<double>(drIntersection.size()) /
                                static_cast<double>(dynexiteStudents.size()) *
                                100.;

        if (percentage > 0.) {
            singleWarnings.append(
                QString(str::error::lowStudentIntersectionFewRwth)
                    .arg(percentage, 0, 'f', 2)
                    .arg(drIntersection.size())
                    .arg(dynexiteStudents.size()));
        } else {
            // Special string for no intersection
            singleWarnings.append(str::error::lowStudentIntersectionNoneRwth);
        }
    }

    // Display warning if few students in dynexite are also in the bonus point
    // file, but ignore this if no students are in the bonus point file since it
    // is optional
    if (!bonusStudents.empty() &&
        static_cast<double>(dbIntersection.size()) <
            defaults::studentIntersectWarningThreshold *
                static_cast<double>(dynexiteStudents.size())) {
        const auto percentage = static_cast<double>(dbIntersection.size()) /
                                static_cast<double>(dynexiteStudents.size()) *
                                100.;

        if (percentage > 0.) {
            singleWarnings.append(
                QString(str::error::lowStudentIntersectionFewBonus)
                    .arg(percentage, 0, 'f', 2)
                    .arg(dbIntersection.size())
                    .arg(dynexiteStudents.size()));
        } else {
            singleWarnings.append(
                QString(str::error::lowStudentIntersectionNoneBonus));
        }
    }

    // Display all accumulated warnings with a standard prefix
    if (!singleWarnings.empty()) {
        QString warning = str::error::lowStudentIntersection;
        for (const auto &s : singleWarnings) {
            warning += s;
        }
        QMessageBox::warning(parent, str::title::readingFileWarning, warning);
    }
}

bool warnIfTooManyBonusPoints(QWidget *parent, const InputModel &dynexiteModel,
                              const InputModel &bonusModel) noexcept {

    // Find largest number of bonus points
    // There is no info on what the largest possible number of bonus points was,
    // so we have to be satisfied with the largest number of actually achieved
    // bonus points.
    const auto bonusStudents = bonusModel.read<BonusPoints>();
    if (bonusStudents->mTotalBonusPointsPerStudent.empty()) {
        return false;
    }
    const auto largestNrBonusPoints = bonusStudents->mMaxBonusPoints;

    // Get the maximum (combined) points to be achieved
    const auto maxCombinedPoints = dynexiteModel.read<DynexiteExam>()
                                       ->mPointsMaxRaw[GradingType::Combined];

    if (definitelyGreaterThan(largestNrBonusPoints,
                              maxCombinedPoints *
                                  defaults::maxRatioBonusPoints)) {
        QMessageBox::critical(
            parent, str::title::tooManyBonusPoints,
            QString(str::error::tooManyBonusPointsInfo)
                .arg(largestNrBonusPoints.toStringMaxPrecision(),
                     maxCombinedPoints.toStringMaxPrecision())
                .arg(largestNrBonusPoints.p / maxCombinedPoints.p * 100, 0, 'f',
                     2)
                .arg(defaults::maxRatioBonusPoints * 100));
        return true;
    }
    return false;
}
