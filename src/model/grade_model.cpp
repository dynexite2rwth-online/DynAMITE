#include "grade_model.hpp"

#include "data/data_manager.hpp"
#include "util/grade.hpp"

#include <array>
#include <cassert>

GradeModel::GradeModel(const GradingType gradingType, QObject *parent)
    : QAbstractListModel(parent), mGradeCounts(11), mGradingType(gradingType) {}

void GradeModel::updateData() {
    // reset grade counts to zero
    mGradeCounts = QList<int>(11);

    if (const auto cur = DataManager::getCurrent()) {

        // count how many students have each respective grade

        const auto &gradeList = cur->getGradeList(mGradingType);

        for (const auto &g : gradeList) {
            if (static_cast<int>(g) < mGradeCounts.size())
                ++mGradeCounts[static_cast<int>(g)];
            else
                assert(false);
        }
    }
}

int GradeModel::rowCount(const QModelIndex &parent) const {
    if (parent.isValid())
        return 0;
    return 11;
}

QVariant GradeModel::data(const QModelIndex &index,
                          [[maybe_unused]] int role) const {
    // ignore role for now
    if (index.column() >= 1 || index.row() >= 11)
        return {};
    return mGradeCounts[index.row()];
}

inline constexpr std::array gradeStrings{"1.0", "1.3", "1.7", "2.0",
                                         "2.3", "2.7", "3.0", "3.3",
                                         "3.7", "4.0", "5.0"};

QVariant GradeModel::headerData(const int section,
                                const Qt::Orientation orientation,
                                const int role) const {

    if (orientation == Qt::Orientation::Horizontal) {
        return QAbstractListModel::headerData(section, orientation, role);
    } else {
        if (section < 0 || section >= 11 || role != Qt::DisplayRole)
            return QAbstractListModel::headerData(section, orientation, role);
        return gradeStrings[static_cast<std::size_t>(section)];
    }
}

bool GradeModel::setData(const QModelIndex &index, const QVariant &value,
                         const int role) {
    if (role != Qt::EditRole)
        return false;

    if (index.column() >= 1 || index.row() >= 11)
        return false;

    bool conversionSuccess = false;
    const auto intValue = value.toInt(&conversionSuccess);
    if (conversionSuccess) {

        mGradeCounts[index.row()] = intValue;
        Q_EMIT dataChanged(index, index);
        return true;
    } else {
        return false;
    }
}

Qt::ItemFlags GradeModel::flags(const QModelIndex &) const {
    return Qt::ItemIsEditable | Qt::ItemIsEnabled;
}
