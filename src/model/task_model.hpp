#pragma once

#include "data/task_plot_data.hpp"
#include "util/enums.hpp"

#include <QAbstractTableModel>
#include <QColor>
#include <QList>

namespace TaskType {
enum TaskType {
    TaskItem,
    TaskPool,
    SingletonTaskItem, ///< A task pool containing just a single item
};
}

// Columns of the task tab table
namespace TaskColumnIndex {
enum TaskColumnIndex {
    TaskIndex,
    TaskName,
    TaskNameAvgSpread,
    Plot,
    Avg,
    Spread,
    TaskType,
    GradingType,
    NR_VALS
};
}

class TaskModel : public QAbstractTableModel {
    Q_OBJECT

public:
    explicit TaskModel(QObject *parent);

    [[nodiscard]] int
    rowCount(const QModelIndex &parent = QModelIndex()) const override;
    [[nodiscard]] int
    columnCount(const QModelIndex &parent = QModelIndex()) const override;

    [[nodiscard]] QVariant data(const QModelIndex &index,
                                int role = Qt::DisplayRole) const override;

    [[nodiscard]] QVariant
    headerData(int section, Qt::Orientation orientation,
               int role = Qt::DisplayRole) const override;

public Q_SLOTS:
    /**
     * \brief New tasks.
     */
    void examChanged();

    // TODO: slot task elimination changed

private:
    // We must duplicate the data here, because we can change it only after
    // beginResetModel or similar has been called
    QList<int> mTaskColorIndices;
    QList<QString> mTaskNames;
    QList<qreal> mAveragePercentage;
    QList<qreal> mSpread;
    QList<TaskPlotData> mPlotData;
    QList<TaskType::TaskType> mTaskTypes;
    QList<GradingType> mGradingTypes;
};
