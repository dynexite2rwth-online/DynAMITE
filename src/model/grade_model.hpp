#pragma once

#include "util/enums.hpp"

#include <QAbstractListModel>
#include <QList>

// todo: why is this a model and not part of data?
class GradeModel : public QAbstractListModel {
    Q_OBJECT
public:
    // todo: implement with non-split-grade-configs
    explicit GradeModel(GradingType gradingType, QObject *parent = nullptr);

    // todo: problem if multiple views update one model
    void updateData();

    [[nodiscard]] int
    rowCount(const QModelIndex &parent = QModelIndex()) const override;

    [[nodiscard]] QVariant data(const QModelIndex &index,
                                int role = Qt::DisplayRole) const override;

    [[nodiscard]] QVariant
    headerData(int section, Qt::Orientation orientation,
               int role = Qt::DisplayRole) const override;

    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    [[nodiscard]] Qt::ItemFlags flags(const QModelIndex &index) const override;

public:
    QList<int> mGradeCounts;
    GradingType mGradingType;
};
