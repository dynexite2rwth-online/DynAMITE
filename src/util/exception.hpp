#pragma once

#include <QString>

#include <stdexcept>

/**
 * \brief In our custom exception we use QString instead of std::string in the
 * constructor for convenience
 */
struct DynamiteException : std::runtime_error {
    explicit DynamiteException(const QString &message) noexcept
        : std::runtime_error(message.toStdString()) {}
};
