#pragma once

#include "enums.hpp"
#include "grade_boundary_step.hpp"
#include "points.hpp"

#include <QList>
#include <QPair>
#include <QtGlobal>

#include <cmath>
#include <ranges>

class QString;
struct DynexiteExam;

/**
 * \brief Try to guess the smallest point step for this exam.
 *
 * Reads through all point values in the given exam and returns the smallest
 * point step found in them. The actually smallest used point step might be
 * smaller, but just never happened to show up. For any found point step < 0.25,
 * 0.25 is returned.
 *
 * \param exam Exam from which to grab the point values.
 * \return An upper bound on the smallest point step.
 */
[[nodiscard]] GradeBoundaryStepSize
guessGradeBoundaryStepSize(const DynexiteExam &exam) noexcept;

// Guess by the left-most header what file type this is
[[nodiscard]] FileType getApproximateFileType(const QString &s) noexcept;

/**
 * \brief Returns a "good" maximum and step size for a y axis with the given
 * parameters.
 *
 * \param maxBarHeight Maximum value of any bar in the graph. Minimum value is
 * assumed to be 0.
 * \param maxNrYLabels How many labels fit on the y axis at most, including min
 * and max.
 */
[[nodiscard]] QPair<int, int> niceYAxis(int maxBarHeight,
                                        int maxNrYLabels) noexcept;

//-----------------------------------------------------------------------
// IMPLEMENTATION
//-----------------------------------------------------------------------

namespace impl {
/**
 * \brief Returns the guessed step size for the points in the range, e.g. 1 if
 * all point-values are integers.
 */
[[nodiscard]] constexpr double
minStepSize(std::ranges::input_range auto &&range) noexcept {
    double minStep = 1.;
    for (const Points p : range) {
        if (qFuzzyIsNull(std::fmod(p.p, 1.)))
            continue;
        else if (qFuzzyIsNull(std::fmod(p.p, 0.5)))
            minStep = qMin(minStep, 0.5);
        else if (qFuzzyIsNull(std::fmod(p.p, 0.25)))
            minStep = qMin(minStep, 0.25);
        else
            minStep = qMin(minStep, std::fmod(p.p, 0.25));
    }
    return minStep;
}

[[nodiscard]] GradeBoundaryStepSize
guessGradeBoundaryStepSize(const QList<Points> &allPointValues) noexcept;
} // namespace impl
