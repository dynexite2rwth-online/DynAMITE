#pragma once

/**
 * \file enums.hpp
 * \brief Declarations of various universally useful enums.
 */

/**
 * \brief Whether something refers to closed points/grades (from multiple
 * choice), open points/grades (from input fields) or points/grades that are
 * calculated from both.
 */
enum class GradingType { Closed, Open, Combined, NR_VALS };

[[nodiscard]] constexpr const char *toString(const GradingType e) noexcept {
    switch (e) {
    case GradingType::Closed:
        return "Closed";
    case GradingType::Open:
        return "Open";
    case GradingType::Combined:
        return "Resulting";
    default:
        return "unknown";
    }
}

/**
 * \brief States each file selection can be in.
 */
enum class FileSelectState {

    empty,
    error,
    ok,
    NR_VALS
};

/**
 * \brief Different types of input files.
 */
enum class FileType {

    dynexite, /**< dynexite exam result. */
    rwth,     /**< RWTHonline student information. */
    bonus,    /**< eTest bonus points. */
    d2r,      /**< Custom file format that includes exam-, student- and
                 grade-boundary-information. */
    NR_VALS   /**< Total number of files to be imported (keep at the end).
               */
};

enum class TaskEliminationType { notEliminated, withdrawTask, creditFull };
