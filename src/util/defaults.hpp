/**
 * \file defaults.hpp
 * \brief Constant values used throughout the app.
 */

#pragma once

#include "grade_boundary_step.hpp"
#include "storage_helper.hpp"

#include <QColor>

#include <array>

namespace defaults {

/** \brief Whether to show an option to change grade boundary rounding. */
inline constexpr bool showRoundingSetting = false;

/** \brief Default grade boundary rounding  */
inline constexpr GradeBoundaryRounding boundaryRounding =
    GradeBoundaryRounding::Down;

/** \brief Whether to show an option to require passing the closed and open part
 * to pass the exam.
 *
 * The updated grading guidelines (march 2023) deem this not permissible.
 */
inline constexpr bool showRequireBothPartsSetting = false;

/** \brief Require passing both open and closed part to pass the exam.
 *
 * The updated grading guidelines (march 2023) deem this not permissible
 */
inline constexpr bool requirePassAllParts = false;

/**
 * \brief Default value for the setting "Credit bonus points even if the exam
 * (without bonus points) is assessed as "failed" (5.0)"
 */
inline constexpr bool passDueToBonus = true;

/** \brief Default grade boundary step size  */
inline constexpr GradeBoundaryStepSize boundaryStepSize =
    GradeBoundaryStepSize::Min1_00;

/** \brief Apply the rules from the 17th (?) revised version of the general
 * examination regulations, in particular no longer grade open and closed points
 * differently */
inline constexpr bool newGeneralExaminationRegulations = true;

//-----------------------------------------------------------------------

/** \brief Count students with attempt nr 0 as if it were their first attempt
 *
 * Confirmed to be always false after servicedesk-ticket [#20230630-0128].
 */
inline constexpr bool countZerothAttemptAsFirstAttempt = false;

//-----------------------------------------------------------------------

/**
 * \brief Epsilon (multiplied by max points) for comparison of sum of earned
 * points vs dynexite-reported sum of earned points.
 *
 * Rationale:
 * https://git.rwth-aachen.de/dynexite2rwth-online/DynAMITE/-/issues/185#note_2471163
 */
inline constexpr double importEpsilon = 0.001;

//-----------------------------------------------------------------------

/**
 * \brief Number of decimal digits to which the final \ref Points value of each
 * student is rounded. When printed outside of error messages, this many digits
 * are printed but trailing zeroes are removed.
 */
inline constexpr int pointsDecimalDigits = 2;

/**
 * \brief Number of decimal digits displayed when a point average or similar is
 * printed.
 */
inline constexpr int pointsHigherDecimalDigits = 4;

//-----------------------------------------------------------------------

/** \brief Percentage of window width of the grade tab, which is given to the
 * grade histogram. Between 0-100. */
inline constexpr int gradeTabHistogramWidthPercentage = 70;

//-----------------------------------------------------------------------

/** \brief Maximum selectable open points passing limit. */
inline constexpr double openPointsPassingLimit = 0.5;

/** \brief Starting/Default value for the open points passing limit. */
inline constexpr double openPointsStartingPassingLimit = 0.5;

/** \brief The maximum amount of awarded bonus points may not be larger than
 * this value times total points in the exam. */
inline constexpr double maxRatioBonusPoints = 0.2;

//-----------------------------------------------------------------------

/** \brief Minimum settable point histogram bucket size. */
inline constexpr double minBucketSize = 0.25;
/** \brief Maximum settable point histogram bucket size. */
inline constexpr double maxBucketSize = 5.;
/** \brief Point histogram bucket size selector step size. */
inline constexpr double bucketStep = 0.25;

//-----------------------------------------------------------------------

/** \brief The factor by which the small font size is smaller than the large
 * font size. The small font size is never smaller than the default font size.*/
inline constexpr double smallFontSizeRatio = 0.5;

/**
 * The y axis max of a chart should not be exactly the maximum bar size of that
 * chart, because the upper border of the bar can be cropped off. Therefore we
 * scale the max axis value with this constant.
 */
inline constexpr qreal yAxisRangeScaling = 1.01;

//-----------------------------------------------------------------------

/** \brief If less than this share of dynexite students are in the RWTHonline or
 * bonus point file, issue a warning */
inline constexpr double studentIntersectWarningThreshold = 0.5;

//-----------------------------------------------------------------------

enum class BoxPlotWhiskers {
    Percentile,
    Iqr,
    MinMax,
};

/** \brief Select with which method box plot whiskers will be calculated. */
inline constexpr BoxPlotWhiskers boxPlotWhiskersMethod =
    BoxPlotWhiskers::Percentile;

/** \brief In the case that the box plot whisker method is "Percentile", choose
 * that percentile (0\<p\<1) for the lower extreme. A sensible value (from the
 * seven-number summary) is 0.0887. */
inline constexpr double boxPlotWhiskerPercentile = 0.0887;

//-----------------------------------------------------------------------

namespace rwthColors {
inline constexpr auto blue100 = QColor{0, 84, 159};
inline constexpr auto blue75 = QColor{64, 127, 183};
inline constexpr auto black = QColor{0, 0, 0};
inline constexpr auto yellow = QColor{255, 237, 0};
inline constexpr auto green = QColor{87, 171, 39};
inline constexpr auto mayGreen = QColor{189, 205, 0};
inline constexpr auto orange = QColor{246, 168, 0};
inline constexpr auto red = QColor{204, 7, 30};

// Light (10%) RWTH colors in decreasing pleasantness
inline constexpr std::array lightColors = {
    QColor{232, 241, 250} /*blue*/,
    QColor{255, 247, 234} /*orange*/,
    QColor{245, 232, 229} /*bordeaux*/,
    QColor{242, 247, 236} /*green*/,
    QColor{236, 237, 237} /*black*/,
    QColor{253, 238, 240} /*magenta*/,
    QColor{255, 253, 238} /*yellow*/,
    QColor{235, 246, 246} /*turquoise*/,
    QColor{242, 240, 247} /*purple*/,
    QColor{249, 250, 237} /*may green*/,
    QColor{237, 229, 234} /*violet*/,
    QColor{250, 235, 227} /*red*/,
    QColor{230, 236, 236} /*petrol*/
};
// 25% RWTH colors in decreasing pleasantness
inline constexpr std::array semiLightColors = {
    QColor{199, 221, 242} /*blue*/,
    QColor{254, 234, 201} /*orange*/,
    QColor{229, 197, 192} /*bordeaux*/,
    QColor{221, 235, 206} /*green*/,
    QColor{207, 209, 210} /*black*/,
    QColor{249, 210, 218} /*magenta*/,
    QColor{255, 250, 209} /*yellow*/,
    QColor{202, 231, 231} /*turquoise*/,
    QColor{222, 218, 235} /*purple*/,
    QColor{240, 243, 208} /*may green*/,
    QColor{210, 192, 205} /*violet*/,
    QColor{243, 205, 187} /*red*/,
    QColor{191, 208, 209} /*petrol*/
};

} // namespace rwthColors

/**
 * \brief Colors used in the app for the grading types open/closed/combined
 */
inline constexpr EnumStorage<QColor> gradingTypeColors = {
    rwthColors::orange,  /* Closed */
    rwthColors::blue75,  /* Open */
    rwthColors::mayGreen /* Combined */
};
inline constexpr auto notPassedColor = rwthColors::red;
inline constexpr auto passedColor = rwthColors::green;
inline constexpr auto passedColor1_0 = rwthColors::mayGreen;

} // namespace defaults
