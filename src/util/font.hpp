#pragma once

#include <QtGlobal>

#include <compare>

class QFont;
class QDebug;

enum class FontSizeUnit { Pixel, Point };

struct FontSize {
    qreal size;
    FontSizeUnit unit;

    [[nodiscard]] qreal getPixelSize() const noexcept;

    std::partial_ordering operator<=>(const FontSize &that) const;

    template <typename T>
    constexpr FontSize &operator*=(const T &rhs) noexcept {
        size *= rhs;
        return *this;
    }
};

template <typename T>
[[nodiscard]] constexpr FontSize operator*(FontSize lhs,
                                           const T &rhs) noexcept {
    lhs *= rhs;
    return lhs;
}

QDebug operator<<(QDebug debug, const FontSize &object);

// A point is 1/72 of an inch
[[nodiscard]] constexpr double ptToPx(const double pt,
                                      const double dpi) noexcept {
    return pt / 72 * dpi;
}

[[nodiscard]] constexpr double pxToPt(const double px,
                                      const double dpi) noexcept {
    return px * 72 / dpi;
}

void setFontSize(QFont &f, FontSize size) noexcept;
