#pragma once

#include "grade_boundary_step.hpp"
#include "numeric.hpp"
#include "points.hpp"

#include <compare>

class QString;

/**
 * \brief Represents the points required for a grade.
 *
 * Points required for a grade (= grade boundaries) should not be placed at
 * arbitrary decimal numbers (like 3.14159), but at multiples of the chosen step
 * size, e.g. only whole numbers. This class stores that multiple in mMultiple
 * for type-safe use.
 */
struct GradeBoundary {

    constexpr GradeBoundary() = default;

    /**
     * \brief Constructs a GradeBoundary object with the given parameters.
     *
     * mMultiple will be set to the largest integer that, if multiplied with
     * stepSize, is not definitively larger than p.
     */
    GradeBoundary(Points p, GradeBoundaryStepSize stepSize,
                  GradeBoundaryRounding roundingMode) noexcept;

    constexpr explicit GradeBoundary(const int multiple) noexcept
        : mMultiple(multiple) {}

    [[nodiscard]] auto
    operator<=>(const GradeBoundary &) const noexcept = default;

    /**
     * \brief Creates a QString from a grade boundary.
     *
     * Since a grade boundary is always a multiple of 1, 0.5 or 0.25, it is
     * always printed with full precision and no trailing zeroes.
     */
    [[nodiscard]] QString
    toString(GradeBoundaryStepSize stepSize) const noexcept;

    int mMultiple = 0;
};

[[nodiscard]] constexpr Points
toPoints(const GradeBoundary b, const GradeBoundaryStepSize stepSize) noexcept {
    return Points{static_cast<double>(b.mMultiple) * as<double>(stepSize)};
}

[[nodiscard]] inline bool
boundaryGreater(const GradeBoundary &b, const Points p,
                const GradeBoundaryStepSize stepSize) noexcept {
    return definitelyGreaterThan(toPoints(b, stepSize), p);
}

[[nodiscard]] constexpr double operator/(const GradeBoundary &a,
                                         const GradeBoundary &b) noexcept {
    return static_cast<double>(a.mMultiple) / static_cast<double>(b.mMultiple);
}
[[nodiscard]] constexpr GradeBoundary
operator+(const GradeBoundary &a, const GradeBoundary &b) noexcept {
    return GradeBoundary{a.mMultiple + b.mMultiple};
}

/**
 * \brief Rounds the given number to the nearest integer based on the specified
 * rounding mode.
 *
 * If the number is only a rounding error away from an integer, returns that
 * integer. Otherwise, rounds the number according to the specified rounding
 * mode.
 */
[[nodiscard]] GradeBoundary
roundMultiple(double nrMultiple, GradeBoundaryRounding roundingMode) noexcept;

/**
 * \brief Simplify grade boundary construction (only provide stepSize and
 * roundingMode once)
 */
struct GradeBoundaryFactory {
    GradeBoundaryFactory(GradeBoundaryStepSize stepSize,
                         GradeBoundaryRounding roundingMode) noexcept;

    [[nodiscard]] GradeBoundary operator()(Points p) const noexcept;

    GradeBoundaryStepSize mStepSize;
    GradeBoundaryRounding mRoundingMode;
};
