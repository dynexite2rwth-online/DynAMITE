#pragma once

/**
 * \file grade.hpp
 * \brief Declarations of the grade enum and related useful helper functions.
 */

#include "enums.hpp"

#include <QChar>

#include <array>
#include <cassert>
#include <iosfwd>

class QString;

/**
 * \brief Represents the grade in the value name.
 *
 * E.g. `grade1_7` = 1.7
 *
 * Don't change the order or the integer value of the enumerators, because a lot
 * of functions depend on those!
 *
 * Also, as for all enums, keep the `NR_VALS` at the end.
 *
 * When comparing grades, the better grade is the smaller one!
 */
enum class Grade {
    grade1_0,
    grade1_3,
    grade1_7,
    grade2_0,
    grade2_3,
    grade2_7,
    grade3_0,
    grade3_3,
    grade3_7,
    grade4_0,
    grade5_0,
    NR_VALS
};

/**
 * \brief Returns the previous (better) grade.
 */
[[nodiscard]] constexpr Grade previousGrade(const Grade g) noexcept {
    if (g > Grade::grade5_0 || g == Grade::grade1_0) {
        assert(false);
        return g;
    }
    return static_cast<Grade>(static_cast<int>(g) - 1);
}

/**
 * \brief Returns the next (worse) grade.
 */
[[nodiscard]] constexpr Grade nextGrade(const Grade g) noexcept {
    if (g > Grade::grade5_0) {
        assert(false);
        return g;
    }
    return static_cast<Grade>(static_cast<int>(g) + 1);
}

/**
 * \brief Integer representations for all grade values with correct ratios.
 *
 * Calculated as 10*grade.
 *
 * Note that the grade 1.3 is not 1.333..., but just 1.3.
 */
inline constexpr auto gradeIntValues =
    std::to_array({10, 13, 17, 20, 23, 27, 30, 33, 37, 40, 50});

inline constexpr auto grades = std::to_array(
    {Grade::grade1_0, Grade::grade1_3, Grade::grade1_7, Grade::grade2_0,
     Grade::grade2_3, Grade::grade2_7, Grade::grade3_0, Grade::grade3_3,
     Grade::grade3_7, Grade::grade4_0, Grade::grade5_0});

/**
 * \brief Floating point representations for all grades.
 */
inline constexpr auto gradeDoubles =
    std::to_array({1.0, 1.3, 1.7, 2.0, 2.3, 2.7, 3.0, 3.3, 3.7, 4.0, 5.0});

[[nodiscard]] QString toString(Grade g, QChar separator = '.') noexcept;

std::ostream &operator<<(std::ostream &os, const Grade &g);

[[nodiscard]] constexpr auto gradeToInt(const Grade g) noexcept {
    if (static_cast<unsigned>(g) >= gradeIntValues.size()) {
        assert(false);
        return 50;
    }

    return gradeIntValues[static_cast<decltype(gradeIntValues)::size_type>(g)];
}

[[nodiscard]] constexpr double gradeToDouble(const Grade g) noexcept {

    if (static_cast<unsigned>(g) >= gradeDoubles.size()) {
        assert(false);
        return 5.0;
    }

    return gradeDoubles[static_cast<decltype(gradeDoubles)::size_type>(g)];
}

/**
 * \brief Returns if the user is allowed to change the grade boundary for the
 * given grade.
 *
 * Some grade boundaries are not changeable / not changeable by the user:
 * - The grade boundary for an invalid grade (`NR_VALS`) is invalid
 * - There is no grade boundary for 5.0 since it is the worst grade (or the
 * grade boundary is -inf)
 * - Some closed grade boundaries are fixed in place in accordance to the
 * guidelines, see par. 10 (5) GER, par. 13 (5) GER LAB, and par. 13 (5) GER
 * M.Ed. 2017.
 *
 * \return True if the user is allowed to change the grade boundary.
 */
[[nodiscard]] constexpr bool
gradeBoundaryIsUserChangeable(const GradingType type, const Grade g) noexcept {
    using enum Grade;

    // 5.0 and invalid are not user changeable grade boundaries
    if (static_cast<unsigned>(g) > static_cast<unsigned>(grade4_0))
        return false;

    // The closed grade boundaries for 1.3, 2.3 and 3.3 are fixed
    return !(type == GradingType::Closed &&
             (g == grade1_3 || g == grade2_3 || g == grade3_3));
}

/**
 * \brief Computes the average of a closed grade with the maximum points to be
 * achieved in the closed section and an open grade with the maximum points to
 * be achieved in the open section.
 *
 * If closedMax or openMax is zero, then the corresponding grade is ignored.
 *
 * \note The guidelines demand a rounding to the closest grade, we have decided
 * to always round towards the better grade.
 *
 * \note Since the average of the two grades is truncated behind the first
 * decimal place, the exam is passed as long as the average grade is < 4.1. Some
 * examination regulations require a truncation behind the second decimal place,
 * but we always use the first.
 *
 * \param closedMax Maximum achievable closed points. Note that since we are
 * only interested in the ratio between this and openMax, you can scale both
 * values by the same constant. Therefore you can use the mMultiple-member of
 * GradeBoundary for this parameter regardless of GradeBoundaryStepSize
 * \param closedGrade Grade achieved in the closed exam section.
 * \param openMax Maximum achievable open points. Note that since we are only
 * interested in the ratio between this and closedMax, you can scale both values
 * by the same constant. Therefore you can use the mMultiple-member of
 * GradeBoundary for this parameter regardless of GradeBoundaryStepSize
 * \param openGrade Grade achieved in the open exam section.
 * \param requirePassAllParts If this is `true`, then a student can only pass if
 * the closed and the open part were passed individually.
 *
 * \return The resulting grade.
 */
[[nodiscard]] Grade getGradeAverage(int closedMax, Grade closedGrade,
                                    int openMax, Grade openGrade,
                                    bool requirePassAllParts) noexcept;
