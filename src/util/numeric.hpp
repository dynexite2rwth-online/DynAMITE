#pragma once

#include <QtGlobal>

#include <iterator>

/**
 * \brief Kahan summation algorithm
 *
 * Calculates the sum of a range of floating point numbers
 * with minimal loss of precision (in our case with only
 * positive numbers ~2*eps).
 *
 * \note If NaN or an infinity is in the input range, the result is NaN.
 */
template <class InputIt>
[[nodiscard]] constexpr typename std::iterator_traits<InputIt>::value_type
kahanSum(InputIt first, InputIt last) noexcept {

    using T = typename std::iterator_traits<InputIt>::value_type;

    T sum{0};

    // Compensate for lost low-order bits
    T c{0};

    for (; first != last; ++first) {
        const T y = *first - c;
        const T t = sum + y;
        c = (t - sum) - y;
        sum = t;
    }
    return sum;
}

/**
 * \brief Returns true if a is definitely strictly greater than b
 *
 * Correctly handles infinities and NaN.
 *
 * qFuzzyCompare does not work around zero, which is why there is an additional
 * check for that.
 */
template <typename T>
[[nodiscard]] bool definitelyGreaterThan(const T a, const T b) noexcept {
    return a > b && (!qFuzzyCompare(a, b) || qIsInf(a)) &&
           !(qFuzzyIsNull(a) && qFuzzyIsNull(b));
}

/**
 * \brief Returns true if a is definitely strictly less than b
 *
 * Correctly handles infinities and NaN.
 *
 * qFuzzyCompare does not work around zero, which is why there is an additional
 * check for that.
 */
template <typename T>
[[nodiscard]] bool definitelyLessThan(const T a, const T b) noexcept {
    return a < b && (!qFuzzyCompare(a, b) || qIsInf(a)) &&
           !(qFuzzyIsNull(a) && qFuzzyIsNull(b));
}
