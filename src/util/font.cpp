#include "font.hpp"

#include <QDebug>
#include <QFont>
#include <QGuiApplication>
#include <QScreen>

#include <cassert>

qreal FontSize::getPixelSize() const noexcept {

    if (unit == FontSizeUnit::Pixel) {
        return size;
    } else {
        return ptToPx(size,
                      QGuiApplication::primaryScreen()->logicalDotsPerInch());
    }
}

std::partial_ordering FontSize::operator<=>(const FontSize &that) const {

    if (unit == that.unit) {
        return size <=> that.size;
    } else if (unit == FontSizeUnit::Pixel) {
        return pxToPt(
                   size,
                   QGuiApplication::primaryScreen()->logicalDotsPerInch()) <=>
               that.size;
    } else {
        return size <=>
               pxToPt(that.size,
                      QGuiApplication::primaryScreen()->logicalDotsPerInch());
    }
}

QDebug operator<<(QDebug debug, const FontSize &object) {
    debug.nospace() << object.size
                    << (object.unit == FontSizeUnit::Pixel ? "px" : "pt");
    return debug.maybeSpace();
}

void setFontSize(QFont &f, const FontSize size) noexcept {

    // point / pixel size must be greater than zero
    assert(size.size > 0 || !qIsInf(size.size));

    if (size.unit == FontSizeUnit::Pixel) {
        f.setPixelSize(static_cast<int>(size.size));
    } else {
        f.setPointSizeF(size.size);
    }
}
