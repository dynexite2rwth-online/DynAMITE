#pragma once

#include <QtGlobal>

#include <compare>
#include <type_traits>

class QString;

template <typename T>
concept arithmetic = std::is_arithmetic_v<T>;

struct Points {
    double p = 0.;

    [[nodiscard]] auto operator<=>(const Points &) const noexcept = default;

    constexpr Points &operator+=(const Points &rhs) noexcept {
        p += rhs.p;
        return *this;
    }
    constexpr Points &operator-=(const Points &rhs) noexcept {
        p -= rhs.p;
        return *this;
    }
    template <arithmetic Nr>
    constexpr Points &operator/=(const Nr &rhs) noexcept {
        p /= rhs;
        return *this;
    }
    template <arithmetic Nr>
    constexpr Points &operator*=(const Nr &rhs) noexcept {
        p *= rhs;
        return *this;
    }

    /**
     * \brief Creates a QString from a points value.
     *
     * Use this function to stringify point values of students outside of error
     * messages.
     *
     * The result will be rounded to defaults::pointsMaxDecimalDigits decimal
     * digits and trailing zeroes are removed as long as at least
     * defaults::pointsMinDecimalDigits decimal digits remain.
     */
    [[nodiscard]] QString toString() const noexcept;

    /**
     * \brief Prints the point value in the shortest, exact string form.
     *
     * Supposed to be used in error or debug messages.
     */
    [[nodiscard]] QString toStringMaxPrecision() const noexcept;

    /**
     * \brief Prints the point value with a higher fixed number of decimal
     * digits.
     *
     * Supposed to be when displaying averages or intermediate values
     * (calculation window).
     */
    [[nodiscard]] QString toStringHigherPrecision() const noexcept;
};

[[nodiscard]] constexpr Points operator+(Points a, const Points &b) noexcept {
    a += b;
    return a;
}
[[nodiscard]] constexpr Points operator-(Points a, const Points &b) noexcept {
    a -= b;
    return a;
}

template <arithmetic Nr>
[[nodiscard]] constexpr Points operator/(Points a, const Nr b) noexcept {
    a /= b;
    return a;
}

[[nodiscard]] constexpr auto operator/(const Points a,
                                       const Points b) noexcept {
    return a.p / b.p;
}

template <arithmetic Nr>
[[nodiscard]] constexpr Points operator*(Points a, const Nr b) noexcept {
    a *= b;
    return a;
}

/**
 * \brief Interprets an empty string as 0.
 */
[[nodiscard]] Points stringToPoints(QString s) noexcept(false);

/**
 * \brief Forwards to qFuzzyCompare with doubles
 *
 * qFuzzyCompare for double uses an relative epsilon of 10^-12.
 */
[[nodiscard]] inline bool qFuzzyCompare(const Points p1,
                                        const Points p2) noexcept {
    return qFuzzyCompare(p1.p, p2.p);
}

/**
 * \brief Forwards to qFuzzyIsNull with a double
 */
[[nodiscard]] inline bool qFuzzyIsNull(const Points p) noexcept {
    return qFuzzyIsNull(p.p);
}

/**
 * \brief Forwards to qIsInf with a double
 */
[[nodiscard]] inline bool qIsInf(const Points p) noexcept {
    return qIsInf(p.p);
}

/**
 * \brief Returns true if the absolute difference between p1 and p2 is smaller
 * or equal than epsilon * maxPoints.
 *
 * Rationale:
 * https://git.rwth-aachen.de/dynexite2rwth-online/DynAMITE/-/issues/185#note_2471163
 */
[[nodiscard]] bool importEpsCompare(Points p1, Points p2,
                                    Points maxPoints) noexcept;
