#include "grade_boundary.hpp"

#include <QString>
#include <QtGlobal>

#include <cassert>
#include <cmath>
#include <limits>

namespace {
int getNrMultiple(const Points p, const GradeBoundaryStepSize stepSize,
                  const GradeBoundaryRounding roundingMode) noexcept {

    return roundMultiple(p.p / as<double>(stepSize), roundingMode).mMultiple;
}
} // namespace

GradeBoundary::GradeBoundary(const Points p,
                             const GradeBoundaryStepSize stepSize,
                             const GradeBoundaryRounding roundingMode) noexcept
    : mMultiple(getNrMultiple(p, stepSize, roundingMode)) {}

QString
GradeBoundary::toString(const GradeBoundaryStepSize stepSize) const noexcept {
    return QString::number(toPoints(*this, stepSize).p, 'f',
                           nrDecimalPlaces(stepSize));
}

GradeBoundary roundMultiple(const double nrMultiple,
                            const GradeBoundaryRounding roundingMode) noexcept {

    // Handle inf and NaN
    if (!std::isfinite(nrMultiple)) [[unlikely]] {
        if (std::isnan(nrMultiple)) {
            return GradeBoundary(0);
        } else {
            // is inf
            if (nrMultiple > 0) {
                return GradeBoundary(std::numeric_limits<int>::max());
            } else {
                return GradeBoundary(std::numeric_limits<int>::min());
            }
        }
    }

    // If nrMultiple is only a rounding error away from an integer, use that
    // integer. Else, round according to roundingMode.
    if (qFuzzyCompare(nrMultiple, std::round(nrMultiple))) {
        return GradeBoundary(static_cast<int>(std::round(nrMultiple)));
    } else {
        switch (roundingMode) {
        default:
            assert(false);
            [[fallthrough]];
        case GradeBoundaryRounding::Down:
            return GradeBoundary(static_cast<int>(std::floor(nrMultiple)));
        case GradeBoundaryRounding::Up:
            return GradeBoundary(static_cast<int>(std::ceil(nrMultiple)));
        case GradeBoundaryRounding::Nearest:
            return GradeBoundary(static_cast<int>(std::round(nrMultiple)));
        }
    }
}

GradeBoundaryFactory::GradeBoundaryFactory(
    const GradeBoundaryStepSize stepSize,
    const GradeBoundaryRounding roundingMode) noexcept
    : mStepSize(stepSize), mRoundingMode(roundingMode) {}

GradeBoundary GradeBoundaryFactory::operator()(const Points p) const noexcept {
    return {p, mStepSize, mRoundingMode};
}
