#include "points.hpp"

#include "defaults.hpp"
#include "exception.hpp"

#include <QString>
#include <QVariant>

#include <cmath>
#include <concepts>

namespace {
template <std::floating_point T>
[[nodiscard]] QString
numberNoTrailingZeroes(T t, const int nrDecimalDigits) noexcept {
    auto ret = QString::number(t, 'f', nrDecimalDigits);

    if (nrDecimalDigits > 0) {
        // Remove trailing zeroes ('f' format always prints a fixed number of
        // digits after the decimal point)
        while (ret.back() == '0') {
            ret.chop(1);
        }
        if (ret.back() == '.') {
            ret.chop(1);
        }
    }

    return ret;
}
} // namespace

QString Points::toString() const noexcept {

    return numberNoTrailingZeroes(p, defaults::pointsDecimalDigits);
}

QString Points::toStringMaxPrecision() const noexcept {
    // The QVariant-String-conversion has the useful property that it is the
    // shortest, exact string form (probably - not much documentation on this.
    // It is definitely exact, because the conversion back from string to double
    // must yield the same result).
    return QVariant{p}.toString();
}

QString Points::toStringHigherPrecision() const noexcept {

    return numberNoTrailingZeroes(p, defaults::pointsHigherDecimalDigits);
}

Points stringToPoints(QString s) noexcept(false) {
    if (s.isEmpty() || s == "-") {
        return Points{0.};
    }

    // dynexite file might use dot or comma as decimal separator
    s.replace(',', '.');

    bool success;
    const auto res = s.toDouble(&success);
    if (success) {
        return Points{res};
    } else {
        throw DynamiteException{"Could not convert string \"" + s +
                                "\" to a number."};
    }
}

bool importEpsCompare(const Points p1, const Points p2,
                      const Points maxPoints) noexcept {
    return std::abs(p1.p - p2.p) <= maxPoints.p * defaults::importEpsilon;
}
