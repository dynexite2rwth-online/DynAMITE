#pragma once

#include "enums.hpp"

#include <array>
#include <cassert>
#include <compare>
#include <cstddef>
#include <type_traits>

/**
 * \brief Stores a value of type T for each enum value of the specified Enum.
 * Allows access with operator[] and an enum value.
 *
 * This is type-safe as opposed to converting the enum to an int and accessing a
 * regular array.
 */
template <typename T, typename Enum = GradingType,
          std::size_t Size = static_cast<std::size_t>(Enum::NR_VALS)>
struct EnumStorage {

    // the internal array is public so that this is an aggregate type, which
    // helps with constructing instances of this class with non-copyable and
    // non-movable classes for T (like all classes that inherit from QObject)
    std::array<T, Size> mData;

    // terminates if out-of-bounds
    [[nodiscard]] constexpr const T &operator[](const Enum e) const noexcept {
        return mData.at(static_cast<std::size_t>(e));
    }
    // terminates if out-of-bounds
    [[nodiscard]] constexpr T &operator[](const Enum e) noexcept {
        return const_cast<T &>(static_cast<const EnumStorage &>(*this)[e]);
    }

    using value_type = typename decltype(mData)::value_type;
    using size_type = typename decltype(mData)::size_type;
    using difference_type = typename decltype(mData)::difference_type;
    using reference = typename decltype(mData)::reference;
    using const_reference = typename decltype(mData)::const_reference;
    using pointer = typename decltype(mData)::pointer;
    using const_pointer = typename decltype(mData)::const_pointer;
    using iterator = typename decltype(mData)::iterator;
    using const_iterator = typename decltype(mData)::const_iterator;
    using reverse_iterator = typename decltype(mData)::reverse_iterator;
    using const_reverse_iterator =
        typename decltype(mData)::const_reverse_iterator;

    [[nodiscard]] auto
    operator<=>(const EnumStorage &) const noexcept = default;

    [[nodiscard]] constexpr iterator begin() noexcept { return mData.begin(); }
    [[nodiscard]] constexpr const_iterator begin() const noexcept {
        return mData.begin();
    }
    [[nodiscard]] constexpr const_iterator cbegin() const noexcept {
        return mData.cbegin();
    }
    [[nodiscard]] constexpr iterator end() noexcept { return mData.end(); }

    [[nodiscard]] constexpr const_iterator end() const noexcept {
        return mData.end();
    }

    [[nodiscard]] constexpr const_iterator cend() const noexcept {
        return mData.cend();
    }
    [[nodiscard]] constexpr size_type size() const noexcept {
        return mData.size();
    }

    constexpr void
    swap(EnumStorage &other) noexcept(std::is_nothrow_swappable_v<T>) {
        mData.swap(other.mData);
    }
};

template <typename T, typename Enum, std::size_t Size>
[[nodiscard]] auto begin(EnumStorage<T, Enum, Size> &s) noexcept {
    return s.mData.begin();
}

template <typename T, typename Enum, std::size_t Size>
[[nodiscard]] auto end(EnumStorage<T, Enum, Size> &s) noexcept {
    return s.mData.end();
}
template <typename T, typename Enum, std::size_t Size>
[[nodiscard]] auto cbegin(const EnumStorage<T, Enum, Size> &s) noexcept {
    return s.mData.cbegin();
}
template <typename T, typename Enum, std::size_t Size>
[[nodiscard]] auto cend(const EnumStorage<T, Enum, Size> &s) noexcept {
    return s.mData.cend();
}

// helper for convenience
template <std::size_t BSize>
constexpr EnumStorage<bool, GradingType, BSize>
getBoolStorage(const GradingType type) noexcept
    requires(BSize >= 2 && BSize <= 3)
{
    using enum GradingType;
    if constexpr (BSize == 3) {
        return {type == Closed, type == Open, type == Combined};
    } else {
        assert(type != Combined);
        return {type == Closed, type == Open};
    }
}

template <typename T, typename... Ts>
struct first_type_of {
    using type = std::decay_t<T>;
};
template <typename... Ts>
using first_type_of_t = typename first_type_of<Ts...>::type;

/**
 * \brief Makes it easier to construct an EnumStorage without specifying the
 * type
 */
template <typename... Ts, typename Enum = GradingType,
          std::size_t Size = static_cast<std::size_t>(Enum::NR_VALS)>
[[nodiscard]] constexpr auto getStorage(Ts &&...values) noexcept(
    ((std::is_nothrow_copy_constructible_v<Ts> &&
      std::is_nothrow_move_constructible_v<Ts>)&&...)) {
    using FirstType = first_type_of_t<Ts...>;
    return EnumStorage<FirstType, Enum, Size>{std::forward<Ts>(values)...};
}
