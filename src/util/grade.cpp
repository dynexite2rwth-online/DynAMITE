#include "grade.hpp"

#include <QString>

#include <ostream>

namespace {
/**
 * \brief Given the precise result of an averaging of two grades, it rounds that
 * result to the next best grade.
 *
 * \note The guidelines demand a rounding to the closest grade, we have decided
 * to always round towards the better grade.
 *
 * \note Since the average of the two grades is truncated behind the first
 * decimal place, the exam is passed as long as the average grade is < 4.1
 *
 * \param weightedGradeInt A grade of the form w1*g1*10+g2*g2*10, with w1, w2
 * being the weights of the closed/open part and g1/g2 the grade of the
 * closed/open part
 * \param multiple (w1+w2)
 * \return The resulting grade
 */
[[nodiscard]] constexpr Grade
roundToNextBestMultiple(const int weightedGradeInt,
                        const int multiple) noexcept {

    using enum Grade;

    // Since we always round towards the better grade, we test for all grades
    // between 1.3 and 4.0 if the our parameter is strictly better than it and
    // return the better grade, if it is.
    for (auto i = grade1_3; i <= grade4_0; i = nextGrade(i)) {
        if (weightedGradeInt < gradeToInt(i) * multiple) {
            // Since our grade is better than the grade at index i, return the
            // better grade.
            return previousGrade(i);
        }
    }
    // The grade is 4.0 or worse. Because an averaged grade still counts as
    // passed as long as it is <4.1, test against that value converted to an
    // integer. `gradeIntValues` holds the exact values*10, so we add 0.1*10=1
    // to the grade integer value of 4.0.
    if (weightedGradeInt < (gradeToInt(grade4_0) + 1) * multiple) {
        return grade4_0;
    }
    return grade5_0;
}
} // namespace

[[nodiscard]] QString toString(const Grade g, const QChar separator) noexcept {

    if (g <= Grade::grade5_0) {
        if (separator == '.') {
            return QString::number(
                gradeDoubles[static_cast<decltype(gradeDoubles)::size_type>(g)],
                'f', 1);
        } else {
            return QString::number(gradeDoubles[static_cast<
                                       decltype(gradeDoubles)::size_type>(g)],
                                   'f', 1)
                .replace('.', separator);
        }
    } else if (g == Grade::NR_VALS) {
        return "NR_GRADES";
    } else {
        return "unknown";
    }
}

std::ostream &operator<<(std::ostream &os, const Grade &g) {
    os << toString(g).toStdString();

    return os;
}

[[nodiscard]] Grade getGradeAverage(const int closedMax,
                                    const Grade closedGrade, const int openMax,
                                    const Grade openGrade,
                                    const bool requirePassAllParts) noexcept {

    // An exam section with 0 maximum achievable points is always graded 1.0, so
    // if both sections have 0 maximum achievable points we return a 1.0.

    if (openMax == 0)
        return closedGrade;
    if (closedMax == 0)
        return openGrade;

    if (requirePassAllParts &&
        (closedGrade == Grade::grade5_0 || openGrade == Grade::grade5_0))
        return Grade::grade5_0;

    const auto combinedMax = closedMax + openMax;
    const auto preciseResultTimesCombinedMax =
        (openMax * gradeToInt(openGrade) + closedMax * gradeToInt(closedGrade));

    // The resulting double value, e.g. 1.67, will always be rounded towards the
    // next best grade, in that case 1.3.
    // Exception: when the grade is >=4.1, it will always be rounded to 5.0.
    return roundToNextBestMultiple(preciseResultTimesCombinedMax, combinedMax);
}
