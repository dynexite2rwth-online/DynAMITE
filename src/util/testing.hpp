/**
 * \file testing.hpp
 * \brief Useful macros for googletest
 */

#pragma once

#define FRIEND_TEST(test_case_name, test_name)                                 \
    friend class test_case_name##_##test_name##_Test
