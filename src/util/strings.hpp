#pragma once

#include <QStringList>

#include <array>

namespace str {
namespace ui {

constexpr auto saveDialog =
    "Do you want to save the current configuration in a .d2r-file?";

constexpr auto gradingGuidelinesUrlGerman =
    "https://www.rwth-aachen.de/global/show_document.asp?id=aaaaaaaaacxybai";

constexpr auto gradingGuidelinesUrlEnglish =
    "https://www.rwth-aachen.de/global/show_document.asp?id=aaaaaaaaaejylxi";

constexpr auto bonusFormatUrl =
    "https://git.rwth-aachen.de/dynexite2rwth-online/DynAMITE/-/wikis/"
    "Bonus-Point-File-Format-Documentation";

constexpr auto licenseInRepo =
    "https://git.rwth-aachen.de/dynexite2rwth-online/DynAMITE/-/"
    "blob/master/LICENSE";

namespace boundary {
constexpr auto autoCheckbox = "auto";
constexpr auto closedInfoLabel =
    "Due to regulations, some grade boundaries cannot be changed. See the "
    "grading guidelines available from the help menu of this app for more "
    "information.";
constexpr auto min = "<b>Min</b>";
constexpr auto max = "<b>Max</b>";
constexpr auto grade = "<b>%1</b>";
constexpr auto maxSmaller = "-";

constexpr auto requirePassAllCheckbox =
    "Only pass the exam if all\nexam parts are passed individually";
constexpr auto passDueToBonusCheckbox =
    "Credit bonus points even if the\nexam (without bonus points) is\nassessed "
    "as \"failed\" (5.0)";
constexpr auto round1 = "Round grade boundaries: 1";
constexpr auto round0_5 = "Round grade boundaries: 0.5";
constexpr auto round0_25 = "Round grade boundaries: 0.25";
constexpr auto roundDown = "Round grade boundaries down";
constexpr auto roundUp = "Round grade boundaries up";
constexpr auto roundNearest = "Round grade boundaries nearest";
constexpr auto openTab = "Open";
constexpr auto closedTab = "Closed";
constexpr auto allPointsTab = "All points";
} // namespace boundary

namespace fileElim {
/*
 * [0] = NotEliminated
 * [1] = WithdrawTask
 * [2] = CreditFull
 */
static const QStringList comboOptions = {
    "Task not eliminated", "No students get any points, max points is reduced",
    "All students get full points"};
} // namespace fileElim

namespace gradingTab {
constexpr auto maxSlider = "Maximal Grading Points%1";
constexpr auto minSlider = "Minimal Points%1";

constexpr auto maxExplanationTitle = "Maximal Grading Points Explanation";

constexpr auto maxExplanation =
    "%1The points configured here are used for the upper limit of the grade "
    "1.0. Changing this widens the range of points a 1.0 is issued and on the "
    "other hand keeps the proportions of all other grade levels. The maximum "
    "points within the exam are not reduced. This can only be achieved by "
    "eliminating explicitly selected tasks (see menu point \"Eliminate tasks\" "
    "for this).%2";

constexpr auto avgAttended = "Average grade (Attended):";
constexpr auto avgPassed = "Average grade (Passed):";
constexpr auto attendedPassed = "Attended students passed:";
constexpr auto attendedFailed = "Attended students failed:";

constexpr auto gradeResultNone = "-";
constexpr auto attendedResultNone = "- of -";
constexpr auto attendedResultAny = "%1 of %2 (%3%)";
} // namespace gradingTab

namespace input {
static const QString guidelineHint =
    QString("Document that details how to grade multiple choice exams: <a "
            "href=\"%1\">German</a> / <a href=\"%2\">English</a>")
        .arg(gradingGuidelinesUrlGerman, gradingGuidelinesUrlEnglish);
constexpr auto csvTab = "Standard (.csv)";
constexpr auto d2rTab = "All in one (.d2r)";
inline constexpr std::array helpTexts = {
    "CSV export of results", "CSV export of \"List of Candidates\"",
    "<a href=\"%1\">CSV export of only \"Bonus points\" from moodle</a>",
    "Import previously exported d2r-file from this app"};
constexpr auto csvFilter =
    "Comma-separated values (CSV) file (*.csv);;All files (*.*)";
constexpr auto d2rFilter = "All-in-one (D2R) file (*.d2r);;All files (*.*)";
constexpr auto importButton = "Import...";
constexpr auto radioRoundQuestion = "Grade boundary step:";
constexpr auto radioRound1 = "1.0 point";
constexpr auto radioRound0_5 = "0.5 points";
constexpr auto radioRound0_25 = "0.25 points";
} // namespace input

namespace tasks {
constexpr auto namePlusStats =
    "%1\nAverage percentage: %2%\nStandard deviation: %3";

constexpr auto headerIndex = "#";
constexpr auto headerName = "Task name";
constexpr auto headerPlot = "Plots";
constexpr auto headerAvg = "Avg. percentage";
constexpr auto headerSpread = "Standard deviation";
constexpr auto headerTaskType = "Task type";
constexpr auto headerGradingType = "Grading type";
} // namespace tasks

} // namespace ui

namespace title {
// Window titles
constexpr auto readingFileWarning = "Reading file warnings";
constexpr auto readingFileError = "Reading file failed";
constexpr auto boundaryWindow = "Options";
constexpr auto inputWindow = "Select files...";
constexpr auto invalidElim = "Invalid elimination method";
constexpr auto closedPassingMarkHelp = "Closed passing mark";
constexpr auto fixedD2rSetting = "Fixed d2r setting";
constexpr auto newD2rSetting = "New d2r setting";
constexpr auto tooManyBonusPoints = "Too many bonus points";
constexpr auto disabledAutoGradeBoundaries = "Manual Grade Boundary Adjustment";
constexpr auto eliminateTaskUnavailable = "Can not eliminate task";
constexpr auto boxPlotHelpText = "Box plot explanation";

} // namespace title

namespace error {
// Error prefix for any error when parsing a dynexite file
constexpr auto defaultDynexite =
    "Please download the CSV results file from the \"Download the "
    "results\" section from dynexite.\n\n";

// Error prefix for any error when parsing an RWTH Online file
constexpr auto defaultRwthOnline =
    "Please download the export from RWTH Online as a CSV file with "
    "encoding-setting ISO 8859-1.\n\n";

// Error prefix for any error when parsing a bonus point file
constexpr auto defaultBonus =
    "Please use a properly formatted bonus point file with at "
    "least the columns \"Matrikelnummer\" and \"Bonuspunkte "
    "(Punkte)\" from moodle.\n\n";

// Error prefix for any error when parsing a d2r file
constexpr auto defaultD2R =
    "Reading the d2r file has generated errors. Please select a file that you "
    "previously exported with this app.\n\n";
constexpr auto d2rDynexite = "While parsing the embedded dynexite file:\n\n";
constexpr auto d2rRwthOnline =
    "While parsing the embedded RWTHonline file:\n\n";
constexpr auto d2rBonus = "While parsing the embedded bonus point file:\n\n";

constexpr auto d2rInvalidNrEliminatedTasks =
    "The number of eliminated tasks in the d2r-file is inconsistent with the "
    "number of tasks.";

// Error when parsing an empty or non-text file
constexpr auto empty = "File is empty or of the wrong file type.";

// Error when there are no header columns
constexpr auto noHeader = "No header.";

// Error when there are no rows beside the header
constexpr auto noData = "No data rows.";

// Error when there are no rows beside the header in a dynexite file with
// content besides the identifier
constexpr auto noStudents = "No student attended?";

constexpr auto unexpectedDynexite = "You selected a dynexite file.";
constexpr auto unexpectedRwthOnline = "You selected an RWTH Online file.";
constexpr auto unexpectedBonus = "You selected a bonus point file.";

constexpr auto invalidColumnNr = "Unexpected number of columns: ";

constexpr auto insertFailed = "Inserting data failed.";

constexpr auto headerValidationFailure =
    R"(Header validation error: expected "%1", found "%2")";

constexpr auto headerValidationFailureEnding =
    R"(Header validation error: expected ending "%1", found string "%2")";

constexpr auto headerValidationColumnNr =
    "Header validation: "
    "Unexpected number of columns: %1 (To disable this check, set "
    "RWTHONLINE_PARSER_PERMISSIVE to true)";

constexpr auto differentMaxSums =
    "max closed points + max open points != max combined points: %1 + %2 != %3";

constexpr auto notAllOfOpenClosedEarned =
    "The student with ID %1 has some, but not all of open/closed/earned points "
    "for the task %2.";

constexpr auto inconsistentTaskMaxPoints =
    "Max points of task %1 are inconsistent across students: Was %2, now is "
    "%3.";

constexpr auto inconsistentTaskGradingMaxPoints =
    "Max %4 points of task %1 are inconsistent across students: Was %2, now is "
    "%3.";

constexpr auto inconsistentMaxTypePoints =
    "Max %1 points of the exam are inconsistent across students: Was %2, now "
    "is %3.";

constexpr auto studentMultipleTaskPools =
    "Could not group task items into task pools: Overlap of students between "
    "tasks \"%1\" and \"%2\".";

constexpr auto taskItemsDifferentMax =
    "Items of one task have a different amount of max points: \"%1\" has max "
    "%2, \"%3\" has max %4.";

constexpr auto lastTaskPoolIncomplete =
    "The last task pool (including the task %1) is incomplete.";

constexpr auto lastTaskPoolNotEmpty =
    "Some tasks were left over when task pool creation was finished, "
    "including the task \"%1\".";

constexpr auto taskMaxLt0 =
    "Internal error: task max of task %1 is less than 0: %2";

constexpr auto taskPoolMaxLt0 =
    "Internal error: task max of task pool\n%1\nis less than 0: %2";

constexpr auto taskMaxDeduceSumFailure =
    "Could not deduce the maximum number of closed/open points for all tasks. "
    "The deduced maximums sum up to %1/%2, but should be %3/%4.";

constexpr auto taskMaxDeduceFailure =
    "Could not deduce the maximum number of closed/open points for the "
    "task\n%1\nThe deduced maximums sum are %2 and %3, but the sum should be "
    "%4.\n\nSee issue 178 in GitLab for more information.";

constexpr auto sumOfTasksNotTotalMax =
    "The sum of the max points of the individual task pools does not equal "
    "the stated max points of the exam: %1 != %2";

constexpr auto studentEarnedTooManyPointsTask =
    "The student %1 earned in the task \"%2\" more points than should have "
    "been possible: %3 (closed) + %4 (open) > %5";

constexpr auto differentMaxSumsTask =
    "The student %1 earned in the task \"%2\" %3 closed and %4 open points, "
    "which does not sum up to their combined earned points, %5";

constexpr auto differentTaskSumEarned =
    "The sum of earned %1 points over all tasks for the student %2 is %3, "
    "which is not equal to their stated total earned %1 points: %4";

constexpr auto maxPointOverflow =
    "This exam uses higher point scores than this app can safely "
    "handle.\n\nThe maximum achievable points is %1, while this app for safety "
    "reasons only supports point scores up to %2.";

constexpr auto duplicateRegistrationNr =
    "The registration number \"%1\" appears in multiple rows.";

constexpr auto noColumn = "No %1 column found.";

constexpr auto parseFailure = "Failed to parse %1 %2";

constexpr auto multipleCourseTitles = "Multiple course titles: %1";
constexpr auto multipleSemesters = "Multiple semesters: %1";

constexpr auto bonusArraySizeMismatch =
    "Internal error while reading bonus points (array size mismatch).";
constexpr auto rwthArraySizeMismatch =
    "Internal error while reading student info (array size mismatch).";

constexpr auto readingFileWarning =
    "Reading the file generated the following warnings:\n";

// Warnings when there is little overlap between students in the different input
// files
constexpr auto lowStudentIntersection =
    "Are you sure you selected files from the same exam?";
constexpr auto lowStudentIntersectionFewRwth =
    "\n\nOnly %1% (%2 of %3) of students in the "
    "dynexite file can be found in the "
    "RWTHonline file.";
constexpr auto lowStudentIntersectionNoneRwth =
    "\n\nNone of the students in the dynexite file can be found in "
    "the RWTHonline file.";
constexpr auto lowStudentIntersectionFewBonus =
    "\n\nOnly %1% (%2 of %3) of students in the "
    "dynexite file can be found in the "
    "bonus point file.";
constexpr auto lowStudentIntersectionNoneBonus =
    "\n\nNone of the students in the dynexite file can be found in "
    "the bonus point file.";

constexpr auto addBonusPointsFailed =
    "Failed to add %1 bonus points to student with "
    "registration number %2";

constexpr auto d2rHeader = "Invalid d2r header.";

constexpr auto csvDelimiter =
    "Error while parsing the CSV file: No valid delimiter found";

constexpr auto fixedD2rSettingInfo =
    "The given d2r file contains the setting \"%1\" set to \"%2\". Because of "
    "changes in the grading guidelines, this setting is no longer "
    "user-changeable and was set to the fixed default value \"%3\".";

constexpr auto newD2rSettingInfo =
    "The new setting \"%1\" was set to the default value \"%2\".";

constexpr auto tooManyBonusPointsInfo =
    "The maximum bonus points to be achieved may at most be %4% of the total "
    "score. In the selected files, the maximum achieved bonus points are %1 "
    "points and the total score is %2 points (%3%).";

constexpr auto internalCsvLineLength =
    "Internal CSV parser error: line length %1 smaller "
    "than cell start position %2";

constexpr auto internalCsvNumberCells =
    "Internal CSV parser error: Number of cells (%1) not "
    "one more than number of delimiters (%2)";

} // namespace error

namespace info {
constexpr auto sixtyPercentHover1 =
    "%1Normally, the passing mark (Bestehensgrenze) of an exam should "
    "not be set above the required knowledge of 50% of the obtainable points. "
    "However, since it can be assumed that a candidate who has the required "
    "knowledge (within the closed part) can guess the remaining questions and "
    "hence gain additional points. Therefore the passing mark in the closed "
    "part should not exceed 60% of the achievable points.%2";

constexpr auto sixtyPercentHover2 =
    "Here, as a result of the legal situation (see Guidelines on "
    "Multiple-Choice Exams), the passing mark should not exceed %1%.%2";

constexpr auto studentsTableTabLegend =
    R"(<font color="%1"><b>Legend:</b></font><br>
<font color="%2">Light green: Passed with 1.0</font><br>
<font color="%3">Dark green: Passed</font><br>
<font color="%4">Red: Not passed</font>
)";

constexpr auto initialMax = "Initial point maximum: %1";

constexpr auto eliminationInfoLabel =
    "<font color=\"%1\">Tasks can only be eliminated if they are considered "
    "unsuitable or incorrect.<br>Tasks that are determined to have been too "
    "difficult after the exam cannot be eliminated using this process.</font>";

constexpr auto invalidElimText =
    "If there are several erroneous tasks in one exam, the remedy must be "
    "consistent throughout, i.e., either all the affected tasks shall be "
    "withdrawn, or the full score shall be credited for all the affected "
    "tasks.<br><br>The elimination method for the task<br><b>%1</b><br>has "
    "been changed from \"%2\" to \"%3\".<br><br>If you want to use the "
    "elimination method \"%2\" for all eliminated tasks, reset all tasks to "
    "\"Task not eliminated\" and then set the correct elimination method.";

constexpr auto disabledAutoGradeBoundaries =
    "The \"auto\" feature is now disabled. You are now manually "
    "adjusting individual grade boundaries, including the passing "
    "grade.\n\nChanging the passing grade manually does no longer "
    "impact the other grade boundaries other than to comply with legal "
    "requirements. Therefore ensure that your adjustments align with "
    "your desired grading structure. To minimize possible side effects, "
    "we recommend these adjustments as the very last step.\n\n"
    "You can view the default grade boundary percentiles that are used "
    "when \"auto\" is enabled in the repository wiki.";

constexpr auto eliminateTaskUnavailable =
    "Because bonus points may at max. be %1% of the total score, you can not "
    "eliminate this task.";

constexpr auto boxPlotHelpText =
    "The whiskers of the box plots are drawn at the 9th (8.87%) and "
    "91st (91.13%) percentile of the data. Outliers beyond the "
    "whiskers are drawn as small circles.\n\nIf two values of the box "
    "plot, e.g. median and first quartile, are equal, they are drawn "
    "slightly apart from each other for better legibility. The median "
    "is always at the correct position.";

static const QString aboutString =
    QString("<h3>About DynAMITE</h3>"
            "<p>DynAMITE v%1 Copyright &copy; 2024  RWTH Lehrstuhl Informatik "
            "11</p>"
            "<p>This program is free software: you can redistribute it and/or "
            "modify it under the terms of the GNU General Public License as "
            "published by the Free Software Foundation, either version 3 of "
            "the License, or (at your option) any later version.</p>"
            "<p>This program is distributed in the hope that it will be "
            "useful, but WITHOUT ANY WARRANTY; without even the implied "
            "warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  "
            "See the GNU General Public License for more details.</p>"
            "<p>You can view the license <a href=\"%2\">here</a>.</p>"
            "<br>"
            "<p>We use the following dependencies:</p>"
            "<p>Qt v%3 licensed under the GNU LGPL version 3 and Qt Charts "
            "licensed  under the GNU GPL version 3.</p>"
            "<p>QCustomPlot v2.1.1, licensed under the GNU GPL version 3.</p>")
        .arg(D2R_VERSION, ui::licenseInRepo, QT_VERSION_STR);

} // namespace info

namespace control {
constexpr auto richTextStart = "<html><head/><body><p>";
constexpr auto richTextEnd = "</p></body></html>";
} // namespace control

namespace fileExport {
// Attention to the order of .arg (%1, %2 etc.) in the following strings.
constexpr auto rwthOnlineRemarkNoBonus =
    "%1 questions: %2 pts of max. possible %5 pts (grade: %3)";
constexpr auto rwthOnlineRemarkPassedBonus =
    "%1 questions: %2 pts of max. possible %5 pts + %4 bonus points (grade: "
    "%3)";
constexpr auto rwthOnlineRemarkNotPassedBonus =
    "%1 questions: %2 pts of max. possible %5 pts (grade: %3) + %4 bonus "
    "points";
constexpr auto rwthOnlineRemarkAverageFirstAny =
    " - The average of achieved points in the closed part of the candidates "
    "taking the exam for the first time is %10 pts";
constexpr auto rwthOnlineRemarkAverageFirstNone =
    " - No candidate took the exam for the first time.";

constexpr auto rwthOnlineRemarkNewGerNoBonus =
    "%2 pts of max. possible %5 pts (grade: %3)";
constexpr auto rwthOnlineRemarkNewGerPassedBonus =
    "%2 pts of max. possible %5 pts + %4 bonus points (grade: %3)";
constexpr auto rwthOnlineRemarkNewGerNotPassedBonus =
    "%2 pts of max. possible %5 pts (grade: %3) + %4 bonus points";

constexpr auto knownStudentSaveLocationCaption =
    "Save RWTHonline data including grades";
constexpr auto unknownStudentSaveLocationCaption =
    "Save RWTHonline data including grades of students not in the given "
    "RWTHonline file";
constexpr auto allStudentsSaveLocationCaption =
    "Save the student tab as CSV data";

} // namespace fileExport
} // namespace str
