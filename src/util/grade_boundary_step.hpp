#pragma once

#include <cassert>
#include <concepts>

/**
 * \brief The minimum fraction of points that can be awarded.
 */
enum class GradeBoundaryStepSize { Min1_00, Min0_50, Min0_25, NR_VALS };

[[nodiscard]] constexpr GradeBoundaryStepSize
toStepSize(const double d) noexcept {
    using enum GradeBoundaryStepSize;
    if (d == 1.)
        return Min1_00;
    else if (d == 0.5)
        return Min0_50;
    else if (d == 0.25)
        return Min0_25;
    else {
        assert(false);
        return NR_VALS;
    }
}

template <std::floating_point Float>
[[nodiscard]] constexpr Float as(const GradeBoundaryStepSize pDif) noexcept {
    switch (pDif) {
    case GradeBoundaryStepSize::Min1_00:
        return Float(1);
    case GradeBoundaryStepSize::Min0_50:
        return Float(0.5);
    default:
        assert(false);
        [[fallthrough]];
    case GradeBoundaryStepSize::Min0_25:
        return Float(0.25);
    }
}

[[nodiscard]] constexpr int
nrDecimalPlaces(const GradeBoundaryStepSize pDif) noexcept {
    switch (pDif) {

    case GradeBoundaryStepSize::Min1_00:
        return 0;
    case GradeBoundaryStepSize::Min0_50:
        return 1;
    default:
        assert(false);
        [[fallthrough]];
    case GradeBoundaryStepSize::Min0_25:
        return 2;
    }
}

enum class GradeBoundaryRounding { Up, Down, Nearest };
