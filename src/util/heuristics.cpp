#include "heuristics.hpp"

#include "data/nodes/dynexite_exam.hpp"
#include "util/enums.hpp"

#include <QString>

#include <algorithm>

namespace {
/**
 * \brief Returns the smallest number that is 1/2/2.5/5 times a power of ten and
 * results in less than maxNrTickMarks ticks.
 */
[[nodiscard]] int getRoundTickMark(const int rangeSize,
                                   const int maxNrTickMarks) noexcept {
    const auto nrIntervals = maxNrTickMarks - 1;
    const auto smallestTickInterval =
        static_cast<double>(rangeSize) / nrIntervals;

    // largest power of 10 smaller than smallestTickInterval
    // (not smaller than 1)
    const auto magnitude = std::max(
        1., std::pow(10., std::floor(std::log10(smallestTickInterval))));

    // Residual == "how many times smallestTickInterval is larger than a power
    // of ten"
    const auto residual = smallestTickInterval / magnitude;

    if (residual > 5) {
        return static_cast<int>(10 * magnitude);
    } else if (residual > 2.5) {
        return static_cast<int>(5 * magnitude);
    } else if (residual > 2) {
        if (magnitude >= 10) {
            return static_cast<int>(2.5 * magnitude);
        } else {
            return static_cast<int>(5 * magnitude);
        }
    } else if (residual > 1) {
        return static_cast<int>(2 * magnitude);
    } else {
        return static_cast<int>(magnitude);
    }
}
} // namespace

QPair<int, int> niceYAxis(int maxBarHeight, int maxNrYLabels) noexcept {

    // Ignore erroneous negative numbers. Also a maximum of 0 doesn't look good.
    if (maxBarHeight < 1) {
        maxBarHeight = 1;
    }

    // We always need at least two y axis labels, even if there is not enough
    // space.
    if (maxNrYLabels < 2) {
        maxNrYLabels = 2;
    }

    const auto sensibleStepSize = getRoundTickMark(maxBarHeight, maxNrYLabels);
    const auto alternateMax =
        static_cast<int>(
            std::ceil(static_cast<double>(maxBarHeight) / sensibleStepSize)) *
        sensibleStepSize;

    return qMakePair(alternateMax, sensibleStepSize);
}

namespace impl {
GradeBoundaryStepSize
guessGradeBoundaryStepSize(const QList<Points> &allPointValues) noexcept {

    if (allPointValues.empty()) {
        return GradeBoundaryStepSize::Min1_00;
    } else {
        const auto min = impl::minStepSize(allPointValues);

        // return Min0_25 for step sizes < 0.25
        if (min == 1.0)
            return GradeBoundaryStepSize::Min1_00;
        else if (min == 0.5)
            return GradeBoundaryStepSize::Min0_50;
        else
            return GradeBoundaryStepSize::Min0_25;
    }
}
} // namespace impl

[[nodiscard]] GradeBoundaryStepSize
guessGradeBoundaryStepSize(const DynexiteExam &exam) noexcept {

    // Search all point values* to guess a sensible step size.
    //
    // * = earned closed, earned open and max points (earned combined points
    // should be just the sum of earned closed and earned open, so no need to
    // search that as well)

    auto allPointValues = QList<Points>{};

    for (const auto &task : exam.mTaskItems) {
        allPointValues.append(task.mMaxPoints[GradingType::Closed]);
        allPointValues.append(task.mMaxPoints[GradingType::Open]);
        allPointValues.append(task.assignedEarnedPoints(GradingType::Closed));
        allPointValues.append(task.assignedEarnedPoints(GradingType::Open));
    }
    return impl::guessGradeBoundaryStepSize(allPointValues);
}

FileType getApproximateFileType(const QString &s) noexcept {
    if (QString::compare(s, QString("STUDY_PROGRAMME"), Qt::CaseInsensitive) ==
        0) {
        return FileType::rwth;
    }
    if (QString::compare(s, QString("AttemptID"), Qt::CaseInsensitive) == 0) {
        return FileType::dynexite;
    }
    if (QString::compare(s, QString("Vorname"), Qt::CaseInsensitive) == 0) {
        return FileType::bonus;
    }
    return FileType::NR_VALS;
}
