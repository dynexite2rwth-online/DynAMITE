#include "d2r_parser.hpp"

#include "csv_parser.hpp"
#include "data/csv_dump.hpp"
#include "data/data_manager.hpp"
#include "util/exception.hpp"
#include "util/strings.hpp"

#include <QList>
#include <QMessageBox>
#include <QTextStream>
#include <QVariant>

namespace impl {

bool toBool(const QString &str) {
    return QString::compare(str, "true", Qt::CaseInsensitive) == 0;
}
QString toString(const bool b) { return b ? "true" : "false"; }

} // namespace impl

struct D2rEntry {
    QString parent;
    QString key;
    QString value;
};

namespace {
/**
 * \brief Calculate child depth of d2r key
 */
qsizetype getAndRemoveDepth(QString &key) {

    qsizetype depth = 0;
    while (key.startsWith("  ")) {
        ++depth;
        key = key.sliced(2);
    }
    return depth;
}

/**
 * \brief Loads a single d2r key-value-pair
 *
 * \param data Where to store the new key-value-pair
 * \param in Data source
 * \param parent Current parent, might be modified
 * \return Whether or not a new key-value-pair has been read
 */
bool loadD2rKey(QList<D2rEntry> &data, QTextStream &in, QString &parent) {
    QString line;
    in.readLineInto(&line);

    // Empty key: EOF
    if (line.isEmpty())
        return false;

    auto colonSplit = line.split(':');
    auto key = colonSplit[0];
    // Since the value could theoretically include the character :, we make sure
    // not to lose it here
    colonSplit.removeFirst();
    const auto value = colonSplit.join(':');
    D2rEntry newEntry;

    const bool isChild = getAndRemoveDepth(key) > 0;
    if (isChild) {
        newEntry.parent = parent;
    } else {
        newEntry.parent = "";
        // set current key as potential parent for next key
        parent = key;
    }

    newEntry.key = key;

    // Special case for multi-line keys: they end with a "\"
    // TODO: allow \\ inside the value, and somehow keep this
    //       backwards-compatible
    if (key == "Dynexite" || key == "RWTHonline" || key == "Bonus") {

        if (value.trimmed() == "\\") {
            newEntry.value = "";
        } else {
            // Don't forget first line
            if (value != "") {
                line = value + '\n';
            } else {
                line = "";
            }

            // Read until \ or EOF
            QChar nextChar;
            in >> nextChar;
            while (nextChar != '\\' && !nextChar.isNull()) {
                line.append(nextChar);
                in >> nextChar;
            }
            newEntry.value = line;
        }
    } else {
        newEntry.value = value;
    }

    data.push_back(newEntry);

    return true;
}

/**
 * \brief Apply eliminated tasks from the list in taskEliminationStatus to the
 * exam in dump
 */
void applyTaskElimination(
    QList<TaskPool> &examTaskPools,
    const QList<TaskEliminationType> &taskEliminationStatus) {

    // Allow old d2r-versions without eliminated tasks
    if (!taskEliminationStatus.empty()) {

        // If we provide task elimination status, we have to provide an
        // elimination status for every task pool of the exam
        if (taskEliminationStatus.size() != examTaskPools.size()) {
            throw DynamiteException{str::error::d2rInvalidNrEliminatedTasks};
        }

        for (int i = 0; i < taskEliminationStatus.size(); ++i) {
            examTaskPools[i].mEliminationStatus = taskEliminationStatus[i];
        }
    }
}

/**
 * \brief For all settings that are currently unchangeable, check if the d2r has
 * stored in it a configuration that is no longer allowed. If it does, display a
 * warning and reset the setting to the allowed value.
 */
void checkUnchangeableSettings(D2rSettings &settings, QWidget *parentWidget) {

    QStringList warningText;

    if (settings.mUserPointConfigCountZero !=
        defaults::countZerothAttemptAsFirstAttempt) {

        warningText.append(
            QString(str::error::fixedD2rSettingInfo)
                .arg("Count zeroth attempt as first attempt",
                     QVariant(settings.mUserPointConfigCountZero).toString(),
                     QVariant(defaults::countZerothAttemptAsFirstAttempt)
                         .toString()));

        settings.mUserPointConfigCountZero =
            defaults::countZerothAttemptAsFirstAttempt;
    }

    if (settings.mUserGradeConfigRequirePassAll !=
        defaults::requirePassAllParts) {

        warningText.append(
            QString(str::error::fixedD2rSettingInfo)
                .arg("Require pass all parts",
                     QVariant(settings.mUserGradeConfigRequirePassAll)
                         .toString(),
                     QVariant(defaults::requirePassAllParts).toString()));

        settings.mUserGradeConfigRequirePassAll = defaults::requirePassAllParts;
    }

    if (!warningText.isEmpty()) {
        QMessageBox::warning(parentWidget, str::title::fixedD2rSetting,
                             warningText.join("\n\n"));
    }
}

void checkNewSettings(const QHash<QString, QPair<bool, QVariant>> &newOptions,
                      QWidget *parentWidget) {

    QStringList warningText;

    for (auto i = newOptions.cbegin(), end = newOptions.cend(); i != end; ++i) {
        if (!i.value().first) {
            warningText.append(QString(str::error::newD2rSettingInfo)
                                   .arg(i.key(), i.value().second.toString()));
        }
    }

    if (!warningText.isEmpty()) {

        QMessageBox::warning(parentWidget, str::title::newD2rSetting,
                             warningText.join("\n\n"));
    }
}

QSharedPointer<ExamSave> readStringsToData(const QList<D2rEntry> &entries,
                                           QWidget *parentWidget) {
    auto dump = QSharedPointer<ExamSave>::create();

    using namespace impl;

    QList<TaskEliminationType> taskEliminationStatus;

    // Stores for later-added d2r option whether or not this d2r file contains
    // that option and its default value.
    // Update this variable when there are new d2r options so that the user gets
    // notified when a new option has been set to a default value.
    QHash<QString, QPair<bool, QVariant>> newOptions;
    newOptions["PassDueToBonus"] = {false, defaults::passDueToBonus};
    newOptions["EliminatedTasks"] = {false, "No tasks eliminated"};
    newOptions["DynexiteVersion"] = {false, ""};

    // This is a hack, but we need to read the csv version (if there is any)
    // before parsing the csv
    for (const auto &[parent, key, value] : entries) {

        if (parent.isEmpty() && key == "DynexiteVersion") {
            newOptions["DynexiteVersion"].first = true;
            newOptions["DynexiteVersion"].second =
                QString("data.v%1.csv").arg(value);
        } else {
            continue;
        }
    }

    // set the value for the given parent-key-combination
    for (const auto &[parent, key, value] : entries) {
        if (parent.isEmpty()) {
            if (key == "Dynexite") {
                try {
                    dump->mDynexite = DynexiteExam(*parseCsv(
                        newOptions["DynexiteVersion"].second.toString(),
                        value));
                } catch (const std::exception &e) {
                    throw DynamiteException{QString(str::error::d2rDynexite) +
                                            e.what()};
                }
            } else if (key == "RWTHonline") {
                try {
                    dump->mStudents =
                        StudentData(*parseCsv("data.v1.csv", value));
                } catch (const std::exception &e) {
                    throw DynamiteException{QString(str::error::d2rRwthOnline) +
                                            e.what()};
                }
            } else if (key == "Bonus") {
                try {
                    dump->mBonus = BonusPoints(*parseCsv("data.v1.csv", value));
                } catch (const std::exception &e) {
                    throw DynamiteException{QString(str::error::d2rBonus) +
                                            e.what()};
                }
            } else if (key == "Version") {
                dump->mSettings.mVersion = value;
            } else if (key == "DynexiteVersion") {
                // Already handled in the first loop
                continue;
            }
        } else if (parent == "UserPointConfig") {
            if (key == "StepSize") {
                dump->mStepSize = toStepSize(value.toDouble());
            } else if (key == "CountZerothAttempt") {
                dump->mSettings.mUserPointConfigCountZero = toBool(value);
            }
        } else if (parent == "UserGradeConfig") {
            if (key == "RequirePassAll") {
                dump->mSettings.mUserGradeConfigRequirePassAll = toBool(value);
            } else if (key == "PassDueToBonus") {
                newOptions["PassDueToBonus"].first = true;
                dump->mSettings.mUserGradeConfigPassDueToBonus = toBool(value);
            }
        } else if (parent == "ChangedMaxPointsClosed") {
            if (key == "ChangedMaxPoints") {
                dump->mSettings.mChangedMaxPointsClosed = value.toInt();
            } else if (key == "FollowDefault") {
                dump->mSettings.mChangedMaxPointsClosedFollowDefault =
                    toBool(value);
            }
        } else if (parent == "ChangedMaxPointsOpen") {
            if (key == "ChangedMaxPoints") {
                dump->mSettings.mChangedMaxPointsOpen = value.toInt();

            } else if (key == "FollowDefault") {
                dump->mSettings.mChangedMaxPointsOpenFollowDefault =
                    toBool(value);
            }
        } else if (parent == "UserGradeBoundariesClosed") {
            // To get the grade array index, remove the prefix "Boundary"
            QString prefix = "Boundary";
            if (key == "SetAuto") {
                dump->mSettings.mUserGradeBoundariesClosedAuto = toBool(value);
            } else if (key.contains(prefix)) {
                const auto idx = key.mid(prefix.size()).toUInt();
                dump->mSettings.mUserGradeBoundariesClosed[idx] = value.toInt();
            }
        } else if (parent == "UserGradeBoundariesOpen") {
            // To get the grade array index, remove the prefix "Boundary"
            QString prefix = "Boundary";
            if (key == "SetAuto") {
                dump->mSettings.mUserGradeBoundariesOpenAuto = toBool(value);
            } else if (key.contains(prefix)) {
                const auto idx = key.mid(prefix.size()).toUInt();
                dump->mSettings.mUserGradeBoundariesOpen[idx] = value.toInt();
            }
        } else if (parent == "EliminatedTasks") {
            newOptions["EliminatedTasks"].first = true;
            // To get the task pool index, remove the prefix "TaskPool"
            QString prefix = "TaskPool";
            const auto idx = key.mid(prefix.size()).toUInt();

            // We can only apply the eliminated tasks after we actually read the
            // tasks
            const auto eliminationStatus =
                static_cast<TaskEliminationType>(value.toInt());

            if (taskEliminationStatus.size() == idx) {
                taskEliminationStatus.push_back(eliminationStatus);
            } else {
                if (taskEliminationStatus.size() < idx) {
                    taskEliminationStatus.resize(idx + 1);
                }
                taskEliminationStatus[idx] = eliminationStatus;
            }
        }
    }

    applyTaskElimination(dump->mDynexite.mTaskPools, taskEliminationStatus);

    checkUnchangeableSettings(dump->mSettings, parentWidget);
    checkNewSettings(newOptions, parentWidget);

    return dump;
}

template <typename T>
void writeSingleSetting(QTextStream &out, const QString &name, const T &value,
                        const bool isChild = false) {
    if (isChild) {
        out << "  ";
    }
    out << name << ":" << value << '\n';
}
template <>
void writeSingleSetting<bool>(QTextStream &out, const QString &name,
                              const bool &value, const bool isChild) {
    if (isChild) {
        out << "  ";
    }
    out << name << ":" << impl::toString(value) << '\n';
}
} // namespace

ExamSave::ExamSave(const CsvDump &dynexite, const CsvDump &rwthOnline,
                   const CsvDump &bonus, const GradeBoundaryStepSize stepSize,
                   const D2rSettings &settings)
    : mDynexite(dynexite), mStudents(rwthOnline), mBonus(bonus),
      mStepSize(stepSize), mSettings(settings) {}

QSharedPointer<ExamSave> parseD2r(QString d2rString, QWidget *parentWidget) {

    // store the resulting save on the heap, as it might get quite big
    auto save = QSharedPointer<ExamSave>::create();

    // todo: document d2r format somewhere (simplified yaml)

    QTextStream in(&d2rString);
    QString line;

    // skip ---
    {
        in.readLineInto(&line);
        if (line != "---") {
            throw DynamiteException{str::error::d2rHeader};
        }
    }

    QList<D2rEntry> readData;
    {
        QString parent;
        while (loadD2rKey(readData, in, parent)) {
        }
    }

    // Convert value strings to correct data types
    return readStringsToData(readData, parentWidget);
}

// Is a friend of struct DAG
void writeCurrentSettingsToD2r(QTextStream &out) {
    const auto cur = DataManager::getCurrent();
    if (!cur)
        return;

    // D2R identifier
    out << "---\n";

    // Version
    writeSingleSetting(out, "Version", D2R_VERSION);

    // UserPointConfig
    writeSingleSetting(out, "UserPointConfig", "");
    {
        writeSingleSetting(
            out, "StepSize",
            as<double>(cur->mUserPointConfig.mGradeBoundaryStepSize), true);
        writeSingleSetting(
            out, "CountZerothAttempt",
            cur->mUserPointConfig.mCountZerothAttemptAsFirstAttempt, true);
    }

    // UserGradeConfig
    writeSingleSetting(out, "UserGradeConfig", "");
    {
        writeSingleSetting(out, "RequirePassAll",
                           cur->mUserGradeConfig.mRequirePassAllParts, true);
        writeSingleSetting(out, "PassDueToBonus",
                           cur->mUserGradeConfig.mPassDueToBonus, true);
    }

    // ChangedMaxPointsClosed
    writeSingleSetting(out, "ChangedMaxPointsClosed", "");
    {
        writeSingleSetting(
            out, "ChangedMaxPoints",
            cur->mChangedMaxPointsClosed.mChangedMaxPointBoundary.mMultiple,
            true);
        writeSingleSetting(out, "FollowDefault",
                           cur->mChangedMaxPointsClosed.mFollowDefault, true);
    }

    // ChangedMaxPointsOpen
    writeSingleSetting(out, "ChangedMaxPointsOpen", "");
    {
        writeSingleSetting(
            out, "ChangedMaxPoints",
            cur->mChangedMaxPointsOpen.mChangedMaxPointBoundary.mMultiple,
            true);
        writeSingleSetting(out, "FollowDefault",
                           cur->mChangedMaxPointsOpen.mFollowDefault, true);
    }

    // UserGradeBoundaries
    for (const auto &type : {GradingType::Closed, GradingType::Open}) {
        writeSingleSetting(out, QString("UserGradeBoundaries") + toString(type),
                           "");
        {
            writeSingleSetting(out, "SetAuto",
                               cur->getUserGradeBoundarySetAuto(type), true);
            for (const auto &g : grades) {
                writeSingleSetting(
                    out, "Boundary" + QString::number(static_cast<int>(g)),
                    cur->getUserGradeBoundaries(type)[g].mMultiple, true);
            }
        }
    }

    // Eliminated tasks
    writeSingleSetting(out, QString("EliminatedTasks"), "");
    for (int i = 0; i < cur->mExam.mTaskPools.size(); ++i) {
        writeSingleSetting(
            out, QString("TaskPool%1").arg(i),
            static_cast<int>(cur->mExam.mTaskPools[i].mEliminationStatus),
            true);
    }

    // Dynexite version
    writeSingleSetting(out, "DynexiteVersion",
                       static_cast<int>(cur->mExam.mDynexite.version));

    // Dynexite CSV
    out << "Dynexite:";
    writeCsv(out, cur->mExam.mDynexite);
    out << "\\";

    // RWTHonline CSV
    out << "RWTHonline:";
    writeCsv(out, cur->mStudents.mRwthOnline);
    out << "\\";

    // Bonus CSV
    out << "Bonus:";
    writeCsv(out, cur->mBonus.mBonusPoints);
    out << "\\";
}
