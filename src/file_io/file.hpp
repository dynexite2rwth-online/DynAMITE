#pragma once

/**
 * \file file.hpp
 * \brief Handles opening and correctly decoding text files.
 */

#include <QPair>
#include <QSharedPointer>

class QFile;
class QString;
class QTextStream;

/**
 * \brief Reads the whole file at the given path into a string with normalized
 * line endings. Automatically deduces encoding.
 *
 * \param path The path to the file. The file separator has to be
 * '/' regardless of operating system.
 *
 * \return The whole file content with normalized line
 * endings.
 *
 * \exception DynamiteException Opening file failed.
 * \exception DynamiteException File is empty or an error occurred while
 * reading.
 */
[[nodiscard]] QString readFileIntoString(const QString &path);

/**
 * \brief Provides a file with a linked QTextStream that can be used to write to
 * it.
 */
[[nodiscard]] QPair<QSharedPointer<QFile>, QSharedPointer<QTextStream>>
openFileToWrite(const QString &path);
