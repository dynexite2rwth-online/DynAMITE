/**
 * \file csv_parser.cpp
 * \brief Implements functions to read and write csv data, including internal
 * helper functions.
 */

#include "csv_parser.hpp"

#include "data/csv_dump.hpp"
#include "util/exception.hpp"
#include "util/strings.hpp"

#include <QFileInfo>
#include <QRegularExpression>
#include <QTextStream>

#include <cassert>
#include <ranges>

namespace impl {

/**
 * \brief Delete everything surrounded by quotation marks because delimiters
 * inside quotation marks are escaped.
 *
 * a"b"c""""d --> acd
 */
void deleteQuotationMarksAndContent(QString &str) noexcept {
    auto pos = str.indexOf('\"');
    while (pos != -1) {
        const auto endPos = str.indexOf('\"', pos + 1);

        if (endPos == -1) {
            break;
        }

        str.remove(pos, endPos - pos + 1);

        // Because everything from pos onwards (to the next quotation mark) was
        // deleted, we start searching from pos again.
        pos = str.indexOf('\"', pos);
    }
}

/**
 * \brief Delete only outer quotation marks, leave content intact
 *
 * For every cell in the \ref CsvDump :
 * "abc" --> abc
 * ab""c --> ab""c
 */
void deleteOnlyOuterQuotationMarks(CsvDump *csv) noexcept {
    for (auto &line : csv->data) {
        for (auto &cell : line) {
            if (cell.length() >= 2 && cell.startsWith('\"') &&
                cell.endsWith('\"')) {
                cell = cell.sliced(1, cell.length() - 2);
            }
        }
    }
}
/**
 * \brief Replace escaped quotation marks ("") with single quotation marks (")
 *
 * For every cell in the \ref CsvDump :
 * ab""c --> ab"c
 */
void replaceEscapedQuotationMarks(CsvDump *csv) noexcept {
    for (auto &l : csv->data) {
        l.replaceInStrings(R"("")", R"(")");
    }
}

/**
 * \brief Removes quotation marks surrounding whole cells and un-escapes escaped
 * quotation marks.
 */
void removeControlQuotationMarks(CsvDump *csv) {
    if (!csv) {
        assert(false);
        return;
    }

    deleteOnlyOuterQuotationMarks(csv);
    replaceEscapedQuotationMarks(csv);
}

/**
 * \brief For a quotation mark at position from-1 or earlier, find a
 * corresponding closing quotation mark. That closing quotation mark has to be
 * not escaped.
 */
[[nodiscard]] qsizetype findClosingQuotationMark(const QString &str,
                                                 qsizetype from) {

    while (true) {
        const auto nextQuotationMark = str.indexOf('\"', from);

        // No closing quotation mark found: Should not happen
        if (nextQuotationMark == -1) {
            assert(false);
            return str.length();
        }

        // If the string ends after the next quotation mark, it is the closing
        // quotation mark
        if (str.length() == nextQuotationMark + 1) {
            return nextQuotationMark;
        }

        // If the next character after the quotation mark is not another
        // quotation mark, it is the closing quotation mark
        if (str[nextQuotationMark + 1] != '\"') {
            return nextQuotationMark;
        }

        // Else, we have to continue searching after the found block of two
        // quotation marks
        from = nextQuotationMark + 2;
    }
}

/**
 * \brief Search for first non-escaped (not surrounded by quotation marks)
 * occurrence of `delim`. If `delim` doesn't occur again, return `str.length()`
 */
[[nodiscard]] qsizetype findClosingDelimiter(const QString &str,
                                             const QChar delim,
                                             const qsizetype from,
                                             const bool excelStyleQMs) {

    if (from >= str.length()) {
        return str.length();
    }

    // Whether or not the cell is surrounded by quotation marks
    if (!excelStyleQMs || str[from] != '\"') {
        const auto pos = str.indexOf(delim, from);
        if (pos == -1) {
            return str.length();
        } else {
            return pos;
        }
    } else {
        // The next delimiter is after the closing quotation mark (or the line
        // ends there)
        const auto closingQMPos = findClosingQuotationMark(str, from + 1);
        assert(str.length() <= closingQMPos + 1 ||
               str[closingQMPos + 1] == delim);
        return closingQMPos + 1;
    }
}

void splitByUnescapedDelimiter(CsvDump *csv, const QStringList &fileLines,
                               const QChar delim, const qsizetype nrDelims,
                               const bool excelStyleQMs) {
    if (!csv) {
        assert(false);
        return;
    }

    csv->data.reserve(fileLines.size());
    for (auto &line : fileLines) {

        csv->data.emplace_back();

        qsizetype cellEnd = -1;
        for (qsizetype cellNr = 0; cellNr < nrDelims + 1; ++cellNr) {
            const auto cellStart = cellEnd + 1;

            if (line.length() < cellStart) {
                throw DynamiteException(
                    QString(str::error::internalCsvLineLength)
                        .arg(line.length())
                        .arg(cellStart));
            }

            cellEnd =
                findClosingDelimiter(line, delim, cellStart, excelStyleQMs);

            csv->data.back().emplace_back(
                line.sliced(cellStart, cellEnd - cellStart));
        }

        if (csv->data.back().size() != static_cast<unsigned>(nrDelims + 1)) {
            throw DynamiteException(QString(str::error::internalCsvNumberCells)
                                        .arg(csv->data.back().size())
                                        .arg(nrDelims));
        }
    }
}

/**
 * \brief Handle quotation marks in the strings
 *
 * A cell with surrounding quotation marks escapes delimiters in that cell.
 * Quotation marks themselves are escaped by doubling them.
 *
 * \warning The line a,"b,c",d can either mean
 * a | "b | c" | d    or    a | b,c | d.
 * We assume in means the latter, because that's how Excel assumes it too.
 *
 * \return True if the file seems to use Excel-style quotation marks.
 */
bool handleQuotationMarks(QStringList &fileLines, const QChar delim) {

    // Since many of our csv files don't contain quotation marks at all, just
    // skip this section if that is the case.
    if (std::ranges::none_of(
            fileLines, [](const QString &s) { return s.contains('\"'); })) {
        return false;
    }

    // If any line has an uneven number of quotation marks, then this csv file
    // doesn't escape quotation marks or delimiters and we skip the rest.
    // (We don't handle escaped newlines)
    for (const auto &s : fileLines) {
        if (const auto nr = s.count('\"'); nr % 2 == 1) {
            return false;
        }
    }

    // Delete all escaped quotation marks to make it easier to count the
    // non-escaped delimiters.
    for (auto &l : fileLines) {
        l.remove(R"("")");
    }

    // If there are single quotation marks not at the beginning or end of a
    // cell, then this csv file doesn't escape quotation marks or delimiters
    // and we skip the rest.
    // Our regex is [^;"](?<!^)"(?!$)[^;"] with ; replaced with our delimiter
    const QRegularExpression quotationNotAtCellBorder(
        QString("[^") + delim + "\"](?<!^)\"(?!$)[^" + delim + "\"]");
    for (const auto &s : fileLines) {
        if (const auto i = s.indexOf(quotationNotAtCellBorder); i != -1) {
            return false;
        }
    }

    for (auto &l : fileLines) {
        deleteQuotationMarksAndContent(l);
    }

    return true;
}

/**
 * \brief Counts the number of delimiters in each line, and throws an error if
 * the number is inconsistent.
 *
 * \param[in] fileLines  One string per line of the original file, with no empty
 * lines.
 * \param[in] delim      Delimiter character
 *
 * \exception DynamiteException The number of delimiters per line is
 * inconsistent or the given delimiter is not used in the first line.
 *
 * \return Returns the consistent number of delimiters per line, and whether or
 * not the lines use excel-style quotation marks (surround cell with special
 * characters with quotation marks)
 */
[[nodiscard]] QPair<qsizetype, bool> getNrDelims(QStringList fileLines,
                                                 const QChar delim) {
    if (fileLines.empty()) {
        return qMakePair(qsizetype{0}, false);
    }

    assert(!fileLines[0].isEmpty());

    const auto excelStyleQMs = handleQuotationMarks(fileLines, delim);

    // take number of delimiters in the first line as baseline (empty lines are
    // already removed)
    const auto nrDelims = fileLines[0].count(delim);

    if (nrDelims == 0) {
        throw DynamiteException("Specified delimiter not used");
    }

    // check that the number of delimiters in every other line is the same
    for (const auto &l : fileLines) {
        if (l.count(delim) != nrDelims) {
            throw DynamiteException(
                "Inconsistent number of delimiter characters");
        }
    }

    return qMakePair(nrDelims, excelStyleQMs);
}

/**
 * \brief Tries to guess the delimiter by guessing ';', then ',', and then 0x09
 * (tab character) and testing if that produces a valid CSV file.
 *
 * \exception DynamiteException No delimiter produces a valid CSV file.
 *
 * \return The deduced delimiter, the number of delimiters per line and whether
 * or not the file uses Excel-style quotation marks.
 */
[[nodiscard]] std::tuple<QChar, qsizetype, bool>
guessDelimiter(const QStringList &fileLines) {
    // Possible delimiter characters in decreasing likelihood
    static constexpr std::array possibleDelimiters{QChar(';'), QChar(','),
                                                   QChar('\t')};

    for (const auto delim : possibleDelimiters) {
        try {
            auto [nrDelims, excelStyleQMs] = getNrDelims(fileLines, delim);
            return std::make_tuple(delim, nrDelims, excelStyleQMs);
        } catch (...) {
            // try next
        }
    }
    throw DynamiteException(str::error::csvDelimiter);
}
QString addEscapeCharacters(QString str, const QChar delim) {

    // Escape quotation marks
    str.replace(QString("\""), QString("\"\""));

    // Add surrounding quotation marks if we have delimiters or quotation marks
    // in the string
    if (str.contains('\"') || str.contains(delim)) {
        str.prepend('\"');
        str.append('\"');
    }

    return str;
}
} // namespace impl

[[nodiscard]] auto getDynexiteVersion(const QString &path,
                                      QStringList &fileLines) noexcept {

    // First, try to read out the version from the file name
    // This should only be used for versions >= 3, but we allow it for all
    // versions
    const QFileInfo info(path);

    // Remove path and .csv, but not .v3
    const auto base = info.completeBaseName();

    if (base.endsWith(".v1")) {
        return CsvVersion::Version1;
    } else if (base.endsWith(".v2")) {
        return CsvVersion::Version2;
    } else if (base.endsWith(".v3")) {
        return CsvVersion::Version3;
    }

    // Else, look for a version indicator at the beginning of the file
    // If there is none, this is version 1 or version 3+, where somebody
    // deleted the version indicator from the file name.

    // Case that the file is completely empty
    if (fileLines.empty()) {
        return CsvVersion::Version1;
    }
    auto versionLine = fileLines[0].trimmed();
    versionLine.replace(',', ';');
    versionLine.replace('\t', ';');

    if (QString::compare(versionLine, "Version;2", Qt::CaseInsensitive) == 0) {
        // Remove the version indicator
        // There should be an empty line after the version indicator, but we
        // removed it already when reading in fileLines
        fileLines.remove(0, 1);
        return CsvVersion::Version2;
    } else {

        // Search for version 2+ headers (open max) and (closed max)
        // We replaced the delimiter with ";" , therefore we can do this:
        const auto nrOpen = versionLine.count(" (open);");
        const auto nrOpenMax = versionLine.count(" (open max);");
        if (nrOpen > 0 && nrOpen == nrOpenMax) {
            return CsvVersion::Version3;
        } else {
            return CsvVersion::Version1;
        }
    }
}

QSharedPointer<CsvDump> parseCsv(const QString &path,
                                 const QString &csvString) {

    using namespace impl;

    // store the resulting dump on the heap, as it might get quite big
    auto dump = QSharedPointer<CsvDump>::create();

    // Split into list of lines, without caring about the delimiter for now.
    // We don't have to care about system-specific line endings here, because we
    // took care of them in \ref openFile.
    // `Qt::SkipEmptyParts` makes us ignore empty lines.
    auto fileLines = csvString.split('\n', Qt::SkipEmptyParts);

    // Guess dynexite version from file name and possible version indicator,
    // also removes the first line with the version indicator from fileLines if
    // found.
    dump->version = getDynexiteVersion(path, fileLines);

    // Obtain number of delimiters per line, or throw std::runtime_error if it
    // isn't consistent
    const auto [delim, nrDelims, excelStyleQMs] = guessDelimiter(fileLines);

    // Write text without delimiters into the CsvDump
    splitByUnescapedDelimiter(dump.data(), fileLines, delim, nrDelims,
                              excelStyleQMs);

    removeControlQuotationMarks(dump.data());

    return dump;
}

void writeCsv(QTextStream &out, const CsvDump &csvDump, const QChar delim) {

    using namespace impl;

    for (const auto &line : csvDump.data) {

        // Delimiter before every item except the first item
        if (!line.empty()) {
            out << addEscapeCharacters(line[0], delim);
            for (const auto &v : line | std::views::drop(1)) {
                out << delim << addEscapeCharacters(v, delim);
            }
        }

        // Newline after all items, including last
        out << '\n';
    }
}
