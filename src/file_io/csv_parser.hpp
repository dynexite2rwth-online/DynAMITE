#pragma once

#include <QSharedPointer>

class QTextStream;
struct CsvDump;

namespace impl {
void deleteQuotationMarksAndContent(QString &str) noexcept;
void deleteOnlyOuterQuotationMarks(CsvDump *csv) noexcept;
void replaceEscapedQuotationMarks(CsvDump *csv) noexcept;
void removeControlQuotationMarks(CsvDump *csv);
[[nodiscard]] qsizetype findClosingQuotationMark(const QString &str,
                                                 qsizetype from);
[[nodiscard]] qsizetype findClosingDelimiter(const QString &str, QChar delim,
                                             qsizetype from,
                                             bool excelStyleQMs);
void splitByUnescapedDelimiter(CsvDump *csv, const QStringList &fileLines,
                               QChar delim, qsizetype nrDelims,
                               bool excelStyleQMs);
bool handleQuotationMarks(QStringList &fileLines, QChar delim);
[[nodiscard]] QPair<qsizetype, bool> getNrDelims(QStringList fileLines,
                                                 QChar delim);
[[nodiscard]] std::tuple<QChar, qsizetype, bool>
guessDelimiter(const QStringList &fileLines);
QString addEscapeCharacters(QString str, QChar delim);
} // namespace impl

/**
 * \brief Parses the given string into a \ref CsvDump if possible.
 *
 * Checks if the number of delimiters per line is consistent and throws an
 * `std::runtime_error` if that is not the case. Afterwards it stores the data
 * in a \ref CsvDump.
 *
 * It tries the following delimiters in order until one seems plausible:
 * `;` `,` `\t`
 *
 * If you don't care about the version because you don't pass a dynexite file,
 * set the path to "data.v1.csv" for minimal performance penalty.
 *
 * \param[in] path Path to the csv file (for reading out the version)
 * \param[in] csvString String with csv data
 *
 * \exception DynamiteException The number of delimiters per line is
 * inconsistent.
 *
 * \return Returns a pointer to the extracted CSV-data in the given string.
 */
[[nodiscard]] QSharedPointer<CsvDump> parseCsv(const QString &path,
                                               const QString &csvString);

/**
 * \brief Exports the given csv dump to the given stream.
 */
void writeCsv(QTextStream &out, const CsvDump &csvDump, QChar delim = ';');
