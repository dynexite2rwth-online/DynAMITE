#pragma once

#include "data/nodes/bonus_points.hpp"
#include "data/nodes/dynexite_exam.hpp"
#include "data/nodes/student_data.hpp"
#include "util/defaults.hpp"
#include "util/grade.hpp"
#include "util/grade_boundary_step.hpp"

#include <QSharedPointer>
#include <QString>

#include <array>

class QWidget;
struct CsvDump;
class QTextStream;

struct D2rSettings {

    /*
     * When adding new settings, remember to update "newOptions" in
     * "readStringsToData".
     */

    QString mVersion{};

    bool mUserPointConfigCountZero = defaults::countZerothAttemptAsFirstAttempt;

    bool mUserGradeConfigRequirePassAll = defaults::requirePassAllParts;
    bool mUserGradeConfigPassDueToBonus = defaults::passDueToBonus;

    int mChangedMaxPointsClosed{};
    bool mChangedMaxPointsClosedFollowDefault{};

    int mChangedMaxPointsOpen{};
    bool mChangedMaxPointsOpenFollowDefault{};

    bool mUserGradeBoundariesClosedAuto{};
    std::array<int, grades.size()> mUserGradeBoundariesClosed{};

    bool mUserGradeBoundariesOpenAuto{};
    std::array<int, grades.size()> mUserGradeBoundariesOpen{};
};

/**
 * \brief Contains all data loaded from a d2r file
 */
struct ExamSave {
    ExamSave() = default;
    ExamSave(const CsvDump &dynexite, const CsvDump &rwthOnline,
             const CsvDump &bonus, GradeBoundaryStepSize stepSize,
             const D2rSettings &settings);

    DynexiteExam mDynexite;
    StudentData mStudents;
    BonusPoints mBonus;
    GradeBoundaryStepSize mStepSize; /* TODO? */
    D2rSettings mSettings;
};

[[nodiscard]] QSharedPointer<ExamSave> parseD2r(QString d2rString,
                                                QWidget *parentWidget);

void writeCurrentSettingsToD2r(QTextStream &out);
