#define QT_EXPLICIT_QFILE_CONSTRUCTION_FROM_PATH
#include "file.hpp"

#include "util/exception.hpp"

#include <QDebug>
#include <QFile>
#include <QString>
#include <QStringDecoder>
#include <QTextStream>

QString readFileIntoString(const QString &path) {
    QFile file(path);

    if (!file.open(QIODevice::ReadOnly)) {
        throw DynamiteException(
            "Failed to open a file to read from. Last device error: " +
            file.errorString());
    }

    // todo: make sure the file is not overly long, else we might run out of
    //       memory here

    // Dump all into byte array
    const auto text = file.readAll();

    if (text.isEmpty()) {
        throw DynamiteException("File is empty OR an error occurred while "
                                "reading. Last device error: " +
                                file.errorString());
    }

    QString result;
    {
        QStringDecoder utfDecoder(QStringConverter::Utf8);
        const QString fromUtf = utfDecoder(text);

        if (!utfDecoder.hasError()) {
            // UTF8 file
            result = fromUtf;
        } else {
            // ISO 8859-1 file (or something different, but all our files are
            // either UTF8 or ISO 8859-1
            QStringDecoder isoDecoder(QStringConverter::Latin1);
            const QString fromIso = isoDecoder(text);

            if (isoDecoder.hasError()) {
                qDebug() << "ISO decoding still has errors";
            }

            result = fromIso;
        }
    }

    // Standardize line endings
    result.replace(QString("\r\n"), QString("\n"));
    result.replace(QString("\r"), QString("\n"));

    return result;
}

QPair<QSharedPointer<QFile>, QSharedPointer<QTextStream>>
openFileToWrite(const QString &path) {

    auto file = QSharedPointer<QFile>::create(path);

    if (!file->open(QIODevice::WriteOnly)) {
        throw DynamiteException(
            "Failed to open a file to write to. Last device error: " +
            file->errorString());
    }

    auto out = QSharedPointer<QTextStream>::create(file.data());
    return qMakePair(file, out);
}
