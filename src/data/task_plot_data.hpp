#pragma once

#include "util/grade_boundary_step.hpp"

#include <QColor>
#include <QList>
#include <QMetaType>

struct TaskPlotData {
    QList<qreal> mPoints{};
    qreal mMaxPoints = 0.;
    GradeBoundaryStepSize mStepSize = GradeBoundaryStepSize::Min1_00;

    QColor mBackgroundColor{};
};
Q_DECLARE_METATYPE(TaskPlotData);
