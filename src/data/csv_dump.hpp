#pragma once

#include <QList>

class QString;

/**
 * \brief Short hand for a data record.
 */
using CsvLine = QList<QString>;

/**
 * \brief Version indicator for the CSV data
 *
 * Only relevant for dynexite:
 *
 * Version 1:
 * - no version indicator
 * - columns for open, closed, earned, max
 *
 * Version 2:
 * - 2023/11
 * - version indicator "Version;2" in first line
 * - columns for open, open max, closed, closed max, earned, max
 *
 * Version 3:
 * - 2024/1
 * - file name "[name].v[X].csv" instead of version indicator in first line
 * - otherwise no changes
 */
enum class CsvVersion {
    Version1 = 1,
    Version2,
    Version3,
};

/**
 * \brief Struct storing the data from a CSV-file in memory, with the cells
 * separated into strings and the delimiters removed.
 */
struct CsvDump {
    CsvVersion version = CsvVersion::Version1;
    QList<QStringList> data;
};
