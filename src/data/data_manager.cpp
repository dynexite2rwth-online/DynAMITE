#include "data_manager.hpp"

#include "file_io/d2r_parser.hpp"

#include <cassert>

DataManager &DataManager::getInstance() noexcept {
    // Guaranteed to be destroyed, instantiated on first use.
    static DataManager instance;

    // Whenever grades change, set the "unsaved changes" flag
    connect(&instance, &DataManager::gradesChanged,
            [] { setUnsavedChanges(true); });

    return instance;
}

bool DataManager::loadNewExam(const DynexiteExam &exam,
                              const StudentData &students,
                              const BonusPoints &bonus,
                              const UserGradeConfig &gradeConfig,
                              const UserPointConfig &pointConfig) noexcept {

    // Try to create a DAG object, if successful, move it into mNew,
    // else leave mNew untouched

    // Currently, creating a DAG cannot fail (except bad_alloc, which
    // we don't care about and results in immediate std::terminate)
    getInstance().mNew =
        std::make_unique<DAG>(exam, students, bonus, gradeConfig, pointConfig);

    return true;
}
void DataManager::makeNewCurrent(const bool emitGradesChanged) noexcept {

    getInstance().mCurrent = std::move(getInstance().mNew);
    // mNew is nullptr after this move

    Q_EMIT getInstance().examChanged();
    Q_EMIT getInstance().taskEliminationChanged(
        EnumStorage<bool>{true, true, true});
    if (emitGradesChanged) {
        Q_EMIT getInstance().gradesChanged(EnumStorage<bool>{true, true, true});
    }

    // Whenever a new exam is loaded, unset the "unsaved changes" flag
    setUnsavedChanges(false);
}

const DAG *DataManager::getCurrent() noexcept {
    return getInstance().mCurrent.get();
}

void DataManager::maxChange(const GradingType grading,
                            const GradeBoundary newMax, DAG *alternateExam,
                            const bool emitGradesChanged) noexcept {
    auto *configuredExam =
        alternateExam ? alternateExam : getInstance().mCurrent.get();

    if (!configuredExam)
        return;

    switch (grading) {
    default: // fallthrough, "combined" not allowed
        assert(false);
        [[fallthrough]];
    case GradingType::Closed:
        configuredExam->mChangedMaxPointsClosed.setMax(newMax);
        configuredExam->updateMemberNodesIfNecessary<
            ChangedMaxPoints<GradingType::Closed>>();
        break;
    case GradingType::Open:
        configuredExam->mChangedMaxPointsOpen.setMax(newMax);
        configuredExam->updateMemberNodesIfNecessary<
            ChangedMaxPoints<GradingType::Open>>();
        break;
    }

    if (!alternateExam && emitGradesChanged) {
        Q_EMIT getInstance().gradesChanged(
            EnumStorage<bool>{grading == GradingType::Closed,
                              grading == GradingType::Open, true});
    }
}

bool DataManager::boundaryChange(const GradingType grading,
                                 const Grade boundaryGrade,
                                 const Points newBoundaryPoints,
                                 DAG *alternateExam,
                                 const bool emitGradesChanged) noexcept {
    const auto *configuredExam =
        alternateExam ? alternateExam : getInstance().mCurrent.get();

    if (!configuredExam)
        return false;

    if (std::isnan(newBoundaryPoints.p))
        return false;

    const GradeBoundary newBoundary(
        newBoundaryPoints,
        configuredExam->mUserPointConfig.mGradeBoundaryStepSize,
        configuredExam->mUserPointConfig.mGradeBoundaryRounding);

    return boundaryChange(grading, boundaryGrade, newBoundary, alternateExam,
                          emitGradesChanged);
}

bool DataManager::boundaryChange(const GradingType grading,
                                 const Grade boundaryGrade,
                                 const GradeBoundary newBoundary,
                                 DAG *alternateExam,
                                 const bool emitGradesChanged) noexcept {
    auto *configuredExam =
        alternateExam ? alternateExam : getInstance().mCurrent.get();

    if (!configuredExam)
        return false;

    bool ret;

    switch (grading) {
    default: // fallthrough, "combined" not allowed
        assert(false);
        [[fallthrough]];
    case GradingType::Closed:
        ret = configuredExam->mClosedUserBoundaries.setBoundary(newBoundary,
                                                                boundaryGrade);
        configuredExam->updateMemberNodesIfNecessary<
            UserGradeBoundaries<GradingType::Closed>>();
        break;
    case GradingType::Open:
        ret = configuredExam->mOpenUserBoundaries.setBoundary(newBoundary,
                                                              boundaryGrade);
        configuredExam->updateMemberNodesIfNecessary<
            UserGradeBoundaries<GradingType::Open>>();
        break;
    }

    if (!alternateExam && emitGradesChanged) {
        Q_EMIT getInstance().gradesChanged(
            EnumStorage<bool>{grading == GradingType::Closed,
                              grading == GradingType::Open, true});
    }

    return ret;
}

void DataManager::autoBoundaryChange(const GradingType grading,
                                     const bool followDefault,
                                     DAG *alternateExam,
                                     const bool emitGradesChanged) noexcept {
    auto *configuredExam =
        alternateExam ? alternateExam : getInstance().mCurrent.get();

    if (!configuredExam)
        return;

    switch (grading) {
    default: // fallthrough, "combined" not allowed
        assert(false);
        [[fallthrough]];
    case GradingType::Closed:
        configuredExam->mClosedUserBoundaries.setFollowDefaultAll(
            followDefault);
        configuredExam->updateMemberNodesIfNecessary<
            UserGradeBoundaries<GradingType::Closed>>();
        break;
    case GradingType::Open:
        configuredExam->mOpenUserBoundaries.setFollowDefaultAll(followDefault);
        configuredExam->updateMemberNodesIfNecessary<
            UserGradeBoundaries<GradingType::Open>>();
        break;
    }

    if (!alternateExam && emitGradesChanged) {
        Q_EMIT getInstance().gradesChanged(
            EnumStorage<bool>{grading == GradingType::Closed,
                              grading == GradingType::Open, true});
    }
}

void DataManager::gradeConfigChange(const UserGradeConfig &newConfig,
                                    DAG *alternateExam,
                                    const bool emitGradesChanged) noexcept {
    auto *configuredExam =
        alternateExam ? alternateExam : getInstance().mCurrent.get();

    if (!configuredExam)
        return;

    configuredExam->mUserGradeConfig = newConfig;
    configuredExam->updateMemberNodesIfNecessary<UserGradeConfig>();

    if (!alternateExam && emitGradesChanged) {
        Q_EMIT getInstance().gradesChanged({true, true, true});
    }
}

void DataManager::pointConfigChange(const UserPointConfig &newConfig,
                                    DAG *alternateExam,
                                    const bool emitGradesChanged) noexcept {
    auto *configuredExam =
        alternateExam ? alternateExam : getInstance().mCurrent.get();

    if (!configuredExam)
        return;

    configuredExam->mUserPointConfig = newConfig;
    configuredExam->updateMemberNodesIfNecessary<UserPointConfig>();

    if (!alternateExam && emitGradesChanged) {
        Q_EMIT getInstance().gradesChanged({true, true, true});
    }
}

void DataManager::taskEliminationChange(const int taskPoolIdx,
                                        const TaskEliminationType newElimType,
                                        const bool emitGradesChanged) noexcept {

    using enum GradingType;

    auto *configuredExam = getInstance().mCurrent.get();

    assert(taskPoolIdx < configuredExam->mExam.mTaskPools.size());

    auto &poolElimStatus =
        configuredExam->mExam.mTaskPools[taskPoolIdx].mEliminationStatus;
    const auto poolHasClosed =
        configuredExam->mExam.mTaskPools[taskPoolIdx].mMaxPoints[Closed].p > 0.;
    const auto poolHasOpen =
        configuredExam->mExam.mTaskPools[taskPoolIdx].mMaxPoints[Open].p > 0.;

    if (poolElimStatus == newElimType) {
        // Nothing changes
        return;
    }

    poolElimStatus = newElimType;

    if (poolHasClosed)
        configuredExam->mClosedPointsWithoutBonus.recalculate();

    if (poolHasOpen)
        configuredExam->mOpenPointsWithoutBonus.recalculate();

    if (poolHasClosed || poolHasOpen) {
        configuredExam->mCombinedPointsWithoutBonus.recalculate();

        configuredExam->updateMemberNodesIfNecessary<
            PointsWithoutBonus<Closed>, PointsWithoutBonus<Open>,
            PointsWithoutBonus<Combined>>();

        const auto changes = getStorage(poolHasClosed, poolHasOpen, true);

        Q_EMIT getInstance().taskEliminationChanged(changes);
        if (emitGradesChanged) {
            Q_EMIT getInstance().gradesChanged(changes);
        }
    }
    // Nothing needs to be done if pool has no open or closed points
}

void DataManager::applySettings(const D2rSettings &s) {

    const auto cur = getCurrent();

    if (!cur)
        return;

    if (s.mVersion.isEmpty())
        return;

    // count zero
    {
        auto currentConfig = cur->mUserPointConfig;
        currentConfig.mCountZerothAttemptAsFirstAttempt =
            s.mUserPointConfigCountZero;
        pointConfigChange(currentConfig, nullptr, false);
    }

    // user grade config
    {
        auto currentConfig = cur->mUserGradeConfig;
        currentConfig.mRequirePassAllParts = s.mUserGradeConfigRequirePassAll;
        currentConfig.mPassDueToBonus = s.mUserGradeConfigPassDueToBonus;
        gradeConfigChange(currentConfig, nullptr, false);
    }

    // max boundary changes
    maxChange(GradingType::Closed, GradeBoundary{s.mChangedMaxPointsClosed},
              nullptr, false);
    maxChange(GradingType::Open, GradeBoundary{s.mChangedMaxPointsOpen},
              nullptr, false);

    // changedMaxPointsClosedFollowDefault isn't used at the moment (?)

    // autoBoundaryChange
    autoBoundaryChange(GradingType::Closed, s.mUserGradeBoundariesClosedAuto,
                       nullptr, false);
    autoBoundaryChange(GradingType::Open, s.mUserGradeBoundariesOpenAuto,
                       nullptr, false);

    // grade boundaries
    for (auto i = static_cast<int>(grades.size()) - 1; i >= 0; --i) {

        // make unsigned for array access
        const auto ui = static_cast<decltype(grades)::size_type>(i);

        boundaryChange(GradingType::Closed, grades[ui],
                       GradeBoundary{s.mUserGradeBoundariesClosed[ui]}, nullptr,
                       false);
        boundaryChange(GradingType::Open, grades[ui],
                       GradeBoundary{s.mUserGradeBoundariesOpen[ui]}, nullptr,
                       false);
    }

    Q_EMIT getInstance().gradesChanged({true, true, true});

    // Don't set the "unsaved changes" flag
    setUnsavedChanges(false);
}

void DataManager::setUnsavedChanges(const bool hasUnsavedChanges) noexcept {
    mUnsavedChanges = hasUnsavedChanges;
}
bool DataManager::hasUnsavedChanges() noexcept { return mUnsavedChanges; }

bool DataManager::mUnsavedChanges = false;
