#pragma once

#include "changed_max_points.hpp"
#include "grade_list.hpp"
#include "node.hpp"

#include "util/enums.hpp"
#include "util/grade.hpp"

#include <QList>

struct UserGradeConfig;

struct CombinedGradeList
    : Node<ChangedMaxPoints<GradingType::Closed>,
           ChangedMaxPoints<GradingType::Open>, GradeList<GradingType::Closed>,
           GradeList<GradingType::Open>, UserGradeConfig> {
    using Node::Node;

    void recalculate();

    QList<Grade> mGrades{};
};
