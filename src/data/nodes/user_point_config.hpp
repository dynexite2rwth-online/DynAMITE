#pragma once

#include "node.hpp"

#include "util/defaults.hpp"
#include "util/grade_boundary_step.hpp"

/**
 * \brief Options regarding points.
 */
struct UserPointConfig : Node<> {
    using Node::Node;

    /**
     * \brief Every grade boundary is rounded to a multiple of this.
     */
    GradeBoundaryStepSize mGradeBoundaryStepSize = defaults::boundaryStepSize;

    /**
     * \brief The grade boundaries are the calculated precise boundaries rounded
     * up or down according to this value.
     */
    GradeBoundaryRounding mGradeBoundaryRounding = defaults::boundaryRounding;

    /**
     * \brief The RWTHonline file sometimes contains students with 0 attempts
     * (usually "1 attempt" means "first attempt"), this settings handles
     * whether or not to count them as students on their first attempt.
     */
    bool mCountZerothAttemptAsFirstAttempt =
        defaults::countZerothAttemptAsFirstAttempt;
};
