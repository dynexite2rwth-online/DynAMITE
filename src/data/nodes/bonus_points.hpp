#pragma once

#include "node.hpp"

#include "data/csv_dump.hpp"
#include "util/points.hpp"

#include <QHash>
#include <QString>

/**
 * \brief Contains the data from a bonus point-csv-file
 */
struct BonusPoints : Node<> {
    using Node::Node;

    /**
     * \brief Construct an empty bonus point list, meaning no students gets any
     * bonus points.
     */
    BonusPoints() = default;

    /**
     * \brief Try to take the bonus point data from the given csv dump. Throws
     * if this fails.
     */
    explicit BonusPoints(const CsvDump &c) noexcept(false);

    /**
     * \brief A map of all students found in the given csv file and their
     * corresponding number of bonus points. The registration number may be
     * non-numerical.
     */
    QHash<QString, Points> mTotalBonusPointsPerStudent{};

    /**
     * \brief The maximum amount of bonus points any student got.
     */
    Points mMaxBonusPoints{0};

    /**
     * \brief A map of all students with invalid (non-numerical) registration
     * numbers and/or with invalid bonus point counts. If only the registration
     * number is non-numerical, then the student is in both this map and in the
     * regular map `mTotalBonusPointsPerStudent`. The key is the registration
     * number and the value is the bonus point count.
     */
    QHash<QString, QString> mInvalidRegNrs{};

    /**
     * \brief Store the entire csv dump so that we can more easily export it
     * later.
     */
    CsvDump mBonusPoints;
};
