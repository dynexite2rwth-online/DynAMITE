#include "changed_max_points.hpp"

#include "dynexite_exam.hpp"
#include "user_point_config.hpp"

#include "util/enums.hpp"

#include <algorithm>

template <GradingType Grading>
void ChangedMaxPoints<Grading>::recalculate() {

    const auto &pointConfigRef =
        std::get<const UserPointConfig &>(this->mParentRefs);
    const auto &pointsRef =
        std::get<const PointsWithoutBonus<Grading> &>(this->mParentRefs);

    // If the exam has changed, we reset mFollowDefault to true.
    // But since in our current implementation the exam never changes (a new
    // object is created with a new exam), we don't have to do anything

    // TODO: What if the mMaxPoints is not a multiple of stepSize?
    // Max points does not have to be a grade boundary
    mOriginalMaxPointBoundary = GradeBoundary(
        pointsRef.mMaxPoints, pointConfigRef.mGradeBoundaryStepSize,
        pointConfigRef.mGradeBoundaryRounding);

    if (mFollowDefault) {
        // round the precise max points up or down to the next grade boundary
        // step, according to the point configuration
        mChangedMaxPointBoundary = mOriginalMaxPointBoundary;
    } else {

        // calculate the min possible boundary
        const auto smallestMaxPointBoundary =
            GradeBoundary{Points{0.}, pointConfigRef.mGradeBoundaryStepSize,
                          pointConfigRef.mGradeBoundaryRounding};

        // We use mChangedMaxPoints instead of mChangedMaxPointBoundary here,
        // because mChangedMaxPointBoundary might have been invalidated by a
        // changed stepSize
        mChangedMaxPointBoundary =
            std::clamp(GradeBoundary{mChangedMaxPoints,
                                     pointConfigRef.mGradeBoundaryStepSize,
                                     pointConfigRef.mGradeBoundaryRounding},
                       smallestMaxPointBoundary, mOriginalMaxPointBoundary);
    }

    mChangedMaxPoints = toPoints(mChangedMaxPointBoundary,
                                 pointConfigRef.mGradeBoundaryStepSize);
}

template <GradingType Grading>
void ChangedMaxPoints<Grading>::setFollowDefault(
    const bool followDefault) noexcept {
    mFollowDefault = followDefault;

    recalculate();
}

template <GradingType Grading>
void ChangedMaxPoints<Grading>::setMax(
    const GradeBoundary &maxPointBoundary) noexcept {

    const auto &pointConfigRef =
        std::get<const UserPointConfig &>(this->mParentRefs);

    mFollowDefault = false;
    mChangedMaxPointBoundary = maxPointBoundary;
    mChangedMaxPoints = toPoints(mChangedMaxPointBoundary,
                                 pointConfigRef.mGradeBoundaryStepSize);

    recalculate();
}

template struct ChangedMaxPoints<GradingType::Closed>;
template struct ChangedMaxPoints<GradingType::Open>;
