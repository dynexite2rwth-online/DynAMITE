#include "combined_grade_list.hpp"

#include "user_grade_config.hpp"

#include <algorithm>
#include <cassert>
#include <ranges>
#include <utility>

void CombinedGradeList::recalculate() {

    const auto &closedMaxRef =
        std::get<const ChangedMaxPoints<GradingType::Closed> &>(
            this->mParentRefs);
    const auto &openMaxRef =
        std::get<const ChangedMaxPoints<GradingType::Open> &>(
            this->mParentRefs);

    const auto &closedGradesRef =
        std::get<const GradeList<GradingType::Closed> &>(this->mParentRefs);
    const auto &openGradesRef =
        std::get<const GradeList<GradingType::Open> &>(this->mParentRefs);

    const auto &configRef =
        std::get<const UserGradeConfig &>(this->mParentRefs);

    // Since we want to compute the average for each pair of grades, the
    // two lists have to have the same size.
    assert(closedGradesRef.mGrades.size() == openGradesRef.mGrades.size());

    // Resize the QList, reset the grades
    mGrades = QList<Grade>(closedGradesRef.mGrades.size());

    const auto closedWeight = closedMaxRef.mChangedMaxPointBoundary.mMultiple;
    const auto openWeight = openMaxRef.mChangedMaxPointBoundary.mMultiple;
    const auto requirePassAllParts = configRef.mRequirePassAllParts;

    // Takes an element from the list of closed and open grades, calculates the
    // average of those two grades together with their weights and stores it in
    // mGrades
    std::ranges::transform(closedGradesRef.mGrades, openGradesRef.mGrades,
                           std::ranges::begin(mGrades),
                           [closedWeight, openWeight, requirePassAllParts](
                               const Grade &gClosed, const Grade &gOpen) {
                               return getGradeAverage(closedWeight, gClosed,
                                                      openWeight, gOpen,
                                                      requirePassAllParts);
                           });
}
