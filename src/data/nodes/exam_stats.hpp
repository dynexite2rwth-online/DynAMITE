#pragma once

#include "node.hpp"

struct DynexiteExam;
struct StudentData;

struct ExamStats : Node<DynexiteExam, StudentData> {
    using Node::Node;

    void recalculate() noexcept;

    /**
     * \brief Number of students who attended (have points in DynexiteExam).
     */
    int mNrAttended = 0;

    /**
     * \brief Number of students who registered (are listed in StudentData).
     */
    int mNrRegistered = 0;

    /**
     * \brief Number of students who both attended and registered.
     */
    int mNrAttendedRegistered = 0;

    /**
     * \brief Number of students who attended but were not registered.
     */
    int mNrAttendedNotRegistered = 0;

    /**
     * \brief Number of students who were registered but did not attend.
     */
    int mNrRegisteredNotAttended = 0;
};
