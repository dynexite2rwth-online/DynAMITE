#pragma once

#include "node.hpp"

#include "data/csv_dump.hpp"

#include <QList>
#include <QString>

struct StudentData : Node<> {
    using Node::Node;

    /**
     * \brief Interprets the CsvDump as an RWTHOnline-csv-file and copies all
     * necessary info. Also runs sanity checks on the given data and throws on
     * error.
     *
     * \exception DynamiteException Parsing the csv file failed. More
     * information is in the message of the exception.
     */
    explicit StudentData(const CsvDump &c) noexcept(false);

    QList<QString> mCourseTitles;
    QList<QString> mSemesters;

    QList<QString> mRegistrationNr;
    QList<int> mAttemptNr;

    // Store the entire csv dump so that we can more easily export it later
    CsvDump mRwthOnline;

private:
    // internal helper functions
    void reserveArrays(qsizetype nrStudents) noexcept;
    void
    insertData(const CsvDump &c,
               const std::array<qsizetype, 4> &headerIndices) noexcept(false);
    /**
     * \brief Internal helper function. After the member objects have been
     * initialized, checks if there are inconsistencies.
     */
    void checkDataSanity() const noexcept(false);
};
