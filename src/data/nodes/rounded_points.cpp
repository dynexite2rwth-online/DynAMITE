#include "rounded_points.hpp"

#include "util/defaults.hpp"

#include <cassert>

template <GradingType Grading>
void RoundedPoints<Grading>::recalculate() {
    const auto &pointsWithoutBonusRef =
        std::get<const PointsWithoutBonus<Grading> &>(this->mParentRefs);
    const auto &pointsWithBonusRef =
        std::get<const StudentPoints<Grading> &>(this->mParentRefs);

    // Resize the QLists, reset the points
    mRoundedPointsWithoutBonus =
        QList<Points>(pointsWithoutBonusRef.mEarnedPoints.size());
    mRoundedPointsWithBonus =
        QList<Points>(pointsWithBonusRef.mPointsWithBonus.size());

    assert(mRoundedPointsWithoutBonus.size() == mRoundedPointsWithBonus.size());

    const auto roundingMultiplier =
        std::pow(10., defaults::pointsDecimalDigits);

    // Round the points
    for (qsizetype i = 0; i < mRoundedPointsWithoutBonus.size(); ++i) {

        mRoundedPointsWithoutBonus[i] =
            Points{std::round(pointsWithoutBonusRef.mEarnedPoints[i].p *
                              roundingMultiplier) /
                   roundingMultiplier};
    }
    for (qsizetype i = 0; i < mRoundedPointsWithBonus.size(); ++i) {

        mRoundedPointsWithBonus[i] =
            Points{std::round(pointsWithBonusRef.mPointsWithBonus[i].p *
                              roundingMultiplier) /
                   roundingMultiplier};
    }
}

void RoundedPoints<GradingType::Combined>::recalculate() {

    const auto &closedRef =
        std::get<const RoundedPoints<GradingType::Closed> &>(this->mParentRefs);
    const auto &openRef =
        std::get<const RoundedPoints<GradingType::Open> &>(this->mParentRefs);

    assert(closedRef.mRoundedPointsWithBonus.size() ==
           openRef.mRoundedPointsWithBonus.size());

    // Resize the QLists, reset the points
    mRoundedPointsWithoutBonus =
        QList<Points>(closedRef.mRoundedPointsWithoutBonus.size());
    mRoundedPointsWithBonus =
        QList<Points>(closedRef.mRoundedPointsWithBonus.size());

    // Add the points
    for (qsizetype i = 0; i < mRoundedPointsWithoutBonus.size(); ++i) {

        mRoundedPointsWithoutBonus[i] =
            closedRef.mRoundedPointsWithoutBonus[i] +
            openRef.mRoundedPointsWithoutBonus[i];
    }
    for (qsizetype i = 0; i < mRoundedPointsWithBonus.size(); ++i) {

        mRoundedPointsWithBonus[i] = closedRef.mRoundedPointsWithBonus[i] +
                                     openRef.mRoundedPointsWithBonus[i];
    }
}

template struct RoundedPoints<GradingType::Closed>;
template struct RoundedPoints<GradingType::Open>;
