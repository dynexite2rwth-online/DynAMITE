#include "points_without_bonus.hpp"

#include "dynexite_exam.hpp"
#include "util/numeric.hpp"

template <GradingType Grading>
void PointsWithoutBonus<Grading>::recalculate() {

    const auto &examRef = std::get<const DynexiteExam &>(this->mParentRefs);

    // Store all max points from non-eliminated tasks, so that they can be
    // summed up later in a numerically stable way.
    QList<Points> maxPointsNonEliminatedTasks;
    maxPointsNonEliminatedTasks.reserve(examRef.mTaskPools.size());

    // Store all earned points for all students (regular for normal tasks, none
    // for withdrawn tasks, full points for creditFull tasks), so that they can
    // be summed up later in a numerically stable way.
    QList<QList<Points>> studentEarnedPoints;
    studentEarnedPoints.resize(examRef.mIdentifier.size());
    mEarnedPoints.resize(examRef.mIdentifier.size());

    // Store earned points for every student and max points for every task in
    // the intermediate lists maxPointsNonEliminatedTasks and
    // studentEarnedPoints dependent on the TaskEliminationType.
    for (const auto &pool : examRef.mTaskPools) {
        if (pool.mEliminationStatus == TaskEliminationType::withdrawTask) {
            // Don't add anything to mEarnedPoints[studentIdx] or mMaxPoints

        } else {
            assert(pool.mEliminationStatus == TaskEliminationType::creditFull ||
                   pool.mEliminationStatus ==
                       TaskEliminationType::notEliminated);

            // Add mMaxPoints
            maxPointsNonEliminatedTasks.push_back(pool.mMaxPoints[Grading]);

            // Add earned points for each student
            for (const auto &task : pool.mTasks) {
                for (int studentIdx = 0;
                     studentIdx < examRef.mIdentifier.size(); ++studentIdx) {

                    if (task->mParticipatedStudents.testBit(studentIdx)) {
                        if (pool.mEliminationStatus ==
                            TaskEliminationType::notEliminated) {

                            // Add earned points from that task
                            studentEarnedPoints[studentIdx].push_back(
                                *task->mEarnedPoints[Grading][studentIdx]);
                        } else {
                            // Add all achievable points from that task
                            studentEarnedPoints[studentIdx].push_back(
                                task->mMaxPoints[Grading]);
                        }
                    }
                }
            }
        }
    }

    // Perform numerically stable summations
    mMaxPoints = kahanSum(maxPointsNonEliminatedTasks.begin(),
                          maxPointsNonEliminatedTasks.end());

    for (int studentIdx = 0; studentIdx < examRef.mIdentifier.size();
         ++studentIdx) {
        mEarnedPoints[studentIdx] =
            kahanSum(studentEarnedPoints[studentIdx].begin(),
                     studentEarnedPoints[studentIdx].end());
    }
}

template struct PointsWithoutBonus<GradingType::Closed>;
template struct PointsWithoutBonus<GradingType::Open>;
template struct PointsWithoutBonus<GradingType::Combined>;
