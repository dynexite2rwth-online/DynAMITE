#include "applied_bonus_points.hpp"

#include "bonus_points.hpp"

#include "util/points.hpp"

#include <cassert>

template <GradingType Grading>
void AppliedBonusPoints<Grading>::recalculate() {

    const auto &bonusRef = std::get<const BonusPoints &>(this->mParentRefs);
    const auto &maxClosedRef =
        std::get<const ChangedMaxPoints<GradingType::Closed> &>(
            this->mParentRefs);
    const auto &maxOpenRef =
        std::get<const ChangedMaxPoints<GradingType::Open> &>(
            this->mParentRefs);

    mBonusPointsPerStudent = bonusRef.mTotalBonusPointsPerStudent;

    // Calculate how to distribute the bonus points to the open/closed part
    const auto totalPoints = maxClosedRef.mChangedMaxPointBoundary +
                             maxOpenRef.mChangedMaxPointBoundary;

    const auto fraction =
        totalPoints.mMultiple > 0
            ? std::get<const ChangedMaxPoints<Grading> &>(this->mParentRefs)
                      .mChangedMaxPointBoundary /
                  totalPoints
            : 0.5;

    assert(fraction >= 0. && fraction <= 1.);

    for (auto &p : mBonusPointsPerStudent) {
        p *= fraction;
    }
}

template struct AppliedBonusPoints<GradingType::Closed>;
template struct AppliedBonusPoints<GradingType::Open>;
