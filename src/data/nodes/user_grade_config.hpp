#pragma once

#include "node.hpp"

#include "util/defaults.hpp"

/**
 * \brief Options regarding grades.
 */
struct UserGradeConfig : Node<> {
    using Node::Node;

    /**
     * \brief If this is set, then a student can only get a passing grade if
     * both the open and closed grade was passing.
     */
    bool mRequirePassAllParts = defaults::requirePassAllParts;

    /**
     * \brief If this is set, then bonus points are credited even if the exam
     * (without bonus points) is assessed as "failed" (5.0). Therefore a student
     * can pass with less than the minimal points, if they have enough bonus
     * points.
     */
    bool mPassDueToBonus = defaults::passDueToBonus;
};
