#pragma once

#include "node.hpp"

#include "util/enums.hpp"
#include "util/points.hpp"

#include <QList>

struct DynexiteExam;

/**
 * \brief Calculates the points without bonus for each student, taking into
 * account eliminated tasks.
 */
template <GradingType Grading>
struct PointsWithoutBonus : Node<DynexiteExam> {
    using Node::Node;

    void recalculate();

    /**
     * \brief The sum of earned points over all tasks for each student, taking
     * into account eliminated tasks, without bonus points.
     */
    QList<Points> mEarnedPoints;

    /**
     * \brief The maximum achievable points of GradingType Grading, taking into
     * account eliminated tasks.
     */
    Points mMaxPoints;
};
