#pragma once

#include "changed_max_points.hpp"
#include "node.hpp"
#include "passing_limit.hpp"

#include "util/enums.hpp"
#include "util/grade.hpp"
#include "util/grade_boundary.hpp"
#include "util/storage_helper.hpp"

struct UserPointConfig;

template <GradingType Grading>
struct UserGradeBoundaries
    : Node<UserPointConfig, ChangedMaxPoints<Grading>, PassingLimit<Grading>> {
    using UserGradeBoundaries::Node::Node;

    void recalculate();

    // void setFollowDefault(bool setAuto, Grade g) noexcept;
    void setFollowDefaultAll(bool setAuto) noexcept;

    bool setBoundary(GradeBoundary newBoundary, Grade g) noexcept;

    EnumStorage<GradeBoundary, Grade> mBoundaries{};

    /* TODO: implement that every grade on its own can be set to automatically
     * adjust to the neighboring grades or not.
     * The problem with that is that the prescribed default automatic grade
     * boundary distribution is pretty irregular, and it is unclear how to set
     * the grade boundaries if some of them are automatic and some are not */
    bool mSetAutomatically = true;
    // EnumStorage<bool, Grade> mSetAutomatically{
    //     true, true, true, true, true, true, true, true, true, false, true};

private:
    EnumStorage<Points, Grade> mBoundaryPoints;
    bool mInitialized = false;

    void recalculateFixed();
    void recalculateManual();
    void recalculateAuto();
};
