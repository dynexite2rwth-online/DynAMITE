#include "bonus_points.hpp"

#include "util/enums.hpp"
#include "util/exception.hpp"
#include "util/heuristics.hpp"
#include "util/points.hpp"
#include "util/strings.hpp"

namespace {
void throwIfWrongFileType(const QString &firstCell) noexcept(false) {
    const auto fileType = getApproximateFileType(firstCell);

    switch (fileType) {
    case FileType::dynexite:
        throw DynamiteException(str::error::unexpectedDynexite);
    case FileType::rwth:
        throw DynamiteException(str::error::unexpectedRwthOnline);
    default:
        // the file might still be invalid, but that will be decided later
        break;
    }
}
} // namespace

BonusPoints::BonusPoints(const CsvDump &c) noexcept(false) : mBonusPoints(c) {

    // empty bonus point file is ok, just no bonus points for anybody
    if (c.data.empty()) {
        return;
    }

    // If we do have data, we need a header
    if (c.data[0].empty())
        throw DynamiteException(str::error::noHeader);

    // Quick check: reading wrong file type?
    throwIfWrongFileType(c.data[0][0]);

    const auto nrRows = c.data.size();

    // It's fine if there is no data aside from the header
    if (nrRows < 2)
        return;

    mTotalBonusPointsPerStudent.reserve(nrRows - 1);

    // Get column index
    const auto registrationNrColumn = c.data[0].indexOf("Matrikelnummer");
    const auto bonusPointColumn = c.data[0].indexOf("Bonuspunkte (Punkte)");

    // Throw if relevant columns not found
    if (registrationNrColumn == -1)
        throw DynamiteException(
            QString(str::error::noColumn).arg("registration number"));
    if (bonusPointColumn == -1)
        throw DynamiteException(
            QString(str::error::noColumn).arg("bonus point"));

    // Insert relevant data into lists
    for (qsizetype i = 1; i < nrRows; ++i) {

        // Registration number column sometimes contains an email address
        // instead of a registration number. Issue a warning for all those
        // students.
        // If the bonus point number can't be read, issue an warning and don't
        // count the bonus points.

        bool regNrParseSuccess, bonusPointParseSuccess;
        const auto &regNr = c.data[i][registrationNrColumn];
        regNr.toInt(&regNrParseSuccess);

        Points bonusPoints;
        try {
            bonusPoints = stringToPoints(c.data[i][bonusPointColumn]);
            bonusPointParseSuccess = true;
        } catch (...) {
            bonusPointParseSuccess = false;
        }

        // No duplicate registration numbers
        if (mTotalBonusPointsPerStudent.contains(regNr) ||
            mInvalidRegNrs.contains(regNr)) {
            throw DynamiteException(str::error::duplicateRegistrationNr);
        }

        if (bonusPointParseSuccess) {
            mTotalBonusPointsPerStudent[regNr] = bonusPoints;
            if (bonusPoints > mMaxBonusPoints) {
                mMaxBonusPoints = bonusPoints;
            }
        }

        if (!bonusPointParseSuccess || !regNrParseSuccess) {
            mInvalidRegNrs[regNr] = c.data[i][bonusPointColumn];
        }
    }
}
