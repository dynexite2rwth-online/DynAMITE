#pragma once

#include "node.hpp"

#include <optional>

struct CombinedGradeList;

struct CombinedGradeAverage : Node<CombinedGradeList> {
    using Node::Node;

    void recalculate() noexcept;

    /**
     * \brief Average grade considering all grades of students who attended, or
     * nullopt if nobody attended.
     */
    std::optional<double> mAverageGrade;
    /**
     * \brief Average grade when ignoring the grade 5.0, or nullopt if nobody
     * passed.
     */
    std::optional<double> mAverageGradePassed = 0.;

    /**
     * \brief The number of students who passed.
     */
    int mNrPassed = 0;
};
