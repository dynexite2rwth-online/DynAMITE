#include "student_points.hpp"

template <GradingType Grading>
void StudentPoints<Grading>::recalculate() {

    const auto &examRef = std::get<const DynexiteExam &>(this->mParentRefs);
    const auto &pointsRef =
        std::get<const PointsWithoutBonus<Grading> &>(this->mParentRefs);
    const auto &bonusRef =
        std::get<const AppliedBonusPoints<Grading> &>(this->mParentRefs);

    // Resize the QList, reset the points
    mPointsWithBonus = QList<Points>(pointsRef.mEarnedPoints.size());

    // Generate a list of grades in the same order the points were in
    for (qsizetype i = 0; i < mPointsWithBonus.size(); ++i) {

        mPointsWithBonus[i] =
            pointsRef.mEarnedPoints[i] +
            bonusRef.mBonusPointsPerStudent.value(examRef.mIdentifier[i]);
    }
}

template struct StudentPoints<GradingType::Closed>;
template struct StudentPoints<GradingType::Open>;
