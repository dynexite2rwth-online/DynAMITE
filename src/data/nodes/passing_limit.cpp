#include "passing_limit.hpp"

#include "first_try.hpp"
#include "user_point_config.hpp"

#include "util/defaults.hpp"

#include <algorithm>
#include <optional>

/*
 * Quotes from:
 * Guidelines on Multiple-Choice Exams - Part of the Handbook for Studying and
 * Teaching, section 5, version from 14th of March 2023
 */

namespace impl {
/**
 * \brief Calculate the absolute pass mark (absolute Bestehensgrenze)
 *
 * 60% of the maximum points
 */
Points
calculateAbsolutePassingLimit(const GradeBoundary max,
                              const GradeBoundaryStepSize stepSize) noexcept {
    return toPoints(max, stepSize) * 0.6;
}

/**
 * \brief Calculate the relative pass mark (relative Bestehensgrenze)
 *
 * A closed-exam is considered passed, if "at least 50% of the maximum points
 * has been achieved and the points attained are no lower than 22% of the
 * average of the candidates taking the exam for the first time"
 */
std::optional<Points> calculateRelativePassingLimit(
    const GradeBoundary max, const GradeBoundaryStepSize stepSize,
    const std::optional<Points> &avgTryPoints) noexcept {

    // if there weren't any first-time exam writers, we don't have a separate
    // relative passing limit
    if (!avgTryPoints.has_value())
        return std::nullopt;

    return std::max(toPoints(max, stepSize) * 0.5, *avgTryPoints * 0.78);
}

/**
 * \brief Calculates the passing limit
 *
 * "The more favorable pass mark for students will be applied."
 */
Points calculatePassingLimit(
    const Points absolutePassingLimit,
    const std::optional<Points> relativePassingLimit) noexcept {
    if (relativePassingLimit.has_value()) {
        return std::min(absolutePassingLimit, *relativePassingLimit);
    } else {
        return absolutePassingLimit;
    }
}
} // namespace impl

void PassingLimit<GradingType::Closed>::recalculate() {
    const auto &firstTryRef = std::get<const FirstTry &>(this->mParentRefs);
    const auto &maxRef =
        std::get<const ChangedMaxPoints<GradingType::Closed> &>(
            this->mParentRefs);
    const auto &configRef =
        std::get<const UserPointConfig &>(this->mParentRefs);

    mAbsPassingLimit = impl::calculateAbsolutePassingLimit(
        maxRef.mChangedMaxPointBoundary, configRef.mGradeBoundaryStepSize);

    // If attempt nr 0 counts as first attempt, include them in the relative
    // passing limit calculation
    const auto avgFirstTryPoints = configRef.mCountZerothAttemptAsFirstAttempt
                                       ? firstTryRef.mAvgFirstAndZerothTryPoints
                                       : firstTryRef.mAvgFirstTryPoints;

    mRelPassingLimit = impl::calculateRelativePassingLimit(
        maxRef.mChangedMaxPointBoundary, configRef.mGradeBoundaryStepSize,
        avgFirstTryPoints);

    // "The more favorable pass mark for students will be applied."
    const auto unroundedPassingLimit =
        impl::calculatePassingLimit(mAbsPassingLimit, mRelPassingLimit);

    mInitialPassingLimit =
        GradeBoundary{unroundedPassingLimit, configRef.mGradeBoundaryStepSize,
                      configRef.mGradeBoundaryRounding};

    // mMaxPassingLimit is unused for closed points
    mMaxPassingLimit = mInitialPassingLimit;
}

void PassingLimit<GradingType::Open>::recalculate() {
    const auto &maxRef = std::get<const ChangedMaxPoints<GradingType::Open> &>(
        this->mParentRefs);
    const auto &configRef =
        std::get<const UserPointConfig &>(this->mParentRefs);

    const auto rawPassingLimit = toPoints(maxRef.mChangedMaxPointBoundary,
                                          configRef.mGradeBoundaryStepSize) *
                                 defaults::openPointsStartingPassingLimit;

    mInitialPassingLimit =
        GradeBoundary{rawPassingLimit, configRef.mGradeBoundaryStepSize,
                      configRef.mGradeBoundaryRounding};

    const auto rawMaxPassingLimit = toPoints(maxRef.mChangedMaxPointBoundary,
                                             configRef.mGradeBoundaryStepSize) *
                                    defaults::openPointsPassingLimit;

    mMaxPassingLimit =
        GradeBoundary{rawMaxPassingLimit, configRef.mGradeBoundaryStepSize,
                      configRef.mGradeBoundaryRounding};
}
