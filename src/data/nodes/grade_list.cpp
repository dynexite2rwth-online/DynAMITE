#include "grade_list.hpp"

#include "user_grade_config.hpp"
#include "user_point_config.hpp"

#include <algorithm>
#include <cassert>

namespace {
/**
 * \brief Computes a grade for the given points with the given grade boundaries
 */
template <GradingType Grading>
[[nodiscard]] Grade
mapPointsToGrade(const Points p,
                 const UserGradeBoundaries<Grading> &gradeBoundaries,
                 const GradeBoundaryStepSize stepSize) noexcept {

    // Boundaries are sorted from good to bad grades, so from many to few
    // points. Find the first boundary b that does not satisfy b > points.
    const auto res = std::lower_bound(
        gradeBoundaries.mBoundaries.begin(), gradeBoundaries.mBoundaries.end(),
        p, [stepSize](const GradeBoundary &b, const Points &points) {
            return boundaryGreater(b, points, stepSize);
        });

    if (res != gradeBoundaries.mBoundaries.end()) {

        // res is now an iterator to the grade boundary of the resulting grade
        return static_cast<Grade>(res - gradeBoundaries.mBoundaries.begin());

    } else {
        // Since gradeBoundaries.mBoundaries[Grade::grade5_0] = 0, this
        // should be unreachable
        assert(false);
        return Grade::grade5_0;
    }
}

/**
 * \brief Computes a grade for the given points and bonus points with the given
 * grade boundaries
 */
template <GradingType Grading>
[[nodiscard]] Grade
getGrade(const Points pointsWithoutBonus, const Points pointsWithBonus,
         const UserGradeBoundaries<Grading> &gradeBoundaries,
         const GradeBoundaryStepSize stepSize,
         const bool allowPassDueToBonus) noexcept {

    if (allowPassDueToBonus) {
        // Always calculate the final grade with bonus points
        return mapPointsToGrade(pointsWithBonus, gradeBoundaries, stepSize);

    } else {
        // First calculate the grade without taking bonus points into account
        const auto gradeWithoutBonus =
            mapPointsToGrade(pointsWithoutBonus, gradeBoundaries, stepSize);

        // Bonus points are only taken into account if the student passes the
        // exam without bonus points
        if (gradeWithoutBonus == Grade::grade5_0 ||
            pointsWithoutBonus == pointsWithBonus) {
            return gradeWithoutBonus;
        }

        // If the student passes without bonus points, calculate the final grade
        // with bonus points
        return mapPointsToGrade(pointsWithBonus, gradeBoundaries, stepSize);
    }
}
} // namespace

template <GradingType Grading>
void GradeList<Grading>::recalculate() {

    const auto &userGradeBoundsRef =
        std::get<const UserGradeBoundaries<Grading> &>(this->mParentRefs);
    const auto &roundedPointsRef =
        std::get<const RoundedPoints<Grading> &>(this->mParentRefs);
    const auto &pointConfigRef =
        std::get<const UserPointConfig &>(this->mParentRefs);
    const auto &gradeConfigRef =
        std::get<const UserGradeConfig &>(this->mParentRefs);

    // Resize the QList, reset the grades
    mGrades = QList<Grade>(roundedPointsRef.mRoundedPointsWithBonus.size());

    // Generate a list of grades in the same order the points were in
    for (qsizetype i = 0; i < mGrades.size(); ++i) {

        mGrades[i] =
            getGrade(roundedPointsRef.mRoundedPointsWithoutBonus[i],
                     roundedPointsRef.mRoundedPointsWithBonus[i],
                     userGradeBoundsRef, pointConfigRef.mGradeBoundaryStepSize,
                     gradeConfigRef.mPassDueToBonus);
    }
}

template struct GradeList<GradingType::Closed>;
template struct GradeList<GradingType::Open>;
