#pragma once

#include "node.hpp"
#include "rounded_points.hpp"
#include "user_grade_boundaries.hpp"

#include <QList>

struct UserPointConfig;
struct UserGradeConfig;

template <GradingType Grading>
struct GradeList : Node<UserPointConfig, UserGradeConfig,
                        UserGradeBoundaries<Grading>, RoundedPoints<Grading>> {
    using GradeList::Node::Node;

    void recalculate();

    QList<Grade> mGrades{};
};
