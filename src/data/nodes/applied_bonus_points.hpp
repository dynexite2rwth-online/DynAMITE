#pragma once

#include "changed_max_points.hpp"
#include "node.hpp"

#include "util/enums.hpp"

#include <QHash>
#include <QString>

struct BonusPoints;
struct Points;

/**
 * \brief For both open and closed grading, stores how many bonus points a
 * student has earned for that exam part.
 */
template <GradingType Grading>
struct AppliedBonusPoints
    : Node<BonusPoints, ChangedMaxPoints<GradingType::Closed>,
           ChangedMaxPoints<GradingType::Open>> {
    using Node::Node;

    void recalculate();

    /**
     * \brief A map of all students found in the bonus csv file and their
     * corresponding number of bonus points for the GradingType of this class.
     * The registration number may be non-numerical.
     */
    QHash<QString, Points> mBonusPointsPerStudent{};
};
