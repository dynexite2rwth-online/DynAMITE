#include "first_try.hpp"

#include "dynexite_exam.hpp"
#include "student_data.hpp"
#include "util/exception.hpp"
#include "util/numeric.hpp"

#include <QHash>
#include <QString>

#include <numeric>

void FirstTry::recalculate() {

    const auto &examRef = std::get<const DynexiteExam &>(this->mParentRefs);
    const auto &studentRef = std::get<const StudentData &>(this->mParentRefs);
    const auto &pointsRef =
        std::get<const PointsWithoutBonus<GradingType::Closed> &>(
            this->mParentRefs);

    mFirstTryClosedPoints.clear();
    mZerothTryClosedPoints.clear();

    // Calculate lookup table for indices
    // When we want to match up a student from the RWTHonline file with the same
    // student in the dynexite file, we have to find the index of that student
    // in the unsorted dynexite identifier list. To improve performance, we
    // store the indices in a map with the identifier as key.
    QHash<QString, int> identifierIndices;
    identifierIndices.reserve(examRef.mIdentifier.size());
    for (int i = 0; i < examRef.mIdentifier.size(); ++i) {
        identifierIndices[examRef.mIdentifier[i]] = i;
    }

    /*
     * Students who don't appear in the RWTHOnline file are not counted as
     * students with attempt number 0/1.
     */

    // add the closed points of every student to the list, for whom this is
    // their "zeroth" / first try
    for (qsizetype studentIdx = 0;
         studentIdx < studentRef.mRegistrationNr.size(); ++studentIdx) {

        if (studentRef.mAttemptNr[studentIdx] <= 1) {

            // Get the array index for the same student in the exam file
            const auto idxIt = identifierIndices.constFind(
                studentRef.mRegistrationNr[studentIdx]);

            // If we didn't find them, then the student registered (for the
            // first/zeroth time) but did not attend
            if (idxIt == identifierIndices.constEnd()) {
                continue;
            }

            // Add their points to the right list
            if (studentRef.mAttemptNr[studentIdx] == 1) {
                mFirstTryClosedPoints.emplace_back(
                    pointsRef.mEarnedPoints[*idxIt]);
            } else if (studentRef.mAttemptNr[studentIdx] == 0) {
                mZerothTryClosedPoints.emplace_back(
                    pointsRef.mEarnedPoints[*idxIt]);
            } else {
                throw DynamiteException(
                    "Unexpected student attempt number: " +
                    QString::number(studentRef.mAttemptNr[studentIdx]));
            }
        }
    }

    const auto firstTryClosedPointsSum =
        kahanSum(mFirstTryClosedPoints.begin(), mFirstTryClosedPoints.end());
    const auto zerothTryClosedPointsSum =
        kahanSum(mZerothTryClosedPoints.begin(), mZerothTryClosedPoints.end());

    // Calculate average first try points, if only attemptNr=1 counts as first
    // attempt
    if (mFirstTryClosedPoints.empty()) {
        mAvgFirstTryPoints = std::nullopt;
    } else {
        mAvgFirstTryPoints = firstTryClosedPointsSum /
                             static_cast<double>(mFirstTryClosedPoints.size());
    }

    // Calculate average first try points, if attemptNr=1 and attemptNr=0 counts
    // as first attempt
    if (mFirstTryClosedPoints.empty() && mZerothTryClosedPoints.empty()) {
        mAvgFirstAndZerothTryPoints = std::nullopt;
    } else {
        mAvgFirstAndZerothTryPoints =
            (firstTryClosedPointsSum + zerothTryClosedPointsSum) /
            static_cast<double>(mZerothTryClosedPoints.size() +
                                mFirstTryClosedPoints.size());
    }
}
