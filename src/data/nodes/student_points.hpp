#pragma once

#include "applied_bonus_points.hpp"
#include "dynexite_exam.hpp"
#include "node.hpp"
#include "points_without_bonus.hpp"

#include "util/points.hpp"

#include <QList>

struct UserPointConfig;

template <GradingType Grading>
struct StudentPoints : Node<DynexiteExam, PointsWithoutBonus<Grading>,
                            AppliedBonusPoints<Grading>> {
    using StudentPoints::Node::Node;

    void recalculate();

    QList<Points> mPointsWithBonus{};
};
