#include "user_grade_boundaries.hpp"

#include "user_point_config.hpp"

#include <algorithm>
#include <cassert>
#include <iterator>
#include <ranges>

namespace {
/**
 * \brief Sets oneThird to one third and twoThirds to two thirds of the way
 * between lowerBound and upperBound
 */
void interpolateGrade3(GradeBoundary &oneThird, GradeBoundary &twoThirds,
                       const GradeBoundary lowerBound,
                       const GradeBoundary upperBound,
                       const GradeBoundaryRounding rounding) noexcept {
    assert(lowerBound <= upperBound);
    const auto oneThirdMultiple =
        std::lerp(lowerBound.mMultiple, upperBound.mMultiple, 1. / 3.);
    const auto twoThirdsMultiple =
        std::lerp(lowerBound.mMultiple, upperBound.mMultiple, 2. / 3.);

    oneThird = roundMultiple(oneThirdMultiple, rounding);
    twoThirds = roundMultiple(twoThirdsMultiple, rounding);
}
} // namespace

template <GradingType Grading>
void UserGradeBoundaries<Grading>::recalculateFixed() {

    using enum Grade;

    const auto &passingLimitRef =
        std::get<const PassingLimit<Grading> &>(this->mParentRefs);
    const auto &pointConfigRef =
        std::get<const UserPointConfig &>(this->mParentRefs);
    const auto &maxRef =
        std::get<const ChangedMaxPoints<Grading> &>(this->mParentRefs);

    // on first update, set the passing limit boundary to the default value
    if (!mInitialized) {
        mBoundaries[grade4_0] = passingLimitRef.mInitialPassingLimit;
        mBoundaries[grade5_0] = GradeBoundary{0};

        mInitialized = true;
    } else {
        mBoundaries[grade4_0] = std::clamp(
            // use mBoundaryPoints instead of mBoundaries,
            // because grade boundaries might have been invalidated because of a
            // changed StepSize
            GradeBoundary{mBoundaryPoints[grade4_0],
                          pointConfigRef.mGradeBoundaryStepSize,
                          pointConfigRef.mGradeBoundaryRounding},
            GradeBoundary{0}, passingLimitRef.mMaxPassingLimit);
    }
    mBoundaryPoints[grade4_0] =
        toPoints(mBoundaries[grade4_0], pointConfigRef.mGradeBoundaryStepSize);
    mBoundaryPoints[grade5_0] =
        toPoints(mBoundaries[grade5_0], pointConfigRef.mGradeBoundaryStepSize);

    // set fixed points
    if (mSetAutomatically || Grading == GradingType::Closed) {
        // How many points are there to get beyond the minimum required points
        // to pass
        const auto pointsBeyondPassing =
            toPoints(maxRef.mChangedMaxPointBoundary,
                     pointConfigRef.mGradeBoundaryStepSize) -
            toPoints(mBoundaries[grade4_0],
                     pointConfigRef.mGradeBoundaryStepSize);

        GradeBoundaryFactory factory(pointConfigRef.mGradeBoundaryStepSize,
                                     pointConfigRef.mGradeBoundaryRounding);

        const auto getGradeBoundary =
            [passingLimitBoundary = mBoundaries[grade4_0],
             stepSize = pointConfigRef.mGradeBoundaryStepSize,
             pointsBeyondPassing, factory](const double factor) {
                return factory(toPoints(passingLimitBoundary, stepSize) +
                               pointsBeyondPassing * factor);
            };

        // This calculation for grade boundaries is only mandatory for the
        // closed exam part. We use it for both exam parts, but the user can
        // change the open grade boundaries later if they wish.
        mBoundaries[grade1_3] = getGradeBoundary(0.75);
        mBoundaries[grade2_3] = getGradeBoundary(0.5);
        mBoundaries[grade3_3] = getGradeBoundary(0.25);

        mBoundaryPoints[grade1_3] = toPoints(
            mBoundaries[grade1_3], pointConfigRef.mGradeBoundaryStepSize);
        mBoundaryPoints[grade2_3] = toPoints(
            mBoundaries[grade2_3], pointConfigRef.mGradeBoundaryStepSize);
        mBoundaryPoints[grade3_3] = toPoints(
            mBoundaries[grade3_3], pointConfigRef.mGradeBoundaryStepSize);
    }
}

template <GradingType Grading>
void UserGradeBoundaries<Grading>::recalculateManual() {

    const auto &maxRef =
        std::get<const ChangedMaxPoints<Grading> &>(this->mParentRefs);
    const auto &pointConfigRef =
        std::get<const UserPointConfig &>(this->mParentRefs);

#if 1
    // Clamp the changeable boundaries (up to 3.7) between their nearest
    // non-changeable boundaries
    for (int gradeIdx = 0; gradeIdx < static_cast<int>(Grade::grade4_0);
         ++gradeIdx) {
        const auto g = static_cast<Grade>(gradeIdx);
        if (gradeBoundaryIsUserChangeable(Grading, g)) {

            // Find previous non-changeable grade (higher point value)
            GradeBoundary prevBoundary = maxRef.mChangedMaxPointBoundary;
            for (int prevDefaultGrade = gradeIdx - 1; prevDefaultGrade >= 0;
                 --prevDefaultGrade) {

                if (!gradeBoundaryIsUserChangeable(
                        Grading, static_cast<Grade>(prevDefaultGrade))) {

                    prevBoundary =
                        mBoundaries[static_cast<Grade>(prevDefaultGrade)];
                    break;
                }
            }

            // Find next non-changeable grade or 4.0 (lower point value)
            GradeBoundary nextBoundary = mBoundaries[Grade::grade4_0];
            for (int nextDefaultGrade = gradeIdx + 1;
                 nextDefaultGrade < static_cast<int>(Grade::grade4_0);
                 ++nextDefaultGrade) {

                if (!gradeBoundaryIsUserChangeable(
                        Grading, static_cast<Grade>(nextDefaultGrade))) {

                    nextBoundary = GradeBoundary{
                        mBoundaryPoints[static_cast<Grade>(nextDefaultGrade)],
                        pointConfigRef.mGradeBoundaryStepSize,
                        pointConfigRef.mGradeBoundaryRounding};
                    break;
                }
            }

            // Make sure the custom boundary lies between the next
            // non-changeable boundaries
            mBoundaries[g] =
                std::clamp(GradeBoundary{mBoundaryPoints[g],
                                         pointConfigRef.mGradeBoundaryStepSize,
                                         pointConfigRef.mGradeBoundaryRounding},
                           nextBoundary, prevBoundary);
            mBoundaryPoints[g] =
                toPoints(mBoundaries[g], pointConfigRef.mGradeBoundaryStepSize);
        }
    }
#else

    // Clamp every changeable grade value between its neighbors
    for (int gradeIdx = 0; gradeIdx < static_cast<int>(Grade::grade4_0);
         ++gradeIdx) {

        const auto g = static_cast<Grade>(gradeIdx);

        if (gradeBoundaryIsUserChangeable(Grading, g)) {
            // there is no grade g-1 for grade 1.0, use max_points there instead
            const auto greaterBoundary =
                gradeIdx == 0 ? maxRef.mChangedMaxPointBoundary
                              : mBoundaries[static_cast<Grade>(gradeIdx - 1)];

            // In the range from 1.0 to 4.0, there is always a grade gradeIdx+1
            const auto lesserBoundary =
                mBoundaries[static_cast<Grade>(gradeIdx + 1)];

            mBoundaries[g] =
                std::clamp(GradeBoundary{mBoundaryPoints[g],
                                         pointConfigRef.mGradeBoundaryStepSize,
                                         pointConfigRef.mGradeBoundaryRounding},
                           lesserBoundary, greaterBoundary);
        }
        mBoundaryPoints[g] =
            toPoints(mBoundaries[g], pointConfigRef.mGradeBoundaryStepSize);
    }
#endif
}

template <GradingType Grading>
void UserGradeBoundaries<Grading>::recalculateAuto() {
    const auto &pointConfigRef =
        std::get<const UserPointConfig &>(this->mParentRefs);
    const auto &maxRef =
        std::get<const ChangedMaxPoints<Grading> &>(this->mParentRefs);

    // Interpolate non-fixed boundaries
    // Space the grades evenly out by default, the user can tweak them by
    // de-activating "auto"
    GradeBoundary filler(0);
    interpolateGrade3(
        mBoundaries[Grade::grade1_0], filler, mBoundaries[Grade::grade1_3],
        maxRef.mChangedMaxPointBoundary, pointConfigRef.mGradeBoundaryRounding);
    interpolateGrade3(
        mBoundaries[Grade::grade2_0], mBoundaries[Grade::grade1_7],
        mBoundaries[Grade::grade2_3], mBoundaries[Grade::grade1_3],
        pointConfigRef.mGradeBoundaryRounding);
    interpolateGrade3(
        mBoundaries[Grade::grade3_0], mBoundaries[Grade::grade2_7],
        mBoundaries[Grade::grade3_3], mBoundaries[Grade::grade2_3],
        pointConfigRef.mGradeBoundaryRounding);
    interpolateGrade3(
        filler, mBoundaries[Grade::grade3_7], mBoundaries[Grade::grade4_0],
        mBoundaries[Grade::grade3_3], pointConfigRef.mGradeBoundaryRounding);

    // set mBoundaryPoints
    for (const auto g : grades) {
        if (g >= Grade::grade4_0)
            break;

        mBoundaryPoints[g] =
            toPoints(mBoundaries[g], pointConfigRef.mGradeBoundaryStepSize);
    }
}

template <GradingType Grading>
void UserGradeBoundaries<Grading>::recalculate() {

    // First, set the boundary for 4.0 to it's previous value clamped to the new
    // legal range
    recalculateFixed();

    if (mSetAutomatically) {
        // Interpolate the other boundaries as prescribed
        recalculateAuto();
    } else {
        // Clamp the manual boundaries to their valid ranges
        recalculateManual();
    }

    // Sanity check: are the point boundaries in decreasing order?
    assert(std::ranges::is_sorted_until(mBoundaries, std::ranges::greater()) ==
           std::ranges::end(mBoundaries));
}

// template <GradingType Grading>
// void UserGradeBoundaries<Grading>::setFollowDefault(const bool setAuto,
//                                                     const Grade g) noexcept {
//     if (gradeBoundaryIsUserChangeable(Grading, g)) {
//         mSetAutomatically[g] = setAuto;
//     } else {
//         mSetAutomatically[g] = true;
//     }
//
//     recalculate();
// }

template <GradingType Grading>
void UserGradeBoundaries<Grading>::setFollowDefaultAll(
    const bool setAuto) noexcept {

    mSetAutomatically = setAuto;

    recalculate();
}

template <GradingType Grading>
bool UserGradeBoundaries<Grading>::setBoundary(const GradeBoundary newBoundary,
                                               const Grade g) noexcept {

    // verify that we are allowed to change this boundary
    if (!gradeBoundaryIsUserChangeable(Grading, g))
        return false;

    if (g != Grade::grade4_0 && mSetAutomatically)
        return false;

    if (g != Grade::grade4_0) {
        const auto &pointConfigRef =
            std::get<const UserPointConfig &>(this->mParentRefs);

        const auto &maxRef =
            std::get<const ChangedMaxPoints<Grading> &>(this->mParentRefs);

        // new boundary may not be smaller/larger than the neighboring
        // boundaries since 5.0 is not changeable, there is always a grade g+1
        const auto lesserBoundary =
            mBoundaries[static_cast<Grade>(static_cast<std::size_t>(g) + 1)];
        // there is no grade g-1 for grade 1.0, use max_points there instead
        const auto greaterBoundary =
            g == Grade::grade1_0 ? maxRef.mChangedMaxPointBoundary
                                 : mBoundaries[static_cast<Grade>(
                                       static_cast<std::size_t>(g) - 1)];

        mBoundaries[g] =
            std::clamp(newBoundary, lesserBoundary, greaterBoundary);
        mBoundaryPoints[g] =
            toPoints(mBoundaries[g], pointConfigRef.mGradeBoundaryStepSize);
    } else {
        // don't clamp 4.0 between neighbors, adjust neighbors based on 4.0

        const auto &pointConfigRef =
            std::get<const UserPointConfig &>(this->mParentRefs);

        mBoundaries[Grade::grade4_0] = newBoundary;
        mBoundaryPoints[Grade::grade4_0] =
            toPoints(newBoundary, pointConfigRef.mGradeBoundaryStepSize);
        recalculate();
    }

    return mBoundaries[g] == newBoundary;
}

template struct UserGradeBoundaries<GradingType::Closed>;
template struct UserGradeBoundaries<GradingType::Open>;
