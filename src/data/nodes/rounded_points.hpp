#pragma once

#include "node.hpp"
#include "points_without_bonus.hpp"
#include "student_points.hpp"
#include "util/enums.hpp"
#include "util/points.hpp"

#include <QList>

template <GradingType Grading>
struct RoundedPoints
    : Node<PointsWithoutBonus<Grading>, StudentPoints<Grading>> {
    using RoundedPoints::Node::Node;

    void recalculate();

    QList<Points> mRoundedPointsWithBonus{};
    QList<Points> mRoundedPointsWithoutBonus{};
};

template <>
struct RoundedPoints<GradingType::Combined>
    : Node<RoundedPoints<GradingType::Closed>,
           RoundedPoints<GradingType::Open>> {
    using Node::Node;

    void recalculate();

    QList<Points> mRoundedPointsWithBonus{};
    QList<Points> mRoundedPointsWithoutBonus{};
};
