#include "exam_stats.hpp"

#include "dynexite_exam.hpp"
#include "student_data.hpp"

#include <QList>
#include <QString>

#include <algorithm>

void ExamStats::recalculate() noexcept {
    const auto &examRef = std::get<const DynexiteExam &>(this->mParentRefs);
    const auto &studentRef = std::get<const StudentData &>(this->mParentRefs);

    // Find number of students who are registered and attended
    // Dynexite lists attended students,
    // RWTHonline lists registered students
    auto attendedStudents = examRef.mIdentifier;
    auto registeredStudents = studentRef.mRegistrationNr;

    std::ranges::sort(attendedStudents);
    std::ranges::sort(registeredStudents);

    QList<QString> intersection;
    intersection.reserve(
        std::min(registeredStudents.size(), attendedStudents.size()));
    std::ranges::set_intersection(registeredStudents, attendedStudents,
                                  std::back_inserter(intersection));

    mNrAttended = static_cast<int>(attendedStudents.size());
    mNrRegistered = static_cast<int>(registeredStudents.size());
    mNrAttendedRegistered = static_cast<int>(intersection.size());
    mNrAttendedNotRegistered = mNrAttended - mNrAttendedRegistered;
    mNrRegisteredNotAttended = mNrRegistered - mNrAttendedRegistered;
}
