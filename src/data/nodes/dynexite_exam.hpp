#pragma once

#include "node.hpp"

#include "data/csv_dump.hpp"
#include "util/points.hpp"
#include "util/storage_helper.hpp"

#include <QBitArray>
#include <QList>
#include <QString>

#include <optional>

struct TaskPool;

/**
 * \brief Represents a single task item. Not every student might have been
 * assigned this item.
 */
struct Task {
    explicit Task(QString taskName, int nrStudents) noexcept;

    /**
     * \brief Name of this item.
     *
     * Different items of the same task might have different names. The name for
     * a task pool is usually given as the concatenation of the names of the
     * items.
     */
    QString mTaskName{};

    /**
     * \brief The maximum achievable points of this task item.
     *
     * Every task item in a task pool must have the same maximum achievable
     * points.
     *
     * Note that we have to guess the the maximum achievable points of only the
     * closed / open section of this task, as the dynexite csv file does not
     * provide that information.
     */
    EnumStorage<Points> mMaxPoints{Points{-1}, Points{-1}, Points{-1}};

    /**
     * \brief Records which student was assigned this item. "true" at idx i
     * means the student at index i in the DynexiteExam was assigned this item.
     */
    QBitArray mParticipatedStudents;

    /**
     * \brief For all students lists their open, closed and combined earned
     * points in this task (if they were assigned it)
     */
    EnumStorage<QList<std::optional<Points>>> mEarnedPoints{};

    /**
     * \brief The task pool this task (item) is a part of. The task pool may or
     * may not contain any more items.
     */
    TaskPool *mTaskPool = nullptr;

    /**
     * \brief Returns mEarnedPoints with nullopts filtered out and converted to
     * Points, in an arbitrary order.
     */
    [[nodiscard]] QList<Points>
    assignedEarnedPoints(GradingType grading) const noexcept;
};

/**
 * \brief Represents a set of task items. Either every student was assigned one
 * of the task items (normal case), or the TaskPool only contains a single task
 * item that no student was assigned (rare).
 */
struct TaskPool {
    /**
     * \brief The maximum achievable points in each task item. Every task item
     * must have the same maximum achievable points.
     */
    EnumStorage<Points> mMaxPoints{Points{-1}, Points{-1}, Points{-1}};

    /**
     * \brief All items of this task, size >= 1.
     */
    QList<Task *> mTasks;

    /**
     * \brief What the user has has set the elimination status of this task to
     *
     * Task start out as being notEliminated.
     */
    TaskEliminationType mEliminationStatus = TaskEliminationType::notEliminated;

    /**
     * \brief Construct task pool name by concatenating the task names.
     */
    [[nodiscard]] QString
    constructTaskPoolName(const QString &delimiter = "\n") const noexcept;
};

/**
 * \brief Represents the data in the csv-file directly from dynexite in
 * a more accessible manner.
 *
 * This data structure contains only two pieces of data not found in the
 * dynexite-csv-file:
 *
 * - The maximum achievable closed and open points per task. They are necessary
 * for removing tasks later, but sadly not provided by dynexite, so we guess
 * them here
 * - The elimination status of a task. This is also the only field from this
 * class or its members that may change after initialization.
 *
 * Stores data only for students who attended, discards everyone else.
 */
struct DynexiteExam : Node<> {
    using Node::Node;

    /**
     * \brief Interprets the CsvDump as a dynexite-csv-file and copies all
     * necessary info. Also runs sanity checks on the given data and throws on
     * error.
     *
     * \exception DynamiteException A sanity check on the given csv file
     * failed (e.g. some header is missing, student got more than max points,
     * ...), see the description in the exception for details, or a
     * stringToDouble conversion failed.
     */
    explicit DynexiteExam(CsvDump c) noexcept(false);

    /**
     * \brief Unique identifier (usually Matrikelnummer), called mRegistrationNr
     * in the students data
     */
    QList<QString> mIdentifier{};

    /**
     * \brief Holds all task items.
     *
     * For a distinction between task item and task pool, see \ref TaskPool
     */
    QList<Task> mTaskItems{};

    /**
     * \brief Holds all task pools.
     */
    QList<TaskPool> mTaskPools{};

    /**
     * \brief Store the entire csv dump so that we can more easily export it
     * later.
     */
    CsvDump mDynexite;

    /**
     * \brief The maximum achievable open, closed and combined points as stated
     * by dynexite. Not influenced by later changes from the user, those are
     * stored in \ref ChangedMaxPoints
     *
     * \warning This variable should only be used before the input files are
     * combined. If you need the max points of the exam, use
     * PointsWithoutBonus::mMaxPoints.
     */
    EnumStorage<Points> mPointsMaxRaw{};

private:
    /**
     * \brief Open, closed and combined achieved points of each student, without
     * bonus points and ignoring eliminated tasks.
     *
     * \note This member if private because it is only used internally to check
     * whether our calculated student points coincide with this. If you need the
     * student points of the exam, use PointsWithoutBonus::mEarnedPoints.
     */
    EnumStorage<QList<Points>> mPointsWithoutBonusOrElim{};

private:
    /**
     * \brief Internal helper function. Reserves space in the member arrays or
     * resizes them.
     */
    void reserveArrays(qsizetype nrStudents, qsizetype nrTasks) noexcept;

    /**
     * \brief Internal helper function. Parses the given \ref CsvDump and enters
     * the data from it into the correct member lists.
     */
    void insertData(const CsvDump &c, CsvLine::size_type nrStudents,
                    CsvLine::size_type nrTasks) noexcept(false);

    /**
     * \brief Internal helper function. Analyzes which task item was assigned to
     * which student and creates task pool based on that.
     */
    void createTaskPools() noexcept(false);

    /**
     * \brief Internal helper function. After the member objects have been
     * initialized, checks if there are inconsistencies.
     */
    void checkDataSanity() const noexcept(false);

    /**
     * \brief Guesses for each task pool the maximum achievable closed and open
     * points, based on the maximum achievable total points and the actually
     * achieved points from students in the task items of the task pool.
     *
     * For more information, look at the comments in the implementation.
     *
     * Only necessary for version 1 of the dynexite csv file.
     */
    void guessMaxOpenClosedPerTask() noexcept(false);

    /**
     * \brief Set for each task pool the maximum achievable closed and open
     * points as given in version 2 of the dynexite csv file.
     */
    void setMaxOpenClosedPerTask() noexcept;

    /**
     * The new general examination regulations no longer grade open and closed
     * points differently, instead all points are graded in the way open points
     * were previously graded. We implement this by treating both closed and
     * open points as open points. To achieve this, this function adds the
     * closed points to the open points in the CsvDump and sets the closed
     * points to 0.
     */
    void makeAllPointsOpen(CsvDump &c, CsvLine::size_type nrTasks) const
        noexcept(false);
};
