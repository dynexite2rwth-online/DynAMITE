#pragma once

#include <functional>
#include <tuple>
#include <type_traits>

template <typename U, typename Tuple>
struct HasType;
template <typename U, typename... Vs>
struct HasType<U, std::tuple<Vs...>>
    : std::disjunction<std::is_same<U, Vs>...> {};

template <typename U, typename V>
struct IsDirectPredecessor : HasType<U, typename V::Parents> {};

template <typename U, typename VTuple>
struct IsPredecessorOfAny;
template <typename U, typename... Vs>
struct IsPredecessorOfAny<U, std::tuple<Vs...>>
    : std::disjunction<IsPredecessorOfAny<U, typename Vs::Parents>...,
                       IsDirectPredecessor<U, Vs>...> {};

template <typename UTuple, typename VTuple>
struct AnyIsPredecessorOfAny;
template <typename... Us, typename... Vs>
struct AnyIsPredecessorOfAny<std::tuple<Us...>, std::tuple<Vs...>>
    : std::disjunction<IsPredecessorOfAny<Us, std::tuple<Vs...>>...> {};

template <typename... ParentParams>
struct Node {
    using Parents = std::tuple<ParentParams...>;

    constexpr explicit Node(const ParentParams &...parents) noexcept
        : mParentRefs(std::make_tuple(std::cref(parents)...)) {}

    // In derived classes you can only access this member using "this->"
    // Read here for details:
    // https://isocpp.org/wiki/faq/templates#nondependent-name-lookup-members
    std::tuple<const ParentParams &...> mParentRefs;
};
