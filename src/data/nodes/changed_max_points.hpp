#pragma once

#include "node.hpp"
#include "points_without_bonus.hpp"

#include "util/enums.hpp"
#include "util/grade_boundary.hpp"

struct UserPointConfig;

template <GradingType Grading>
struct ChangedMaxPoints : Node<UserPointConfig, PointsWithoutBonus<Grading>> {
    using ChangedMaxPoints::Node::Node;

    void recalculate();
    void setFollowDefault(bool followDefault = true) noexcept;
    void setMax(const GradeBoundary &maxPointBoundary) noexcept;

    /**
     * \brief How many points are achievable in the exam (only non-eliminated
     * tasks, without bonus points, ignoring manually reduced maximum).
     *
     * Just points_without_bonus::mMaxPoints converted to a \ref GradeBoundary.
     */
    GradeBoundary mOriginalMaxPointBoundary{};

    /**
     * \brief The value to which the user has manually reduced the point maximum
     * (via a slider, not via task elimination)
     */
    GradeBoundary mChangedMaxPointBoundary{};

    /**
     * \brief If `mFollowDefault` is true, `mChangedMaxPointBoundary` is set to
     * `mOriginalMaxPointBoundary` when this node is recalculated. If
     * `mFollowDefault` is false, `mChangedMaxPointBoundary` is clamped between
     * 0 and `mOriginalMaxPointBoundary`.
     *
     * If we load a new exam, this should usually be true. If we change
     * `mOriginalMaxPointBoundary` while keeping the same exam, this should
     * usually be false.
     */
    bool mFollowDefault = true;

private:
    /**
     * \brief Internally used to keep track of `mChangedMaxPointBoundary` even
     * if the StepSize changes.
     *
     * Just \ref mChangedMaxPointBoundary converted to points.
     */
    Points mChangedMaxPoints{};
};
