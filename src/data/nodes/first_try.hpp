#pragma once

#include "node.hpp"
#include "points_without_bonus.hpp"

#include "util/points.hpp"

#include <QList>

#include <optional>

struct DynexiteExam;
struct StudentData;

/**
 * \brief Closed points (without bonus points) of students for whom this is
 * their first attempt. This is needed to calculate the relative passing limit.
 *
 * \note Calls `std::terminate` if memory allocation fails.
 */
struct FirstTry
    : Node<DynexiteExam, StudentData, PointsWithoutBonus<GradingType::Closed>> {
    using Node::Node;

    void recalculate();

    /**
     * \brief List with the closed points of only those students for whom this
     * is their first attempt at this exam. Note that this list might be smaller
     * than the points-list in the exam data structure, or might be empty.
     */
    QList<Points> mFirstTryClosedPoints;

    /**
     * \brief List with the closed points of only those students for whom the
     * value for `GUEL_U_AKTUELLE_ANTRITTE_SPO` is 0. This can happen for
     * students with an unusual `SPO_KONTEXT`. Is is unclear whether to include
     * these students in the calculation for the average first time grade.
     */
    QList<Points> mZerothTryClosedPoints;

    /**
     * \brief Average number of achieved points of the candidates taking the
     * exam for the first time (attempt number 1).
     *
     * If no students takes the exam for the first time, the value is nullopt.
     */
    std::optional<Points> mAvgFirstTryPoints;

    /**
     * \brief Average number of achieved points of the candidates with attempt
     * number 1 or 0.
     *
     * If no students has attempt number 1 or 0, the value is nullopt.
     */
    std::optional<Points> mAvgFirstAndZerothTryPoints;
};
