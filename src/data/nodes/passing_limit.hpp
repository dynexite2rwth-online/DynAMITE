#pragma once

#include "changed_max_points.hpp"
#include "node.hpp"

#include "util/enums.hpp"
#include "util/grade_boundary.hpp"
#include "util/points.hpp"

#include <QList>

struct FirstTry;
struct UserPointConfig;
template <GradingType Grading>
struct UserGradeBoundaries;

namespace impl {
[[nodiscard]] Points
calculateAbsolutePassingLimit(GradeBoundary max,
                              GradeBoundaryStepSize stepSize) noexcept;

[[nodiscard]] std::optional<Points> calculateRelativePassingLimit(
    GradeBoundary max, GradeBoundaryStepSize stepSize,
    const std::optional<Points> &avgTryPoints) noexcept;

[[nodiscard]] Points
calculatePassingLimit(Points absolutePassingLimit,
                      std::optional<Points> relativePassingLimit) noexcept;
} // namespace impl

/**
 * \brief Calculates the default passing limit (grade boundary of 4.0) according
 * to the maximum achievable points, the step size and, in case of the closed
 * passing limit, the points of students on their first attempt.
 *
 * Because of the latter we specialize this class for open and closed.
 */
template <GradingType Grading>
struct PassingLimit;

template <>
struct PassingLimit<GradingType::Closed>
    : Node<ChangedMaxPoints<GradingType::Closed>, FirstTry, UserPointConfig> {
    using Node::Node;

    void recalculate();

    /**
     * \brief Max allowed value for the passing limit (grade boundary for 4.0).
     * It can be set lower by the user, that value is not stored here but in
     * \ref UserGradeBoundaries
     */
    GradeBoundary mMaxPassingLimit;

    /*
     * The following values are intermediate results of the passing limit
     * calculation. Used in CalculationWindow.
     */
    Points mAbsPassingLimit;
    std::optional<Points> mRelPassingLimit;

private:
    /**
     * \brief Starting value for the passing limit (4.0 grade boundary), based
     * on the ChangedMaxPoints. You probably want to use mMaxPassingLimit.
     */
    GradeBoundary mInitialPassingLimit;

    friend struct UserGradeBoundaries<GradingType::Closed>;
};

template <>
struct PassingLimit<GradingType::Open>
    : Node<ChangedMaxPoints<GradingType::Open>, UserPointConfig> {
    using Node::Node;

    void recalculate();

    /**
     * \brief Max allowed value for the passing limit. It can be set lower by
     * the user, that value is not stored here but in UserGradeBoundaries
     */
    GradeBoundary mMaxPassingLimit;

private:
    /**
     * \brief Starting value for the passing limit (4.0 grade boundary), based
     * on the ChangedMaxPoints. You probably want to use mMaxPassingLimit.
     */
    GradeBoundary mInitialPassingLimit;

    friend struct UserGradeBoundaries<GradingType::Open>;
};
