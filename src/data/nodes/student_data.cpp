#include "student_data.hpp"

#include "util/exception.hpp"
#include "util/heuristics.hpp"
#include "util/strings.hpp"

#include <QPair>

#include <array>
#include <ranges>

#define RWTHONLINE_PARSER_PERMISSIVE 1

namespace {
[[maybe_unused]] void checkHeader(const QString &found,
                                  const QString &expected) {
    if (found != expected) {
        throw DynamiteException(
            QString(str::error::headerValidationFailure).arg(expected, found));
    }
}

enum IndexArray {
    reg_nr_idx = 0,
    attempt_nr_idx = 1,
    course_title_idx = 2,
    semester_idx = 3,
    NR_HEADER_INDICES
};

/**
 * \brief Check the naming of all headers.
 */
std::array<qsizetype, 4> validateHeaders(const CsvDump &c) {

    // Do headers exist at all?
    if (c.data.empty())
        throw DynamiteException(str::error::empty);

    const auto &headers = c.data[0];

    std::array<qsizetype, NR_HEADER_INDICES> result;

    // In permissive mode we only check for the names of headers we require and
    // ignore all other header names.
#if !RWTHONLINE_PARSER_PERMISSIVE
    if (c.data[0].size() != 24) {
        throw DynamiteException(QString(str::error::headerValidationColumnNr)
                                    .arg(c.data[0].size()));
    }

    static constexpr std::array headerStrings = {"STUDY_PROGRAMME",
                                                 "CODE_OF_STUDY_PROGRAMME",
                                                 "Studienplan_Version",
                                                 "SPO_KONTEXT",
                                                 "REGISTRATION_NUMBER",
                                                 "FAMILY_NAME_OF_STUDENT",
                                                 "FIRST_NAME_OF_STUDENT",
                                                 "GESCHLECHT",
                                                 "DATE_OF_ASSESSMENT",
                                                 "GUEL_U_AKTUELLE_ANTRITTE_SPO",
                                                 "GRADE",
                                                 "REMARK",
                                                 "Number_Of_The_Course",
                                                 "SEMESTER_OF_The_COURSE",
                                                 "COURSE_TITLE",
                                                 "Examiner",
                                                 "Start_Time",
                                                 "TERMIN_ORT",
                                                 "DB_Primary_Key_Of_Exam",
                                                 "DB_Primary_Key_Of_Candidate",
                                                 "COURSE_GROUP_NAME",
                                                 "FILE_REMARK",
                                                 "EMAIL_ADDRESS",
                                                 "ECTS_GRADE"};

    for (int i = 0; i < 24; ++i) {
        checkHeader(headers[i], headerStrings[static_cast<unsigned>(i)]);
    }
    result[reg_nr_idx] = 4;
    result[attempt_nr_idx] = 9;
    result[course_title_idx] = 14;
    result[semester_idx] = 13;
    return result;
#else
    // find columns we are interested in
    const auto registrationNrIdx = headers.indexOf("REGISTRATION_NUMBER");
    const auto attemptNrIdx = headers.indexOf("GUEL_U_AKTUELLE_ANTRITTE_SPO");
    const auto courseTitleIdx = headers.indexOf("COURSE_TITLE");
    const auto semesterIdx = headers.indexOf("SEMESTER_OF_The_COURSE");

    if (registrationNrIdx == -1) {
        throw DynamiteException(
            QString(str::error::noColumn).arg("registration number"));
    }
    if (attemptNrIdx == -1) {
        throw DynamiteException(
            QString(str::error::noColumn).arg("attempt number"));
    }
    if (courseTitleIdx == -1) {
        throw DynamiteException(
            QString(str::error::noColumn).arg("course title"));
    }
    if (semesterIdx == -1) {
        throw DynamiteException(QString(str::error::noColumn).arg("semester"));
    }

    result[reg_nr_idx] = registrationNrIdx;
    result[attempt_nr_idx] = attemptNrIdx;
    result[course_title_idx] = courseTitleIdx;
    result[semester_idx] = semesterIdx;
    return result;
#endif
}
} // namespace

StudentData::StudentData(const CsvDump &c) noexcept(false) : mRwthOnline(c) {
    if (c.data.empty())
        throw DynamiteException(str::error::empty);

    if (c.data[0].empty())
        throw DynamiteException(str::error::noHeader);

    // Quick check: reading wrong file type?
    {
        const auto fileType = getApproximateFileType(c.data[0][0]);

        switch (fileType) {
        case FileType::bonus:
            throw DynamiteException(str::error::unexpectedBonus);
        case FileType::dynexite:
            throw DynamiteException(str::error::unexpectedDynexite);
        default:
            // the file might still be invalid, but that will be decided later
            break;
        }
    }

    const auto nrRows = c.data.size();

    // No data aside from header?
    if (nrRows < 2)
        throw DynamiteException(str::error::noData);

    const auto nrStudents = nrRows - 1;
    reserveArrays(nrStudents);

    const auto headerIndices = validateHeaders(c);
    insertData(c, headerIndices);

    checkDataSanity();
}

void StudentData::reserveArrays(const qsizetype nrStudents) noexcept {
    mRegistrationNr.reserve(nrStudents);
    mAttemptNr.reserve(nrStudents);
}

void StudentData::insertData(
    const CsvDump &c,
    const std::array<qsizetype, 4> &headerIndices) noexcept(false) {

    const auto regIdx = headerIndices[reg_nr_idx];
    const auto attemptIdx = headerIndices[attempt_nr_idx];
    const auto courseTitleIdx = headerIndices[course_title_idx];
    const auto semesterIdx = headerIndices[semester_idx];

    // Course title and semester are the same for all students
    // So just read them from the first student
    mCourseTitles.emplaceBack(c.data[1][courseTitleIdx]);
    mSemesters.emplaceBack(c.data[1][semesterIdx]);

    // Read the registration number and attempt number from the csv-file to our
    // struct
    for (const auto &line : c.data | std::views::drop(1)) {

        bool regNrSuccess, attemptNrSuccess;
        line[regIdx].toInt(&regNrSuccess);
        const auto attemptNr = line[attemptIdx].toInt(&attemptNrSuccess);

        if (!attemptNrSuccess) {
            throw DynamiteException(
                QString(str::error::parseFailure)
                    .arg("attempt number", line[attemptIdx]));
        }
        if (!regNrSuccess) {
            throw DynamiteException(
                QString(str::error::parseFailure)
                    .arg("registration number", line[regIdx]));
        }

        mRegistrationNr.emplaceBack(line[regIdx]);
        mAttemptNr.emplaceBack(attemptNr);

        // Assert that the two arrays don't get out of sync
        if (mRegistrationNr.size() != mAttemptNr.size()) {
            throw DynamiteException(str::error::rwthArraySizeMismatch);
        }

        // Assert that course title and semester really are consistent among all
        // students
        if (!mCourseTitles.contains(line[courseTitleIdx])) {
            mCourseTitles.emplaceBack(line[courseTitleIdx]);
        }
        if (!mSemesters.contains(line[semesterIdx])) {
            mSemesters.emplaceBack(line[semesterIdx]);
        }
    }
}

void StudentData::checkDataSanity() const noexcept(false) {

    // No duplicate registration numbers
    {
        auto sortedIdentifiers = mRegistrationNr;
        std::ranges::sort(sortedIdentifiers);

        if (const auto firstDuplicate =
                std::ranges::adjacent_find(sortedIdentifiers);
            firstDuplicate != sortedIdentifiers.end()) {

            throw DynamiteException(QString(str::error::duplicateRegistrationNr)
                                        .arg(*firstDuplicate));
        }
    }
}
