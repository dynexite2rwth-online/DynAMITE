#include "combined_grade_average.hpp"

#include "combined_grade_list.hpp"

void CombinedGradeAverage::recalculate() noexcept {

    const auto &gradesRef =
        std::get<const CombinedGradeList &>(this->mParentRefs);

    auto sumAll = 0., sumPassed = 0.;
    mNrPassed = 0;

    // Add all passing grades / all grades together.
    // Count how many students passed.
    // Since the average is only for display purposes, the rounding error due to
    // naive summation is acceptable
    for (const auto grade : gradesRef.mGrades) {
        const auto gradeDouble = gradeToDouble(grade);

        sumAll += gradeDouble;

        if (grade != Grade::grade5_0) {
            sumPassed += gradeDouble;
            ++mNrPassed;
        }
    }

    mAverageGrade = gradesRef.mGrades.empty()
                        ? std::nullopt
                        : std::optional(sumAll / static_cast<double>(
                                                     gradesRef.mGrades.size()));
    mAverageGradePassed =
        mNrPassed == 0
            ? std::nullopt
            : std::optional(sumPassed / static_cast<double>(mNrPassed));
}
