#include "dynexite_exam.hpp"

#include "util/defaults.hpp"
#include "util/enums.hpp"
#include "util/exception.hpp"
#include "util/heuristics.hpp"
#include "util/numeric.hpp"
#include "util/strings.hpp"

#include <algorithm>
#include <cassert>
#include <limits>
#include <numeric>
#include <ranges>

namespace {
void checkHeader(const QString &found,
                 const QString &expected) noexcept(false) {
    if (found != expected) {
        throw DynamiteException(
            QString(str::error::headerValidationFailure).arg(expected, found));
    }
}

void checkHeaderEndsWith(const QString &found,
                         const QString &ending) noexcept(false) {

    if (!found.endsWith(ending)) {
        throw DynamiteException(
            QString(str::error::headerValidationFailureEnding)
                .arg(ending, found));
    }
}

/**
 * \brief Check the naming of all headers.
 *
 * We currently don't support arbitrarily sorted headers.
 */
void validateHeaders(const CsvDump &c,
                     const CsvLine::size_type nrTasks) noexcept(false) {

    // Do headers exist at all?
    if (c.data.empty())
        throw DynamiteException(str::error::empty);

    const auto &headers = c.data[0];
    const auto nrColumns = c.data[0].size();

    checkHeader(headers[0], "AttemptID");
    checkHeader(headers[1], "Identifier");

    if (c.version == CsvVersion::Version1) {
        for (CsvLine::size_type i = 0; i < nrTasks; ++i) {
            checkHeaderEndsWith(headers[i * 4 + 2], " (open)");
            checkHeaderEndsWith(headers[i * 4 + 3], " (closed)");
            checkHeaderEndsWith(headers[i * 4 + 4], " (earned)");
            checkHeaderEndsWith(headers[i * 4 + 5], " (max)");
        }
    } else {
        for (CsvLine::size_type i = 0; i < nrTasks; ++i) {
            checkHeaderEndsWith(headers[i * 6 + 2], " (open)");
            checkHeaderEndsWith(headers[i * 6 + 3], " (open max)");
            checkHeaderEndsWith(headers[i * 6 + 4], " (closed)");
            checkHeaderEndsWith(headers[i * 6 + 5], " (closed max)");
            checkHeaderEndsWith(headers[i * 6 + 6], " (earned)");
            checkHeaderEndsWith(headers[i * 6 + 7], " (max)");
        }
    }

    for (CsvLine::size_type i = 3;
         const auto &s : {"Open", "Closed", "Overall"}) {
        checkHeader(headers[nrColumns - 3 - i * 3], s);
        checkHeader(headers[nrColumns - 2 - i * 3], QString(s) + " (max)");
        checkHeader(headers[nrColumns - 1 - i * 3], QString(s) + " (%)");

        --i;
    }

    checkHeader(headers[nrColumns - 3], "Progress");
    checkHeader(headers[nrColumns - 2], "Execution Time");
    checkHeader(headers[nrColumns - 1], "Late Registration");
}

void setPoolMax(TaskPool &g,
                const EnumStorage<Points, GradingType, 2> &newMax) {
    using enum GradingType;

    assert(
        qFuzzyCompare(newMax[Closed] + newMax[Open], g.mMaxPoints[Combined]));

    g.mMaxPoints[Closed] = newMax[Closed];
    g.mMaxPoints[Open] = newMax[Open];

    for (const auto &task : g.mTasks) {
        task->mMaxPoints[Closed] = newMax[Closed];
        task->mMaxPoints[Open] = newMax[Open];
    }
};
} // namespace

Task::Task(QString taskName, const int nrStudents) noexcept
    : mTaskName(std::move(taskName)), mParticipatedStudents(nrStudents) {
    for (auto &l : mEarnedPoints) {
        l.resize(nrStudents);
    }
}

QList<Points>
Task::assignedEarnedPoints(const GradingType grading) const noexcept {

    auto optPoints = mEarnedPoints[grading];

    const auto pastTheEnd =
        std::remove(optPoints.begin(), optPoints.end(), std::nullopt);

    QList<Points> points;
    points.reserve(pastTheEnd - optPoints.begin());
    std::transform(optPoints.begin(), pastTheEnd, std::back_inserter(points),
                   [](const std::optional<Points> &p) { return *p; });

    return points;
}

QString
TaskPool::constructTaskPoolName(const QString &delimiter) const noexcept {

    QString taskPoolNames;
    bool isFirst = true;

    for (const auto &task : mTasks) {
        if (!isFirst) {
            taskPoolNames.append(delimiter);
        } else {
            isFirst = false;
        }
        taskPoolNames.append(task->mTaskName);
    }
    return taskPoolNames;
}

DynexiteExam::DynexiteExam(CsvDump c) noexcept(false) : mDynexite(c) {
    if (c.data.empty())
        throw DynamiteException(str::error::empty);

    if (c.data[0].empty())
        throw DynamiteException(str::error::noHeader);

    // Quick check: reading wrong file type?
    {
        const auto fileType = getApproximateFileType(c.data[0][0]);

        switch (fileType) {
        case FileType::bonus:
            throw DynamiteException(str::error::unexpectedBonus);
        case FileType::rwth:
            throw DynamiteException(str::error::unexpectedRwthOnline);
        default:
            // the file might still be invalid, but that will be decided later
            break;
        }
    }

    const auto nrRows = c.data.size();
    const auto nrColumns = c.data[0].size();
    const auto version = c.version;

    // No data aside from header?
    if (nrRows < 2)
        throw DynamiteException(str::error::noData);

    // Non-attending students are listed as lines in the csv-file, but the lines
    // are empty aside from column 1, especially they are empty in column 0
    // (AttemptID)
    const auto nrStudents =
        std::ranges::count_if(c.data | std::views::drop(1),
                              [](const CsvLine &l) { return !l[0].isEmpty(); });

    // No-one attended?
    if (nrStudents <= 0)
        throw DynamiteException(str::error::noStudents);

    // Assert that all necessary columns are there and the number of columns
    // makes sense (4/6 columns per task, 2 extra columns to the left and 12
    // extra columns to the right)
    if (version == CsvVersion::Version1) {
        if (!(nrColumns > 14 && (nrColumns - 14) % 4 == 0)) {
            throw DynamiteException(str::error::invalidColumnNr +
                                    QString::number(nrColumns));
        }
    } else {
        if (!(nrColumns > 14 && (nrColumns - 14) % 6 == 0)) {
            throw DynamiteException(str::error::invalidColumnNr +
                                    QString::number(nrColumns));
        }
    }

    CsvLine::size_type nrTasks;
    if (version == CsvVersion::Version1) {
        nrTasks = (nrColumns - 14) / 4;
    } else {
        nrTasks = (nrColumns - 14) / 6;
    }

    reserveArrays(nrStudents, nrTasks);

    validateHeaders(c, nrTasks);

    if (defaults::newGeneralExaminationRegulations) {
        makeAllPointsOpen(c, nrTasks);
    }

    insertData(c, nrStudents, nrTasks);

    checkDataSanity();
}

/**
 * \brief Since we know the future sizes of all data structure, we reserve them
 * for some performance improvements. Also, we already create an empty (but
 * reserved) list of points for each task.
 */
void DynexiteExam::reserveArrays(const qsizetype nrStudents,
                                 const qsizetype nrTasks) noexcept {

    mIdentifier.reserve(nrStudents);

    using enum GradingType;

    mPointsWithoutBonusOrElim[Closed].reserve(nrStudents);
    mPointsWithoutBonusOrElim[Open].reserve(nrStudents);
    mPointsWithoutBonusOrElim[Combined].reserve(nrStudents);

    mTaskItems.reserve(nrTasks);

    // If task pools are used, then the array can be smaller than this.
    mTaskPools.reserve(nrTasks);
}

void DynexiteExam::insertData(
    const CsvDump &c, const CsvLine::size_type nrStudents,
    const CsvLine::size_type nrTasks) noexcept(false) {

    const auto nrColumns = c.data[0].size();
    const auto columnsPerTask = c.version == CsvVersion::Version1 ? 4 : 6;

    using enum GradingType;

    // Max points are stored in every line, but should be the same everywhere
    mPointsMaxRaw[Open] = stringToPoints(c.data[1][nrColumns - 11]);
    mPointsMaxRaw[Closed] = stringToPoints(c.data[1][nrColumns - 8]);
    mPointsMaxRaw[Combined] = stringToPoints(c.data[1][nrColumns - 5]);

    if (!qFuzzyCompare(mPointsMaxRaw[Closed] + mPointsMaxRaw[Open],
                       mPointsMaxRaw[Combined])) {
        throw DynamiteException(
            QString(str::error::differentMaxSums)
                .arg(mPointsMaxRaw[Closed].toStringMaxPrecision(),
                     mPointsMaxRaw[Open].toStringMaxPrecision(),
                     mPointsMaxRaw[Combined].toStringMaxPrecision()));
    }

    // Read in task names
    {
        const auto &headerLine = c.data[0];

        for (CsvLine::size_type i = 0; i < nrTasks; ++i) {
            // The first two columns are not task related.
            static constexpr CsvLine::size_type startOffset = 2;

            auto taskName = headerLine[startOffset + i * columnsPerTask];

            // Remove the " (open)" at the end
            taskName.chop(7);

            mTaskItems.emplace_back(taskName, nrStudents);
        }
    }

    // Ignore header line, ignore empty, non-attendant students
    for (auto studentIdx = 0; const auto &line : c.data | std::views::drop(1)) {

        // This statement does nothing but avoids -Wunused-but-set-variable Error with Clang
        (void)studentIdx;

        // Skip non-attending students
        if (line[0].isEmpty())
            continue;

        // Skip attempt id at column 0

        // Identifier (Matrikelnummer) might be a string for test exam data
        // todo: consistent handling of non-numerical identifiers (dynexite: ok,
        //       rwthonline: ok, bonus: warning)
        mIdentifier.push_back(line[1]);

        // Points per task
        for (CsvLine::size_type taskIdx = 0; taskIdx < nrTasks; ++taskIdx) {

            // The first two columns are not task related.
            static constexpr CsvLine::size_type startOffset = 2;

            /*
             * The columns are in the order:
             *      Earned open
             *      [Max open] (if v2)
             *      Earned closed
             *      [Max closed] (if v2)
             *      Earned combined
             *      Max overall
             *
             * If a line is empty, that means that this student was not
             * assigned this task.
             */

            int openEarnedIdx, closedEarnedIdx, totalEarnedIdx, maxIdx,
                openMaxIdx = -1, closedMaxIdx = -1;
            if (c.version == CsvVersion::Version1) {
                openEarnedIdx = 0;
                closedEarnedIdx = 1;
                totalEarnedIdx = 2;
                maxIdx = 3;
            } else {
                openEarnedIdx = 0;
                openMaxIdx = 1;
                closedEarnedIdx = 2;
                closedMaxIdx = 3;
                totalEarnedIdx = 4;
                maxIdx = 5;
            }

            // Count number of empty cells per task per student
            // Either all or none of the cells should be empty
            int nrEmptyCells = 0;
            for (const auto idx :
                 {openEarnedIdx, closedEarnedIdx, totalEarnedIdx, maxIdx}) {
                nrEmptyCells += static_cast<int>(
                    line[startOffset + taskIdx * columnsPerTask + idx]
                        .isEmpty());
            }

            // Only relevant in Version 2+
            if (c.version == CsvVersion::Version1) {
                // For convenience, so that the sum is 6 or 0 regardless of
                // version
                nrEmptyCells += nrEmptyCells > 0 ? 2 : 0;
            } else {
                for (const auto idx : {openMaxIdx, closedMaxIdx}) {
                    nrEmptyCells += static_cast<int>(
                        line[startOffset + taskIdx * columnsPerTask + idx]
                            .isEmpty());
                }
            }

            // Validate that either all or none of a tasks cells are filled with
            // data
            if (nrEmptyCells != 0 && nrEmptyCells != 6) {
                throw DynamiteException(
                    QString(str::error::notAllOfOpenClosedEarned)
                        .arg(mIdentifier.back(),
                             mTaskItems[taskIdx].mTaskName));
            }

            // Store the earned points for students who were assigned this task
            // item
            if (nrEmptyCells == 0) {

                mTaskItems[taskIdx].mParticipatedStudents.setBit(studentIdx,
                                                                 true);

                // Earned open
                mTaskItems[taskIdx].mEarnedPoints[Open][studentIdx] =
                    stringToPoints(line[startOffset + taskIdx * columnsPerTask +
                                        openEarnedIdx]);

                // Earned closed
                mTaskItems[taskIdx].mEarnedPoints[Closed][studentIdx] =
                    stringToPoints(line[startOffset + taskIdx * columnsPerTask +
                                        closedEarnedIdx]);

                // Earned combined
                mTaskItems[taskIdx].mEarnedPoints[Combined][studentIdx] =
                    stringToPoints(line[startOffset + taskIdx * columnsPerTask +
                                        totalEarnedIdx]);

                // Max open/closed/combined
                const auto combinedMaxPoints = stringToPoints(
                    line[startOffset + taskIdx * columnsPerTask + maxIdx]);
                auto openMaxPoints = Points{-1}, closedMaxPoints = Points{-1};
                if (c.version != CsvVersion::Version1) {
                    openMaxPoints = stringToPoints(
                        line[startOffset + taskIdx * columnsPerTask +
                             openMaxIdx]);
                    closedMaxPoints = stringToPoints(
                        line[startOffset + taskIdx * columnsPerTask +
                             closedMaxIdx]);
                }

                // Check that max open/closed/combined are the same in every row
                if (mTaskItems[taskIdx].mMaxPoints[Combined] == Points{-1.}) {
                    mTaskItems[taskIdx].mMaxPoints[Combined] =
                        combinedMaxPoints;
                    if (c.version != CsvVersion::Version1) {
                        mTaskItems[taskIdx].mMaxPoints[Open] = openMaxPoints;
                        mTaskItems[taskIdx].mMaxPoints[Closed] =
                            closedMaxPoints;
                    }
                } else {
                    // Assert that all lines where the max points are shown show
                    // the same max points for a task
                    if (mTaskItems[taskIdx].mMaxPoints[Combined] !=
                        combinedMaxPoints)
                        throw DynamiteException(
                            QString(str::error::inconsistentTaskMaxPoints)
                                .arg(mTaskItems[taskIdx].mTaskName,
                                     mTaskItems[taskIdx]
                                         .mMaxPoints[Combined]
                                         .toStringMaxPrecision(),
                                     combinedMaxPoints.toStringMaxPrecision()));
                    if (c.version != CsvVersion::Version1) {
                        // Assert that open max/closed max are the same (exist
                        // only in dynexite version 2+)
                        if (mTaskItems[taskIdx].mMaxPoints[Open] !=
                            openMaxPoints) {
                            throw DynamiteException(
                                QString(str::error::
                                            inconsistentTaskGradingMaxPoints)
                                    .arg(mTaskItems[taskIdx].mTaskName,
                                         mTaskItems[taskIdx]
                                             .mMaxPoints[Open]
                                             .toStringMaxPrecision(),
                                         openMaxPoints.toStringMaxPrecision(),
                                         "open"));
                        }
                        if (mTaskItems[taskIdx].mMaxPoints[Closed] !=
                            closedMaxPoints) {
                            throw DynamiteException(
                                QString(str::error::
                                            inconsistentTaskGradingMaxPoints)
                                    .arg(mTaskItems[taskIdx].mTaskName,
                                         mTaskItems[taskIdx]
                                             .mMaxPoints[Closed]
                                             .toStringMaxPrecision(),
                                         closedMaxPoints.toStringMaxPrecision(),
                                         "closed"));
                        }
                    }
                }
            }
        }

        // Total exam points
        if (line[nrColumns - 12].isEmpty()) {
            // An empty line indicates a non-attending student.
            // Lines with non-attending students should have been skipped!
            assert(false);

            // Anyways, if the student didn't attend, fill in zeros.
            mPointsWithoutBonusOrElim[Closed].push_back(Points{0.});
            mPointsWithoutBonusOrElim[Open].push_back(Points{0.});
            mPointsWithoutBonusOrElim[Combined].push_back(Points{0.});

        } else {
            // Achieved open points
            mPointsWithoutBonusOrElim[Open].push_back(
                stringToPoints(line[nrColumns - 12]));

            // Total open points should be the same everywhere
            if (mPointsMaxRaw[Open] != stringToPoints(line[nrColumns - 11]))
                throw DynamiteException(
                    QString(str::error::inconsistentMaxTypePoints)
                        .arg(toString(Open),
                             mPointsMaxRaw[Open].toStringMaxPrecision(),
                             stringToPoints(line[nrColumns - 11])
                                 .toStringMaxPrecision()));

            // Skip open points percentage at nrColumns - 10

            // Achieved closed points
            mPointsWithoutBonusOrElim[Closed].push_back(
                stringToPoints(line[nrColumns - 9]));

            // Total closed points should be the same everywhere
            if (mPointsMaxRaw[Closed] != stringToPoints(line[nrColumns - 8]))
                throw DynamiteException(
                    QString(str::error::inconsistentMaxTypePoints)
                        .arg(toString(Closed),
                             mPointsMaxRaw[Closed].toStringMaxPrecision(),
                             stringToPoints(line[nrColumns - 8])
                                 .toStringMaxPrecision()));

            // Skip closed points percentage at nrColumns - 7

            // Achieved combined points
            mPointsWithoutBonusOrElim[Combined].push_back(
                stringToPoints(line[nrColumns - 6]));

            // Total combined points should be the same everywhere
            if (mPointsMaxRaw[Combined] != stringToPoints(line[nrColumns - 5]))
                throw DynamiteException(
                    QString(str::error::inconsistentMaxTypePoints)
                        .arg(toString(Combined),
                             mPointsMaxRaw[Combined].toStringMaxPrecision(),
                             stringToPoints(line[nrColumns - 5])
                                 .toStringMaxPrecision()));

            // Skip overall points percentage at nrColumns - 4

            // Skip progress, execution time, late registration (unused)
        }

        ++studentIdx;
    }

    // Set maximum achievable points of task items that were assigned to nobody
    // to 0
    for (auto &taskItem : mTaskItems) {
        if (taskItem.mMaxPoints[Combined] == Points{-1.}) {
            taskItem.mMaxPoints[Closed] = Points{0.};
            taskItem.mMaxPoints[Open] = Points{0.};
            taskItem.mMaxPoints[Combined] = Points{0.};
        }
    }

    createTaskPools();

    if (c.version == CsvVersion::Version1) {
        guessMaxOpenClosedPerTask();
    } else {
        setMaxOpenClosedPerTask();
    }
}

void DynexiteExam::createTaskPools() noexcept(false) {
    const auto nrStudents = mIdentifier.size();

    using enum GradingType;

    /*
     * Not all tasks are assigned to all students. Some tasks are split into
     * multiple items and each student is given one of the items. A collection
     * of items so that each student was assigned one of them is called a task
     * pool.
     *
     * We iterate over all tasks and build up task pools as we go. We assume
     * therefore that items from the same task pool are adjacent to each other
     * in the dynexite csv file.
     *
     * If we stumble over a item that no student was assigned to, then we have
     * the problem that there is no way for us to confidently determine to which
     * task pool this item belongs. If we are in the middle of creating a task
     * pool, we add the task to that task pool. If we are not, we create a new
     * task pool with that task as it's only item.
     */

    // "true" at idx i means the student at index i was assigned a task from any
    // task in the task pool that is currently being built up. When the task
    // pool is complete, all bits are true.
    QBitArray currentTaskPoolParticipants(nrStudents);
    TaskPool currentTaskPool;

    for (qsizetype taskIdx = 0; auto &task : mTaskItems) {

        // All bitsets should be the size nrStudents
        assert(currentTaskPoolParticipants.size() ==
                   task.mParticipatedStudents.size() &&
               currentTaskPoolParticipants.size() == nrStudents);

        // Make sure no student was assigned multiple tasks from one task pool
        if ((currentTaskPoolParticipants & task.mParticipatedStudents)
                .count(true) > 0) {

            throw DynamiteException(
                QString(str::error::studentMultipleTaskPools)
                    .arg(mTaskItems[taskIdx - 1].mTaskName, task.mTaskName));
        }

        // All items in the task pool must have the same mMaxPoints-value
        if (currentTaskPool.mMaxPoints[Combined] < Points{0.}) {

            currentTaskPool.mMaxPoints[Combined] = task.mMaxPoints[Combined];
        } else {
            if (currentTaskPool.mMaxPoints[Combined] !=
                task.mMaxPoints[Combined]) {

                throw DynamiteException(
                    QString(str::error::taskItemsDifferentMax)
                        .arg(task.mTaskName,
                             task.mMaxPoints[Combined].toStringMaxPrecision(),
                             mTaskItems[taskIdx - 1].mTaskName,
                             currentTaskPool.mMaxPoints[Combined]
                                 .toStringMaxPrecision()));
            }
        }
        currentTaskPool.mTasks.push_back(&task);
        currentTaskPoolParticipants |= task.mParticipatedStudents;

        const auto currentTaskPoolCount =
            currentTaskPoolParticipants.count(true);

        // task pool full?
        if (currentTaskPoolCount == nrStudents || currentTaskPoolCount == 0) {

            // task pool is complete or no student was assigned this task
            // add it to the list of task pools
            assert(!currentTaskPool.mTasks.empty());
            mTaskPools.push_back(currentTaskPool);

            // Store the task pool in all tasks in it
            for (const auto taskItemInPool : currentTaskPool.mTasks) {
                taskItemInPool->mTaskPool = &mTaskPools.back();
            }

            // reset currentTaskPoolParticipants and currentTaskPool
            currentTaskPoolParticipants.fill(false);
            currentTaskPool = TaskPool{};
        } else if (currentTaskPoolCount < nrStudents) {
            // task pool is not complete yet
            // keep currentTaskPoolParticipants and currentTaskPool
        } else {
            // task pool is overfull
            // should never happen (already checked above with &)
            assert(false && "Task pool overfull");
        }

        ++taskIdx;
    }

    // At the end no task pool should be left only partially filled
    if (currentTaskPoolParticipants.count(true) > 0) {
        throw DynamiteException(QString(str::error::lastTaskPoolIncomplete)
                                    .arg(currentTaskPool.mTasks[0]->mTaskName));
    }

    if (!currentTaskPool.mTasks.empty()) {
        throw DynamiteException(QString(str::error::lastTaskPoolNotEmpty)
                                    .arg(currentTaskPool.mTasks[0]->mTaskName));
    }
}

void DynexiteExam::checkDataSanity() const noexcept(false) {

    using enum GradingType;

    // all task item max points have been filled in
    for (const auto &task : mTaskItems) {
        for (const auto &taskMax : task.mMaxPoints) {
            if (taskMax < Points{0.}) {
                throw DynamiteException(
                    QString(str::error::taskMaxLt0)
                        .arg(task.mTaskName, taskMax.toStringMaxPrecision()));
            }
        }
    }

    // all task item task pools have been filled in
    for (const auto &task : mTaskItems) {
        if (!task.mTaskPool) {
            throw DynamiteException(
                QString("Didn't provide a task pool for task %1")
                    .arg(task.mTaskName));
        }
    }

    // all task pool max points have been filled in
    for (const auto &taskPool : mTaskPools) {
        for (const auto &poolMax : taskPool.mMaxPoints) {
            if (poolMax < Points{0.}) {
                throw DynamiteException(
                    QString(str::error::taskPoolMaxLt0)
                        .arg(taskPool.constructTaskPoolName(),
                             poolMax.toStringMaxPrecision()));
            }
        }
    }

    // sum of task max points == total max points
    {
        // Might have slight numerical error because of std::accumulate instead
        // of kahanSum, which is ok because it is only used in verifications
        // with qFuzzyCompare
        const auto getSumOfTaskMax = [this](GradingType grading) {
            return std::accumulate(
                mTaskPools.begin(), mTaskPools.end(), Points{0.},
                [grading](const Points p, const TaskPool &g2) {
                    return p + g2.mMaxPoints[grading];
                });
        };

        const auto sumOfTaskMax =
            getStorage(getSumOfTaskMax(Closed), getSumOfTaskMax(Open),
                       getSumOfTaskMax(Combined));
        if (!qFuzzyCompare(sumOfTaskMax[Combined], mPointsMaxRaw[Combined])) {

            throw DynamiteException(
                QString(str::error::sumOfTasksNotTotalMax)
                    .arg(sumOfTaskMax[Combined].toStringMaxPrecision(),
                         mPointsMaxRaw[Combined].toStringMaxPrecision()));
        } else if (!qFuzzyCompare(sumOfTaskMax[Closed],
                                  mPointsMaxRaw[Closed]) ||
                   !qFuzzyCompare(sumOfTaskMax[Open], mPointsMaxRaw[Open])) {
            throw DynamiteException(
                QString(str::error::taskMaxDeduceSumFailure)
                    .arg(sumOfTaskMax[Closed].toStringMaxPrecision(),
                         sumOfTaskMax[Open].toStringMaxPrecision(),
                         mPointsMaxRaw[Closed].toStringMaxPrecision(),
                         mPointsMaxRaw[Open].toStringMaxPrecision()));
        }
    }

    // For each task and each student:
    // - task earned points < task max points
    // - earned open + earned closed = earned combined
    for (const auto &taskItem : mTaskItems) {
        for (int studentIdx = 0;
             studentIdx < taskItem.mEarnedPoints[Closed].size(); ++studentIdx) {

            // only relevant if the student was assigned this item
            if (taskItem.mEarnedPoints[Closed][studentIdx]) {

                const auto &earnedPointsClosed =
                    *taskItem.mEarnedPoints[Closed][studentIdx];
                const auto &earnedPointsOpen =
                    *taskItem.mEarnedPoints[Open][studentIdx];
                const auto &earnedPointsCombined =
                    *taskItem.mEarnedPoints[Combined][studentIdx];

                if (definitelyGreaterThan(earnedPointsClosed + earnedPointsOpen,
                                          taskItem.mMaxPoints[Combined])) {

                    throw DynamiteException(
                        QString(str::error::studentEarnedTooManyPointsTask)
                            .arg(mIdentifier[studentIdx], taskItem.mTaskName,
                                 earnedPointsClosed.toStringMaxPrecision(),
                                 earnedPointsOpen.toStringMaxPrecision(),
                                 taskItem.mMaxPoints[Combined]
                                     .toStringMaxPrecision()));
                }

                if (!qFuzzyCompare(earnedPointsClosed + earnedPointsOpen,
                                   earnedPointsCombined)) {

                    throw DynamiteException(
                        QString(str::error::differentMaxSumsTask)
                            .arg(mIdentifier[studentIdx], taskItem.mTaskName,
                                 earnedPointsClosed.toStringMaxPrecision(),
                                 earnedPointsOpen.toStringMaxPrecision(),
                                 earnedPointsCombined.toStringMaxPrecision()));
                }
            }
        }
    }

    // For each student:
    // sum of task earned closed/open/combined ==
    // total earned closed/open/combined as stated by dynexite
    for (int studentIdx = 0; studentIdx < mIdentifier.size(); ++studentIdx) {
        EnumStorage<Points> earned;

        for (const auto &taskItem : mTaskItems) {

            // only relevant if the student was assigned this item
            if (taskItem.mEarnedPoints[Closed][studentIdx]) {
                for (const auto g : {Closed, Open, Combined}) {
                    // Numerical inaccuracies of this addition are compensated
                    // by importEpsCompare
                    earned[g] += *taskItem.mEarnedPoints[g][studentIdx];
                }
            }
        }
        for (const auto g : {Closed, Open, Combined}) {
            if (!importEpsCompare(earned[g],
                                  mPointsWithoutBonusOrElim[g][studentIdx],
                                  mPointsMaxRaw[g])) {
                throw DynamiteException(
                    QString(str::error::differentTaskSumEarned)
                        .arg(toString(g), mIdentifier[studentIdx],
                             earned[g].toStringMaxPrecision(),
                             mPointsWithoutBonusOrElim[g][studentIdx]
                                 .toStringMaxPrecision()));
            }
        }
    }

    // No duplicate registration numbers
    {
        auto sortedIdentifiers = mIdentifier;
        std::ranges::sort(sortedIdentifiers);

        if (const auto firstDuplicate =
                std::ranges::adjacent_find(sortedIdentifiers);
            firstDuplicate != sortedIdentifiers.end()) {

            throw DynamiteException(QString(str::error::duplicateRegistrationNr)
                                        .arg(*firstDuplicate));
        }
    }

    // Highest allowed point value: 1'000'000
    // If this fails there probably went something wrong when reading the file.
    static constexpr auto largestAllowedPointValue = 1'000'000;
    if (mPointsMaxRaw[Combined].p > largestAllowedPointValue) {

        throw DynamiteException(
            QString(str::error::maxPointOverflow)
                .arg(mPointsMaxRaw[Combined].toStringMaxPrecision())
                .arg(largestAllowedPointValue));
    }
}

void DynexiteExam::guessMaxOpenClosedPerTask() noexcept(false) {

    using enum GradingType;

    for (auto &taskPool : mTaskPools) {

        // Version 1 of the input csv file sadly does not provide information on
        // what the maximum achievable closed vs open points of a task is, only
        // the maximum achievable combined points.
        // Therefore we use a simple heuristic for now:
        // If the maximum achieved closed points + maximum achieved open
        // points == maximum achievable combined points, then those are used as
        // maximum for closed and open points.
        // Else if no student gathered any closed/open points, it is assumed
        // that there were no closed/open points.
        // Else error for now.

        EnumStorage<Points, GradingType, 2> maxAchievedPoints;

        for (const auto grading : {Closed, Open}) {
            QList<Points> maxPointsPerItem;
            for (const auto &item : taskPool.mTasks) {
                if (item->mParticipatedStudents.count(true) > 0) {
                    maxPointsPerItem.push_back(**std::ranges::max_element(
                        item->mEarnedPoints[grading]));
                }
            }
            maxAchievedPoints[grading] =
                maxPointsPerItem.empty()
                    ? Points{0.}
                    : *std::ranges::max_element(maxPointsPerItem);
        }

        if (qFuzzyCompare(maxAchievedPoints[Closed] + maxAchievedPoints[Open],
                          taskPool.mMaxPoints[Combined])) {

            // We can safely deduce the max closed and open points, because the
            // max achieved closed and open points sum up to the max combined
            // points
            setPoolMax(taskPool, maxAchievedPoints);

        } else if (mPointsMaxRaw[Open] == Points{0}) {

            // We can safely deduce the max closed and open points, because the
            // exam is closed
            maxAchievedPoints[Closed] = taskPool.mMaxPoints[Combined];
            setPoolMax(taskPool, maxAchievedPoints);
        } else if (mPointsMaxRaw[Closed] == Points{0}) {

            // We can safely deduce the max closed and open points, because the
            // exam is open
            maxAchievedPoints[Open] = taskPool.mMaxPoints[Combined];
            setPoolMax(taskPool, maxAchievedPoints);
        } else {

            // todo: accept dynexite file, but make this task un-disableable
            throw DynamiteException(
                QString(str::error::taskMaxDeduceFailure)
                    .arg(taskPool.constructTaskPoolName(),
                         maxAchievedPoints[Closed].toStringMaxPrecision(),
                         maxAchievedPoints[Open].toStringMaxPrecision(),
                         taskPool.mMaxPoints[Combined].toStringMaxPrecision()));
        }
    }
}

void DynexiteExam::setMaxOpenClosedPerTask() noexcept {

    for (auto &taskPool : mTaskPools) {

        using enum GradingType;

        EnumStorage<Points, GradingType, 2> maxAchievedPoints;
        maxAchievedPoints[Open] = taskPool.mTasks[0]->mMaxPoints[Open];
        maxAchievedPoints[Closed] = taskPool.mTasks[0]->mMaxPoints[Closed];

        for ([[maybe_unused]] const auto item : taskPool.mTasks) {
            assert(
                qFuzzyCompare(item->mMaxPoints[Open], maxAchievedPoints[Open]));
            assert(qFuzzyCompare(item->mMaxPoints[Closed],
                                 maxAchievedPoints[Closed]));
        }

        setPoolMax(taskPool, maxAchievedPoints);
    }
}

void DynexiteExam::makeAllPointsOpen(CsvDump &c,
                                     const CsvLine::size_type nrTasks) const
    noexcept(false) {

    const auto nrColumns = c.data[0].size();
    const auto columnsPerTask = c.version == CsvVersion::Version1 ? 4 : 6;

    using enum GradingType;

    // Ignore header line, ignore empty, non-attendant students
    for (auto studentIdx = 0; auto &line : c.data | std::views::drop(1)) {

        // Skip non-attending students
        if (line[0].isEmpty())
            continue;

        // Points per task
        for (CsvLine::size_type taskIdx = 0; taskIdx < nrTasks; ++taskIdx) {

            // The first two columns are not task related.
            static constexpr CsvLine::size_type startOffset = 2;

            /*
             * The columns are in the order:
             *      Earned open
             *      [Max open] (if v2)
             *      Earned closed
             *      [Max closed] (if v2)
             *      Earned combined
             *      Max overall
             *
             * If a line is empty, that means that this student was not
             * assigned this task.
             */

            int openEarnedIdx, closedEarnedIdx, totalEarnedIdx, maxIdx,
                openMaxIdx = -1, closedMaxIdx = -1;
            if (c.version == CsvVersion::Version1) {
                openEarnedIdx = 0;
                closedEarnedIdx = 1;
                totalEarnedIdx = 2;
                maxIdx = 3;
            } else {
                openEarnedIdx = 0;
                openMaxIdx = 1;
                closedEarnedIdx = 2;
                closedMaxIdx = 3;
                totalEarnedIdx = 4;
                maxIdx = 5;
            }

            // Count number of empty cells per task per student
            // Either all or none of the cells should be empty
            int nrEmptyCells = 0;
            for (const auto idx :
                 {openEarnedIdx, closedEarnedIdx, totalEarnedIdx, maxIdx}) {
                nrEmptyCells += static_cast<int>(
                    line[startOffset + taskIdx * columnsPerTask + idx]
                        .isEmpty());
            }

            // Only relevant in Version 2+
            if (c.version == CsvVersion::Version1) {
                // For convenience, so that the sum is 6 or 0 regardless of
                // version
                nrEmptyCells += nrEmptyCells > 0 ? 2 : 0;
            } else {
                for (const auto idx : {openMaxIdx, closedMaxIdx}) {
                    nrEmptyCells += static_cast<int>(
                        line[startOffset + taskIdx * columnsPerTask + idx]
                            .isEmpty());
                }
            }

            // Validate that either all or none of a tasks cells are filled with
            // data
            if (nrEmptyCells != 0 && nrEmptyCells != 6) {
                throw DynamiteException(
                    QString(str::error::notAllOfOpenClosedEarned)
                        .arg(mIdentifier.back(),
                             mTaskItems[taskIdx].mTaskName));
            }

            if (nrEmptyCells == 0) {

                // Add closed points to open points for students who were
                // assigned this task item
                line[startOffset + taskIdx * columnsPerTask + openEarnedIdx] =
                    (stringToPoints(
                         line[startOffset + taskIdx * columnsPerTask +
                              openEarnedIdx]) +
                     stringToPoints(
                         line[startOffset + taskIdx * columnsPerTask +
                              closedEarnedIdx]))
                        .toStringMaxPrecision();
                line[startOffset + taskIdx * columnsPerTask + closedEarnedIdx] =
                    "0";

                // Add closed max points to open max points for students who
                // were assigned this task item
                if (c.version != CsvVersion::Version1) {
                    line[startOffset + taskIdx * columnsPerTask + openMaxIdx] =
                        (stringToPoints(
                             line[startOffset + taskIdx * columnsPerTask +
                                  openMaxIdx]) +
                         stringToPoints(
                             line[startOffset + taskIdx * columnsPerTask +
                                  closedMaxIdx]))
                            .toStringMaxPrecision();
                    line[startOffset + taskIdx * columnsPerTask +
                         closedMaxIdx] = "0";
                }
            }
        }

        // Total exam points
        if (!line[nrColumns - 12].isEmpty()) {
            // Achieved open points
            line[nrColumns - 12] = (stringToPoints(line[nrColumns - 12]) +
                                    stringToPoints(line[nrColumns - 9]))
                                       .toStringMaxPrecision();

            // Total open points
            line[nrColumns - 11] = (stringToPoints(line[nrColumns - 11]) +
                                    stringToPoints(line[nrColumns - 8]))
                                       .toStringMaxPrecision();

            // Achieved closed points
            line[nrColumns - 9] = "0";

            // Total closed points
            line[nrColumns - 8] = "0";
        }

        ++studentIdx;
    }
}
