#pragma once

#include "graph.hpp"

#include "util/enums.hpp"
#include "util/storage_helper.hpp"

#include <QObject>

#include <memory>

struct D2rSettings;
/**
 * \brief Stores and manages access to the exam data.
 *
 * Provides an interface for:
 *  - seamlessly swapping the underlying exam data
 *  - modifying the exam data in the allowed ways
 *  - receiving signals when the exam data changes
 */
class DataManager : public QObject {
    Q_OBJECT

public:
    /**
     * \brief Tries to creates a new \ref DAG object with the given
     * arguments.
     *
     * If it succeeds, the result is stored in \ref mNew and true is returned,
     * else false is returned and \ref mNew remains untouched.
     *
     * \param exam Argument for \ref DAG.
     * \param students Argument for \ref DAG.
     * \param bonus Argument for \ref DAG.
     * \param gradeConfig Argument for \ref DAG.
     * \param pointConfig Argument for \ref DAG.
     * \return Whether mNew now points to the \ref DAG created with
     * the given parameters.
     */
    [[nodiscard]] static bool
    loadNewExam(const DynexiteExam &exam, const StudentData &students,
                const BonusPoints &bonus, const UserGradeConfig &gradeConfig,
                const UserPointConfig &pointConfig) noexcept;
    /**
     * \brief Replaces the \ref mCurrent -exam with the one currently stored in
     * \ref mNew, clears mNew.
     */
    static void makeNewCurrent(bool emitGradesChanged = true) noexcept;

    /**
     * \brief Allows const access to the current exam (might be NULL).
     * \return \ref mCurrent
     */
    [[nodiscard]] static const DAG *getCurrent() noexcept;

    /**
     * \brief Change custom total points.
     *
     * \param grading What `GradingType` these changes are meant for.
     * \param newMax New total points.
     * \param alternateExam If not NULL, use this DAG instead of
     * `mCurrent` and don't emit a signal. Useful for unit tests.
     * \param emitGradesChanged Whether or not to emit a gradesChanged-signal at
     * the end.
     */
    static void maxChange(GradingType grading, GradeBoundary newMax,
                          DAG *alternateExam = nullptr,
                          bool emitGradesChanged = true) noexcept;

    /**
     * \brief Manually change a grade boundary.
     *
     * \param grading Grading type for which the change should apply.
     * \param boundaryGrade Grade for which to change the boundary.
     * \param newBoundaryPoints New value (internally converted to corresponding
     * grade boundary).
     * \param alternateExam If not NULL, use this DAG instead of `mCurrent` and
     * don't emit a signal. Useful for unit tests.
     * \param emitGradesChanged Whether or not to emit a gradesChanged-signal at
     * the end.
     *
     * \return Whether the boundary was set to exactly the provided value.
     */
    static bool boundaryChange(GradingType grading, Grade boundaryGrade,
                               Points newBoundaryPoints,
                               DAG *alternateExam = nullptr,
                               bool emitGradesChanged = true) noexcept;
    static bool boundaryChange(GradingType grading, Grade boundaryGrade,
                               GradeBoundary newBoundary,
                               DAG *alternateExam = nullptr,
                               bool emitGradesChanged = true) noexcept;

    /**
     * \brief Change whether to automatically adopt the automatically calculated
     * grading limits.
     *
     * \param grading Grading type for which the change should apply.
     * \param followDefault New value.
     * \param alternateExam If not NULL, use this DAG instead of
     * `mCurrent` and don't emit a signal. Useful for unit tests.
     * \param emitGradesChanged Whether or not to emit a gradesChanged-signal at
     * the end.
     */
    static void autoBoundaryChange(GradingType grading, bool followDefault,
                                   DAG *alternateExam = nullptr,
                                   bool emitGradesChanged = true) noexcept;

    /**
     * \brief Change runtime user configuration.
     *
     * \param newConfig New settings.
     * \param alternateExam If not NULL, use this DAG instead of
     * `mCurrent` and don't emit a signal. Useful for unit tests.
     * \param emitGradesChanged Whether or not to emit a gradesChanged-signal at
     * the end.
     */
    static void gradeConfigChange(const UserGradeConfig &newConfig,
                                  DAG *alternateExam = nullptr,
                                  bool emitGradesChanged = true) noexcept;
    static void pointConfigChange(const UserPointConfig &newConfig,
                                  DAG *alternateExam = nullptr,
                                  bool emitGradesChanged = true) noexcept;

    /**
     * \brief Changes the elimination type of the task pool at index
     * `taskPoolIdx`.
     */
    static void taskEliminationChange(int taskPoolIdx,
                                      TaskEliminationType newElimType,
                                      bool emitGradesChanged = true) noexcept;

    static void applySettings(const D2rSettings &s);

    // Getter / setter for the mUnsavedChanges-flag
    static void setUnsavedChanges(bool hasUnsavedChanges) noexcept;
    [[nodiscard]] static bool hasUnsavedChanges() noexcept;

Q_SIGNALS:
    /**
     * \brief Signals that the entire exam has changed.
     *
     * This can mean that the number of students or tasks has changed
     * completely.
     *
     * This is usually triggered after loading in new exam data.
     *
     * \note When an exam changes, this signal as well as \ref
     * taskEliminationChanged and \ref gradesChanged are sent.
     */
    void examChanged();

    /**
     * \brief Signals changes in the points-data of students because tasks have
     * been eliminated (or been re-added).
     *
     * \note When tasks are eliminated changes, both this signal and \ref
     * gradesChanged are sent.
     *
     * \param changedTypes Specifies whether the changed points are in open or
     * closed section or both. The combined grades are always assumed to be
     * changed.
     */
    void taskEliminationChanged(const EnumStorage<bool> &changedTypes);

    /**
     * \brief Signals changes in grade-related data.
     *
     * This is usually triggered by a changed grade boundary.
     *
     * \param changedTypes Specifies whether the changes are in open or closed
     * grades or both. The combined grades are always assumed to be changed.
     */
    void gradesChanged(const EnumStorage<bool> &changedTypes);

private:
    /**
     * \brief Stores the DAG that should get displayed right now.
     */
    std::unique_ptr<DAG> mCurrent = nullptr;
    /**
     * \brief Stores the DAG that is currently being loaded in, e.g. selected in
     * the input dialog.
     */
    std::unique_ptr<DAG> mNew = nullptr;

    /**
     * \brief Whether any changes to task elimination or grades were made after
     * loading the current exam.
     */
    static bool mUnsavedChanges;

public:
    /**
     * \brief We use the singleton design pattern for this class.
     *
     * \note An instance of this class is necessary when binding a slot the
     * functions this class emits.
     */
    static DataManager &getInstance() noexcept;

    // don't allow copy of singleton
    DataManager(const DataManager &) = delete;
    DataManager &operator=(const DataManager &) = delete;

private:
    // don't allow new instantiations of singleton
    DataManager() = default;
};
