#pragma once

#include "nodes/applied_bonus_points.hpp"
#include "nodes/bonus_points.hpp"
#include "nodes/changed_max_points.hpp"
#include "nodes/combined_grade_average.hpp"
#include "nodes/combined_grade_list.hpp"
#include "nodes/dynexite_exam.hpp"
#include "nodes/exam_stats.hpp"
#include "nodes/first_try.hpp"
#include "nodes/grade_list.hpp"
#include "nodes/node.hpp"
#include "nodes/passing_limit.hpp"
#include "nodes/points_without_bonus.hpp"
#include "nodes/student_data.hpp"
#include "nodes/student_points.hpp"
#include "nodes/user_grade_boundaries.hpp"
#include "nodes/user_grade_config.hpp"
#include "nodes/user_point_config.hpp"

#include "util/enums.hpp"
#include "util/testing.hpp"

#include <cassert>
#include <tuple>
#include <utility>

class QTextStream;

/**
 * \brief If N has a member function 'recalculate' and any of ChangedNodes is a
 * predecessor to N, returns true if 'recalculate' is declared to not throw any
 * exceptions. Else, returns true.
 *
 * \warning Parameter order swapped around compared to \ref
 * updateNodesIfNecessary
 */
template <typename N, typename... ChangedNodes>
[[nodiscard]] consteval bool recalculateIsNoexcept() noexcept {
    if constexpr (AnyIsPredecessorOfAny<std::tuple<ChangedNodes...>,
                                        std::tuple<N>>::value) {
        return noexcept(std::declval<N>().recalculate());
    }
    return true;
}

template <typename... ChangedNode>
void updateNodesIfNecessary() noexcept {}

/**
 * \brief For every node in the parameters (n and nodes), calls their
 * recalculate function if any of the ChangedNodes is a predecessor to them.
 */
template <typename... ChangedNodes, typename N, typename... Nodes>
void updateNodesIfNecessary(N &n, Nodes &...nodes) noexcept(
    noexcept(updateNodesIfNecessary<ChangedNodes...>(nodes...)) &&
    recalculateIsNoexcept<N, ChangedNodes...>()) {
    if constexpr (AnyIsPredecessorOfAny<std::tuple<ChangedNodes...>,
                                        std::tuple<N>>::value) {
        n.recalculate();
    }
    updateNodesIfNecessary<ChangedNodes...>(nodes...);
}

struct DAG {

    using enum GradingType;

    // Struct members have to be sorted so that predecessors are initialized
    // before their descendants

    UserPointConfig mUserPointConfig{};
    UserGradeConfig mUserGradeConfig{};
    DynexiteExam mExam{};
    StudentData mStudents{};
    BonusPoints mBonus{};

private:
    PointsWithoutBonus<Closed> mClosedPointsWithoutBonus{mExam};
    PointsWithoutBonus<Open> mOpenPointsWithoutBonus{mExam};
    PointsWithoutBonus<Combined> mCombinedPointsWithoutBonus{mExam};

    ExamStats mStats{mExam, mStudents};

    ChangedMaxPoints<Closed> mChangedMaxPointsClosed{mUserPointConfig,
                                                     mClosedPointsWithoutBonus};
    ChangedMaxPoints<Open> mChangedMaxPointsOpen{mUserPointConfig,
                                                 mOpenPointsWithoutBonus};

    AppliedBonusPoints<Closed> mAppliedBonusPointsClosed{
        mBonus, mChangedMaxPointsClosed, mChangedMaxPointsOpen};
    AppliedBonusPoints<Open> mAppliedBonusPointsOpen{
        mBonus, mChangedMaxPointsClosed, mChangedMaxPointsOpen};

    StudentPoints<Closed> mStudentPointsClosed{mExam, mClosedPointsWithoutBonus,
                                               mAppliedBonusPointsClosed};
    StudentPoints<Open> mStudentPointsOpen{mExam, mOpenPointsWithoutBonus,
                                           mAppliedBonusPointsOpen};

    FirstTry mFirstTry{mExam, mStudents, mClosedPointsWithoutBonus};

    PassingLimit<Closed> mClosedPassingLimit{mChangedMaxPointsClosed, mFirstTry,
                                             mUserPointConfig};
    PassingLimit<Open> mOpenPassingLimit{mChangedMaxPointsOpen,
                                         mUserPointConfig};

    UserGradeBoundaries<Closed> mClosedUserBoundaries{
        mUserPointConfig, mChangedMaxPointsClosed, mClosedPassingLimit};
    UserGradeBoundaries<Open> mOpenUserBoundaries{
        mUserPointConfig, mChangedMaxPointsOpen, mOpenPassingLimit};

    RoundedPoints<Closed> mClosedRoundedPoints{mClosedPointsWithoutBonus,
                                               mStudentPointsClosed};
    RoundedPoints<Open> mOpenRoundedPoints{mOpenPointsWithoutBonus,
                                           mStudentPointsOpen};
    RoundedPoints<Combined> mCombinedRoundedPoints{mClosedRoundedPoints,
                                                   mOpenRoundedPoints};

    GradeList<Closed> mClosedGrades{mUserPointConfig, mUserGradeConfig,
                                    mClosedUserBoundaries,
                                    mClosedRoundedPoints};
    GradeList<Open> mOpenGrades{mUserPointConfig, mUserGradeConfig,
                                mOpenUserBoundaries, mOpenRoundedPoints};

    CombinedGradeList mGrades{mChangedMaxPointsClosed, mChangedMaxPointsOpen,
                              mClosedGrades, mOpenGrades, mUserGradeConfig};

    CombinedGradeAverage mGradeAverage{mGrades};

    template <typename... ChangedNodes>
    void updateMemberNodesIfNecessary() /* specifying noexcept is too tedious */
    {
        /*
         * The order of function arguments determines the order in which their
         * recalculate method will be (potentially) called. So make sure that
         * every node appears after all its predecessors.
         */
        updateNodesIfNecessary<ChangedNodes...>(
            mUserPointConfig, mUserGradeConfig, mExam, mStudents, mBonus,
            mClosedPointsWithoutBonus, mOpenPointsWithoutBonus,
            mCombinedPointsWithoutBonus, mStats, mChangedMaxPointsClosed,
            mChangedMaxPointsOpen, mAppliedBonusPointsClosed,
            mAppliedBonusPointsOpen, mStudentPointsClosed, mStudentPointsOpen,
            mFirstTry, mClosedPassingLimit, mOpenPassingLimit,
            mClosedUserBoundaries, mOpenUserBoundaries, mClosedRoundedPoints,
            mOpenRoundedPoints, mCombinedRoundedPoints, mClosedGrades,
            mOpenGrades, mGrades, mGradeAverage);
    }

public:
    DAG(DynexiteExam exam, StudentData students, BonusPoints bonus,
        UserGradeConfig gradeConfig, UserPointConfig pointConfig)
        : mUserPointConfig(std::move(pointConfig)),
          mUserGradeConfig(std::move(gradeConfig)), mExam(std::move(exam)),
          mStudents(std::move(students)), mBonus(std::move(bonus)) {
        updateMemberNodesIfNecessary<DynexiteExam, StudentData, BonusPoints,
                                     UserGradeConfig, UserPointConfig>();
    }

    /**
     * \brief Copy constructor: copy root nodes, bind references of non-root
     * nodes to root nodes of this object, not root nodes of the copied-from
     * object
     *
     * Sadly, that means that we have to recalculate all non-root nodes.
     *
     * \note Only used in unit tests.
     */
    DAG(const DAG &other)
        : mUserPointConfig(other.mUserPointConfig),
          mUserGradeConfig(other.mUserGradeConfig), mExam(other.mExam),
          mStudents(other.mStudents), mBonus(other.mBonus) {
        updateMemberNodesIfNecessary<DynexiteExam, StudentData, BonusPoints,
                                     UserGradeConfig, UserPointConfig>();
    }
    // Other operators are too tedious, given that this is only used in tests
    // and the copy constructor is enough there.
    DAG(DAG &&other) = delete;
    DAG &operator=(const DAG &other) = delete;
    DAG &operator=(DAG &&other) = delete;

    ~DAG() = default;

    // helper functions for easier access
    [[nodiscard]] constexpr auto
    getMaxPassingLimit(const GradingType grading) const noexcept {
        switch (grading) {
        default:
            assert(false);
            [[fallthrough]];
        case Closed:
            return mClosedPassingLimit.mMaxPassingLimit;
        case Open:
            return mOpenPassingLimit.mMaxPassingLimit;
        }
    }
    [[nodiscard]] constexpr auto getRelPassingLimit() const noexcept {
        return mClosedPassingLimit.mRelPassingLimit;
    }
    [[nodiscard]] constexpr auto getAbsPassingLimit() const noexcept {
        return mClosedPassingLimit.mAbsPassingLimit;
    }
    [[nodiscard]] constexpr const auto &
    getUserGradeBoundaries(const GradingType grading) const noexcept {
        switch (grading) {
        default:
            assert(false);
            [[fallthrough]];
        case Closed:
            return mClosedUserBoundaries.mBoundaries;
        case Open:
            return mOpenUserBoundaries.mBoundaries;
        }
    }
    [[nodiscard]] constexpr auto
    getUserGradeBoundarySetAuto(const GradingType grading) const noexcept {
        switch (grading) {
        default:
            assert(false);
            [[fallthrough]];
        case Closed:
            return mClosedUserBoundaries.mSetAutomatically;
        case Open:
            return mOpenUserBoundaries.mSetAutomatically;
        }
    }
    [[nodiscard]] constexpr const auto &
    getGradeList(const GradingType grading) const noexcept {
        switch (grading) {
        default:
            assert(false);
            [[fallthrough]];
        case Closed:
            return mClosedGrades.mGrades;
        case Open:
            return mOpenGrades.mGrades;
        case Combined:
            return mGrades.mGrades;
        }
    }
    [[nodiscard]] constexpr const auto &
    getPointsWithoutBonus(const GradingType grading) const noexcept {
        switch (grading) {
        default:
            assert(false);
            [[fallthrough]];
        case Closed:
            return mClosedRoundedPoints.mRoundedPointsWithoutBonus;
        case Open:
            return mOpenRoundedPoints.mRoundedPointsWithoutBonus;
        case Combined:
            return mCombinedRoundedPoints.mRoundedPointsWithoutBonus;
        }
    }
    /**
     * \brief The unrounded, maximum achievable points of GradingType grading
     * without bonus points, taking into account eliminated tasks.
     */
    [[nodiscard]] constexpr auto
    getMaxPoints(const GradingType grading) const noexcept {
        switch (grading) {
        default:
            assert(false);
            [[fallthrough]];
        case Closed:
            return mClosedPointsWithoutBonus.mMaxPoints;
        case Open:
            return mOpenPointsWithoutBonus.mMaxPoints;
        case Combined:
            return mCombinedPointsWithoutBonus.mMaxPoints;
        }
    }
    [[nodiscard]] constexpr auto getAvgFirstTryPoints() const noexcept {

        return mUserPointConfig.mCountZerothAttemptAsFirstAttempt
                   ? mFirstTry.mAvgFirstAndZerothTryPoints
                   : mFirstTry.mAvgFirstTryPoints;
    }
    [[nodiscard]] constexpr auto
    getOriginalMaxPointsBoundary(const GradingType grading) const noexcept {
        switch (grading) {
        default:
            assert(false);
            [[fallthrough]];
        case Closed:
            return mChangedMaxPointsClosed.mOriginalMaxPointBoundary;
        case Open:
            return mChangedMaxPointsOpen.mOriginalMaxPointBoundary;
        }
    }
    [[nodiscard]] constexpr auto
    getChangedMaxPointsBoundary(const GradingType grading) const noexcept {
        switch (grading) {
        default:
            assert(false);
            [[fallthrough]];
        case Closed:
            return mChangedMaxPointsClosed.mChangedMaxPointBoundary;
        case Open:
            return mChangedMaxPointsOpen.mChangedMaxPointBoundary;
        }
    }
    [[nodiscard]] constexpr const auto &
    getPointsWithBonus(const GradingType grading) const noexcept {
        switch (grading) {
        default:
            assert(false);
            [[fallthrough]];
        case Closed:
            return mClosedRoundedPoints.mRoundedPointsWithBonus;
        case Open:
            return mOpenRoundedPoints.mRoundedPointsWithBonus;
        case Combined:
            return mCombinedRoundedPoints.mRoundedPointsWithBonus;
        }
    }

    /**
     * \brief Whether the given GradingType is present in the non-eliminated
     * tasks of this exam.
     *
     * \note If the user has manually reduced the max of one of the grading
     * types to zero, hasType still returns true for that type, if there are
     * still tasks that give points in that type.
     *
     * `hasType(Combined)` returns true if the exam has both closed and open
     * points.
     */
    [[nodiscard]] constexpr bool
    hasType(const GradingType grading) const noexcept {
        if (grading == Combined) {
            return hasType(Closed) && hasType(Open);
        } else {
            return getMaxPoints(grading) > Points{0.};
        }
    }

    /**
     * \brief Whether any number of bonus points for any student (participating
     * or not) were specified.
     *
     * Note that even if this returns true, no student who participated might
     * have any bonus points.
     */
    [[nodiscard]] auto hasBonus() const noexcept {
        return !mBonus.mTotalBonusPointsPerStudent.empty();
    }

    [[nodiscard]] auto getStats() const noexcept { return mStats; }

    [[nodiscard]] auto getGradeAverages() const noexcept {
        return mGradeAverage;
    }

    friend class DataManager;
    friend void writeCurrentSettingsToD2r(QTextStream &);
    FRIEND_TEST(FirstTryDagTest, CorrectlyCalculatedInDag);
};
