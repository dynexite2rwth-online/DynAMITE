Packaging
=========

This directory contains files and utilities for creating binary packages of DynAMITE. Items in this directory are not
referenced, used, or included in the regular DynAMITE builds.
