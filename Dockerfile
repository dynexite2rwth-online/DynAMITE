################################
#                              #
# Linux build, test & coverage #
#                              #
################################
FROM ubuntu:22.04 AS linux-builder

RUN apt-get update && apt-get install -y \
    ca-certificates \
    gpg \
    software-properties-common \
    sudo \
    wget
RUN ["/bin/bash", "-c", "set -o pipefail && wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | sudo tee /usr/share/keyrings/kitware-archive-keyring.gpg >/dev/null"]
RUN ["/bin/bash", "-c", "set -o pipefail && echo 'deb [signed-by=/usr/share/keyrings/kitware-archive-keyring.gpg] https://apt.kitware.com/ubuntu/ jammy main' | sudo tee /etc/apt/sources.list.d/kitware.list >/dev/null"]
RUN apt-get update && rm /usr/share/keyrings/kitware-archive-keyring.gpg && apt-get install -y kitware-archive-keyring
RUN rm -rf /var/lib/apt/lists/* && \
    apt-get --fix-missing update  && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
    g++-12 \
    g++-11 \
    clang-15 \
    libc++-15-dev \
    libc++abi-15-dev \
    cmake \
    qt6-base-dev \
    libqt6charts6-dev \
    git \
    gcovr \
    libgl1-mesa-dev \
    lsb-release \
    gnupg && \
    rm -rf /var/lib/apt/lists/*

# Clang-16 (no official release yet)
RUN wget https://apt.llvm.org/llvm.sh \
    && chmod +x llvm.sh \
    && sudo ./llvm.sh 16

COPY test/unittest_data /usr/src/unittest_data


############################
#                          #
# Linux build & deployment #
#                          #
############################
# linuxdeployqt requires the use of the oldest still-supported Ubuntu LTS release,
# which is 20.04 at the time of writing (2024-03).
FROM ubuntu:20.04 AS linux-deploy

RUN apt-get update && apt-get install -y \
    ca-certificates \
    gpg \
    software-properties-common \
    sudo \
    wget
RUN add-apt-repository -y ppa:ubuntu-toolchain-r/test && add-apt-repository ppa:okirby/qt6-backports
RUN ["/bin/bash", "-c", "set -o pipefail && wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | sudo tee /usr/share/keyrings/kitware-archive-keyring.gpg >/dev/null"]
RUN ["/bin/bash", "-c", "set -o pipefail && echo 'deb [signed-by=/usr/share/keyrings/kitware-archive-keyring.gpg] https://apt.kitware.com/ubuntu/ focal main' | sudo tee /etc/apt/sources.list.d/kitware.list >/dev/null"]
RUN apt-get update && rm /usr/share/keyrings/kitware-archive-keyring.gpg
RUN apt-get update && apt-get install -y \
    cmake \
    curl \
    git \
    g++-11 \
    kitware-archive-keyring \
    libgl1-mesa-dev \
    libqt6charts6-dev \
    qt6-base-dev \
    qtchooser && \
    rm -rf /var/lib/apt/lists/*

RUN qtchooser -install qt6 $(which qmake6) && \
    cp /usr/lib/$(uname -p)-linux-gnu/qt-default/qtchooser/qt6.conf /usr/share/qtchooser/qt6.conf && \
    ln -n /usr/share/qtchooser/qt6.conf /usr/lib/$(uname -p)-linux-gnu/qt-default/qtchooser/default.conf


##############################
#                            #
# Windows build & deployment #
#                            #
##############################
FROM ubuntu:20.04 AS linux-xcompile-windows

# https://mxe.cc/#requirements-debian
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
    autoconf \
    automake \
    autopoint \
    bash \
    bison \
    bzip2 \
    curl \
    flex \
    g++ \
    g++-multilib \
    gettext \
    git \
    gperf \
    intltool \
    libc6-dev-i386 \
    libgdk-pixbuf2.0-dev \
    libltdl-dev \
    libgl-dev \
    libpcre3-dev \
    libssl-dev \
    libtool-bin \
    libxml-parser-perl \
    lzip \
    make \
    openssl \
    p7zip-full \
    patch \
    perl \
    python3 \
    python3-distutils \
    python3-mako \
    python3-pkg-resources \
    python-is-python3 \
    ruby \
    sed \
    unzip \
    wget \
    xz-utils && \
    rm -rf /var/lib/apt/lists/*

# https://mxe.cc/#tutorial
WORKDIR "/opt"
RUN git clone https://github.com/mxe/mxe.git
WORKDIR "/opt/mxe"
RUN make MXE_TARGETS='x86_64-w64-mingw32.static' qt6-qtbase qt6-qtcharts
ENV PATH="/opt/mxe/usr/bin:$PATH"
