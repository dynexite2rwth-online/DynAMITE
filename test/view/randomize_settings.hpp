#pragma once

#include <gtest/gtest.h>

#include <random>
#include <string>
#include <utility>

class ViewSettingsWindow;
class QValueAxis;
class QStackedBarSeries;
class PointHistogram;
struct DAG;
class MainWindow;

// Execute for each DAG
class GuiDagTest : public testing::TestWithParam<std::pair<std::string, DAG>> {
protected:
    void correctVisible(const MainWindow &mw);
    void selectTab(const MainWindow &mw, PointHistogram *p);
    QStackedBarSeries *oneSeries(PointHistogram *p);
    QValueAxis *oneXAxis(PointHistogram *p, QStackedBarSeries *series);
    QValueAxis *oneYAxis(PointHistogram *p, QStackedBarSeries *series);
    void noTruncatedLabels(PointHistogram *p, QValueAxis *xAxis,
                           QValueAxis *yAxis);
    void integerLabels(QValueAxis *xAxis, QValueAxis *yAxis);
    void seriesValid(const ViewSettingsWindow &viewSettings, PointHistogram *p,
                     QStackedBarSeries *series, QValueAxis *yAxis);

    auto randomizeSettings(std::default_random_engine &e, MainWindow &mw);
};
