#include "randomize_settings.hpp"

#include "data/data_manager.hpp"
#include "data/testable_objects.hpp"
#include "file_io/d2r_parser.hpp"
#include "util/defaults.hpp"
#include "util/print_types.hpp"
#include "view/main_window/grade_tab.hpp"
#include "view/main_window/main_window.hpp"
#include "view/main_window/point_histogram.hpp"

#include <gtest/gtest.h>

#include <QCoreApplication>

#include <random>
#include <string>

inline constexpr int nrRandomSettingChanges = 5;

INSTANTIATE_TEST_SUITE_P(GuiDagParameterizedTests, GuiDagTest,
                         testing::ValuesIn(exams()));

auto GuiDagTest::randomizeSettings(std::default_random_engine &e,
                                   MainWindow &mw) {

    std::string trace{"\n"};

    // Resolution
    {
        std::uniform_int_distribution<> uX(800, 2560);
        std::uniform_int_distribution<> uY(800, 1400);
        const auto width = uX(e);
        const auto height = uY(e);
        mw.resize(width, height);

        trace += "Resolution: " + std::to_string(width) + "x" +
                 std::to_string(height) + "\n";
    }

    // Point histogram step size
    {
        std::uniform_int_distribution uMode(0, 2);
        const auto mode = uMode(e);

        if (mode == 0) {
            // use multiple of 0.25 as new value
            static constexpr auto nrSteps = static_cast<int>(
                (defaults::maxBucketSize - defaults::minBucketSize) /
                defaults::bucketStep);
            std::uniform_int_distribution uSteps(0, nrSteps);
            mw.mViewSettings.mGradingTabBucketSize.setValue(
                defaults::minBucketSize + defaults::bucketStep * uSteps(e));

        } else if (mode == 1) {
            // use random real value as new value

            std::uniform_real_distribution range(0.,
                                                 defaults::maxBucketSize + 1.);
            mw.mViewSettings.mGradingTabBucketSize.setValue(range(e));

        } else if (mode == 2) {
            // step by a random amount

            // max steps: a bit larger than the max sensible amount
            static constexpr auto maxSteps =
                static_cast<int>(
                    (defaults::maxBucketSize - defaults::minBucketSize) /
                    defaults::bucketStep) +
                2;
            std::uniform_int_distribution uSteps(-maxSteps, maxSteps);
            mw.mViewSettings.mGradingTabBucketSize.stepBy(uSteps(e));
        }

        trace +=
            "Point histogram step size: " +
            std::to_string(mw.mViewSettings.mGradingTabBucketSize.value()) +
            "\n";
    }

    return trace;
}

// Check the results for our known test files
TEST_P(GuiDagTest, CheckKnownTestFiles) {

    SCOPED_TRACE("File: " + GetParam().first);

    const auto &graph = GetParam().second;

    using enum GradingType;

    MainWindow mw;

    mw.resetToInputFiles(graph.mExam, graph.mStudents, graph.mBonus,
                         graph.mUserPointConfig.mGradeBoundaryStepSize,
                         std::nullopt);

    std::default_random_engine e(std::random_device{}());

    const auto cur = DataManager::getCurrent();
    ASSERT_TRUE(cur);

    for (int i = 0; i < nrRandomSettingChanges; ++i) {

        if ((i % 100) == 99) {
            qDebug() << "Gui test iteration " << (i + 1);
        }

        // Run first iteration with default settings
        SCOPED_TRACE((i > 0) ? randomizeSettings(e, mw) : "Default settings");

        // wait until main window is properly initialized
        QCoreApplication::processEvents(QEventLoop::AllEvents);

        // assert that point histograms for closed/open are only visible if
        // there are closed/open points
        // todo: ASSERT_NO_FAIL
        correctVisible(mw);

        const auto &ps = mw.mGradeTab->mPointHistos;
        for (const auto &p : ps) {
            SCOPED_TRACE(std::string("Histogram for ") +
                         toString(p->mGradingType) + " points");
            selectTab(mw, p);
            auto series = oneSeries(p);
            auto xAxis = oneXAxis(p, series);
            auto yAxis = oneYAxis(p, series);
            // Truncation test disabled, because the results are not reliable
            // ("true" does not always result in visible truncation)
            // noTruncatedLabels(p, xAxis, yAxis);
            integerLabels(xAxis, yAxis);
            seriesValid(mw.mViewSettings, p, series, yAxis);
        }
    }
}
