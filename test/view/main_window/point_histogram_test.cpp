#include "view/main_window/point_histogram.hpp"

#include "data/data_manager.hpp"
#include "data/graph.hpp"
#include "data/testable_objects.hpp"
#include "util/print_types.hpp"
#include "view/main_window/grade_tab.hpp"
#include "view/main_window/main_window.hpp"
#include "view/main_window/view_settings.hpp"
#include "view/randomize_settings.hpp"

#include "gtest/gtest.h"

#include <QApplication>
#include <QBarSet>
#include <QStackedBarSeries>
#ifdef _MSC_VER
#pragma warning(disable : 4701)
#endif
#include <QTest>
#ifdef _MSC_VER
#pragma warning(default : 4701)
#endif
#include <QGraphicsSceneMouseEvent>
#include <QTime>
#include <QValueAxis>

#include <cmath>
#include <string>

// assert that point histograms for closed/open are only visible if there
// are closed/open points
void GuiDagTest::correctVisible(const MainWindow &mw) {
    const auto &graph = GetParam().second;

    if (graph.hasType(GradingType::Combined)) {
        EXPECT_TRUE(mw.mGradeTab->mPointHistoTabs->isTabVisible(0));
        EXPECT_TRUE(mw.mGradeTab->mPointHistoTabs->isTabVisible(1));
        EXPECT_TRUE(mw.mGradeTab->mPointHistoTabs->isTabVisible(2));
    } else {
        EXPECT_FALSE(mw.mGradeTab->mPointHistoTabs->isTabVisible(0));
        EXPECT_FALSE(mw.mGradeTab->mPointHistoTabs->isTabVisible(1));
        EXPECT_TRUE(mw.mGradeTab->mPointHistoTabs->isTabVisible(2));
    }
}

// select the tab of the current point histogram
void GuiDagTest::selectTab(const MainWindow &mw, PointHistogram *p) {
    const auto &graph = GetParam().second;

    using enum GradingType;
    if (graph.hasType(Combined) || p->mGradingType == Combined) {

        const auto pIdx = mw.mGradeTab->mPointHistoTabs->indexOf(p);
        // negative index means tab not found
        ASSERT_GE(pIdx, 0);
        mw.mGradeTab->mPointHistoTabs->setCurrentIndex(pIdx);
        QCoreApplication::processEvents();
    }
}

// only one QStackedBarSeries
QStackedBarSeries *GuiDagTest::oneSeries(PointHistogram *p) {

    auto seriesList = p->chart()->series();
    EXPECT_EQ(seriesList.size(), 1);
    return dynamic_cast<QStackedBarSeries *>(seriesList[0]);
}

QValueAxis *GuiDagTest::oneXAxis(PointHistogram *p, QStackedBarSeries *series) {

    // only one x-axis, which is not attached to the series
    auto xAxesList = p->chart()->axes(Qt::Horizontal, series);
    EXPECT_EQ(xAxesList.size(), 0);
    xAxesList = p->chart()->axes(Qt::Horizontal);
    EXPECT_EQ(xAxesList.size(), 1);
    return dynamic_cast<QValueAxis *>(xAxesList[0]);
}

QValueAxis *GuiDagTest::oneYAxis(PointHistogram *p, QStackedBarSeries *series) {
    // only one y-axis, which is attached to the series
    auto yAxesList = p->chart()->axes(Qt::Vertical, series);
    EXPECT_EQ(yAxesList.size(), 1);
    yAxesList = p->chart()->axes(Qt::Vertical);
    EXPECT_EQ(yAxesList.size(), 1);
    return dynamic_cast<QValueAxis *>(yAxesList[0]);
}

void GuiDagTest::noTruncatedLabels(PointHistogram *p, QValueAxis *xAxis,
                                   QValueAxis *yAxis) {
    using enum GradingType;
    const auto &graph = GetParam().second;
    if (graph.hasType(Combined) || p->mGradingType == Combined) {
        // no axis labels of visible charts are truncated
        EXPECT_FALSE(xAxis->labelsTruncated());
        EXPECT_FALSE(yAxis->labelsTruncated());
    }
}

void GuiDagTest::integerLabels(QValueAxis *xAxis, QValueAxis *yAxis) {
    // axis labels at integer values
    for (const auto &axis : {xAxis, yAxis}) {
        EXPECT_EQ(std::trunc(axis->tickInterval()), axis->tickInterval());
        EXPECT_EQ(std::trunc(axis->tickAnchor()), axis->tickAnchor());
        EXPECT_EQ(axis->tickType(), QValueAxis::TickType::TicksDynamic);
    }
}

void GuiDagTest::seriesValid(const ViewSettingsWindow &viewSettings,
                             PointHistogram *p, QStackedBarSeries *series,
                             QValueAxis *yAxis) {
    // Series

    const auto cur = DataManager::getCurrent();
    ASSERT_TRUE(cur);

    {
        EXPECT_EQ(series->barWidth(), 1.);

        // 2 bar sets: failed + passed
        EXPECT_EQ(series->count(), 2);
        auto barSetList = series->barSets();
        ASSERT_EQ(barSetList.size(), 2);

        // sum of failed + passed bars is the number of students
        EXPECT_EQ(barSetList[0]->sum() + barSetList[1]->sum(),
                  static_cast<qreal>(
                      cur->getPointsWithoutBonus(p->mGradingType).size()));

        // a bar for every achievable point value plus one for zero points
        for (const auto &barSet : barSetList) {

            const auto maxWithoutBonus = cur->getMaxPoints(p->mGradingType).p;
            const auto maxWithBonus =
                std::ranges::max_element(
                    cur->getPointsWithBonus(p->mGradingType))
                    ->p;
            const auto max = std::max(maxWithoutBonus, maxWithBonus);
            const auto pointBucketSize = viewSettings.mCurrentGradingBucketSize;
            EXPECT_EQ(
                barSet->count(),
                static_cast<int>(
                    impl::getBarIdx(pointBucketSize, Points{max},
                                    std::numeric_limits<qsizetype>::max()) +
                    1));
        }

        // all bar sizes are integers
        for (const auto &barSet : barSetList) {

            for (auto i = 0; i < barSet->count(); ++i) {
                EXPECT_EQ(std::trunc((*barSet)[i]), (*barSet)[i]);
                EXPECT_LE((*barSet)[i], yAxis->max());
            }
        }

        // sum of both bar sets is less than the y axis max
        for (int i = 0; i < barSetList[0]->count(); ++i) {

            EXPECT_LE((*barSetList[0])[i] + (*barSetList[1])[i], yAxis->max());
        }
    }
}
