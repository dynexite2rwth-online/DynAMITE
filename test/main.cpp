#include "gtest/gtest.h"

#include <QApplication>
#include <QLocale>
#include <QTimer>

int main(int argc, char *argv[]) {

    QLocale::setDefault(QLocale(QLocale::English, QLocale::UnitedKingdom));

    const QApplication app{argc, argv};

    QTimer::singleShot(0, &app, [&] {
        ::testing::InitGoogleTest(&argc, argv);
        const auto testResult = RUN_ALL_TESTS();
        QApplication::exit(testResult);
    });

    return QApplication::exec();
}
