import csv
import sys
import random
import codecs
import math
import os.path

# Result still contains information about:
# - rough point/bonus point distribution
# - number of tasks, max points in open/closed per task
# - number of participating students
# - ratios of zeroth/first/second/third tries


# Mappings of old registration id (matrikelnummer) to new
reg_id_map = {}

# Our new registration numbers
MIN_NR_REG_IDS = 100000
MAX_NR_REG_IDS = 150000
new_reg_ids = random.sample(range(MIN_NR_REG_IDS, MAX_NR_REG_IDS), MAX_NR_REG_IDS - MIN_NR_REG_IDS)
new_reg_id_idx = 0


def guess_encoding(path):
    encodings = ['utf-8-sig', 'windows-1252']
    for e in encodings:
        try:
            fh = codecs.open(path, 'r', encoding=e)
            fh.readlines()
            fh.seek(0)
        except UnicodeDecodeError:
            continue
        else:
            return e


# Verify that the dynexite header has the columns we expect
# Also replaces all task names with "task 0" - "task n"
# Returns that as a new list of headers
def pseudonymize_dynexite_tasks(reader, dynexite_version):
    headers = reader.fieldnames

    # At these positions in the original header there are all the non-task columns
    # Sanity-check that they are all there
    fixedHeaderIdx = [0, 1, -12, -11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1]
    fixedHeaders = ["AttemptID", "Identifier", "Open", "Open (max)", "Open (%)", "Closed", "Closed (max)", "Closed (%)",
                    "Overall", "Overall (max)", "Overall (%)", "Progress", "Execution Time", "Late Registration"]
    for idx in fixedHeaderIdx:
        if not headers[idx] == fixedHeaders[idx]:
            print("Unexpected header at position " + str(idx) + " in the dynexite file: Found " +
                  headers[idx] + ", expected " + fixedHeaders[idx])
            sys.exit(1)

    new_headers = fixedHeaders[:2]
    header_mapping = {}
    for h in fixedHeaders:
        header_mapping[h] = h

    # Change task names
    task_counter = 0

    # Dependent on the dynexite version, there are different columns
    # (open/closed max were added in version 2)
    if dynexite_version < 2:
        step = 4
    else:
        step = 6

    for original_open_task_header in headers[2:-12:step]:
        if not original_open_task_header.endswith(" (open)"):
            print("Unexpected task name in the dynexite file: Found " + original_open_task_header +
                  ", expected an ending of \" (open)\"")
            sys.exit(1)

        task_header = "task " + str(task_counter)
        new_headers.append(task_header + " (open)")
        if dynexite_version >= 2:
            new_headers.append(task_header + " (open max)")
        new_headers.append(task_header + " (closed)")
        if dynexite_version >= 2:
            new_headers.append(task_header + " (closed max)")
        new_headers.append(task_header + " (earned)")
        new_headers.append(task_header + " (max)")

        original_task_header = original_open_task_header[:-7]
        header_mapping[original_task_header + " (open)"] = task_header + " (open)"
        if dynexite_version >= 2:
            header_mapping[original_task_header + " (open max)"] = task_header + " (open max)"
        header_mapping[original_task_header + " (closed)"] = task_header + " (closed)"
        if dynexite_version >= 2:
            header_mapping[original_task_header + " (closed max)"] = task_header + " (closed max)"
        header_mapping[original_task_header + " (earned)"] = task_header + " (earned)"
        header_mapping[original_task_header + " (max)"] = task_header + " (max)"

        task_counter += 1

    new_headers += fixedHeaders[-12:]

    return new_headers, header_mapping, task_counter


# Returns the dynexite version and the offset of the csv table in the file
def read_dynexite_version(file):
    first_line = file.readline()
    first_line.replace(',', ';')
    first_line.replace('\t', ';')
    tokens = first_line.split(';')

    if len(tokens) >= 2 and tokens[0].lower() == 'version':
        # Skip additional empty line
        second_line = file.readline()
        if len(second_line.strip()) == 0:
            return int(tokens[1]), len(first_line) + len(second_line)
        else:
            return int(tokens[1]), len(first_line)

    # Undo read of first line
    file.seek(0)
    return 1, 0


# Writes the version indicator to the beginning of the file (if the version supports this)
def write_dynexite_version(file, version):
    if version > 1:
        file.write("Version;" + str(version) + "\n\n")


def process_dynexite(path):
    global new_reg_id_idx

    with (open(path, newline='', encoding=guess_encoding(path)) as csvfile,
          open(path[:-4] + "_pseudo.csv", 'w', newline='', encoding='utf-8') as outfile):

        dynexite_version, start_offset = read_dynexite_version(csvfile)
        print("DynAMITE version " + str(dynexite_version))

        dialect = csv.Sniffer().sniff(csvfile.read(65536))
        csvfile.seek(start_offset)

        reader = csv.DictReader(csvfile, dialect=dialect)

        # hide task names
        new_headers, header_mapping, nr_tasks = pseudonymize_dynexite_tasks(reader, dynexite_version)

        # Add version header to new file
        write_dynexite_version(outfile, dynexite_version)

        writer = csv.DictWriter(outfile, fieldnames=new_headers, dialect=dialect)
        writer.writeheader()

        rows = []

        row: dict
        for row in reader:
            # replace keys (csv headers) in rows with the new headers (hidden task names)
            for old_key in header_mapping:
                new_key = header_mapping[old_key]
                row[new_key] = row.pop(old_key)

            # if attemptid is empty, all rows except identifier are empty
            if row['AttemptID']:
                # replace AttemptID
                row['AttemptID'] = "attemptid"
                has_data = True
            else:
                has_data = False

            # replace registration number, store mapping in reg_id_map
            reg_id_map[row['Identifier']] = new_reg_ids[new_reg_id_idx]
            row['Identifier'] = new_reg_ids[new_reg_id_idx]
            new_reg_id_idx += 1

            if not has_data:
                continue

            total_open_earned_points = 0
            total_closed_earned_points = 0

            # randomize point value
            for taskNr in range(0, nr_tasks):
                # tasks may be empty
                if not row['task ' + str(taskNr) + ' (open)']:
                    if row['task ' + str(taskNr) + ' (closed)'] or row['task ' + str(taskNr) + ' (earned)'] or \
                            row['task ' + str(taskNr) + ' (max)']:
                        print("Found not all of open, closed, earned and max columns for task " + str(taskNr))
                        sys.exit(1)
                    continue

                task_open_earned_points = float(row['task ' + str(taskNr) + ' (open)'].replace(',', '.') or 0)
                task_closed_earned_points = float(row['task ' + str(taskNr) + ' (closed)'].replace(',', '.') or 0)
                task_total_earned_points = float(row['task ' + str(taskNr) + ' (earned)'].replace(',', '.') or 0)
                task_total_points = float(row['task ' + str(taskNr) + ' (max)'].replace(',', '.') or 0)
                vary_amount = math.floor(task_total_points * 0.05)
                if vary_amount < 1:
                    vary_amount = 1

                if task_open_earned_points + task_closed_earned_points != task_total_earned_points:
                    print("Task " + str(taskNr) + ": open+closed != max")
                    sys.exit(1)
                if task_total_earned_points > task_total_points:
                    print("Task " + str(taskNr) + ": earned > max")
                    sys.exit(1)

                open_earned_decrease_amount = min(math.floor(task_open_earned_points), vary_amount)
                new_task_open_earned_points = task_open_earned_points - random.randint(0, open_earned_decrease_amount)
                row['task ' + str(taskNr) + ' (open)'] = f'{new_task_open_earned_points:g}'
                total_open_earned_points += new_task_open_earned_points

                closed_earned_decrease_amount = min(math.floor(task_closed_earned_points), vary_amount)
                new_task_closed_earned_points = task_closed_earned_points - \
                    random.randint(0, closed_earned_decrease_amount)
                row['task ' + str(taskNr) + ' (closed)'] = f'{new_task_closed_earned_points:g}'
                total_closed_earned_points += new_task_closed_earned_points

                new_task_total_earned_points = float(row['task ' + str(taskNr) + ' (open)'].replace(',', '.')) + float(
                    row['task ' + str(taskNr) + ' (closed)'].replace(',', '.'))
                row['task ' + str(taskNr) + ' (earned)'] = f'{new_task_total_earned_points:g}'

            # update total open/closed/earned points
            if total_open_earned_points > float(row['Open'].replace(',', '.')):
                print("We only decrease the earned open points, but somehow they ended up increasing from " +
                      row['Open'] + " to " + str(total_open_earned_points) + "? Investigate.")
                sys.exit(1)
            row['Open'] = f'{total_open_earned_points:g}'
            if float(row['Open (max)'].replace(',', '.')) > 0:
                row['Open (%)'] = float(row['Open'].replace(',', '.')) / float(row['Open (max)'].replace(',', '.'))
            else:
                row['Open (%)'] = 0
            if total_closed_earned_points > float(row['Closed'].replace(',', '.')):
                print("We only decrease the earned closed points, but somehow they ended up increasing from " +
                      row['Closed'] + " to " + str(total_closed_earned_points) + "? Investigate.")
                sys.exit(1)
            row['Closed'] = f'{total_closed_earned_points:g}'
            if float(row['Closed (max)'].replace(',', '.')) > 0:
                row['Closed (%)'] = float(row['Closed'].replace(',', '.')) / float(
                    row['Closed (max)'].replace(',', '.'))
            else:
                row['Closed (%)'] = 0
            row['Overall'] = float(row['Open'].replace(',', '.')) + float(row['Closed'].replace(',', '.'))
            row['Overall (%)'] = float(row['Overall']) / float(row['Overall (max)'].replace(',', '.'))

            # replace Progress, Execution Time, Late Registration
            row['Progress'] = f'{0.9:g}'
            row['Execution Time'] = "1h30m0s"
            row['Late Registration'] = "true"

            rows.append(row)

        random.shuffle(rows)

        writer.writerows(rows)


def process_rwth(path):
    global new_reg_id_idx

    # Find out delimiter
    with open(path, newline='', encoding=guess_encoding(path)) as csvfile, \
            open(path[:-4] + "_pseudo.csv", 'w', newline='', encoding='utf-8') as outfile:
        dialect = csv.Sniffer().sniff(csvfile.read(65536))
        csvfile.seek(0)

        reader = csv.DictReader(csvfile, dialect=dialect)

        writer = csv.DictWriter(outfile, fieldnames=reader.fieldnames, dialect=dialect)
        writer.writeheader()

        rows = []

        row: dict
        for row in reader:
            row['STUDY_PROGRAMME'] = "Bachelorstudium - Informatik"
            row['CODE_OF_STUDY_PROGRAMME'] = "1234 56 789"
            row['Studienplan_Version'] = "2018"
            row['SPO_KONTEXT'] = "[2018] Informatik > Fach"

            # replace REGISTRATION_NUMBER
            if row['REGISTRATION_NUMBER'] in reg_id_map:
                row['REGISTRATION_NUMBER'] = reg_id_map[row['REGISTRATION_NUMBER']]
            else:
                reg_id_map[row['REGISTRATION_NUMBER']] = new_reg_ids[new_reg_id_idx]
                row['REGISTRATION_NUMBER'] = new_reg_ids[new_reg_id_idx]
                new_reg_id_idx += 1

            row['FAMILY_NAME_OF_STUDENT'] = "Last name"
            row['FIRST_NAME_OF_STUDENT'] = "First name"
            row['GESCHLECHT'] = "X"
            row['DATE_OF_ASSESSMENT'] = "01.01.1971"

            # Don't replace GUEL_U_AKTUELLE_ANTRITTE_SPO!

            row['GRADE'] = ""
            row['REMARK'] = ""
            row['Number_Of_The_Course'] = "12AB34567"
            row['SEMESTER_OF_The_COURSE'] = "71W"
            row['COURSE_TITLE'] = "Kurs"
            row['Examiner'] = "Mustermann Max"
            row['Start_Time'] = "09:00"
            row['TERMIN_ORT'] = "ZuseLab C1 (2315|107)"
            row['DB_Primary_Key_Of_Exam'] = "1234567"
            row['DB_Primary_Key_Of_Candidate'] = "9876543"
            row['COURSE_GROUP_NAME'] = ""
            row['FILE_REMARK'] = ""
            row['EMAIL_ADDRESS'] = "some-name@rwth-aachen.de"
            row['ECTS_GRADE'] = ""

            rows.append(row)

        random.shuffle(rows)

        writer.writerows(rows)


def process_bonus(path):
    global new_reg_id_idx

    # Find out delimiter
    with open(path, newline='', encoding=guess_encoding(path)) as csvfile, \
            open(path[:-4] + "_pseudo.csv", 'w', newline='', encoding='utf-8') as outfile:
        dialect = csv.Sniffer().sniff(csvfile.read(65536))
        csvfile.seek(0)

        reader = csv.DictReader(csvfile, dialect=dialect)

        writer = csv.DictWriter(outfile, fieldnames=reader.fieldnames, dialect=dialect)
        writer.writeheader()

        rows = []

        row: dict
        for row in reader:
            row['Vorname'] = "First name"
            row['Nachname'] = "Last name"

            # replace Matrikelnummer
            if row['Matrikelnummer'] in reg_id_map:
                row['Matrikelnummer'] = reg_id_map[row['Matrikelnummer']]
            else:
                reg_id_map[row['Matrikelnummer']] = new_reg_ids[new_reg_id_idx]
                row['Matrikelnummer'] = new_reg_ids[new_reg_id_idx]
                new_reg_id_idx += 1

            row['LMS-ID'] = 'abc_123defgh'

            # Randomly change Bonuspunkte, but don't go above 20% of total points
            # For simplicity, only ever decrease them
            nr_bonus_points = 0
            if row['Bonuspunkte (Punkte)'] != '-':
                nr_bonus_points = float(row['Bonuspunkte (Punkte)'].replace(',', '.'))
            if nr_bonus_points >= 1 and bool(random.getrandbits(1)):
                row['Bonuspunkte (Punkte)'] = nr_bonus_points - 1

            row['Zuletzt aus diesem Kurs geladen'] = '1234567890'

            rows.append(row)

        random.shuffle(rows)

        writer.writerows(rows)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python pseudonymize.py <filename without extension>")
        sys.exit(1)

    dynexite_path = "dynexite/" + sys.argv[1] + ".csv"
    rwth_path = "rwthonline/" + sys.argv[1] + ".csv"
    bonus_path = "bonus/" + sys.argv[1] + ".csv"

    # Check that files exist
    if not os.path.isfile(dynexite_path):
        print(dynexite_path + " does not exist!")
        sys.exit(1)
    if not os.path.isfile(rwth_path):
        print(rwth_path + " does not exist!")
        sys.exit(1)

    process_dynexite(dynexite_path)
    process_rwth(rwth_path)
    # It's ok if there is no bonus point file
    if os.path.isfile(bonus_path):
        process_bonus(bonus_path)
    else:
        print("No bonus point file found.")

    print(f"File '{sys.argv[1]}' has been processed successfully!")
