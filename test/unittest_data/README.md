### Input files for unit tests

Store in the subfolders of this directory dynexite-, RWTHonline- and bonus-point-csv-files, that the unit tests should use as input.

For each triple of dynexite-, RWTHonline- and bonus-point-files, use the same file name in the three directories, e.g.:

```
├── README.md        << you are here
├── pseudonymize.py  << helper script to pseudonymize exam files
├── dynexite
│   ├── .gitkeep
│   └── split.csv    << dynexite exam file
├── rwthonline
│   ├── .gitkeep
│   └── split.csv    << corresponding RWTHonline students file
└── bonus
    ├── .gitkeep
    └── split.csv    << corresponding bonus point file (optional)
```
