#include "util/numeric.hpp"

#include "util/print_types.hpp"

#include <gtest/gtest.h>

#include <algorithm>
#include <cmath>
#include <limits>
#include <numbers>
#include <numeric>
#include <random>
#include <vector>

#define MY_NAN std::numeric_limits<double>::quiet_NaN()
#define MY_INF std::numeric_limits<double>::infinity()

TEST(NumericUtilTest, DefinitelyGreaterThan) {
    // a is greater than b
    EXPECT_TRUE(definitelyGreaterThan(5.0, 2.0));

    // a is equal to b
    EXPECT_FALSE(definitelyGreaterThan(3.14, 3.14));

    // a is less than b
    EXPECT_FALSE(definitelyGreaterThan(-10.0, 20.0));

    // a is very close to b, but not equal
    EXPECT_FALSE(definitelyGreaterThan(0.1 + 0.2, 0.3));
    EXPECT_FALSE(definitelyGreaterThan(0.3, std::nextafter(0.3, 1.)));

    // a is reasonably larger than b
    EXPECT_TRUE(definitelyGreaterThan(0.1001 + 0.2, 0.3));

    // a is very close to b, but not equal
    EXPECT_FALSE(definitelyGreaterThan(0.3, 0.1 + 0.2));
    EXPECT_FALSE(definitelyGreaterThan(std::nextafter(0.3, 1.), 0.3));

    // b is reasonably larger than a
    EXPECT_FALSE(definitelyGreaterThan(0.3, 0.1001 + 0.2));

    // Checks around 0
    EXPECT_FALSE(definitelyGreaterThan(0., 0.));
    EXPECT_FALSE(definitelyGreaterThan(0., std::nextafter(0., 1.)));
    EXPECT_FALSE(definitelyGreaterThan(std::nextafter(0., 1.), 0.));
    EXPECT_FALSE(definitelyGreaterThan(0., 0.0001));
    EXPECT_TRUE(definitelyGreaterThan(0.0001, 0.));

    // NaN
    EXPECT_FALSE(definitelyGreaterThan(MY_NAN, 10.0));
    EXPECT_FALSE(definitelyGreaterThan(5.0, MY_NAN));
    EXPECT_FALSE(definitelyGreaterThan(MY_NAN, MY_NAN));

    // Infinity
    EXPECT_TRUE(definitelyGreaterThan(MY_INF, 10.0));
    EXPECT_FALSE(definitelyGreaterThan(10.0, MY_INF));
    EXPECT_FALSE(definitelyGreaterThan(-MY_INF, 10.0));
    EXPECT_TRUE(definitelyGreaterThan(10.0, -MY_INF));
    EXPECT_TRUE(definitelyGreaterThan(MY_INF, -MY_INF));
    EXPECT_FALSE(definitelyGreaterThan(-MY_INF, MY_INF));
    EXPECT_FALSE(definitelyGreaterThan(MY_INF, MY_INF));
    EXPECT_FALSE(definitelyGreaterThan(-MY_INF, -MY_INF));
}

TEST(NumericUtilTest, DefinitelyLessThan) {
    // a is greater than b
    EXPECT_FALSE(definitelyLessThan(5.0, 2.0));

    // a is equal to b
    EXPECT_FALSE(definitelyLessThan(3.14, 3.14));

    // a is less than b
    EXPECT_TRUE(definitelyLessThan(-10.0, 20.0));

    // a is very close to b, but not equal
    EXPECT_FALSE(definitelyLessThan(0.1 + 0.2, 0.3));
    EXPECT_FALSE(definitelyLessThan(0.3, std::nextafter(0.3, 1.)));

    // a is reasonably less than b
    EXPECT_TRUE(definitelyLessThan(0.0999 + 0.2, 0.3));

    // a is very close to b, but not equal
    EXPECT_FALSE(definitelyLessThan(0.3, 0.1 + 0.2));
    EXPECT_FALSE(definitelyLessThan(std::nextafter(0.3, 1.), 0.3));

    // b is reasonably larger than a
    EXPECT_FALSE(definitelyLessThan(0.3, 0.0999 + 0.2));

    // Checks around 0
    EXPECT_FALSE(definitelyLessThan(0., 0.));
    EXPECT_FALSE(definitelyLessThan(0., std::nextafter(0., 1.)));
    EXPECT_FALSE(definitelyLessThan(std::nextafter(0., 1.), 0.));
    EXPECT_TRUE(definitelyLessThan(0., 0.0001));
    EXPECT_FALSE(definitelyLessThan(0.0001, 0.));

    // NaN
    EXPECT_FALSE(definitelyLessThan(MY_NAN, 10.0));
    EXPECT_FALSE(definitelyLessThan(5.0, MY_NAN));
    EXPECT_FALSE(definitelyLessThan(MY_NAN, MY_NAN));

    // Infinity
    EXPECT_FALSE(definitelyLessThan(MY_INF, 10.0));
    EXPECT_TRUE(definitelyLessThan(10.0, MY_INF));
    EXPECT_TRUE(definitelyLessThan(-MY_INF, 10.0));
    EXPECT_FALSE(definitelyLessThan(10.0, -MY_INF));
    EXPECT_FALSE(definitelyLessThan(MY_INF, -MY_INF));
    EXPECT_TRUE(definitelyLessThan(-MY_INF, MY_INF));
    EXPECT_FALSE(definitelyLessThan(MY_INF, MY_INF));
    EXPECT_FALSE(definitelyLessThan(-MY_INF, -MY_INF));
}

TEST(NumericUtilTest, KahanSum) {
    std::default_random_engine e(std::random_device{}());
    static constexpr auto eps = std::numeric_limits<double>::epsilon();

    // Empty range
    {
        std::vector<double> emptyRange;
        EXPECT_EQ(0.0, kahanSum(emptyRange.begin(), emptyRange.end()));
    }

    // Single element range
    {
        std::vector singleElementRange = {0.5};
        EXPECT_EQ(0.5, kahanSum(singleElementRange.begin(),
                                singleElementRange.end()));
    }

    // Range with positive numbers
    {
        std::vector positiveRange = {0., 0.1, 0.2, 0.3, 0.4, 0.5};
        EXPECT_DOUBLE_EQ(1.5,
                         kahanSum(positiveRange.begin(), positiveRange.end()));
    }

    // Range with negative numbers
    {
        std::vector negativeRange = {-1.0, -2.0, -3.0, -4.0, -5.0};
        EXPECT_DOUBLE_EQ(-15.0,
                         kahanSum(negativeRange.begin(), negativeRange.end()));
    }

    // Range with mixed positive and negative numbers
    {
        std::vector mixedRange = {-1.0, 2.0, -3.0, 0.4, -5.0};
        EXPECT_DOUBLE_EQ(-6.6, kahanSum(mixedRange.begin(), mixedRange.end()));
    }

    // Range with NaN
    {
        std::vector nanRange = {0.1, MY_NAN, 0.1, 0.1, 0.1};
        EXPECT_TRUE(std::isnan(kahanSum(nanRange.begin(), nanRange.end())));
    }

    // Range with Infinity
    // A better result would be that the ranges sum up to infinity/negative
    // infinity, but this seems to costly to guarantee, so instead they both sum
    // up to NaN.
    {
        std::vector infinityRange = {0.1, MY_INF, 0.1, 0.1, 0.1};
        // EXPECT_TRUE(
        //     std::isinf(kahanSum(infinityRange.begin(),
        //     infinityRange.end())));
        EXPECT_TRUE(
            std::isnan(kahanSum(infinityRange.begin(), infinityRange.end())));

        std::vector negInfinityRange = {0.1, -MY_INF, 0.1, 0.1, 0.1};
        // EXPECT_TRUE(std::isinf(
        //     kahanSum(negInfinityRange.begin(), negInfinityRange.end())));
        EXPECT_TRUE(std::isnan(
            kahanSum(negInfinityRange.begin(), negInfinityRange.end())));
    }

    // Sup up large range of identical numbers
    {
        for (int i = 0; i < 17; ++i) {

            // Get range sizes in a wide range of different magnitudes
            int nrMultiples;
            {
                const auto powRange = 7 + i;
                std::uniform_int_distribution nrMultiplesD(2 << powRange,
                                                           2 << (powRange + 1));
                nrMultiples = nrMultiplesD(e);
            }

            std::vector<double> testValues;
            for (int j = -1; j <= 11; ++j) {
                testValues.push_back(0.1 * j);
            }
            testValues.push_back(std::numbers::pi_v<double>);
            testValues.push_back(std::numbers::e_v<double>);
            testValues.push_back(10000 * std::numbers::phi_v<double>);

            for (const auto d : testValues) {

                std::vector largeRange(static_cast<size_t>(nrMultiples), d);

                const auto actualResult = nrMultiples * d;
                const auto kahanResult =
                    kahanSum(largeRange.begin(), largeRange.end());

                // Relative error of kahanSum is up to 2*eps.
                const auto maxAbsDiff = std::abs(2 * actualResult * eps);

                EXPECT_NEAR(actualResult, kahanResult, maxAbsDiff);
            }
        }
    }

    // Range with random numbers: similar result than accumulate (but hopefully
    // more precise)
    {
        std::uniform_real_distribution d(0.0, 10.0);

        auto gen = [&d, &e] { return d(e); };

        static constexpr size_t n = 5000;

        for (int i = 0; i < 1000; ++i) {
            std::vector<double> largeRange(5000);
            std::ranges::generate(largeRange, gen);
            const auto accumulateSum =
                std::accumulate(largeRange.begin(), largeRange.end(), 0.0);
            const auto kahanResult =
                kahanSum(largeRange.begin(), largeRange.end());

            // Require less precision here, because accumulate isn't precise.
            // Relative error of accumulateSum is up to n*eps,
            // Relative error of kahanSum is up to 2*eps.
            const auto maxResult = std::max(accumulateSum, kahanResult);
            const auto maxAbsDiff = (n + 2) * maxResult * eps;

            EXPECT_NEAR(accumulateSum, kahanResult, maxAbsDiff);
        }
    }
}
