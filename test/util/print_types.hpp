#pragma once

#include <QString>

#include <ostream>

struct Points;
struct GradeBoundary;
struct DAG;

/**
 * \brief Write grade boundary to stream
 */
std::ostream &operator<<(std::ostream &os, const GradeBoundary &obj);

/**
 * \brief Write points to stream
 */
std::ostream &operator<<(std::ostream &os, const Points &obj);

/**
 * \brief Write DAG to stream
 */
std::ostream &operator<<(std::ostream &os, const DAG &obj);

/**
 * \brief Write QString to stream
 */
std::ostream &operator<<(std::ostream &os, const QString &obj);
