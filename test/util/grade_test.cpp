#include "util/grade.hpp"

#include "data/testable_objects.hpp"
#include "util/print_types.hpp"

#include <gtest/gtest.h>

#include <random>

TEST(GradeTest, gradeValues) {

    using enum Grade;

    EXPECT_EQ(static_cast<int>(grade1_0), 0);
    EXPECT_EQ(static_cast<int>(grade1_3), 1);
    EXPECT_EQ(static_cast<int>(grade1_7), 2);
    EXPECT_EQ(static_cast<int>(grade2_0), 3);
    EXPECT_EQ(static_cast<int>(grade2_3), 4);
    EXPECT_EQ(static_cast<int>(grade2_7), 5);
    EXPECT_EQ(static_cast<int>(grade3_0), 6);
    EXPECT_EQ(static_cast<int>(grade3_3), 7);
    EXPECT_EQ(static_cast<int>(grade3_7), 8);
    EXPECT_EQ(static_cast<int>(grade4_0), 9);
    EXPECT_EQ(static_cast<int>(grade5_0), 10);
    EXPECT_EQ(static_cast<int>(NR_VALS), 11);
}

TEST(GradeTest, previousNextGrade) {

    using enum Grade;

    auto g1 = grade1_0, g2 = grade1_3;

    for (; g2 <= grade5_0; g1 = nextGrade(g1), g2 = nextGrade(g2)) {
        EXPECT_EQ(g2, nextGrade(g1));
        EXPECT_EQ(g1, previousGrade(g2));
    }
}

TEST(GradeTest, gradeToNumber) {

    using enum Grade;

    for (auto g = grade1_0; g <= grade5_0; g = nextGrade(g)) {
        EXPECT_EQ(gradeToInt(g), gradeToDouble(g) * 10.);
        EXPECT_EQ(
            gradeToInt(g),
            gradeIntValues[static_cast<decltype(gradeIntValues)::size_type>(
                g)]);
        EXPECT_EQ(
            gradeToDouble(g),
            gradeDoubles[static_cast<decltype(gradeDoubles)::size_type>(g)]);
        EXPECT_EQ(gradeToInt(g), gradeToDouble(g) * 10.);
        const auto gStringDouble = toString(g).toDouble();
        const auto gDouble = gradeToDouble(g);
        EXPECT_EQ(gStringDouble, gDouble);
    }
}

TEST(GradeTest, gradeBoundaryUserChangeable) {

    using enum Grade;

    for (auto g = grade1_0; g <= grade5_0; g = nextGrade(g)) {
        if (g >= grade5_0) {
            EXPECT_FALSE(gradeBoundaryIsUserChangeable(GradingType::Open, g));
        } else {
            EXPECT_TRUE(gradeBoundaryIsUserChangeable(GradingType::Open, g));
        }
        if (g >= grade5_0 || g == grade1_3 || g == grade2_3 || g == grade3_3) {
            EXPECT_FALSE(gradeBoundaryIsUserChangeable(GradingType::Closed, g));
        } else {
            EXPECT_TRUE(gradeBoundaryIsUserChangeable(GradingType::Closed, g));
        }
    }
}

// the average of two same grades is always that grade
TEST(GradeTest, gradeAverageSameGrade) {
    using enum Grade;

    static constexpr auto completeUntil = 400;
    static constexpr auto nrRandomIts = 1000;

    for (auto i = 0; i < completeUntil; ++i) {
        for (auto j = 0; j < completeUntil; ++j) {
            for (const auto &g : grades) {

                ASSERT_EQ(getGradeAverage(i, g, j, g, false), g);
                ASSERT_EQ(getGradeAverage(i, g, j, g, true), g);
            }
        }
    }

    std::default_random_engine e(std::random_device{}());
    std::uniform_int_distribution d(completeUntil, 10000);

    for (auto i = 0; i < nrRandomIts; ++i) {
        const auto closedMax = d(e);
        const auto openMax = d(e);
        for (const auto &g : grades) {

            ASSERT_EQ(getGradeAverage(closedMax, g, openMax, g, false), g);
            ASSERT_EQ(getGradeAverage(closedMax, g, openMax, g, true), g);
        }
    }
}

// the average of two grades is always between them (inclusive)
TEST(GradeTest, gradeAverageBetweenGrades) {
    using enum Grade;

    static constexpr auto completeUntil = 300;
    static constexpr auto nrRandomIts = 1000;

    for (auto i = 0; i < completeUntil; ++i) {
        for (auto j = 0; j < completeUntil; ++j) {
            for (auto g1 = grade1_0; g1 <= grade5_0; g1 = nextGrade(g1)) {
                for (auto g2 = nextGrade(g1); g2 <= grade5_0;
                     g2 = nextGrade(g2)) {

                    for (const auto requirePassAll : {true, false}) {
                        const auto gAvg =
                            getGradeAverage(i, g1, j, g2, requirePassAll);
                        ASSERT_GE(gAvg, g1);
                        ASSERT_LE(gAvg, g2);
                    }
                }
            }
        }
    }

    std::default_random_engine e(std::random_device{}());
    std::uniform_int_distribution d(completeUntil, 10000);

    for (auto i = 0; i < nrRandomIts; ++i) {
        const auto closedMax = d(e);
        const auto openMax = d(e);
        for (auto g1 = grade1_0; g1 <= grade5_0; g1 = nextGrade(g1)) {
            for (auto g2 = nextGrade(g1); g2 <= grade5_0; g2 = nextGrade(g2)) {
                for (const auto requirePassAll : {true, false}) {
                    const auto gAvg = getGradeAverage(closedMax, g1, openMax,
                                                      g2, requirePassAll);
                    ASSERT_GE(gAvg, g1);
                    ASSERT_LE(gAvg, g2);
                }
            }
        }
    }
}

namespace {
template <std::floating_point Float>
bool definitelyLessThan_test(Float a, Float b) {
    const auto epsilon = std::numeric_limits<Float>::epsilon() * 2 *
                         std::max(std::abs(a), std::abs(b));
    return (b - a) >
           ((std::abs(a) < std::abs(b) ? std::abs(b) : std::abs(a)) * epsilon);
}

template <std::floating_point Float>
constexpr Grade roundToNextBestEps(const Float grade) noexcept {
    if (definitelyLessThan_test(grade, Float(1.3L)))
        return Grade::grade1_0;
    if (definitelyLessThan_test(grade, Float(1.7L)))
        return Grade::grade1_3;
    if (definitelyLessThan_test(grade, Float(2.0L)))
        return Grade::grade1_7;
    if (definitelyLessThan_test(grade, Float(2.3L)))
        return Grade::grade2_0;
    if (definitelyLessThan_test(grade, Float(2.7L)))
        return Grade::grade2_3;
    if (definitelyLessThan_test(grade, Float(3.0L)))
        return Grade::grade2_7;
    if (definitelyLessThan_test(grade, Float(3.3L)))
        return Grade::grade3_0;
    if (definitelyLessThan_test(grade, Float(3.7L)))
        return Grade::grade3_3;
    if (definitelyLessThan_test(grade, Float(4.0L)))
        return Grade::grade3_7;
    // Guidelines require that 4.09 still passes
    if (definitelyLessThan_test(grade, Float(4.1L)))
        return Grade::grade4_0;
    return Grade::grade5_0;
}

[[nodiscard]] constexpr Grade
getGradeAverageEps(const int closedMax, const Grade closedGrade,
                   const int openMax, const Grade openGrade) noexcept {

    const auto combinedMax = static_cast<double>(closedMax) + openMax;

    if (combinedMax == 0.) {
        return Grade::grade1_0;
    }

    const auto closedWeight = closedMax / combinedMax;
    const auto openWeight = openMax / combinedMax;

    const auto preciseResult = gradeToDouble(closedGrade) * closedWeight +
                               gradeToDouble(openGrade) * openWeight;

    return roundToNextBestEps(preciseResult);
}
} // namespace

// The implementation in grade.cpp (using integers) returns the same result as
// the above calculation (using floating point and epsilon)
TEST(GradeTest, gradeAverageSameAsAlternativeCalculation) {
    static constexpr auto randMax = 500;
    static constexpr auto nrRandomIts = 2000;
    const std::vector<Grade> gradesVector{grades.begin(), grades.end()};
    const std::vector<Grade> onlyGrade1_0{Grade::grade1_0};

    std::default_random_engine e(std::random_device{}());
    std::uniform_int_distribution u(0, randMax);

    for (int itCount = 0; itCount < nrRandomIts; ++itCount) {
        const auto closedMax = u(e);
        const auto openMax = u(e);

        // If the maximum number of points is 0, the grade is always 1.0
        for (const auto gOpen : openMax == 0 ? onlyGrade1_0 : gradesVector) {
            for (const auto gClosed :
                 closedMax == 0 ? onlyGrade1_0 : gradesVector) {
                SCOPED_TRACE("closedMax: " + std::to_string(closedMax) +
                             ", openMax: " + std::to_string(openMax) +
                             ", gOpen: " + toString(gOpen).toStdString() +
                             ", gClosed: " + toString(gClosed).toStdString());
                const auto g1 =
                    getGradeAverageEps(closedMax, gClosed, openMax, gOpen);
                const auto g2 =
                    getGradeAverage(closedMax, gClosed, openMax, gOpen, false);
                ASSERT_EQ(g1, g2);
            }
        }
    }
}
