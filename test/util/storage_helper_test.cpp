#include "util/storage_helper.hpp"

#include "util/enums.hpp"
#include "util/print_types.hpp"

#include <gtest/gtest.h>

#include <array>
#include <tuple>
#include <type_traits>

// each death test causes all static global variables to be recalculated, which
// for us is quite expensive.
#define RUN_DEATH_TESTS 0

// non-aggregate testclass with noexcept-false-ctor
struct TestClassExcept {
    TestClassExcept() noexcept(false) {
        // ... do stuff ...
    }
};
// non-aggregate testclass with noexcept-false-ctor
struct TestClassNoexcept {
    TestClassNoexcept() noexcept {
        // ... do stuff ...
    }
};

TEST(StorageHelperTest, typeTraits) {
    EXPECT_FALSE(std::is_nothrow_constructible_v<TestClassExcept>);
    EXPECT_TRUE(std::is_nothrow_constructible_v<TestClassNoexcept>);

    EXPECT_FALSE(std::is_aggregate_v<TestClassExcept>);
    EXPECT_FALSE(std::is_aggregate_v<TestClassNoexcept>);
    EXPECT_TRUE(std::is_aggregate_v<EnumStorage<TestClassExcept>>);

    EXPECT_FALSE(std::is_nothrow_constructible_v<EnumStorage<TestClassExcept>>);
    EXPECT_TRUE(
        std::is_nothrow_constructible_v<EnumStorage<TestClassNoexcept>>);
}

TEST(StorageHelperTest, iterator) {
    {
        EnumStorage<TestClassExcept> e;
        EXPECT_TRUE(e.begin() == std::begin(e));
        EXPECT_TRUE(e.end() == std::end(e));
        EXPECT_TRUE(e.cbegin() == std::cbegin(e));
        EXPECT_TRUE(e.cend() == std::cend(e));
    }
    {
        const EnumStorage<TestClassExcept> e;
        EXPECT_TRUE(e.cbegin() == std::cbegin(e));
        EXPECT_TRUE(e.cend() == std::cend(e));
    }
}

TEST(StorageHelperTest, swap) {
    const EnumStorage<int> e1Start{1, 2, 3}, e2Start{4, 5, 6};
    EnumStorage<int> e1 = e1Start, e2 = e2Start;

    std::swap(e1, e2);

    EXPECT_EQ(e1, e2Start);
    EXPECT_EQ(e2, e1Start);
}

TEST(StorageHelperTest, size) {
    const EnumStorage<TestClassExcept> e;
    EXPECT_EQ(e.size(), static_cast<std::size_t>(GradingType::NR_VALS));
}

#if RUN_DEATH_TESTS
TEST(StorageHelperDeathTest, access) {
#else
TEST(StorageHelperTest, access) {
#endif
    {
        EnumStorage<int> e{4, 5, 6};

        using enum GradingType;
        EXPECT_EQ(e[Closed], 4);
        EXPECT_EQ(e[Open], 5);
        EXPECT_EQ(e[Combined], 6);
#if RUN_DEATH_TESTS
        EXPECT_DEATH(std::ignore = e[static_cast<GradingType>(4)], "");
#endif
    }

    {
        const EnumStorage<int> e{4, 5, 6};

        using enum GradingType;
        EXPECT_EQ(e[Closed], 4);
        EXPECT_EQ(e[Open], 5);
        EXPECT_EQ(e[Combined], 6);
#if RUN_DEATH_TESTS
        EXPECT_DEATH(std::ignore = e[static_cast<GradingType>(4)], "");
#endif
    }
}
#if !defined(NDEBUG) && RUN_DEATH_TESTS
TEST(StorageHelperDeathTest, boolStorage) {
#else
TEST(StorageHelperTest, boolStorage) {
#endif

    using enum GradingType;

    {
        auto b1 = getBoolStorage<2>(Closed);
        auto b2 = getBoolStorage<2>(Open);

        EXPECT_TRUE(b1[Closed]);
        EXPECT_FALSE(b1[Open]);

        EXPECT_FALSE(b2[Closed]);
        EXPECT_TRUE(b2[Open]);

#if !defined(NDEBUG)
#if RUN_DEATH_TESTS
        EXPECT_DEATH(std::ignore = getBoolStorage<2>(Combined), "");
#endif
#else
        auto b3 = getBoolStorage<2>(Combined);

        EXPECT_FALSE(b3[Closed]);
        EXPECT_FALSE(b3[Open]);
#endif
    }
    {
        auto b1 = getBoolStorage<3>(Closed);
        auto b2 = getBoolStorage<3>(Open);
        auto b3 = getBoolStorage<3>(Combined);

        EXPECT_TRUE(b1[Closed]);
        EXPECT_FALSE(b1[Open]);
        EXPECT_FALSE(b1[Combined]);

        EXPECT_FALSE(b2[Closed]);
        EXPECT_TRUE(b2[Open]);
        EXPECT_FALSE(b2[Combined]);

        EXPECT_FALSE(b3[Closed]);
        EXPECT_FALSE(b3[Open]);
        EXPECT_TRUE(b3[Combined]);
    }
}
