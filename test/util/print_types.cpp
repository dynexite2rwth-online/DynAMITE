#include "print_types.hpp"

#include "data/graph.hpp"
#include "util/grade_boundary.hpp"
#include "util/points.hpp"

std::ostream &operator<<(std::ostream &os, const GradeBoundary &obj) {

    // We can't use toString, because we don't know the stepSize
    os << obj.mMultiple << 'm';

    return os;
}

std::ostream &operator<<(std::ostream &os, const Points &obj) {

    os << obj.p << 'p';

    return os;
}

std::ostream &operator<<(std::ostream &os, const DAG &obj) {

    os << "DAG(Title: " << obj.mStudents.mCourseTitles.join("; ").toStdString()
       << ", semester: " << obj.mStudents.mSemesters.join("; ").toStdString()
       << ")";

    return os;
}
std::ostream &operator<<(std::ostream &os, const QString &obj) {
    os << obj.toStdString();

    return os;
}
