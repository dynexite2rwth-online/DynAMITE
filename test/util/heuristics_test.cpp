#include "util/heuristics.hpp"

#include "data/graph.hpp"
#include "data/testable_objects.hpp"
#include "util/grade_boundary_step.hpp"
#include "util/print_types.hpp"

#include <gtest/gtest.h>

#include <QList>
#include <QtGlobal>

#include <random>
#include <ranges>

inline constexpr int nrIterations = 100;

using enum GradeBoundaryStepSize;

// Test guessGradeBoundaryStepSize on special ranges, e.g. empty ranges
TEST(HeuristicTest, guessGradeBoundarySpecialRanges) {

    auto res = impl::guessGradeBoundaryStepSize(QList<Points>{});
    EXPECT_EQ(res, Min1_00);
}

// No point value has a fractional part different from 0
TEST(HeuristicTest, guessGradeBoundary1_00) {
    std::default_random_engine e(std::random_device{}());

    std::uniform_int_distribution d(0u, 200u);
    for (int i = 0; i < nrIterations; ++i) {
        QList<Points> v;
        v.resize(d(e));
        for (auto &vecI : v) {
            vecI = Points{static_cast<double>(d(e))};
        }
        const auto res = impl::guessGradeBoundaryStepSize(v);
        EXPECT_EQ(res, Min1_00);
    }
}

// One point value has fractional part 0.50
TEST(HeuristicTest, guessGradeBoundary0_50) {
    std::default_random_engine e(std::random_device{}());
    std::uniform_int_distribution d(1u, 200u);
    for (int i = 0; i < nrIterations; ++i) {
        QList<Points> v;
        v.resize(d(e));
        for (auto &vecI : v) {
            vecI = Points{static_cast<double>(d(e))};
        }
        v.insert(v.cbegin() + v.size() / 2,
                 Points{static_cast<double>(d(e)) + 0.5});
        const auto res = impl::guessGradeBoundaryStepSize(v);
        EXPECT_EQ(res, Min0_50);
    }
}

// One point value has fractional part 0.25
TEST(HeuristicTest, guessGradeBoundary0_25) {
    std::default_random_engine e(std::random_device{}());
    std::uniform_int_distribution d(1u, 200u);
    for (int i = 0; i < nrIterations; ++i) {
        QList<Points> v;
        v.resize(d(e));
        for (auto &vecI : v) {
            vecI = Points{static_cast<double>(d(e))};
        }
        if (i % 2) {
            v.insert(v.cbegin() + v.size() / 2,
                     Points{static_cast<double>(d(e)) + 0.25});
        } else {
            v.insert(v.cbegin() + v.size() / 2,
                     Points{static_cast<double>(d(e)) + 0.75});
        }
        const auto res = impl::guessGradeBoundaryStepSize(v);
        EXPECT_EQ(res, Min0_25);
    }
}

// One point value is not a multiple of 0.25
TEST(HeuristicTest, guessGradeBoundaryOneNonStandard) {
    std::default_random_engine e(std::random_device{}());
    std::uniform_int_distribution d(1u, 200u);
    std::uniform_real_distribution dReal(0., 77.);
    for (int i = 0; i < nrIterations; ++i) {
        QList<Points> v;
        v.resize(d(e));
        for (auto &vecI : v) {
            vecI = Points{static_cast<double>(d(e))};
        }

        // insert a value that is not a multiple of 0.25
        const auto middleIt = v.cbegin() + v.size() / 2;
        double nonStandardValue = dReal(e);
        if (qFuzzyIsNull(std::fmod(nonStandardValue, 0.25))) {
            nonStandardValue += 0.1;
        }
        v.insert(middleIt, Points{nonStandardValue});

        const auto res = impl::guessGradeBoundaryStepSize(v);
        EXPECT_EQ(res, Min0_25);
    }
}

// All point values are not multiples of 0.25
TEST(HeuristicTest, guessGradeBoundaryAllNonStandard) {
    std::default_random_engine e(std::random_device{}());
    std::uniform_int_distribution d(1u, 200u);
    std::uniform_real_distribution dReal(0., 77.);
    for (int i = 0; i < nrIterations; ++i) {
        QList<Points> v;
        v.resize(d(e));
        for (auto &vecI : v) {
            vecI = Points{dReal(e)};
        }

        // make sure at least one value is not .0 / .5 / .25
        const auto middleIt = v.cbegin() + v.size() / 2;
        if (qFuzzyIsNull(std::fmod(middleIt->p, 0.25))) {
            v.insert(middleIt, Points{middleIt->p + 0.1});
        }

        const auto res = impl::guessGradeBoundaryStepSize(v);
        EXPECT_EQ(res, Min0_25);
    }
}

// Execute for each DAG
class HeuristicsDagTest
    : public testing::TestWithParam<std::pair<std::string, DAG>> {};

INSTANTIATE_TEST_SUITE_P(HeuristicsDagParameterizedTests, HeuristicsDagTest,
                         testing::ValuesIn(exams()));

// Check the results for our known test files
TEST_P(HeuristicsDagTest, CorrectlyCalculatedInDag) {
    const auto &name = GetParam().first;
    const auto &graph = GetParam().second;

    if (name == "split") {
        const auto res = guessGradeBoundaryStepSize(graph.mExam);
        EXPECT_EQ(res, Min1_00);
    } else if (name == "closed") {
        const auto res = guessGradeBoundaryStepSize(graph.mExam);
        EXPECT_EQ(res, Min0_25);
    } else if (name == "closed_bonus") {
        const auto res = guessGradeBoundaryStepSize(graph.mExam);
        EXPECT_EQ(res, Min0_50);
    } else {
        // FAIL() << "Input file with unknown name " << name;
    }
}

// niceYAxis produces sensible max and step size values
TEST(HeuristicTest, niceYAxisTest) {
    for (int maxBarHeight = 0; maxBarHeight < 1000; ++maxBarHeight) {
        for (int maxNrLabels = 0; maxNrLabels < 1000; ++maxNrLabels) {
            const auto [yMax, yStepSize] = niceYAxis(maxBarHeight, maxNrLabels);

            EXPECT_GT(yMax, 0);
            EXPECT_GT(yStepSize, 0);

            // max is labeled
            EXPECT_EQ(yMax % yStepSize, 0);

            const auto yNrLabels = yMax / yStepSize + 1;

            // Max bar height fits in the graph
            EXPECT_GE(yMax, maxBarHeight);

            // if space for at least two labels: number of labels fit into the
            // y axis
            if (maxNrLabels >= 2) {
                EXPECT_GE(maxNrLabels, yNrLabels);
            }

            // We use at least 2/5 of maxNrLabels (ignore if we have a label for
            // each whole number)
            if (yStepSize > 1) {
                EXPECT_LE(maxNrLabels * 0.4, yNrLabels);
            }
        }
    }
}
