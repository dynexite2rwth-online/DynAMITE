#include "data/nodes/student_data.hpp"

#include "data/graph.hpp"
#include "data/testable_objects.hpp"
#include "util/print_types.hpp"

#include <gtest/gtest.h>

#include <algorithm>
#include <ranges>

// use Value-Parameterized Tests to execute each test for pre-calculated each
// StudentData
class StudentDataTest
    : public testing::TestWithParam<std::pair<std::string, DAG>> {};

TEST(StudentDataTest, construction) {
    for (const auto &[name, csv] : csvs(FileType::rwth)) {

        StudentData s(*csv);

        EXPECT_EQ(s.mRegistrationNr.size(), s.mAttemptNr.size());
        EXPECT_GT(s.mRegistrationNr.size(), 0);
        EXPECT_GT(s.mAttemptNr.size(), 0);
    }
}

INSTANTIATE_TEST_SUITE_P(StudentDataParameterizedTests, StudentDataTest,
                         testing::ValuesIn(exams()));

TEST_P(StudentDataTest, uniqueRegNrs) {
    const auto &graph = GetParam().second;

    QList<QString> sortedRegNrs = graph.mStudents.mRegistrationNr;
    std::ranges::sort(sortedRegNrs);
    EXPECT_EQ(std::ranges::adjacent_find(sortedRegNrs),
              std::ranges::end(sortedRegNrs));
}

TEST_P(StudentDataTest, saneAttemptNrs) {
    const auto &graph = GetParam().second;

    for (auto attemptNr : graph.mStudents.mAttemptNr) {

        EXPECT_GE(attemptNr, 0);
        EXPECT_LE(attemptNr, 3);
    }
}

TEST(StudentDataTest, concreteValues) {
    for (const auto &[name, graph] : exams()) {
        if (name == "split") {
            EXPECT_EQ(graph.mStudents.mRegistrationNr[0], "300005");
            EXPECT_EQ(graph.mStudents.mRegistrationNr[6], "300020");
            EXPECT_EQ(graph.mStudents.mAttemptNr[14], 0);
            EXPECT_EQ(graph.mStudents.mAttemptNr[22], 1);
            EXPECT_EQ(graph.mStudents.mAttemptNr[19], 2);

            return;
        }
    }

    ADD_FAILURE() << "Split test data not found";
}
