#include "data/nodes/user_point_config.hpp"

#include "data/graph.hpp"
#include "data/testable_objects.hpp"
#include "util/defaults.hpp"
#include "util/heuristics.hpp"
#include "util/print_types.hpp"

#include <gtest/gtest.h>

TEST(UserPointConfigTest, constructor) {
    const UserPointConfig uc;

    EXPECT_EQ(uc.mCountZerothAttemptAsFirstAttempt,
              defaults::countZerothAttemptAsFirstAttempt);
    EXPECT_EQ(uc.mGradeBoundaryRounding, defaults::boundaryRounding);
    EXPECT_EQ(uc.mGradeBoundaryStepSize, defaults::boundaryStepSize);
}

TEST(UserPointConfigTest, inDag) {

    const auto &myExams = exams();

    for (const auto &[name, g] : myExams) {
        EXPECT_EQ(g.mUserPointConfig.mCountZerothAttemptAsFirstAttempt,
                  defaults::countZerothAttemptAsFirstAttempt);
        EXPECT_EQ(g.mUserPointConfig.mGradeBoundaryRounding,
                  defaults::boundaryRounding);
        EXPECT_EQ(g.mUserPointConfig.mGradeBoundaryStepSize,
                  guessGradeBoundaryStepSize(g.mExam));
    }
}
