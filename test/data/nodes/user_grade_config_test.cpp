#include "data/nodes/user_grade_config.hpp"

#include "data/graph.hpp"
#include "data/testable_objects.hpp"
#include "util/print_types.hpp"

#include <gtest/gtest.h>

TEST(UserGradeConfigTest, constructor) {
    const UserGradeConfig uc;

    EXPECT_EQ(uc.mRequirePassAllParts, defaults::requirePassAllParts);
    EXPECT_EQ(uc.mPassDueToBonus, defaults::passDueToBonus);
}

TEST(UserGradeConfigTest, inDag) {

    for (const auto &[name, graph] : exams()) {

        EXPECT_EQ(graph.mUserGradeConfig.mRequirePassAllParts,
                  defaults::requirePassAllParts);
        EXPECT_EQ(graph.mUserGradeConfig.mPassDueToBonus,
                  defaults::passDueToBonus);
    }
}
