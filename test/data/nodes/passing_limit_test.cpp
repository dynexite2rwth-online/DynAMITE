#include "data/nodes/passing_limit.hpp"

#include "util/print_types.hpp"

#include <gtest/gtest.h>

TEST(PassingLimitsTest, passingLimit) {

    using enum GradeBoundaryStepSize;

    const GradeBoundaryFactory f(Min1_00, GradeBoundaryRounding::Up);

    // for some reason MSVC gives internal compiler errors if we call these
    // functions in the gtest-macro.

    // Guidelines appendix 1 example 1
    {
        const auto ex1Abs =
            impl::calculateAbsolutePassingLimit(f(Points{90.}), Min1_00);
        const auto ex1Rel = impl::calculateRelativePassingLimit(
            f(Points{90.}), Min1_00, Points{67.});
        const auto ex1Res = impl::calculatePassingLimit(ex1Abs, ex1Rel);

        EXPECT_EQ(ex1Abs.p, 54.);
        ASSERT_TRUE(ex1Rel.has_value());
        EXPECT_NEAR(ex1Rel->p, 52.26, 0.005);
        EXPECT_NEAR(ex1Res.p, 52.26, 0.005);
    }

    // Guidelines appendix 1 example 2
    {
        const auto ex2Abs =
            impl::calculateAbsolutePassingLimit(f(Points{120.}), Min1_00);
        const auto ex2Rel = impl::calculateRelativePassingLimit(
            f(Points{120.}), Min1_00, Points{73.});
        const auto ex2Res = impl::calculatePassingLimit(ex2Abs, ex2Rel);

        EXPECT_EQ(ex2Abs.p, 72.);
        ASSERT_TRUE(ex2Rel.has_value());
        EXPECT_EQ(ex2Rel->p, 60.);
        EXPECT_EQ(ex2Res.p, 60.);
    }

    // Guidelines appendix 1 example 3
    {
        const auto ex3Abs =
            impl::calculateAbsolutePassingLimit(f(Points{40.}), Min1_00);
        const auto ex3Rel = impl::calculateRelativePassingLimit(
            f(Points{40.}), Min1_00, Points{33.});
        const auto ex3Res = impl::calculatePassingLimit(ex3Abs, ex3Rel);

        EXPECT_EQ(ex3Abs.p, 24.);
        ASSERT_TRUE(ex3Rel.has_value());
        EXPECT_NEAR(ex3Rel->p, 25.74, 0.005);
        EXPECT_EQ(ex3Res.p, 24.);
    }

    // empty list of first try points
    {
        const auto abs4 =
            impl::calculateAbsolutePassingLimit(f(Points{43.}), Min1_00);
        const auto rel4 =
            impl::calculateRelativePassingLimit(f(Points{43.}), Min1_00, {});
        const auto res4 = impl::calculatePassingLimit(abs4, rel4);

        ASSERT_FALSE(rel4.has_value());
        EXPECT_EQ(res4, abs4);
    }
}
