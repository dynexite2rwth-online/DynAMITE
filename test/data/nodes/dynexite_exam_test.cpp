#include "data/nodes/dynexite_exam.hpp"

#include "data/graph.hpp"
#include "data/nodes/grade_list.hpp"
#include "data/nodes/student_data.hpp"
#include "data/nodes/user_grade_boundaries.hpp"
#include "data/testable_objects.hpp"
#include "file_io/csv_parser.hpp"
#include "file_io/file.hpp"
#include "file_io/helper.hpp"
#include "util/heuristics.hpp"
#include "util/print_types.hpp"

#include <gtest/gtest.h>

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <filesystem>
#include <ranges>
#include <utility>

using std::filesystem::directory_iterator;

// use Value-Parameterized Tests to execute each test for each DAG
class ConfiguredExamTest
    : public testing::TestWithParam<std::pair<std::string, DAG>> {};

// The other tests rely on ConfiguredExams that are constructed outside of a
// test case, therefore we test the constructor separately here again
TEST(ConfiguredExamTest, createConfiguredExam) {
    for (const auto &dynexiteEntry : directory_iterator(
             std::filesystem::path(TESTING_DYNEXITE_DIRECTORY))) {

        SCOPED_TRACE("Reading in file " + dynexiteEntry.path().string());

        // expect only .csv and .gitignore files in the repository
        if (dynexiteEntry.path().extension() == "") {
            continue;
        }
        if (dynexiteEntry.path().extension() != ".csv") {
            ADD_FAILURE() << "Unknown file extension: "
                          << dynexiteEntry.path().extension();
            continue;
        }

        // Find corresponding rwthonline-file
        const auto rwthonlinePath =
            std::filesystem::path(TESTING_RWTHONLINE_DIRECTORY) /
            dynexiteEntry.path().filename();
        if (!std::filesystem::exists(rwthonlinePath)) {
            ADD_FAILURE() << "Found no corresponding RWTHOnline-path for "
                          << dynexiteEntry.path().filename();
            continue;
        }

        // Find corresponding bonus file
        const auto bonusPath = std::filesystem::path(TESTING_BONUS_DIRECTORY) /
                               dynexiteEntry.path().filename();
        const auto bonusFileExists = std::filesystem::exists(bonusPath);

        QSharedPointer<CsvDump> dynexiteDump;
        QSharedPointer<CsvDump> rwthDump;
        QSharedPointer<CsvDump> bonusDump;
        ASSERT_NO_FATAL_FAILURE(dynexiteDump = getDump(dynexiteEntry.path()));
        ASSERT_NO_FATAL_FAILURE(rwthDump = getDump(rwthonlinePath));
        if (bonusFileExists) {
            ASSERT_NO_FATAL_FAILURE(bonusDump = getDump(bonusPath));
        } else {
            bonusDump = QSharedPointer<CsvDump>::create();
        }

        try {
            DynexiteExam dynexite(*dynexiteDump);
            StudentData students(*rwthDump);
            BonusPoints bonus(*bonusDump);
            UserGradeConfig gc;
            UserPointConfig pc;
            pc.mGradeBoundaryStepSize = guessGradeBoundaryStepSize(dynexite);
            auto g = DAG(dynexite, students, bonus, gc, pc);
        } catch (const std::exception &e) {
            ADD_FAILURE() << "Ctor of DAG threw: " << e.what();
        }
    }
}

INSTANTIATE_TEST_SUITE_P(ConfiguredExamParameterizedTests, ConfiguredExamTest,
                         testing::ValuesIn(exams()));

// Consistent Passing Limit: 0.5*maxClosedPoints <= limit <= 0.6*maxClosedPoints
TEST_P(ConfiguredExamTest, PassingLimitsInRange) {

    const auto &graph = GetParam().second;

    const auto stepSize = graph.mUserPointConfig.mGradeBoundaryStepSize;

    EXPECT_GE(
        toPoints(graph.getMaxPassingLimit(GradingType::Closed), stepSize).p,
        toPoints(graph.getOriginalMaxPointsBoundary(GradingType::Closed),
                 stepSize)
                .p *
            0.5);
    EXPECT_LE(
        toPoints(graph.getMaxPassingLimit(GradingType::Closed), stepSize).p,
        toPoints(graph.getOriginalMaxPointsBoundary(GradingType::Closed),
                 stepSize)
                .p *
            0.6);
}

// Passing Limit total points = exam total points
TEST_P(ConfiguredExamTest, PassingLimitsTotalPoints) {

    const auto &graph = GetParam().second;

    const auto stepSize = graph.mUserPointConfig.mGradeBoundaryStepSize;
    const auto rounding = graph.mUserPointConfig.mGradeBoundaryRounding;

    const auto maxClosedBoundary = GradeBoundary{
        graph.getMaxPoints(GradingType::Closed), stepSize, rounding};
    const auto maxOpenBoundary = GradeBoundary{
        graph.getMaxPoints(GradingType::Open), stepSize, rounding};

    EXPECT_EQ(graph.getOriginalMaxPointsBoundary(GradingType::Closed),
              maxClosedBoundary);
    EXPECT_EQ(graph.getOriginalMaxPointsBoundary(GradingType::Open),
              maxOpenBoundary);
}

// Consistent GradeBoundaries: Sorted, highest boundary less than max
// points, 4.0-boundary equal to passing limit
TEST_P(ConfiguredExamTest, GradeBoundariesConsistent) {

    const auto &graph = GetParam().second;

    for (const auto gradingType : {GradingType::Closed, GradingType::Open}) {
        EXPECT_TRUE(std::ranges::is_sorted(
            graph.getUserGradeBoundaries(gradingType), std::ranges::greater()));
        EXPECT_EQ(graph.getUserGradeBoundaries(gradingType)[Grade::grade4_0],
                  graph.getMaxPassingLimit(gradingType));
    }
    EXPECT_LE(
        graph.getUserGradeBoundaries(GradingType::Closed)[Grade::grade1_0],
        graph.getOriginalMaxPointsBoundary(GradingType::Closed));
    EXPECT_LE(graph.getUserGradeBoundaries(GradingType::Open)[Grade::grade1_0],
              graph.getOriginalMaxPointsBoundary(GradingType::Open));
}

// No invalid grade (Grade::NR_GRADES) in the grade list
TEST_P(ConfiguredExamTest, NoInvalidGrades) {
    const auto &graph = GetParam().second;

    for (const auto gradingType :
         {GradingType::Closed, GradingType::Open, GradingType::Combined}) {
        EXPECT_TRUE(std::ranges::find(graph.getGradeList(gradingType),
                                      Grade::NR_VALS) ==
                    std::ranges::end(graph.getGradeList(gradingType)));
    }
}

// More points == better grade
// This test takes into account that you have to get at least the minimum
// required points without bonus points to pass
TEST_P(ConfiguredExamTest, PointsGiveGrades) {
    const auto &graph = GetParam().second;

    for (const auto gradingType : {GradingType::Closed, GradingType::Open}) {
        for (int i = 0; i < graph.getGradeList(gradingType).size(); ++i) {
            for (int j = 0; j < graph.getGradeList(gradingType).size(); ++j) {
                if (graph.getPointsWithBonus(gradingType)[i] >
                        graph.getPointsWithBonus(gradingType)[j] &&
                    graph.getGradeList(gradingType)[i] != Grade::grade5_0) {
                    EXPECT_LE(graph.getGradeList(gradingType)[i],
                              graph.getGradeList(gradingType)[j]);
                } else if (graph.getGradeList(gradingType)[j] !=
                           Grade::grade5_0) {
                    EXPECT_GE(graph.getGradeList(gradingType)[i],
                              graph.getGradeList(gradingType)[j]);
                }
            }
        }
    }
}
