#include "data/nodes/first_try.hpp"

#include "data/graph.hpp"
#include "data/testable_objects.hpp"
#include "util/print_types.hpp"

#include <gtest/gtest.h>

// Execute for each DAG
class FirstTryDagTest
    : public testing::TestWithParam<std::pair<std::string, DAG>> {};

INSTANTIATE_TEST_SUITE_P(FirstTryDagParameterizedTests, FirstTryDagTest,
                         testing::ValuesIn(exams()));

// Check the results for our known test files
TEST_P(FirstTryDagTest, CorrectlyCalculatedInDag) {
    const auto &name = GetParam().first;
    const auto &graph = GetParam().second;

    if (name != "split" && name != "closed" && name != "closed_bonus") {
        FAIL() << "Input file with unknown name " << name;
    }
}
