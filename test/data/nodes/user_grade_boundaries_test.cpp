#include "data/nodes/user_grade_boundaries.hpp"

#include "data/data_manager.hpp"
#include "data/graph.hpp"
#include "data/testable_objects.hpp"
#include "util/print_types.hpp"

#include "gtest/gtest.h"

#include <QSignalSpy>

#include <ranges>
#include <vector>

class UserGradeBoundaryTest
    : public testing::TestWithParam<std::pair<std::string, DAG>> {};

INSTANTIATE_TEST_SUITE_P(UserGradeBoundaryParameterizedTests,
                         UserGradeBoundaryTest, testing::ValuesIn(exams()));

TEST_P(UserGradeBoundaryTest, boundariesBetweenMinAndMax) {
    const auto &graph = GetParam().second;

    for (const auto gradingType : {GradingType::Closed, GradingType::Open}) {
        for (const auto gPoints :
             graph.getUserGradeBoundaries(gradingType) |
                 std::views::take(static_cast<int>(Grade::grade5_0))) {
            EXPECT_GE(gPoints, GradeBoundary{0});
            if (gradingType == GradingType::Closed) {
                EXPECT_LE(gPoints, graph.getChangedMaxPointsBoundary(
                                       GradingType::Closed));
                EXPECT_LE(gPoints, graph.getChangedMaxPointsBoundary(
                                       GradingType::Closed));
            } else {
                EXPECT_LE(gPoints,
                          graph.getChangedMaxPointsBoundary(GradingType::Open));
                EXPECT_LE(gPoints,
                          graph.getChangedMaxPointsBoundary(GradingType::Open));
            }
        }
        EXPECT_EQ(graph.getUserGradeBoundaries(gradingType)[Grade::grade5_0],
                  GradeBoundary{0});
    }
}

TEST_P(UserGradeBoundaryTest, boundariesDescending) {
    const auto &graph = GetParam().second;

    for (const auto gradingType : {GradingType::Closed, GradingType::Open}) {
        EXPECT_TRUE(
            std::ranges::is_sorted_until(
                graph.getUserGradeBoundaries(gradingType),
                std::ranges::greater()) ==
            std::ranges::end(graph.getUserGradeBoundaries(gradingType)));
    }
}

TEST_P(UserGradeBoundaryTest, defaultFollowDefault) {
    const auto &graph = GetParam().second;

    EXPECT_TRUE(graph.getUserGradeBoundarySetAuto(GradingType::Closed));
    EXPECT_TRUE(graph.getUserGradeBoundarySetAuto(GradingType::Open));
}

TEST_P(UserGradeBoundaryTest, dataManagerInteraction) {
    // non-const copy
    auto graph = GetParam().second;

    for (const auto gradingType : {GradingType::Closed, GradingType::Open}) {

        auto &boundaries = graph.getUserGradeBoundaries(gradingType);
        const auto before = boundaries;

        const auto resetBoundaries = [dagPtr = &graph,
                                      grade4boundary = before[Grade::grade4_0]](
                                         const GradingType grading) {
            DataManager::boundaryChange(grading, Grade::grade4_0,
                                        grade4boundary, dagPtr);
            DataManager::autoBoundaryChange(grading, true, dagPtr);
        };

        {
            SCOPED_TRACE("STAGE 1: just changing followDefault doesn't change "
                         "the boundaries");

            DataManager::autoBoundaryChange(gradingType, true, &graph);
            EXPECT_EQ(before, boundaries);
            DataManager::autoBoundaryChange(gradingType, false, &graph);
            EXPECT_EQ(before, boundaries);
            DataManager::autoBoundaryChange(gradingType, true, &graph);
            EXPECT_EQ(before, boundaries);
        }

        // Select boundary change that would be valid
        GradeBoundary newBoundary{0};
        {
            using enum Grade;

            if (boundaries[grade2_7].mMultiple >
                boundaries[grade3_0].mMultiple + 1)
                newBoundary = GradeBoundary{(boundaries[grade2_7].mMultiple +
                                             boundaries[grade3_0].mMultiple) /
                                            2};
            else
                newBoundary = GradeBoundary{(boundaries[grade3_3].mMultiple +
                                             boundaries[grade3_0].mMultiple) /
                                            2};
        }
        const bool newBoundaryDiffers =
            newBoundary != boundaries[Grade::grade3_0];

        resetBoundaries(gradingType);

        {
            SCOPED_TRACE("STAGE 2: changing boundaries doesn't work when "
                         "mFollowsDefault is true");

            auto ret = DataManager::boundaryChange(gradingType, Grade::grade3_0,
                                                   newBoundary, &graph);

            EXPECT_FALSE(ret);
            EXPECT_EQ(before, boundaries);

            ret = DataManager::boundaryChange(gradingType, Grade::grade3_0,
                                              Points{-1.}, &graph);

            EXPECT_FALSE(ret);
            EXPECT_EQ(before, boundaries);

            ret = DataManager::boundaryChange(
                gradingType, Grade::grade3_0,
                Points{std::numeric_limits<double>::infinity()}, &graph);

            EXPECT_FALSE(ret);
            EXPECT_EQ(before, boundaries);

            ret = DataManager::boundaryChange(
                gradingType, Grade::grade3_0,
                Points{std::numeric_limits<double>::quiet_NaN()}, &graph);

            EXPECT_FALSE(ret);
            EXPECT_EQ(before, boundaries);
        }

        resetBoundaries(gradingType);

        {
            SCOPED_TRACE("STAGE 3: changing boundaries does work when "
                         "mFollowsDefault is false");

            DataManager::autoBoundaryChange(gradingType, false, &graph);

            const auto ret = DataManager::boundaryChange(
                gradingType, Grade::grade3_0, newBoundary, &graph);

            EXPECT_TRUE(ret);
            for (std::size_t i = 0; i < boundaries.size(); ++i) {
                auto g = static_cast<Grade>(i);
                if (g == Grade::grade3_0) {
                    if (newBoundaryDiffers) {
                        EXPECT_NE(before[g], boundaries[g]);
                    } else {
                        EXPECT_EQ(before[g], boundaries[g]);
                    }
                } else {

                    EXPECT_EQ(before[g], boundaries[g]);
                }
            }
        }

        resetBoundaries(gradingType);

        {
            SCOPED_TRACE(
                "STAGE 4: setting mFollowsDefault to true reverts changes");

            DataManager::autoBoundaryChange(gradingType, false, &graph);

            if (newBoundaryDiffers) {

                const auto ret = DataManager::boundaryChange(
                    gradingType, Grade::grade3_0, newBoundary, &graph);

                EXPECT_TRUE(ret);
                EXPECT_NE(before, boundaries);

                DataManager::autoBoundaryChange(gradingType, true, &graph);

                EXPECT_EQ(before, boundaries);
            }
        }

        resetBoundaries(gradingType);

        {
            SCOPED_TRACE("STAGE 5: Setting invalid boundary moves the boundary "
                         "as far as possible in that direction");

            DataManager::autoBoundaryChange(gradingType, false, &graph);

            auto ret = DataManager::boundaryChange(gradingType, Grade::grade3_0,
                                                   Points{-1.}, &graph);

            EXPECT_FALSE(ret);
            // Expect that the grade boundary for 3.0 is now equal to the grade
            // boundary of 3.3 (the minimum value it can have)
            for (std::size_t i = 0; i < boundaries.size(); ++i) {
                auto g = static_cast<Grade>(i);
                if (g == Grade::grade3_0) {

                    EXPECT_EQ(before[Grade::grade3_3], boundaries[g]);
                } else {

                    EXPECT_EQ(before[g], boundaries[g]);
                }
            }

            ret = DataManager::boundaryChange(
                gradingType, Grade::grade3_0,
                Points{std::numeric_limits<double>::infinity()}, &graph);

            EXPECT_FALSE(ret);
            // Expect that the grade boundary for 3.0 is now equal to the grade
            // boundary of 2.7 (the maximum value it can have)
            for (std::size_t i = 0; i < boundaries.size(); ++i) {
                auto g = static_cast<Grade>(i);
                if (g == Grade::grade3_0) {

                    EXPECT_EQ(before[Grade::grade2_7], boundaries[g]);
                } else {

                    EXPECT_EQ(before[g], boundaries[g]);
                }
            }

            // reset
            DataManager::autoBoundaryChange(gradingType, true, &graph);
            DataManager::autoBoundaryChange(gradingType, false, &graph);

            // Expect NaN to not change anything
            ret = DataManager::boundaryChange(
                gradingType, Grade::grade3_0,
                Points{std::numeric_limits<double>::quiet_NaN()}, &graph);

            EXPECT_FALSE(ret);
            EXPECT_EQ(before, boundaries);
        }

        resetBoundaries(gradingType);

        {
            SCOPED_TRACE(
                "STAGE 6: Change to self succeeds and doesn't change anything");

            DataManager::autoBoundaryChange(gradingType, false, &graph);

            auto ret = DataManager::boundaryChange(gradingType, Grade::grade1_7,
                                                   boundaries[Grade::grade1_7],
                                                   &graph);

            EXPECT_TRUE(ret);
            EXPECT_EQ(before, boundaries);
        }

        resetBoundaries(gradingType);

        {
            SCOPED_TRACE("STAGE 7: unchangeable grade boundaries");

            DataManager::autoBoundaryChange(gradingType, false, &graph);

            // Grade boundary for 5.0 is fixed (0)
            // Grade boundary for NR_VALS is invalid
            // For closed grades, grade boundaries for 1.3, 2.3 and 3.3 are
            // fixed

            auto ret = DataManager::boundaryChange(gradingType, Grade::grade5_0,
                                                   boundaries[Grade::grade5_0],
                                                   &graph);

            EXPECT_FALSE(ret);
            EXPECT_EQ(before, boundaries);

            ret = DataManager::boundaryChange(gradingType, Grade::NR_VALS,
                                              boundaries[Grade::grade5_0],
                                              &graph);

            EXPECT_FALSE(ret);
            EXPECT_EQ(before, boundaries);

            ret = DataManager::boundaryChange(gradingType, Grade::grade1_3,
                                              boundaries[Grade::grade1_3],
                                              &graph);

            EXPECT_EQ(ret, gradingType == GradingType::Open);
            EXPECT_EQ(before, boundaries);

            ret = DataManager::boundaryChange(gradingType, Grade::grade2_3,
                                              boundaries[Grade::grade2_3],
                                              &graph);

            EXPECT_EQ(ret, gradingType == GradingType::Open);
            EXPECT_EQ(before, boundaries);

            ret = DataManager::boundaryChange(gradingType, Grade::grade3_3,
                                              boundaries[Grade::grade3_3],
                                              &graph);

            EXPECT_EQ(ret, gradingType == GradingType::Open);
            EXPECT_EQ(before, boundaries);
        }

        resetBoundaries(gradingType);

        {
            SCOPED_TRACE("STAGE 8: changeable grade boundaries");

            DataManager::autoBoundaryChange(gradingType, false, &graph);

            using enum Grade;

            std::vector changeableGrades{grade1_0, grade1_7, grade2_0, grade2_7,
                                         grade3_0, grade3_7, grade4_0};

            if (gradingType == GradingType::Open) {
                changeableGrades.emplace_back(grade1_3);
                changeableGrades.emplace_back(grade2_3);
                changeableGrades.emplace_back(grade3_3);
            }

            for (const auto chG : changeableGrades) {
                // reset
                resetBoundaries(gradingType);
                DataManager::autoBoundaryChange(gradingType, false, &graph);

                EXPECT_EQ(before, boundaries);

                const auto ret = DataManager::boundaryChange(
                    gradingType, chG,
                    boundaries[static_cast<Grade>(static_cast<int>(chG) + 1)],
                    &graph);

                EXPECT_TRUE(ret);

                // If we change 4.0 and are looking at closed grades, than the
                // changing of 4.0 will also changed the fixed grades (1.3 etc.)
                if (!(chG == grade4_0 && gradingType == GradingType::Closed)) {
                    for (std::size_t i = 0; i < boundaries.size(); ++i) {
                        auto g = static_cast<Grade>(i);
                        if (g == chG) {

                            EXPECT_EQ(before[static_cast<Grade>(i + 1)],
                                      boundaries[g]);
                        } else {

                            EXPECT_EQ(before[g], boundaries[g]);
                        }
                    }
                }
            }
        }

        resetBoundaries(gradingType);

        {
            SCOPED_TRACE("STAGE 9: Allow changes past the original value of "
                         "the neighboring grade boundaries");

            DataManager::autoBoundaryChange(gradingType, false, &graph);

            auto ret = DataManager::boundaryChange(gradingType, Grade::grade1_7,
                                                   boundaries[Grade::grade1_3],
                                                   &graph);
            EXPECT_TRUE(ret);

            ret = DataManager::boundaryChange(gradingType, Grade::grade2_0,
                                              boundaries[Grade::grade1_7],
                                              &graph);
            EXPECT_TRUE(ret);

            EXPECT_EQ(boundaries[Grade::grade1_3], boundaries[Grade::grade1_7]);
            EXPECT_EQ(boundaries[Grade::grade2_0], boundaries[Grade::grade1_7]);
        }
    }
}
