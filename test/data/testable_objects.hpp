#pragma once

#include "data/csv_dump.hpp"
#include "util/enums.hpp"

#include <QSharedPointer>

#include <string>
#include <utility>
#include <vector>

struct DAG;

// Create const vectors of expensive objects to minimize the amount of times
// they are constructed from scratch during testing.
// Pair them up with the file name they originate from.
[[nodiscard]] const std::unordered_map<std::string,
                                       QSharedPointer<const CsvDump>> &
csvs(FileType t);

const std::vector<std::pair<std::string, DAG>> &exams();
