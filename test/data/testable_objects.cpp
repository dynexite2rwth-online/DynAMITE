#include "testable_objects.hpp"

#include "data/graph.hpp"
#include "file_io/csv_parser.hpp"
#include "file_io/file.hpp"
#include "util/heuristics.hpp"
#include "util/print_types.hpp"

#include "gtest/gtest.h"

#include <filesystem>
#include <iostream>
#include <memory>
#include <unordered_map>

auto getCsvs(const std::string &directory) {
    std::unordered_map<std::string, QSharedPointer<const CsvDump>> dumps;

    for (const auto &entry : std::filesystem::directory_iterator(
             std::filesystem::path(directory))) {
        if (entry.path().extension() == "") {
            continue;
        }
        if (entry.path().extension() != ".csv") {
            std::cerr << "Unknown file extension: " << entry.path().extension()
                      << std::endl;
            continue;
        }

        // if parseCsv fails, just skip the tests (tested separately elsewhere)
        try {
            const auto fileName =
                QString::fromStdU32String(entry.path().u32string());
            auto fileStr = readFileIntoString(fileName);
            auto dump = parseCsv(fileName, fileStr);
            EXPECT_TRUE(!!dump) << "parseCsv created a NULL-pointer";

            dumps[entry.path().stem().string()] = std::move(dump);

        } catch (const std::exception &e) {
            std::cerr << "Error during parseCsv for the file: " << entry.path()
                      << ": " << e.what() << std::endl;
            continue;
        }
    }

    return dumps;
}

// load exams only once for better performance
// load all exams found in the dynexite_data directory
auto getExams() {
    std::vector<std::pair<std::string, DAG>> configuredExams;
    // temporary vector that stores whether or not for the exam at the same
    // index in configuredExams a corresponding bonus file was found
    std::vector<bool> bonusFound;

    using enum FileType;

    if (csvs(dynexite).size() != csvs(rwth).size()) {
        ADD_FAILURE() << "Different number of dynexite and rwthonline csvs "
                         "found! Aborting tests.";
        return configuredExams;
    }

    for (const auto &[filename, dynexiteCsv] : csvs(dynexite)) {
        if (!csvs(rwth).contains(filename)) {
            ADD_FAILURE() << "Did not find a rwthonline csv file with the name "
                          << filename;
            continue;
        }
        auto rwthCsv = csvs(rwth).at(filename);

        // if the ctor fails, just skip the tests (tested separately elsewhere)
        try {
            const auto dynexiteExam = DynexiteExam(*dynexiteCsv);
            const auto studentData = StudentData(*rwthCsv);
            // Bonus CSV is optional
            const auto bonusFileFound = csvs(bonus).contains(filename);
            const auto bonusPoints =
                bonusFileFound ? BonusPoints{*csvs(bonus).at(filename)}
                               : BonusPoints{};
            UserGradeConfig gc;
            UserPointConfig pc;
            pc.mGradeBoundaryStepSize =
                guessGradeBoundaryStepSize(dynexiteExam);

            auto dag = DAG{dynexiteExam, studentData, bonusPoints, gc, pc};

            configuredExams.emplace_back(filename, std::move(dag));
            bonusFound.emplace_back(bonusFileFound);

        } catch (const std::exception &e) {
            std::cerr << "Error during DAG construction for the file "
                      << filename << ": " << e.what() << std::endl;
            continue;
        }
    }

    // print file names
    std::cout << "Nr valid exam files found: " << configuredExams.size()
              << std::endl;
    assert(configuredExams.size() == bonusFound.size());
    for (std::size_t i = 0; i < configuredExams.size(); ++i) {
        std::cout << configuredExams[i].first
                  << (bonusFound[i] ? " (with bonus)" : " (without bonus)")
                  << std::endl;
    }

    return configuredExams;
}

// all found valid ConfiguredExams together with their file names
const std::unordered_map<std::string, QSharedPointer<const CsvDump>> &
dynexiteCsvs() {
    static auto ret = getCsvs(TESTING_DYNEXITE_DIRECTORY);
    return ret;
}
const std::unordered_map<std::string, QSharedPointer<const CsvDump>> &
rwthOnlineCsvs() {
    static auto ret = getCsvs(TESTING_RWTHONLINE_DIRECTORY);
    return ret;
}
const std::unordered_map<std::string, QSharedPointer<const CsvDump>> &
bonusCsvs() {
    static auto ret = getCsvs(TESTING_BONUS_DIRECTORY);
    return ret;
}

[[nodiscard]] const std::unordered_map<std::string,
                                       QSharedPointer<const CsvDump>> &
csvs(const FileType t) {
    switch (t) {
    case FileType::dynexite:
        return dynexiteCsvs();
    case FileType::rwth:
        return rwthOnlineCsvs();
    case FileType::bonus:
        return bonusCsvs();
    default:
        assert(false);
        {
            static std::unordered_map<std::string,
                                      QSharedPointer<const CsvDump>>
                empty;
            return empty;
        }
    }
}

const std::vector<std::pair<std::string, DAG>> &exams() {
    static auto ret = getExams();
    return ret;
}

// Assert that all test exams are found
// There should be at least three exams distributed with this codebase in the
// test/unittest_data directory
TEST(ExampleExams, allExamsFound) {
    bool closedFound = false, closedBonusFound = false, splitFound = false;

    for (const auto &[name, graph] : exams()) {
        if (name == "closed") {
            closedFound = true;
        } else if (name == "closed_bonus") {
            closedBonusFound = true;
        } else if (name == "split") {
            splitFound = true;
        }
    }

    EXPECT_TRUE(closedFound);
    EXPECT_TRUE(closedBonusFound);
    EXPECT_TRUE(splitFound);
}
