#include "helper.hpp"

#include "file_io/csv_parser.hpp"
#include "file_io/file.hpp"

#include "gtest/gtest.h"

#include <QString>

QSharedPointer<CsvDump> getDump(const std::filesystem::path &path) {

    const auto pathStr = QString::fromStdU32String(path.u32string());
    const auto str = readFileIntoString(pathStr);
    auto dump = parseCsv(pathStr, str);
    if (!dump) {
        ADD_FAILURE() << "parseCsv created a NULL-pointer";
        return nullptr;
    }

    return dump;
}
