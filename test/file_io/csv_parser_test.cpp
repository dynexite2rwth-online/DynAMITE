#include "file_io/csv_parser.hpp"

#include "data/csv_dump.hpp"
#include "file_io/file.hpp"
#include "util/print_types.hpp"

#include "gtest/gtest.h"

#include <QTextStream>

#include <filesystem>
#include <random>

using std::filesystem::directory_iterator;

TEST(CsvParserTest, parseDynexite) {
    try {
        for (const auto &it : {directory_iterator(std::filesystem::path(
                                   TESTING_DYNEXITE_DIRECTORY)),
                               directory_iterator(std::filesystem::path(
                                   TESTING_RWTHONLINE_DIRECTORY))}) {
            for (const auto &f : it) {

                SCOPED_TRACE("Reading in file " + f.path().string());

                // expect only .csv and .gitkeep files in the repository
                if (f.path().extension() ==
                    "") { // .gitkeep counts as empty extension
                    continue;
                }
                if (f.path().extension() != ".csv") {
                    FAIL() << "Unknown file extension: "
                           << f.path().extension();
                }
                try {
                    const auto fileName =
                        QString::fromStdU32String(f.path().u32string());
                    auto fileStr = readFileIntoString(fileName);
                    auto p = parseCsv(fileName, fileStr);
                    EXPECT_TRUE(!!p) << "parseCsv created a NULL-pointer";
                } catch (const std::exception &e) {
                    FAIL() << "parseCsv threw an exception: " << e.what();
                }
            }
        }
    } catch (const std::exception &e) {

        FAIL()
            << "directory_iterator threw an exception(" << e.what()
            << "); ensure that TESTING_DYNEXITE_DIRECTORY is set up correctly";
    }
}

// Test deleteQuotationMarksAndContent
TEST(CsvParserTest, deleteQuotationMarksAndContent) {

    std::array strs = {QString(""),
                       QString("abc"),
                       QString(R"(a"bc)"),
                       QString(R"(a"b"c""""d)"),
                       QString(R"("a"b"c""""d")"),
                       QString(R"("abc")")};

    for (auto &s : strs) {
        impl::deleteQuotationMarksAndContent(s);
    }

    std::array results = {
        strs[0].compare(QString(R"()")),     strs[1].compare(QString(R"(abc)")),
        strs[2].compare(QString(R"(a"bc)")), strs[3].compare(QString(R"(acd)")),
        strs[4].compare(QString(R"(b)")),    strs[5].compare(QString(R"()"))};

    for (auto &r : results) {
        EXPECT_EQ(r, 0);
    }
}

// Test deleteOnlyOuterQuotationMarks
TEST(CsvParserTest, deleteOnlyOuterQuotationMarks) {

    CsvDump d;
    {
        CsvLine strs = {QString(""),
                        QString("abc"),
                        QString(R"(a"bc)"),
                        QString(R"(a"b"c""""d)"),
                        QString(R"("a"b"c""""d")"),
                        QString(R"("abc")"),
                        QString(R"("ab""c")"),
                        QString(R"(ab""c)")};

        d.data.push_back(strs);
    }

    impl::deleteOnlyOuterQuotationMarks(&d);

    std::array results = {d.data[0][0].compare(QString(R"()")),
                          d.data[0][1].compare(QString(R"(abc)")),
                          d.data[0][2].compare(QString(R"(a"bc)")),
                          d.data[0][3].compare(QString(R"(a"b"c""""d)")),
                          d.data[0][4].compare(QString(R"(a"b"c""""d)")),
                          d.data[0][5].compare(QString(R"(abc)")),
                          d.data[0][6].compare(QString(R"(ab""c)")),
                          d.data[0][7].compare(QString(R"(ab""c)"))};

    for (auto &r : results) {
        EXPECT_EQ(r, 0);
    }
}

// Test replaceEscapedQuotationMarks
TEST(CsvParserTest, replaceEscapedQuotationMarks) {

    CsvDump d;
    {
        CsvLine strs = {QString(""),
                        QString("abc"),
                        QString(R"(a"bc)"),
                        QString(R"(a"b"c""""d)"),
                        QString(R"("a"b"c""""d")"),
                        QString(R"("abc")"),
                        QString(R"("ab""c")"),
                        QString(R"(ab""c)")};

        d.data.push_back(strs);
    }

    impl::replaceEscapedQuotationMarks(&d);

    std::array results = {d.data[0][0].compare(QString(R"()")),
                          d.data[0][1].compare(QString(R"(abc)")),
                          d.data[0][2].compare(QString(R"(a"bc)")),
                          d.data[0][3].compare(QString(R"(a"b"c""d)")),
                          d.data[0][4].compare(QString(R"("a"b"c""d")")),
                          d.data[0][5].compare(QString(R"("abc")")),
                          d.data[0][6].compare(QString(R"("ab"c")")),
                          d.data[0][7].compare(QString(R"(ab"c)"))};

    for (auto &r : results) {
        EXPECT_EQ(r, 0);
    }
}

// Test removeControlQuotationMarks
TEST(CsvParserTest, removeControlQuotationMarks) {

    CsvDump d;
    {
        CsvLine strs = {QString(""),
                        QString("abc"),
                        QString(R"(a"bc)"),
                        QString(R"(a"b"c""""d)"),
                        QString(R"("a"b"c""""d")"),
                        QString(R"("abc")"),
                        QString(R"("ab""c")"),
                        QString(R"(ab""c)")};

        d.data.append(strs);
    }

    impl::removeControlQuotationMarks(&d);

    std::array results = {d.data[0][0].compare(QString(R"()")),
                          d.data[0][1].compare(QString(R"(abc)")),
                          d.data[0][2].compare(QString(R"(a"bc)")),
                          d.data[0][3].compare(QString(R"(a"b"c""d)")),
                          d.data[0][4].compare(QString(R"(a"b"c""d)")),
                          d.data[0][5].compare(QString(R"(abc)")),
                          d.data[0][6].compare(QString(R"(ab"c)")),
                          d.data[0][7].compare(QString(R"(ab"c)"))};

    for (auto &r : results) {
        EXPECT_EQ(r, 0);
    }
}

// Test findClosingQuotationMark
TEST(CsvParserTest, findClosingQuotationMark) {

    std::vector<qsizetype> results;
    results.push_back(impl::findClosingQuotationMark("\"", 0));
    results.push_back(impl::findClosingQuotationMark("abc\"", 0));
    results.push_back(impl::findClosingQuotationMark("abc\"def", 0));
    results.push_back(impl::findClosingQuotationMark("abc\"\"def\"", 0));
    results.push_back(impl::findClosingQuotationMark("abc\"\"def\"ghi", 0));
    results.push_back(impl::findClosingQuotationMark("abc\"\"def\"\"ghi\"", 0));
    results.push_back(
        impl::findClosingQuotationMark("abc\"\"def\"\"ghi\" ", 0));

    results.push_back(impl::findClosingQuotationMark("\"\"", 1));
    results.push_back(impl::findClosingQuotationMark("\"abc\"", 1));
    results.push_back(impl::findClosingQuotationMark("\"abc\"def", 1));
    results.push_back(impl::findClosingQuotationMark("\"abc\"\"def\"", 1));
    results.push_back(impl::findClosingQuotationMark("\"abc\"\"def\"ghi", 1));
    results.push_back(
        impl::findClosingQuotationMark("\"abc\"\"def\"\"ghi\"", 1));
    results.push_back(
        impl::findClosingQuotationMark("\"abc\"\"def\"\"ghi\" ", 1));

    EXPECT_EQ(results[0], 0);
    EXPECT_EQ(results[1], 3);
    EXPECT_EQ(results[2], 3);
    EXPECT_EQ(results[3], 8);
    EXPECT_EQ(results[4], 8);
    EXPECT_EQ(results[5], 13);
    EXPECT_EQ(results[6], 13);

    EXPECT_EQ(results[7], 1);
    EXPECT_EQ(results[8], 4);
    EXPECT_EQ(results[9], 4);
    EXPECT_EQ(results[10], 9);
    EXPECT_EQ(results[11], 9);
    EXPECT_EQ(results[12], 14);
    EXPECT_EQ(results[13], 14);
}

// Test findClosingDelimiter
TEST(CsvParserTest, findClosingDelimiter) {
    std::vector<qsizetype> results;
    results.push_back(impl::findClosingDelimiter("ab,", ',', 0, true));
    results.push_back(impl::findClosingDelimiter("ab,", ',', 0, false));
    results.push_back(impl::findClosingDelimiter("ab,cd", ',', 0, true));
    results.push_back(impl::findClosingDelimiter("ab,cd", ',', 0, false));
    results.push_back(impl::findClosingDelimiter("\"ab,\",", ',', 0, true));
    results.push_back(impl::findClosingDelimiter("\"ab,\",cd", ',', 0, true));
    results.push_back(impl::findClosingDelimiter("ab\"\",", ',', 0, true));

    EXPECT_EQ(results[0], 2);
    EXPECT_EQ(results[1], 2);
    EXPECT_EQ(results[2], 2);
    EXPECT_EQ(results[3], 2);
    EXPECT_EQ(results[4], 5);
    EXPECT_EQ(results[5], 5);
    EXPECT_EQ(results[6], 4);
}

// Test addEscapeCharacters
TEST(CsvParserTest, addEscapeCharacters) {
    QList<QString> strs;
    strs.emplace_back("");
    strs.emplace_back("abc");
    strs.emplace_back("a\"bc");
    strs.emplace_back("\"abc\"");
    strs.emplace_back("a,bc");
    strs.emplace_back("\"a,bc\"");

    QList<QString> results;
    results.emplace_back("");
    results.emplace_back("abc");
    results.emplace_back(R"("a""bc")");
    results.emplace_back(R"("""abc""")");
    results.emplace_back(R"("a,bc")");
    results.emplace_back(R"("""a,bc""")");

    QList<int> compareResults;
    ASSERT_EQ(strs.size(), results.size());
    for (int i = 0; i < strs.size(); ++i) {

        compareResults.emplace_back(
            impl::addEscapeCharacters(strs[i], ',').compare(results[i]));
    }

    for (const auto &res : compareResults) {
        EXPECT_EQ(res, 0);
    }
}

// Test parseCsv
TEST(CsvParserTest, parseCsvTest) {
    {
        QString input = R"(abc,def,ghi
",","""",""""""
",,",""",",","""
",,,",",,""",","","
",""""",""",,",""","""
""""",","""""""","""a"""
)";

        auto resPtr = parseCsv("", input);

        ASSERT_FALSE(resPtr.isNull());
        ASSERT_EQ(resPtr->data.size(), 6);
        for (int i = 0; i < 6; ++i) {
            ASSERT_EQ(resPtr->data[i].size(), 3);
        }
        QList<QList<int>> compareResults;
        compareResults.resize(6);
        for (int i = 0; i < 6; ++i) {
            compareResults[i].resize(3);
        }
        compareResults[0][0] = resPtr->data[0][0].compare(R"(abc)");
        compareResults[0][1] = resPtr->data[0][1].compare(R"(def)");
        compareResults[0][2] = resPtr->data[0][2].compare(R"(ghi)");
        compareResults[1][0] = resPtr->data[1][0].compare(R"(,)");
        compareResults[1][1] = resPtr->data[1][1].compare(R"(")");
        compareResults[1][2] = resPtr->data[1][2].compare(R"("")");
        compareResults[2][0] = resPtr->data[2][0].compare(R"(,,)");
        compareResults[2][1] = resPtr->data[2][1].compare(R"(",)");
        compareResults[2][2] = resPtr->data[2][2].compare(R"(,")");
        compareResults[3][0] = resPtr->data[3][0].compare(R"(,,,)");
        compareResults[3][1] = resPtr->data[3][1].compare(R"(,,")");
        compareResults[3][2] = resPtr->data[3][2].compare(R"(,",)");
        compareResults[4][0] = resPtr->data[4][0].compare(R"(,"")");
        compareResults[4][1] = resPtr->data[4][1].compare(R"(",,)");
        compareResults[4][2] = resPtr->data[4][2].compare(R"(",")");
        compareResults[5][0] = resPtr->data[5][0].compare(R"("",)");
        compareResults[5][1] = resPtr->data[5][1].compare(R"(""")");
        compareResults[5][2] = resPtr->data[5][2].compare(R"("a")");

        for (int i = 0; i < 6; ++i) {
            for (int j = 0; j < 3; ++j) {
                EXPECT_EQ(compareResults[i][j], 0);
            }
        }
    }
    {
        QString input = R"(abc,def,ghi
;,"""",""""""
;;,""";",";"""
;;;,";;""",";"";"
";""""",""";;",""";"""
""""";","""""""","""a"""
)";

        auto resPtr = parseCsv("", input);
        ASSERT_FALSE(resPtr.isNull());
        ASSERT_EQ(resPtr->data.size(), 6);
        for (int i = 0; i < 6; ++i) {
            ASSERT_EQ(resPtr->data[i].size(), 3);
        }
        QList<QList<int>> compareResults;
        compareResults.resize(6);
        for (int i = 0; i < 6; ++i) {
            compareResults[i].resize(3);
        }
        compareResults[0][0] = resPtr->data[0][0].compare(R"(abc)");
        compareResults[0][1] = resPtr->data[0][1].compare(R"(def)");
        compareResults[0][2] = resPtr->data[0][2].compare(R"(ghi)");
        compareResults[1][0] = resPtr->data[1][0].compare(R"(;)");
        compareResults[1][1] = resPtr->data[1][1].compare(R"(")");
        compareResults[1][2] = resPtr->data[1][2].compare(R"("")");
        compareResults[2][0] = resPtr->data[2][0].compare(R"(;;)");
        compareResults[2][1] = resPtr->data[2][1].compare(R"(";)");
        compareResults[2][2] = resPtr->data[2][2].compare(R"(;")");
        compareResults[3][0] = resPtr->data[3][0].compare(R"(;;;)");
        compareResults[3][1] = resPtr->data[3][1].compare(R"(;;")");
        compareResults[3][2] = resPtr->data[3][2].compare(R"(;";)");
        compareResults[4][0] = resPtr->data[4][0].compare(R"(;"")");
        compareResults[4][1] = resPtr->data[4][1].compare(R"(";;)");
        compareResults[4][2] = resPtr->data[4][2].compare(R"(";")");
        compareResults[5][0] = resPtr->data[5][0].compare(R"("";)");
        compareResults[5][1] = resPtr->data[5][1].compare(R"(""")");
        compareResults[5][2] = resPtr->data[5][2].compare(R"("a")");

        for (int i = 0; i < 6; ++i) {
            for (int j = 0; j < 3; ++j) {
                EXPECT_EQ(compareResults[i][j], 0);
            }
        }
    }
    {
        QString input = R"(abc,"def",ghi

"jkl","",
)";
        auto resPtr = parseCsv("", input);
        ASSERT_FALSE(resPtr.isNull());
        ASSERT_EQ(resPtr->data.size(), 2);
        for (int i = 0; i < 2; ++i) {
            ASSERT_EQ(resPtr->data[i].size(), 3);
        }
        QList<QList<int>> compareResults;
        compareResults.resize(2);
        for (int i = 0; i < 2; ++i) {
            compareResults[i].resize(3);
        }
        compareResults[0][0] = resPtr->data[0][0].compare(R"(abc)");
        compareResults[0][1] = resPtr->data[0][1].compare(R"(def)");
        compareResults[0][2] = resPtr->data[0][2].compare(R"(ghi)");
        compareResults[1][0] = resPtr->data[1][0].compare(R"(jkl)");
        compareResults[1][1] = resPtr->data[1][1].compare(R"()");
        compareResults[1][2] = resPtr->data[1][2].compare(R"()");

        for (int i = 0; i < 2; ++i) {
            for (int j = 0; j < 3; ++j) {
                EXPECT_EQ(compareResults[i][j], 0);
            }
        }
    }
    {
        QString input = R"(abc,d"ef,ghi"
abc,"def,gh"i
abc,"def,ghi"
)";
        auto resPtr = parseCsv("", input);
        ASSERT_FALSE(resPtr.isNull());
        ASSERT_EQ(resPtr->data.size(), 3);
        for (int i = 0; i < 3; ++i) {
            ASSERT_EQ(resPtr->data[i].size(), 3);
        }
        QList<QList<int>> compareResults;
        compareResults.resize(3);
        for (int i = 0; i < 3; ++i) {
            compareResults[i].resize(3);
        }
        compareResults[0][0] = resPtr->data[0][0].compare(R"(abc)");
        compareResults[0][1] = resPtr->data[0][1].compare(R"(d"ef)");
        compareResults[0][2] = resPtr->data[0][2].compare(R"(ghi")");
        compareResults[1][0] = resPtr->data[1][0].compare(R"(abc)");
        compareResults[1][1] = resPtr->data[1][1].compare(R"("def)");
        compareResults[1][2] = resPtr->data[1][2].compare(R"(gh"i)");
        compareResults[2][0] = resPtr->data[2][0].compare(R"(abc)");
        compareResults[2][1] = resPtr->data[2][1].compare(R"("def)");
        compareResults[2][2] = resPtr->data[2][2].compare(R"(ghi")");

        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                EXPECT_EQ(compareResults[i][j], 0);
            }
        }
    }
}

// Create random csvs, check if number of rows / columns is correct and
// check if they are not altered when written and then read again
TEST(CsvParserTest, writeCsvTest) {

    std::default_random_engine e{std::random_device{}()};
    std::bernoulli_distribution randBool;

    const QList<QChar> possibleChars = {'a', QChar(0x011F), ',',
                                        ';', ' ',           '\"'};
    std::uniform_int_distribution charDist{
        0, static_cast<int>(possibleChars.size() - 1)};

    for (int csvNr = 0; csvNr < 200; ++csvNr) {
        static constexpr auto nrRows = 20;
        static constexpr auto nrColumns = 10;
        static constexpr auto maxStringLength = 8;
        std::uniform_int_distribution stringLengthDist{0, maxStringLength};

        CsvDump inputD;
        inputD.data.resize(nrRows);
        for (int rowNr = 0; rowNr < nrRows; ++rowNr) {
            inputD.data[rowNr].resize(nrColumns);
        }
        for (int rowNr = 0; rowNr < nrRows; ++rowNr) {
            for (int columnNr = 0; columnNr < nrColumns; ++columnNr) {
                const auto stringLength = stringLengthDist(e);
                for (int charNr = 0; charNr < stringLength; ++charNr) {
                    inputD.data[rowNr][columnNr].append(
                        possibleChars[charDist(e)]);
                }
            }
        }

        const QChar delim = randBool(e) ? ';' : ',';
        QString streamStr;
        QTextStream out(&streamStr);
        writeCsv(out, inputD, delim);

        auto outputDPtr = parseCsv("", streamStr);

        ASSERT_FALSE(outputDPtr.isNull());
        ASSERT_EQ(outputDPtr->data.size(), nrRows);
        for (int rowNr = 0; rowNr < nrRows; ++rowNr) {
            ASSERT_EQ(outputDPtr->data[rowNr].size(), nrColumns);
        }
        for (int rowNr = 0; rowNr < nrRows; ++rowNr) {
            for (int columnNr = 0; columnNr < nrColumns; ++columnNr) {
                const auto compareRes = inputD.data[rowNr][columnNr].compare(
                    outputDPtr->data[rowNr][columnNr]);
                EXPECT_EQ(compareRes, 0);
            }
        }
    }
}
