#pragma once
#include "data/csv_dump.hpp"

#include <QSharedPointer>

#include <filesystem>

QSharedPointer<CsvDump> getDump(const std::filesystem::path &path);
