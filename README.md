# DynAMITE - Dynexite Assistant for Marking with Integrated Table Export

Below you can find information on how to download and run the application on different operating systems.

## Download

You can download the latest release of the application from the [release page](https://git.rwth-aachen.de/dynexite2rwth-online/DynAMITE/-/releases). Choose the appropriate binary for your operating system:

- **Windows**: Download the zip file and unpack it. Then, run `DynAMITE.exe`.
- **macOS**: Open the .dmg file and run `DynAMITE.app`. The oldest supported macOS version is macOS 12. *Please note that we couldn't test the macOS build much yet.*
- **Linux**: Download the AppImage file and make it executable by running the command `chmod a+x DynAMITE*.AppImage`. Then, execute the file. Note that you might need to install FUSE 2.x if it's not already installed. Refer to the [installation guide](https://docs.appimage.org/user-guide/troubleshooting/fuse.html#ref-install-fuse) for more information.

## Troubleshooting

### msvcp140.dll
If you encounter the error "msvcp140.dll not found" on Windows, you will need to download the Microsoft Visual C++ Redistributable runtime libraries. You can find the latest supported version at [Microsoft's website](https://learn.microsoft.com/en-US/cpp/windows/latest-supported-vc-redist).

## Usage
Information on how to use the app can be found in the [wiki](https://git.rwth-aachen.de/dynexite2rwth-online/DynAMITE/-/wikis/User-information).

## Building from source

For information about compiling the app yourself or about the code structure, visit the [wiki](https://git.rwth-aachen.de/dynexite2rwth-online/DynAMITE/-/wikis/Development-information).
