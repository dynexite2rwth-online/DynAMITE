# Optionally also build documentation with doxygen
option(BUILD_DOC "Build documentation" ON)

if(BUILD_DOC)
    # Check if Doxygen is installed
    find_package(Doxygen)
    if (DOXYGEN_FOUND)
        message(STATUS "Generating doxygen documentation!")

        # Set input and output files
        set(DOXYGEN_IN ${CMAKE_CURRENT_SOURCE_DIR}/doc/Doxyfile.in)
        set(DOXYGEN_OUT ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile)

        # Replace cmake-variables in Doxyfile.in
        configure_file(${DOXYGEN_IN} ${DOXYGEN_OUT} @ONLY)

        # Add doxygen-target to ALL
        add_custom_target(doc_doxygen ALL
            COMMAND ${DOXYGEN_EXECUTABLE} -q ${DOXYGEN_OUT}
            WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
            COMMENT "Generating API documentation with Doxygen"
            VERBATIM
        )
    else (DOXYGEN_FOUND)
        message(SEND_ERROR
            " Building documentation is enabled, but Doxygen is not installed!"
            " Either install doxygen, or set BUILD_DOC to OFF"
        )
    endif (DOXYGEN_FOUND)
endif()
