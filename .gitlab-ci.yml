# All jobs in this CI are tagged with "dynamite-runner", so that only runners that support our setup will run our jobs.
# Furthermore, jobs that are run on linux in one of the docker containers created in our Dockerfile have the tag "linux".
# Jobs that run in a shell executor on mac have the tag "mac".

# On commits, run test and coverage
# On tags, run only deploy (since the tagged commit was already tested)

# UPDATE 2024/01: The linux-clang jobs are disabled (hidden with a dot) because of linking issues.

# Jobs run on the linux build+test-image, unless otherwise specified or unless they are not linux-tagged jobs.
default:
  image: registry.git.rwth-aachen.de/dynexite2rwth-online/dynamite/2025-test:v0.1.7
  tags:
  - DynAMITE
  - ${DYNAMITE_CI_PLATFORM}

stages:
  - build
  - test
  - coverage
  - deploy

variables:
  BUILDDIR: builddir

#######################
#                     #
#        Build        #
#                     #
#######################

.build:base:
  stage: build
  except:
    - tags
  script:
    - mkdir -p ${BUILDDIR} && cd ${BUILDDIR}
    - cmake -DCMAKE_C_COMPILER=${DYNAMITE_CI_CC_COMPILER}
        -DCMAKE_CXX_COMPILER=${DYNAMITE_CI_CXX_COMPILER}
        -DCMAKE_CXX_FLAGS="${DYNAMITE_CI_CXX_FLAGS}"
        -DCMAKE_BUILD_TYPE=${DYNAMITE_CI_CONFIGURATION}
        ${DYNAMITE_CI_ADDITIONAL_ARGS}
        -DBUILD_DOC=OFF
        ..
    - make -k
  artifacts:
    paths:
      - ${BUILDDIR}/test/unit_tests
    expire_in: 1s
    public: false

########################################################################

.build:linux:base:
  extends: .build:base
  variables:
    DYNAMITE_CI_ADDITIONAL_ARGS:
      "-DTESTING_DYNEXITE_DIRECTORY=/usr/src/unittest_data/dynexite
      -DTESTING_RWTHONLINE_DIRECTORY=/usr/src/unittest_data/rwthonline"
    DYNAMITE_CI_PLATFORM: linux

# GCC 11
build:linux:gcc-11:
  extends: .build:linux:base
  variables:
    DYNAMITE_CI_CXX_COMPILER: "g++-11"
    DYNAMITE_CI_CC_COMPILER: "gcc-11"
    DYNAMITE_CI_CXX_FLAGS:
  parallel:
    matrix:
      - DYNAMITE_CI_CONFIGURATION: [Debug, Release]


# GCC 12
build:linux:gcc-12:
  extends: .build:linux:base
  variables:
    DYNAMITE_CI_CXX_COMPILER: "g++-12"
    DYNAMITE_CI_CC_COMPILER: "gcc-12"
    DYNAMITE_CI_CXX_FLAGS:
  parallel:
    matrix:
      - DYNAMITE_CI_CONFIGURATION: [Debug, Release]

# Clang 15 with libstdc++: not supported!

# Clang 16 with libstdc++
.build:linux:clang-16:libstdcpp:
  extends: .build:linux:base
  variables:
    DYNAMITE_CI_CXX_COMPILER: "clang++-16"
    DYNAMITE_CI_CC_COMPILER: "clang-16"
    DYNAMITE_CI_CXX_FLAGS:
  parallel:
    matrix:
      - DYNAMITE_CI_CONFIGURATION: [Debug, Release]

# Clang 15 with libc++
.build:linux:clang-15:libcpp:
  extends: .build:linux:base
  variables:
    DYNAMITE_CI_CXX_COMPILER: "clang++-15"
    DYNAMITE_CI_CC_COMPILER: "clang-15"
    DYNAMITE_CI_CXX_FLAGS: "-stdlib=libc++ -fexperimental-library"
  parallel:
    matrix:
      - DYNAMITE_CI_CONFIGURATION: [Debug, Release]

# Mac
build:mac:clang:
  extends: .build:base
  variables:
    DYNAMITE_CI_CXX_COMPILER: "clang++"
    DYNAMITE_CI_CC_COMPILER: "clang"
    DYNAMITE_CI_CXX_FLAGS:
    DYNAMITE_CI_ADDITIONAL_ARGS:
      "-DTESTING_DYNEXITE_DIRECTORY=/Users/Shared/unittest_data/dynexite
      -DTESTING_RWTHONLINE_DIRECTORY=/Users/Shared/unittest_data/rwthonline"
    DYNAMITE_CI_PLATFORM: mac
  parallel:
    matrix:
      - DYNAMITE_CI_CONFIGURATION: [Debug, Release]

#######################
#                     #
#        Tests        #
#                     #
#######################

.test:base:
  stage: test
  except:
    - tags
  script:
    - cd ${BUILDDIR} && ./test/unit_tests -platform offscreen

########################################################################

.test:linux:base:
  extends: .test:base
  variables:
    DYNAMITE_CI_PLATFORM: linux

# GCC 11
test:linux:gcc-11:debug:
  extends: .test:linux:base
  needs: [ "build:linux:gcc-11: [Debug]" ]
test:linux:gcc-11:release:
  extends: .test:linux:base
  needs: [ "build:linux:gcc-11: [Release]" ]

# GCC 12
test:linux:gcc-12:debug:
  extends: .test:linux:base
  needs: [ "build:linux:gcc-12: [Debug]" ]
test:linux:gcc-12:release:
  extends: .test:linux:base
  needs: [ "build:linux:gcc-12: [Release]" ]

# Clang 15 with libstdc++: not supported!

# Clang 16 with libstdc++
.test:linux:clang-16:libstdcpp:debug:
  extends: .test:linux:base
  needs: [ "build:linux:clang-16:libstdcpp: [Debug]" ]
.test:linux:clang-16:libstdcpp:release:
  extends: .test:linux:base
  needs: [ "build:linux:clang-16:libstdcpp: [Release]" ]

# Clang 15 with libc++
.test:linux:clang-15:libcpp:debug:
  extends: .test:linux:base
  needs: [ "build:linux:clang-15:libcpp: [Debug]" ]
.test:linux:clang-15:libcpp:release:
  extends: .test:linux:base
  needs: [ "build:linux:clang-15:libcpp: [Release]" ]

# Mac
test:mac:clang:debug:
  extends: .test:base
  needs: [ "build:mac:clang: [Debug]" ]
  variables:
    DYNAMITE_CI_PLATFORM: mac
test:mac:clang:release:
  extends: .test:base
  needs: [ "build:mac:clang: [Release]" ]
  variables:
    DYNAMITE_CI_PLATFORM: mac

#######################
#                     #
#      Coverage       #
#                     #
#######################

.coverage:linux:base:
  stage: coverage
  needs: []
  except:
    - tags
  script:
    - mkdir -p ${BUILDDIR} && cd ${BUILDDIR}
    - cmake -DCMAKE_C_COMPILER=${DYNAMITE_CI_CC_COMPILER}
        -DCMAKE_CXX_COMPILER=${DYNAMITE_CI_CXX_COMPILER}
        -DCMAKE_CXX_FLAGS="${DYNAMITE_CI_CXX_FLAGS}"
        -DCMAKE_BUILD_TYPE=Debug
        -DTEST_COVERAGE=ON
        -DTESTING_DYNEXITE_DIRECTORY=/usr/src/unittest_data/dynexite
        -DTESTING_RWTHONLINE_DIRECTORY=/usr/src/unittest_data/rwthonline
        -DBUILD_DOC=OFF
        ..
    - make -k -j$(nproc)
    - ./test/unit_tests -platform offscreen || true # don't abort if the test fails
    - gcovr --fail-under-line 5 --gcov-executable gcov-12 --html-details coverage.html -s -r ../src/ .
  coverage: /^lines:\ (\d+(?:\.\d+)?\%)/
  artifacts:
    paths:
      - ${BUILDDIR}/coverage*.html
      - ${BUILDDIR}/coverage*.css
    expire_in: 2 weeks


# GCC 12
coverage:linux:gcc-12:
  extends: .coverage:linux:base
  variables:
    DYNAMITE_CI_CXX_COMPILER: "g++-12"
    DYNAMITE_CI_CC_COMPILER: "gcc-12"
    DYNAMITE_CI_CXX_FLAGS:
    DYNAMITE_CI_PLATFORM: linux

#######################
#                     #
#     Deployment      #
#                     #
#######################

.deploy:base:
  stage: deploy
  needs: []
  only:
    - tags

# Linux
deploy:linux:appimage:
  extends: .deploy:base
  image: registry.git.rwth-aachen.de/dynexite2rwth-online/dynamite/2025-linux-deploy:v0.1.7
  script:
    - mkdir -p ${BUILDDIR} && cd ${BUILDDIR}
    - cmake -DCMAKE_C_COMPILER=${DYNAMITE_CI_CC_COMPILER}
        -DCMAKE_CXX_COMPILER=${DYNAMITE_CI_CXX_COMPILER}
        -DCMAKE_CXX_FLAGS="${DYNAMITE_CI_CXX_FLAGS}"
        -DCMAKE_BUILD_TYPE=Release
        -DBUILD_TESTING=OFF -DBUILD_DOC=OFF -DTEST_COVERAGE=OFF
        ..
    - make -j$(nproc)
    - make DESTDIR=$PWD/appdir -j$(nproc) install
    - cp ../packaging/appimage/AppRun appdir/AppRun
    - chmod +x appdir/AppRun
    - cp ../packaging/icon256x256.png ./appdir/usr/icon.png
    - sed 's/$${DYNAMITE_VERSION}/${CI_COMMIT_REF_NAME}/g' ../packaging/appimage/DynAMITE.desktop.template > ./appdir/DynAMITE.desktop
    - cd appdir/
    - wget -nv "https://github.com/probonopd/linuxdeployqt/releases/download/continuous/linuxdeployqt-continuous-x86_64.AppImage"
    - chmod a+x linuxdeployqt-continuous-x86_64.AppImage
    - ./linuxdeployqt-continuous-x86_64.AppImage --appimage-extract-and-run DynAMITE.desktop -appimage -no-translations
    - 'curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file DynAMITE*.AppImage "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/linux/${CI_COMMIT_REF_NAME}/DynAMITE_${CI_COMMIT_REF_NAME}.AppImage"'
    - sleep 3
  variables:
    DYNAMITE_CI_CXX_COMPILER: "g++-11"
    DYNAMITE_CI_CC_COMPILER: "gcc-11"
    DYNAMITE_CI_CXX_FLAGS:
    DYNAMITE_CI_PLATFORM: linux

# Windows
deploy:windows:
  extends: .deploy:base
  image: registry.git.rwth-aachen.de/dynexite2rwth-online/dynamite/2025-windows-deploy:v0.1.7
  script:
    - mkdir -p ${BUILDDIR} && cd ${BUILDDIR}
    - x86_64-w64-mingw32.static-cmake
        -DCMAKE_BUILD_TYPE=Release
        -DBUILD_TESTING=OFF -DBUILD_DOC=OFF -DTEST_COVERAGE=OFF
        ..
    - make -j$(nproc)
    - zip build.zip DynAMITE.exe
    - 'curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file build.zip "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/windows/${CI_COMMIT_REF_NAME}/DynAMITE_${CI_COMMIT_REF_NAME}.zip"'
    - sleep 3
  variables:
    DYNAMITE_CI_PLATFORM: linux

# Mac
deploy:mac:
  extends: .deploy:base
  script:
    - mkdir -p ${BUILDDIR} && cd ${BUILDDIR}
    - cmake -DCMAKE_C_COMPILER=${DYNAMITE_CI_CC_COMPILER}
        -DCMAKE_CXX_COMPILER=${DYNAMITE_CI_CXX_COMPILER}
        -DCMAKE_CXX_FLAGS="${DYNAMITE_CI_CXX_FLAGS}"
        -DCMAKE_BUILD_TYPE=Release
        -DBUILD_TESTING=OFF -DBUILD_DOC=OFF -DTEST_COVERAGE=OFF
        ..
    - make
    - make DESTDIR=$PWD/install install
    - cd install
    - macdeployqt ./usr/local/DynAMITE.app -dmg
    - 'curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ./usr/local/DynAMITE.dmg "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/macos/${CI_COMMIT_REF_NAME}/DynAMITE_${CI_COMMIT_REF_NAME}.dmg"'
    - sleep 3
  variables:
    DYNAMITE_CI_CXX_COMPILER: "clang++"
    DYNAMITE_CI_CC_COMPILER: "clang"
    DYNAMITE_CI_CXX_FLAGS:
    DYNAMITE_CI_PLATFORM: mac
